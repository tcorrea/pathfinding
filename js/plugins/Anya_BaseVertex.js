// =============================================================================
// Anya_BaseVertex.js
// Version:     1.0
// Date:        1/27/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.BaseVertex = class {
    /**
     * Creates an instance of AnyaHeuristic.
     * @param {Number} id
     * @param {PIXI.Point} pos
     * @memberof BaseVertex
     */
    constructor(id, pos) {
        this.id = id;
        this.pos = pos;
        this.outgoings = new Set(); // Outgoing Edges
        this.incomings = new Set(); // Incoming Edges
        this.touchings = new Set(); // Union of outgoings and incomings
    }

    /**
     * @return {Set<BaseEdge>}
     */
    getOutgoings() {
        return this.outgoings;
    }

    /**
     * @return {Set<BaseVertex>}
     */
    getOutgoingNeighbors() {
        on = new Set();
        this.outgoings.forEach((e) => on.add(e.end));
        return on;
    }

    /**
     * @return {Set<BaseEdge>}
     */
    getIncomings() {
        return this.incomings;
    }

    /**
     * @param {BaseVertex} o
     * @return {Boolean}
     */
    equals(o) {
        return this.id === o.id;
    }

    /**
     * Returns the edge outgoing to the given target
     * @param {BaseVertex} target
     * @return {Boolean}
     */
    getOutgoingTo(target) {
        if (target === null) return null;
        for (e of this.outgoings.values()) {
            if (e.end.equals(target)) return e;
        }
        return null;
    }

    /**
     * Returns the edge incoming from the given start
     * @param {BaseVertex} start
     * @return {Boolean}
     */
    getIncomingFrom(start) {
        if (start === null) return null;
        for (e of this.incomings.values()) {
            if (e.start.equals(start)) return e;
        }
        return null;
    }

    /**
     * @param {BaseEdge} e
     */
    addOutgoing(e) {
        this.outgoings.add(e);
        this.touchings.add(e);
    }

    /**
     * @param {BaseEdge} e
     */
    addIncoming(e) {
        this.incomings.add(e);
        this.touchings.add(e);
    }

    /**
     * @param {BaseEdge} e
     */
    removeIncoming(e) {
        this.incomings.delete(e);
        this.touchings.delete(e);
    }

    /**
     * @param {BaseEdge} e
     */
    removeOutgoing(e) {
        this.outgoings.delete(e);
        this.touchings.delete(e);
    }

    /**
     * Compare this edge to another
     * Java compare(long, long) definition:
     * "The compare(long x, long y) method of Long class returns the
     *  value 0 if x == y; a value less than 0 if x < y; and a value
     *  greater than 0 if x > y"
     * @param {BaseVertex} o
     * @return {Number}
     */
    compareTo(o) {
        return o.id - this.id;
    }
};
