// =============================================================================
// SVG_Memory.js
// Version:     1.0
// Date:        1/12/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class Memory
 */
Knight.PATH.Memory = class {
    /**
     *Creates an instance of Memory.
     * @memberof Memory
     */
    constructor() {
        throw new Error('This is a static class!');
    }

    /**
     * @static
     * @param {Memory.Context} context
     * @memberof Memory
     */
    static loadContext(context) {
        this._distance = context._distance;
        this._parent = context._parent;
        this._visited = context._visited;
        this._defaultDistance = context._defaultDistance;
        this._defaultParent = context._defaultParent;
        this._defaultVisited = context._defaultVisited;
        this._ticketCheck = context._ticketCheck;
        this._ticketNumber = context._ticketNumber;
        this._size = context._size;
    }

    /**
     * @static
     * @param {Memory.Context} context
     * @memberof Memory
     */
    static saveContext(context) {
        context._distance = this._distance;
        context._parent = this._parent;
        context._visited = this._visited;
        context._defaultDistance = this._defaultDistance;
        context._defaultParent = this._defaultParent;
        context._defaultVisited = this._defaultVisited;
        context._ticketCheck = this._ticketCheck;
        context._ticketNumber = this._ticketNumber;
        context._size = this._size;
    }

    /**
     * @static
     * @param {Number} size
     * @param {Number} defaultDistance
     * @param {Number} defaultParent
     * @param {Boolean} defaultVisited
     * @return {Number}
     * @memberof Memory
     */
    static initialize(size, defaultDistance, defaultParent, defaultVisited) {
        this._defaultDistance = defaultDistance;
        this._defaultParent = defaultParent;
        this._defaultVisited = defaultVisited;
        this._size = size;

        if (this._ticketCheck === null || this._ticketCheck.length !== size) {
            this._distance = new Array(size).fill(0);
            this._parent = new Array(size).fill(0);
            this._visited = new Array(size).fill(false);
            this._ticketCheck = new Array(size).fill(0);
            this._ticketNumber = 1;
        } else if (this._ticketNumber === -1) {
            this._ticketCheck = new Array(size).fill(0);
            this._ticketNumber = 1;
        } else {
            this._ticketNumber++;
        }
        return this._ticketNumber;
    }

    /**
     * @return {Number}
     * @memberof Memory
     */
    static currentTicket() {
        return this._ticketNumber;
    }

    /**
     * @return {Number}
     * @memberof Memory
     */
    static size() {
        return this._size;
    }

    /**
     * @static
     * @param {Number} index
     * @return {Number}
     * @memberof Memory
     */
    static distance(index) {
        if (this._ticketCheck[index] !== this._ticketNumber) return this._defaultDistance;
        return this._distance[index];
    }

    /**
     * @static
     * @param {Number} index
     * @return {Number}
     * @memberof Memory
     */
    static parent(index) {
        if (this._ticketCheck[index] !== this._ticketNumber) return this._defaultParent;
        return this._parent[index];
    }

    /**
     * @static
     * @param {Number} index
     * @return {Boolean}
     * @memberof Memory
     */
    static visited(index) {
        if (this._ticketCheck[index] !== this._ticketNumber) return this._defaultVisited;
        return this._visited[index];
    }

    /**
     * @static
     * @param {Number} index
     * @param {Number} value
     * @memberof Memory
     */
    static setDistance(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._distance[index] = value;
            this._parent[index] = this._defaultParent;
            this._visited[index] = this._defaultVisited;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._distance[index] = value;
        }
    }

    /**
     * @static
     * @param {Number} index
     * @param {Number} value
     * @memberof Memory
     */
    static setParent(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._distance[index] = this._defaultDistance;
            this._parent[index] = value;
            this._visited[index] = this._defaultVisited;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._parent[index] = value;
        }
    }

    /**
     * @static
     * @param {Number} index
     * @param {Boolean} value
     * @memberof Memory
     */
    static setVisited(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._distance[index] = this._defaultDistance;
            this._parent[index] = this._defaultParent;
            this._visited[index] = value;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._visited[index] = value;
        }
    }

    /**
     * @static
     * @memberof Memory
     */
    static clearMemory() {
        this._distance = null;
        this._parent = null;
        this._visited = null;
        this._ticketCheck = null;
        global.gc();
    }
};

Knight.PATH.Memory._defaultDistance = 0;
Knight.PATH.Memory._distance = null;
Knight.PATH.Memory._parent = null;
Knight.PATH.Memory._visited = null;
Knight.PATH.Memory._defaultDistance = 0;
Knight.PATH.Memory._defaultParent = -1;
Knight.PATH.Memory._defaultVisited = false;
Knight.PATH.Memory._ticketCheck = null;
Knight.PATH.Memory._ticketNumber = 0;
Knight.PATH.Memory._size = 0;

/**
 * @class Context
 */
Knight.PATH.Memory.Context = class {
    /**
     *Creates an instance of Context.
     * @memberof Context
     */
    constructor() {
        this._distance = null;
        this._parent = null;
        this._visited = null;
        this._defaultDistance = null;
        this._defaultParent = null;
        this._defaultVisited = null;
        this._ticketCheck = null;
        this._ticketNumber = null;
        this._size = null;
    };
};
