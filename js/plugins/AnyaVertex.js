// =============================================================================
// AnyaVertex.js
// Version:     1.0
// Date:        1/27/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.AnyaVertex = class extends Knight.PATH.BaseVertex {
    /**
     * Creates an instance of AnyaHeuristic.
     * @param {Number} id
     * @param {PIXI.Point} pos
     * @param {PIXI.Point} gridPos
     * @memberof AnyaVertex
     */
    constructor(id, pos, gridPos) {
        super(id, pos);
        this.gridPos = gridPos; // Lattice coordinates of the vertex
    }
};

Knight.PATH.AnyaVertex.CellDirections = {
    CD_LEFTDOWN: 0,
    CD_LEFTUP: 1,
    CD_RIGHTDOWN: 2,
    CD_RIGHTUP: 3,
};

Knight.PATH.AnyaVertex.VertexDirections = {
    VD_LEFT: 0,
    VD_RIGHT: 1,
    VD_DOWN: 2,
    VD_UP: 3,
};
