// =============================================================================
// Anya_Euclidean.js
// Version:     1.0
// Date:        1/28/2020
// Author:      Kaelan
//
//  A heuristic for computing Euclidean distances in the plane.
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.EuclideanDistanceHeuristic = class {
    /**
     *
     */
    constructor() {}

    /**
     * @param {BaseVertex} n
     * @param {BaseVertex} [t=null]
     * @return {Number}
     */
    getValue(n, t = null) {
        if (n === null || t === null) return 0;
        return this.h(n.pos.x, n.pos.y, t.pos.x, t.pos.y);
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @return {Number}
     */
    h(x1, y1, x2, y2) {
        const dx = x1 - x2;
        const dy = y1 - y2;
        return Math.sqrt(dx*dx + dy*dy);
    }
};
