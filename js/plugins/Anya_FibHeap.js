// =============================================================================
// Anya_FibHeap.js
// Version:     1.0
// Date:        1/28/2020
// Author:      Nathan Fiedler, Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Implements a node of the Fibonacci heap. It holds the information necessary
 * for maintaining the structure of the heap. It also holds the reference to the
 * key value (which is used to determine the heap structure).
 *
 * Ported from the Java implementation in JGraphT
 *
 * @author Nathan Fiedler
 * @class FibonacciHeapNode
 */
Knight.PATH.FibonacciHeapNode = class {
    /**
     * Default constructor. Initializes the right and left pointers, making this
     * a circular doubly-linked list.
     * @param {AnyaNode} data   Node data
     * @memberof FibonacciHeapNode
     */
    constructor(data) {
        this.data = data;
        this.reset();
    }

    /**
     */
    reset() {
        this.parent = null; // parent node
        this.child = null;  // first child node
        this.right = this;  // right sibling node
        this.left = this;   // left sibling node
        this.key = 0;       // key value for this node
        this.secondaryKey = 0;
        this.degree = 0; // number of children of this node (does not count grandchildren)

        // true if this node has had a child removed since this node was added to its parent
        this.mark = false;
    }

    /**
     * @return {Number}
     */
    getKey() {
        return this.key;
    }

    /**
     * @return {Number}
     */
    getSecondaryKey() {
        return this.secondaryKey;
    }

    /**
     * @return {AnyaNode}
     */
    getData() {
        return this.data;
    }

    /**
     * @param {FibonacciHeapNode} other
     * @return {Boolean} true if this node has a lower priority than @param other
     */
    lessThan(other) {
        return Knight.PATH.FibonacciHeapNode.lessThan(this.key, this.secondaryKey,
            other.key, other.secondaryKey);
    }

    /**
     * @static
     * @param {Number} pk_a
     * @param {Number} sk_a
     * @param {Number} pk_b
     * @param {Number} sk_b
     * @return {Boolean}
     */
    static lessThan(pk_a, sk_a, pk_b, sk_b) {
        let tmpKey = Math.trunc(pk_a * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
        let tmpOther = Math.trunc(pk_b * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
        if (tmpKey < tmpOther) {
            return true;
        }

        // tie-break in favour of nodes with higher
        // secondaryKey values
        if (tmpKey === tmpOther) {
            tmpKey = Math.trunc(sk_a * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
            tmpOther = Math.trunc(sk_b * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
            if (tmpKey > tmpOther) {
                return true;
            }
        }
        return false;
    }
};

Knight.PATH.FibonacciHeapNode.BIG_ONE = 100000;
Knight.PATH.FibonacciHeapNode.epsilon = 1 / Knight.PATH.FibonacciHeapNode.BIG_ONE;

/**
 * This class implements a Fibonacci heap data structure. Much of the code in
 * this class is based on the algorithms in the "Introduction to Algorithms"by
 * Cormen, Leiserson, and Rivest in Chapter 21. The amortized running time of
 * most of these methods is O(1), making it a very fast data structure. Several
 * have an actual running time of O(1). removeMin() and delete() have O(log n)
 * amortized running times because they do the heap consolidation. If you
 * attempt to store nodes in this heap with key values of -Infinity
 * (Double.NEGATIVE_INFINITY) the <code>delete()</code> operation may fail to
 * remove the correct element.
 *
 * <p><b>Note that this implementation is not synchronized.</b> If multiple
 * threads access a set concurrently, and at least one of the threads modifies
 * the set, it <i>must</i> be synchronized externally. This is typically
 * accomplished by synchronizing on some object that naturally encapsulates the
 * set.</p>
 *
 * <p>This class was originally developed by Nathan Fiedler for the GraphMaker
 * project. It was imported to JGraphT with permission, courtesy of Nathan
 * Fiedler.</p>
 *
 * Ported from the Java implementation in JGraphT
 *
 * @author Nathan Fiedler
 * @class FibonacciHeap
 */
Knight.PATH.FibonacciHeap = class {
    /**
     * Constructs a FibonacciHeap object that contains no elements.
     * @memberof FibonacciHeap
     */
    constructor() {
        this.minNode = null; // Points to the minimum node in the heap.
        this.nNodes = 0;     // Number of nodes in the heap.
    }

    /**
     * Tests if the Fibonacci heap is empty or not.
     * Running time: O(1) actual
     *
     * @return {Boolean} True if the heap is empty, false otherwise.
     */
    isEmpty() {
        return this.minNode === null;
    }

    /**
     * Removes all elements from this heap.
     */
    clear() {
        this.minNode = null;
        this.nNodes = 0;
    }

    /**
     * Decreases the key value for a heap node, given the new value to take on.
     * The structure of the heap may be changed and will not be consolidated.
     *
     * Running time: O(1) amortized
     *
     * @param {FibonacciHeapNode} x     node to decrease the key of
     * @param {Number} k                new key value for node x
     * @param {Number} newSecondaryKey  secondaryKey used for tie-breaking
     * @throws {Error}                  Thrown if k is larger than x.key value.
     */
    decreaseKey(x, k, newSecondaryKey = null) {
        if (newSecondaryKey !== null) x.secondaryKey = newSecondaryKey;
        const tmp_k = Math.trunc(k * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
        const tmp_x = Math.trunc(x.key * Knight.PATH.FibonacciHeapNode.BIG_ONE + 0.5);
        if (tmp_k > tmp_x) {
            throw new Error("decreaseKey() got larger key value");
        }

        x.key = k;

        const y = x.parent;

        if ((y !== null) && x.lessThan(y)) {
            this.cut(x, y);
            this.cascadingCut(y);
        }

        if (x.lessThan(this.minNode)) {
            this.minNode = x;
        }
    }

    /**
     * Deletes a node from the heap given the reference to the node. The trees
     * in the heap will be consolidated, if necessary. This operation may fail
     * to remove the correct element if there are nodes with key value
     * -Infinity.
     *
     * Running time: O(log n) amortized
     *
     * @param {FibonacciHeapNode} x     node to remove from heap
     */
    delete(x) {
        // make x as small as possible
        this.decreaseKey(x, Number.NEGATIVE_INFINITY);

        // remove the smallest, which decreases n also
        this.removeMin();
    }

    /**
     * Inserts a new data element into the heap. No heap consolidation is
     * performed at this time, the new node is simply inserted into the root
     * list of this heap.
     *
     * Running time: O(1) actual
     *
     * @param {FibonacciHeapNode} node  new node to insert into heap
     * @param {Number} key              key value associated with data object
     * @param {Number} secondaryKey  secondaryKey used for tie-breaking
     */
    insert(node, key, secondaryKey = null) {
        if (secondaryKey !== null) node.secondaryKey = secondaryKey;
        node.key = key;

        // concatenate node into min list
        if (this.minNode !== null) {
            node.left = this.minNode;
            node.right = this.minNode.right;
            this.minNode.right = node;
            node.right.left = node;

            if (node.lessThan(this.minNode)) {
                this.minNode = node;
            }
        } else {
            this.minNode = node;
        }

        this.nNodes++;
    }

    /**
     * Returns the smallest element in the heap. This smallest element is the
     * one with the minimum key value.
     *
     * Running time: O(1) actual
     *
     * @return {FibonacciHeapNode}
     */
    min() {
        return this.minNode;
    }

    /**
     * Removes the smallest element from the heap. This will cause the trees in
     * the heap to be consolidated, if necessary.
     *
     * Running time: O(log n) amortized
     *
     * @return {FibonacciHeapNode} node with the smallest key
     *
     */
    removeMin() {
        const z = this.minNode;

        if (z !== null) {
            let numKids = z.degree;
            let x = z.child;
            let tempRight;

            // for each child of z do...
            while (numKids > 0) {
                tempRight = x.right;

                // remove x from child list
                x.left.right = x.right;
                x.right.left = x.left;

                // add x to root list of heap
                x.left = this.minNode;
                x.right = this.minNode.right;
                this.minNode.right = x;
                x.right.left = x;

                // set parent[x] to null
                x.parent = null;
                x = tempRight;
                numKids--;
            }

            // remove z from root list of heap
            z.left.right = z.right;
            z.right.left = z.left;

            if (z === z.right) {
                this.minNode = null;
            } else {
                this.minNode = z.right;
                this.consolidate();
            }

            // decrement size of heap
            this.nNodes--;
        }
        return z;
    }

    /**
     * Returns the size of the heap which is measured in the number of elements
     * contained in the heap.
     *
     * Running time: O(1) actual
     *
     * @return {Number} number of elements in the heap
     */
    size() {
        return this.nNodes;
    }

    /**
     * Joins two Fibonacci heaps into a new one. No heap consolidation is
     * performed at this time. The two root lists are simply joined together.
     *
     * <p>Running time: O(1) actual</p>
     *
     * @param {FibonacciHeap} h1    first heap
     * @param {FibonacciHeap} h2    second heap
     * @return {FibonacciHeap}      new heap containing h1 and h2
     */
    static union(h1, h2) {
        const h = new FibonacciHeap;

        if ((h1 !== null) && (h2 !== null)) {
            h.minNode = h1.minNode;

            if (h.minNode !== null) {
                if (h2.minNode !== null) {
                    h.minNode.right.left = h2.minNode.left;
                    h2.minNode.left.right = h.minNode.right;
                    h.minNode.right = h2.minNode;
                    h2.minNode.left = h.minNode;

                    if (h2.minNode.lessThan(h1.minNode)) {
                        h.minNode = h2.minNode;
                    }
                }
            } else {
                h.minNode = h2.minNode;
            }
            h.nNodes = h1.nNodes + h2.nNodes;
        }
        return h;
    }

    /**
     * Creates a String representation of this Fibonacci heap.
     *
     * @return {String}
     */
    toString() {
        if (this.minNode === null) {
            return "FibonacciHeap=[]";
        }

        // create a new stack and put root on it
        const stack = [];
        stack.push(this.minNode);

        let str = '';
        str += "FibonacciHeap=[";

        // do a simple breadth-first traversal on the tree
        while (stack.length > 0) {
            curr = stack.pop();
            str += `${curr}, `;

            if (curr.child !== null) {
                stack.push(curr.child);
            }

            const start = curr;
            curr = curr.right;

            while (curr !== start) {
                str += `${curr}, `;

                if (curr.child !== null) {
                    stack.push(curr.child);
                }

                curr = curr.right;
            }
        }

        str += ']';
        return str;
    }

    /**
     * Performs a cascading cut operation. This cuts y from its parent and then
     * does the same for its parent, and so on up the tree.
     *
     * Running time: O(log n); O(1) excluding the recursion
     *
     * @param {FibonacciHeapNode} y     node to perform cascading cut on
     */
    cascadingCut(y) {
        const z = y.parent;

        // if there's a parent...
        if (z !== null) {
            // if y is unmarked, set it marked
            if (!y.mark) {
                y.mark = true;
            } else {
                // it's marked, cut it from parent
                this.cut(y, z);

                // cut its parent as well
                this.cascadingCut(z);
            }
        }
    }

    /**
     *
     */
    consolidate() {
        const arraySize =
            (Math.trunc(Math.floor(Math.log(this.nNodes) * Knight.PATH.FibonacciHeap.oneOverLogPhi))) + 1;

        const array = new Array(arraySize).fill(null);

        // Find the number of root nodes.
        let numRoots = 0;
        let x = this.minNode;

        if (x !== null) {
            numRoots++;
            x = x.right;

            while (x !== this.minNode) {
                numRoots++;
                x = x.right;
            }
        }

        // For each node in root list do...
        while (numRoots > 0) {
            // Access this node's degree..
            let d = x.degree;
            const next = x.right;

            // ..and see if there's another of the same degree.
            for (;;) {
                let y = array[d];
                if (y === null) {
                    // Nope.
                    break;
                }

                // There is, make one of the nodes a child of the other.
                // Do this based on the key value.
                if (y.lessThan(x)) {
                    const temp = y;
                    y = x;
                    x = temp;
                }

                // FibonacciHeapNode y disappears from root list.
                this.link(y, x);

                // We've handled this degree, go to next one.
                array[d] = null;
                d++;
            }

            // Save this node for later when we might encounter another
            // of the same degree.
            array[d] = x;

            // Move forward through list.
            x = next;
            numRoots--;
        }

        // Set min to null (effectively losing the root list) and
        // reconstruct the root list from the array entries in array[].
        this.minNode = null;

        for (let i = 0; i < arraySize; i++) {
            const y = array[i];
            if (y === null) {
                continue;
            }

            // We've got a live one, add it to root list.
            if (this.minNode !== null) {
                // First remove node from root list.
                y.left.right = y.right;
                y.right.left = y.left;

                // Now add to root list, again.
                y.left = this.minNode;
                y.right = this.minNode.right;
                this.minNode.right = y;
                y.right.left = y;

                // Check if this is a new min.
                if (y.lessThan(this.minNode)) {
                    this.minNode = y;
                }
            } else {
                this.minNode = y;
            }
        }
    }

    /**
     * The reverse of the link operation: removes x from the child list of y.
     * This method assumes that min is non-null.
     *
     * Running time: O(1)
     *
     * @param {FibonacciHeapNode} x     child of y to be removed from y's child list
     * @param {FibonacciHeapNode} y     parent of x about to lose a child
     */
    cut(x, y) {
        // remove x from childlist of y and decrement degree[y]
        x.left.right = x.right;
        x.right.left = x.left;
        y.degree--;

        // reset y.child if necessary
        if (y.child === x) {
            y.child = x.right;
        }

        if (y.degree === 0) {
            y.child = null;
        }

        // add x to root list of heap
        x.left = this.minNode;
        x.right = this.minNode.right;
        this.minNode.right = x;
        x.right.left = x;

        // set parent[x] to nil
        x.parent = null;

        // set mark[x] to false
        x.mark = false;
    }

    /**
     * Make node y a child of node x.
     *
     * Running time: O(1) actual
     *
     * @param {FibonacciHeapNode} y     node to become child
     * @param {FibonacciHeapNode} x     node to become parent
     */
    link(y, x) {
        // remove y from root list of heap
        y.left.right = y.right;
        y.right.left = y.left;

        // make y a child of x
        y.parent = x;

        if (x.child === null) {
            x.child = y;
            y.right = y;
            y.left = y;
        } else {
            y.left = x.child;
            y.right = x.child.right;
            x.child.right = y;
            y.right.left = y;
        }

        // increase degree[x]
        x.degree++;

        // set mark[y] false
        y.mark = false;
    }
};

Knight.PATH.FibonacciHeap.oneOverLogPhi = 1.0 / Math.log((1.0 + Math.sqrt(5.0)) / 2.0);
