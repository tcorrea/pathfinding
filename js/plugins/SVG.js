// =============================================================================
// SVG.js
// Version:     1.0
// Date:        1/11/2020
// Author:      Kaelan
// =============================================================================

//=============================================================================
/*:
 * @plugindesc
 * @version 1.0.0
 * @author Kaelan
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * Javascript implementation of the Edge N-Level Sparse Visibility Graph (ENLSVG)
 * algorithm.
 *
 * Based on the Java implementation by Ohohcakester at:
 * https://github.com/Ohohcakester/Any-Angle-Pathfinding
 *
 * For more information, see the paper describing the algorithm at:
 * https://www.aaai.org/ocs/index.php/SOCS/SOCS17/paper/viewFile/15790/15057
 *
 * The original implementation is released under the MIT License.
 */

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class ENLSVG
 */
Knight.PATH.ENLSVG = class {
    /**
     * Creates an instance of ENLSVG.
     * @param {GridGraph} graph
     * @memberof ENLSVG
     */
    constructor(graph) {
        this._graph = graph;
        this._sizeXPlusOne = graph.sizeX+1;
        this._sizeYPlusOne = graph.sizeY+1;
        this._losScanner = null;

        this._nodeIndex = null;
        this._startIndex = -1;
        this._endIndex = -1;

        this._saveSnapshot = null;

        this._originalSize = null;
        this._maxSize = null;
        this._nNodes = null;

        this._startOriginalSize = null;
        this._endOriginalSize = null;

        this._queue = null;
        this._queueSize = null;

        this._originalNEdges = null;

        this.levelLimit = null;

        // Nodes: Indexed by node Index.
        this.xPostions = null;
        this.yPositions = null;
        this.nOutgoingEdges = null;         // value: number of outgoingEdges.
        this.outgoingEdges = null;          // value: nodeIndex of destination
        this.outgoingEdgeIndexes = null;    // value: edgeIndex of edge to destination.
        this.outgoingEdgeOppositeIndexes = null;  // value: index within outgoingEdgess of the opposite edge.
        this.hasEdgeToGoal = null;
        this._outgoingEdgeIsMarked = null;

        // Used to iterate through the Level-W edges quickly
        this.nLevelWNeighbours = null;
        this.levelWEdgeOutgoingIndexes = null;    // value: index of edge in outgoingEdges array.

        // Used to iterate through the marked edges quickly
        this.nMarkedEdges = null;
        this.outgoingMarkedEdgeIndexes = null;    // value: index of edge in outgoingEdges array.

        this.nSkipEdges = null;                   // value: number of outgoing skip-edges.
        this.outgoingSkipEdges = null;            // value: nodeIndex of destination
        this.outgoingSkipEdgeNextNodes = null;    // value: nodeIndex of the next node in the actual path if the skip edge were to be followed.
        this.outgoingSkipEdgeNextNodeEdgeIndexes = null; // value: edgeIndex of edge to next node in the actual path.
        this.outgoingSkipEdgeWeights = null;      // value: weight of edge to destination.

        // Edges: Indexed by edge Index (only for non-skip-edges)
        this.nEdges = null;
        this.edgeLevels = null;
        this.edgeWeights = null;
        this.isMarked = null;
    };

    /**
     * @param {Function} saveSnapshot
     * @memberof ENLSVG
     */
    setSaveSnapshotFunction(saveSnapshot) {
        this._saveSnapshot = saveSnapshot;
    }

    /**
     * @static
     * @param {GridGraph} graph
     * @param {Number} levelLimit
     * @return {ENLSVG}
     * @memberof ENLSVG
     */
    static initializeNew(graph, levelLimit) {
        if (this._storedGridGraph === graph && levelLimit === this._storedLevelLimit) {
            this._storedVisibilityGraph.restoreOriginalGraph();
            return this._storedVisibilityGraph;
        }
        console.time('ENLSVG Construction Time');

        this._storedGridGraph = graph;
        const vGraph = this._storedVisibilityGraph = new Knight.PATH.ENLSVG(graph);
        this._storedLevelLimit = levelLimit;
        vGraph.levelLimit = levelLimit;
        vGraph.constructGraph();

        console.timeEnd('ENLSVG Construction Time');
        return vGraph;
    }

    /// \\\ /// \\\ /// \\\ /// \\\ /// \\\ ///
    ///   GRAPH CONSTRUCTION PHASE - START  ///
    /// \\\ /// \\\ /// \\\ /// \\\ /// \\\ ///

    /**
     * @memberof ENLSVG
     */
    constructGraph() {
        this._losScanner = new Knight.PATH.LineOfSightScanner(this._graph);
        this._queue = new Array(11).fill(0);
        this._queueSize = 0;

        // STEP 1: Construct SVG (Strict Visibility Graph)

        // Initialize SVG Vertices
        this.xPositions = new Array(11).fill(0);
        this.yPositions = new Array(11).fill(0);
        this._nNodes = 0;
        this.addNodes();

        // Now xPositions and yPositions should be correctly initialized.
        //  We then initialize the rest of the node data.
        this._originalSize = this._nNodes;
        this._maxSize = this._nNodes + 2;
        this.xPositions = Knight.arraysCopyOf(this.xPositions, this._maxSize);
        this.yPositions = Knight.arraysCopyOf(this.yPositions, this._maxSize);
        this.hasEdgeToGoal = new Array(this._maxSize).fill(false);
        this.nOutgoingEdges = new Array(this._maxSize).fill(0);
        this.outgoingEdges = new Array(this._maxSize);
        this.outgoingEdgeIndexes = new Array(this._maxSize);
        this.outgoingEdgeOppositeIndexes = new Array(this._maxSize);
        this._outgoingEdgeIsMarked = new Array(this._maxSize);
        for (let i = 0; i < this._maxSize; ++i) {
            this.nOutgoingEdges[i] = 0;
            this.outgoingEdges[i] = new Array(11).fill(0);
            this.outgoingEdgeIndexes[i] = new Array(11).fill(0);
            this.outgoingEdgeOppositeIndexes[i] = new Array(11).fill(0);
            this._outgoingEdgeIsMarked[i] = new Array(11).fill(false);
        }

        // Initialize SVG Edges + edgeWeights
        this.edgeWeights = new Array(11).fill(0);
        this.nEdges = 0;
        this.addAllEdges();

        // Now all the edges, indexes and weights should be correctly initialize.
        //  Now we initialize the rest of the edge data.
        this._originalNEdges = this.nEdges;
        const maxPossibleNEdges = this.nEdges + this._nNodes*2;
        this.edgeWeights = Knight.arraysCopyOf(this.edgeWeights, maxPossibleNEdges);
        this.edgeLevels = new Array(maxPossibleNEdges).fill(Knight.PATH.ENLSVG.LEVEL_W);
        this.isMarked = new Array(maxPossibleNEdges).fill(false);

        // Reserve space in level w edge and marked edge arrays.
        this.nLevelWNeighbours = new Array(this._maxSize).fill(0);
        this.levelWEdgeOutgoingIndexes = new Array(this._maxSize);
        this.nMarkedEdges = new Array(this._maxSize).fill(0);
        this.outgoingMarkedEdgeIndexes = new Array(this._maxSize);
        for (let i = 0; i < this._nNodes; ++i) {
            this.levelWEdgeOutgoingIndexes[i] = new Array(this.nOutgoingEdges[i]).fill(0);
            this.outgoingMarkedEdgeIndexes[i] = new Array(this.nOutgoingEdges[i]).fill(0);
        }
        for (let i = this._nNodes; i < this._maxSize; ++i) {
            this.levelWEdgeOutgoingIndexes[i] = new Array(11).fill(0);
            this.outgoingMarkedEdgeIndexes[i] = new Array(11).fill(0);
        }

        // STEP 2: Label edge levels in SVG.
        this.computeAllEdgeLevelsFast();
        this.addLevelWEdgesToLevelWEdgesArray();

        this.nSkipEdges = new Array(this._maxSize).fill(0);
        this.outgoingSkipEdges = new Array(this._maxSize);
        this.outgoingSkipEdgeNextNodes = new Array(this._maxSize);
        this.outgoingSkipEdgeNextNodeEdgeIndexes = new Array(this._maxSize);
        this.outgoingSkipEdgeWeights = new Array(this._maxSize);

        // STEP 3: Initialize the skip-edges & Group together Level-W edges using isMarkedIndex.
        this.setupSkipEdges();
        this.pruneParallelSkipEdges();
    }

    /**
     * @memberof ENLSVG
     */
    addNodes() {
        this._nodeIndex = new Array(this._sizeYPlusOne * this._sizeXPlusOne);
        for (let y = 0; y < this._sizeYPlusOne; ++y) {
            for (let x = 0; x < this._sizeXPlusOne; ++x) {
                if (this._graph.isOuterCorner(x, y)) {
                    this._nodeIndex[y*this._sizeXPlusOne + x] = this.assignNode(x, y);
                } else {
                    this._nodeIndex[y*this._sizeXPlusOne + x] = -1;
                }
            }
        }
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Number}
     * @memberof ENLSVG
     */
    assignNode(x, y) {
        const index = this._nNodes;
        if (index >= this.xPositions.length) {
            this.xPositions = Knight.arraysCopyOf(this.xPositions, this.xPositions.length*2);
            this.yPositions = Knight.arraysCopyOf(this.yPositions, this.yPositions.length*2);
        }
        this.xPositions[index] = x;
        this.yPositions[index] = y;

        this._nNodes++;
        return index;
    }

    /**
     * @memberof ENLSVG
     */
    addAllEdges() {
        let fromX;
        let fromY;
        let toX;
        let toY;
        for (let i = 0; i < this._nNodes; ++i) {
            fromX = this.xPositions[i];
            fromY = this.yPositions[i];

            this._losScanner.computeAllVisibleTwoWayTautSuccessors(fromX, fromY);
            const nSuccessors = Knight.PATH.LineOfSightScanner._nSuccessors;
            for (let succ = 0; succ < nSuccessors; ++succ) {
                toX = Knight.PATH.LineOfSightScanner._successorsX[succ];
                toY = Knight.PATH.LineOfSightScanner._successorsY[succ];
                const j = this._nodeIndex[toY*this._sizeXPlusOne + toX];

                // We add both ways at the same time. So we use this to avoid duplicates
                if (i >= j) continue;

                const weight = this._graph.distance(fromX, fromY, toX, toY);
                this.addEdge(i, j, weight);
            }
        }
    }

    /**
     * Adds an edge from node v1 to node v2, and from node v2 to node v1
     * @param {Number} v1
     * @param {Number} v2
     * @param {Number} weight
     * @memberof ENLSVG
     */
    addEdge(v1, v2, weight) {
        if (this.nEdges >= this.edgeWeights.length) {
            this.edgeWeights = Knight.arraysCopyOf(this.edgeWeights, this.edgeWeights.length*2);
        }
        const edgeIndex = this.nEdges;

        // add edge to v1
        let v1Index;
        {
            v1Index = this.nOutgoingEdges[v1];
            if (v1Index >= this.outgoingEdges[v1].length) {
                const newLength = this.outgoingEdges[v1].length*2;
                this.outgoingEdges[v1] = Knight.arraysCopyOf(this.outgoingEdges[v1], newLength);
                this.outgoingEdgeIndexes[v1] = Knight.arraysCopyOf(this.outgoingEdgeIndexes[v1], newLength);
                this._outgoingEdgeIsMarked[v1] = Knight.arraysCopyOf(this._outgoingEdgeIsMarked[v1], newLength);
                this.outgoingEdgeOppositeIndexes[v1] = Knight.arraysCopyOf(this.outgoingEdgeOppositeIndexes[v1], newLength);
            }
            this.outgoingEdges[v1][v1Index] = v2;
            this.outgoingEdgeIndexes[v1][v1Index] = edgeIndex;
            this.nOutgoingEdges[v1]++;
        }

        // add edge to v2
        let v2Index;
        {
            v2Index = this.nOutgoingEdges[v2];
            if (v2Index >= this.outgoingEdges[v2].length) {
                const newLength = this.outgoingEdges[v2].length*2;
                this.outgoingEdges[v2] = Knight.arraysCopyOf(this.outgoingEdges[v2], newLength);
                this.outgoingEdgeIndexes[v2] = Knight.arraysCopyOf(this.outgoingEdgeIndexes[v2], newLength);
                this._outgoingEdgeIsMarked[v2] = Knight.arraysCopyOf(this._outgoingEdgeIsMarked[v2], newLength);
                this.outgoingEdgeOppositeIndexes[v2] = Knight.arraysCopyOf(this.outgoingEdgeOppositeIndexes[v2], newLength);
            }
            this.outgoingEdges[v2][v2Index] = v1;
            this.outgoingEdgeIndexes[v2][v2Index] = edgeIndex;
            this.nOutgoingEdges[v2]++;
        }

        this.outgoingEdgeOppositeIndexes[v1][v1Index] = v2Index;
        this.outgoingEdgeOppositeIndexes[v2][v2Index] = v1Index;

        this.edgeWeights[this.nEdges] = weight;
        ++this.nEdges;
    }

    /**
     * @memberof ENLSVG
     */
    computeAllEdgeLevels() {
        let hasChanged = true;
        let currentLevel = 0;

        // Runs in time linesr on the number of edges per iteration.
        // The number of iterations will be the max level of any edge.
        while (hasChanged && currentLevel < this.levelLimit) {
            currentLevel++;
            hasChanged = false;

            // Note: we ignore all edges of level < currentLevel.
            for (let v1 = 0; v1 < this._nNodes; ++v1) {
                const nOutgoingEdges = this.nOutgoingEdges[v1];
                const outgoingEdges = this.outgoingEdges[v1];
                const outgoingEdgeIndexes = this.outgoingEdgeIndexes[v1];

                for (let j = 0; j < nOutgoingEdges; ++j) {
                    const edgeIndex = outgoingEdgeIndexes[j];
                    if (this.edgeLevels[edgeIndex] < currentLevel) continue;

                    const v2 = outgoingEdges[j];

                    // Avoid repeated work.
                    if (v1 >= v2) continue;

                    // This essentially is a loop through all unmarked edges (v1,v2)

                    // Note: not be pruned, the edge must have a taut exit on BOTH ends.
                    if (!this.hasTautExit(v1, v2, currentLevel) || !this.hasTautExit(v2, v1, currentLevel)) {
                        this.edgeLevels[edgeIndex] = currentLevel;
                        hasChanged = true;
                    }
                }
            }
        }
    }

    /**
     * @memberof ENLSVG
     */
    computeAllEdgeLevelsFast() {
        // Note: Each edge is identified by a tuple (nodeId, edgeIndex).

        // list of current level directed edges as a tuple (nodeId, edgeIndex)
        const currentLevelEdgeNodes = new Array(this.nEdges*2).fill(0);
        const currentLevelEdgeIndexes = new Array(this.nEdges*2).fill(0);
        let currEdge = 0;
        let nextLevelEnd = 0;

        /**
         *    \     neighbours are behind the current edge.
         * '.  \
         *   '. \     nNeighbours[vi][ei] = 2
         *     '.\
         *        O-----------> [next]
         *       vi     ei
         */
        const nNeighbours = new Array(this._nNodes);
        for (let vi = 0; vi < this._nNodes; ++vi) {
            const currX = this.xPositions[vi];
            const currY = this.yPositions[vi];

            const nOutgoingEdges = this.nOutgoingEdges[vi];
            const outgoingEdges = this.outgoingEdges[vi];

            const currNodeNNeighbours = new Array(nOutgoingEdges).fill(0);
            for (let ei = 0; ei < nOutgoingEdges; ++ei) {
                // For each directed edge
                const ni = outgoingEdges[ei];
                const nextX = this.xPositions[ni];
                const nextY = this.yPositions[ni];

                // Count taut outgoing edges
                let count = 0;

                const nNextOutgoingEdges = this.nOutgoingEdges[vi];
                const nextOutgoingEdges = this.outgoingEdges[vi];
                for (let j = 0; j < nNextOutgoingEdges; ++j) {
                    const di = nextOutgoingEdges[j];
                    if (this._graph.isTaut(nextX, nextY, currX, currY, this.xPositions[di], this.yPositions[di])) {
                        ++count;
                    }
                }

                currNodeNNeighbours[ei] = count;
                if (count === 0) {
                    currentLevelEdgeNodes[nextLevelEnd] = vi;
                    currentLevelEdgeIndexes[nextLevelEnd] = ei;
                    ++nextLevelEnd;
                }
            }
            nNeighbours[vi] = currNodeNNeighbours;
        }

        let currLevel = 1;
        while (currEdge < nextLevelEnd && currLevel < this.levelLimit) {
            const currentLevelEnd = nextLevelEnd;
            for (; currEdge < currentLevelEnd; ++currEdge) {
                const currNode = currentLevelEdgeNodes[currEdge];
                const currEdgeIndex = currentLevelEdgeIndexes[currEdge];

                if (this.edgeLevels[this.outgoingEdgeIndexes[currNode][currEdgeIndex]] !== Knight.PATH.ENLSVG.LEVEL_W) continue;
                // Set edge level
                this.edgeLevels[this.outgoingEdgeIndexes[currNode][currEdgeIndex]] = currLevel;

                /**
                 * Curr side must have no neighbours.
                 * Opp side (next node) may have neighbours.
                 * So remove neighbours from opposite side.
                 *                              __
                 *                               /|   __
                 *                              /    .-'|
                 * (no neighbours)             /  .-'
                 *                            /.-'
                 *            O-----------> [n]
                 *           vi     ei
                 */
                // No need to remove neighbours from opposite edge. They are ignored automatically.
                const nextNode = this.outgoingEdges[currNode][currEdgeIndex];

                const currX = this.xPositions[currNode];
                const currY = this.yPositions[currNode];
                const nextX = this.xPositions[nextNode];
                const nextY = this.yPositions[nextNode];

                const outgoingEdges = this.outgoingEdges[nextNode];
                const nOutgoingEdges = this.nOutgoingEdges[nextNode];

                const outgoingEdgeIndexes = this.outgoingEdgeIndexes[nextNode];

                const nextNodeNNeighbours = nNeighbours[nextNode];
                for (let j = 0; j < nOutgoingEdges; ++j) {
                    if (this.edgeLevels[outgoingEdgeIndexes[j]] !== Knight.PATH.ENLSVG.LEVEL_W) continue;
                    const nextnextNode = outgoingEdges[j];
                    const nextnextX = this.xPositions[nextnextNode];
                    const nextnextY = this.yPositions[nextnextNode];
                    if (!this._graph.isTaut(currX, currY, nextX, nextY, nextnextX, nextnextY)) continue;

                    --nextNodeNNeighbours[j];
                    if (nextNodeNNeighbours[j] === 0) {
                        // push into next level's list
                        currentLevelEdgeNodes[nextLevelEnd] = nextNode;
                        currentLevelEdgeIndexes[nextLevelEnd] = j;
                        ++nextLevelEnd;
                    }

                    if (nextNodeNNeighbours[j] < 0) console.log("ERROR");
                }
            }
            ++currLevel;
        }
    }

    /**
     * Checks whether there is a taut exist in the graph, considering only unmarked edges.
     * Note: unmarked edges are edges whose level >= currentLevel.
     * @param {Number} vFrom
     * @param {Number} vTo
     * @param {Number} currentLevel
     * @return {Boolean}
     * @memberof ENLSVG
     */
    hasTautExit(vFrom, vTo, currentLevel) {
        const x1 = this.xPositions[vFrom];
        const y1 = this.yPositions[vFrom];
        const x2 = this.xPositions[vTo];
        const y2 = this.yPositions[vTo];

        const nOutgoingEdges = this.nOutgoingEdges[vTo];
        const outgoingEdges = this.outgoingEdges[vTo];
        const outgoingEdgeIndexes = this.outgoingEdgeIndexes[vTo];
        for (let j = 0; j < nOutgoingEdges; ++j) {
            if (this.edgeLevels[outgoingEdgeIndexes[j]] < currentLevel) continue;
            const v3 = outgoingEdges[j];
            const x3 = this.xPositions[v3];
            const y3 = this.yPositions[v3];
            if (this._graph.isTaut(x1, y1, x2, y2, x3, y3)) return true;
        }
        return false;
    }

    /**
     * @memberof ENLSVG
     */
    addLevelWEdgesToLevelWEdgesArray() {
        for (let i = 0; i < this._nNodes; ++i ) {
            const nOutgoingEdges = this.nOutgoingEdges[i];
            const outgoingEdgeIndexes = this.outgoingEdgeIndexes[i];
            const outgoingLevelWEdgeIndexes = this.levelWEdgeOutgoingIndexes[i];

            for (let j = 0; j < nOutgoingEdges; ++j) {
                const edgeIndex = outgoingEdgeIndexes[j];
                if (this.edgeLevels[edgeIndex] === Knight.PATH.ENLSVG.LEVEL_W) {
                    outgoingLevelWEdgeIndexes[this.nLevelWNeighbours[i]] = j;
                    ++this.nLevelWNeighbours[i];
                }
            }
        }
    }

    /**
     * We start from the graph of all Level-W edges.
     * We setup skip edges to reduce the graph to one where every vertex has degree >2.
     * Note: we can prove that the set of Level-W edges has no edge of degree 1.
     * @memberof ENLSVG
     */
    setupSkipEdges() {
        const nSkipVertices = this.markSkipVertices();
        if (nSkipVertices > 0) {
            this.connectSkipEdgesAndGroupLevelWEdges();
        }
    }

    /**
     * For each skip vertex, we initialize outgoingSkipEdgeNextNodes and nSkipEdges for all skip vertices.
     * Thus, we have:
     * nSkipEdgess[v] == 0 iff v is not a skip vertex.
     * if v is a skip vertex, then nSkipEdgess[v] >= 3
     *
     * @return {Number}
     * @memberof ENLSVG
     */
    markSkipVertices() {
        let nSkipVertices = 0;

        for (let i = 0; i < this._nNodes; ++i) {
            const nLevelWNeighbours = this.nLevelWNeighbours[i];

            // Skip vertices must have at most 1 or at least 3 level-W neighbours.
            if (nLevelWNeighbours === 0 || nLevelWNeighbours === 2) continue;

            // Else, i is a skip-vertex.
            this.nSkipEdges[i] = nLevelWNeighbours;
            const nextNodes = new Array(nLevelWNeighbours).fill(0);
            const edgeIndexes = new Array(nLevelWNeighbours).fill(0);
            const weights = new Array(nLevelWNeighbours).fill(0);
            const destinations = new Array(nLevelWNeighbours).fill(0);

            const outgoingEdges = this.outgoingEdges[i];
            const outgoingEdgeIndexes = this.outgoingEdgeIndexes[i];
            const levelWEdgeOutgoingIndexes = this.levelWEdgeOutgoingIndexes[i];

            for (let j = 0; j < nLevelWNeighbours; ++j) {
                const index = levelWEdgeOutgoingIndexes[j];
                nextNodes[j] = outgoingEdges[index];
                edgeIndexes[j] = outgoingEdgeIndexes[index];
            }

            this.outgoingSkipEdges[i] = destinations;
            this.outgoingSkipEdgeNextNodes[i] = nextNodes;
            this.outgoingSkipEdgeNextNodeEdgeIndexes[i] = edgeIndexes;
            this.outgoingSkipEdgeWeights[i] = weights;

            nSkipVertices++;
        }
        return nSkipVertices;
    }

    /**
     * Connects the previously marked skip vertices to form a graph of skip-edges.
     * @memberof ENLSVG
     */
    connectSkipEdgesAndGroupLevelWEdges() {
        for (let v1 = 0; v1 < this._nNodes; ++v1) {
            const nSkipEdges = this.nSkipEdges[v1];
            // Loop through only skip vertices.
            if (nSkipEdges === 0) continue;

            const nextNodes = this.outgoingSkipEdgeNextNodes[v1];
            const nextNodeEdgeIndexes = this.outgoingSkipEdgeNextNodeEdgeIndexes[v1];
            const outgoingSkipEdges = this.outgoingSkipEdges[v1];
            const skipWeights = this.outgoingSkipEdgeWeights[v1];
            for (let j = 0; j < nSkipEdges; ++j) {
                // Start from vertex v1, move in direction j
                let previous = v1;
                let current = nextNodes[j];
                const firstEdgeIndex = nextNodeEdgeIndexes[j];
                outgoingSkipEdges[j] = current;
                skipWeights[j] = this.edgeWeights[firstEdgeIndex];

                // invariants:
                // 1. outgoingSkipEdges[j] == current.
                // 2. skipWeights[j] == path length from v1 up to current.
                // initially, skipWeights[j] = weight(v1, nextNodes[j])

                while (this.nSkipEdges[current] === 0) {
                    const nLevelWNeighbours = this.nLevelWNeighbours[current];
                    const levelWEdgeOutgoingIndexes = this.levelWEdgeOutgoingIndexes[current];

                    // While current is still not yet a skip vertex,
                    // Else continue expanding.
                    const outgoingEdges = this.outgoingEdges[current];

                    for (let k = 0; k < nLevelWNeighbours; ++k) {
                        const index = levelWEdgeOutgoingIndexes[k];
                        const next = outgoingEdges[index];
                        if (next === previous) continue;

                        const edgeIndex = this.outgoingEdgeIndexes[current][index];

                        // now next === the next node in the list
                        previous = current;
                        current = next;

                        outgoingSkipEdges[j] = current;
                        skipWeights[j] += this.edgeWeights[edgeIndex];
                        break;
                    }
                }
                // now all the edges along that subpath will be of the same index group.
            }
        }
    }

    /**
     * OPTIMISATION: Parallel Skip-Edge Reduction
     *  Idea: Fix two vertices u and v. Suppose there exists two parallel skip edges e1, e2, between u and v.
     *  Let length of e1 <= length of e2 WLOG.
     *  Then we can prune e2 from the skip graph.
     *   '-> Note: this also prevents some bookkeeping bugs in the original version.
     *
     * Uses Memory.
     * @memberof ENLSVG
     */
    pruneParallelSkipEdges() {
        // TODO: IF THERE ARE MULTIPLE EDGES WITH THE SAME EDGE WEIGHT, WE ARBITRARILY PICK THE FIRST EDGE TO KEEP
        //       THE ORDERING MAY BE DIFFERENT FROM BOTH SIDES OF THE EDGE, WHICH CAN LEAD TO A NONSYMMETRIC GRAPH
        //       However, no issues have cropped up yet. Perhaps the ordering happens to be the same for both sides,
        //         due to how the graph is constructed. This is not good to rely upon, however.
        Knight.PATH.Memory.initialize(this._maxSize, Number.POSITIVE_INFINITY, -1, false);

        let maxDegree = 0;
        for (let i = 0; i < this._nNodes; ++i) {
            maxDegree = Math.max(maxDegree, this.nSkipEdges[i]);
        }

        const neighbourIndexes = new Array(maxDegree).fill(0);
        const lowestCostEdgeIndex = new Array(maxDegree).fill(0);
        const lowestCost = new Array(maxDegree).fill(0);
        let nUsed = 0;

        for (let i = 0; i < this._nNodes; ++i) {
            nUsed = 0;
            let degree = this.nSkipEdges[i];
            const sEdges = this.outgoingSkipEdges[i];
            const sWeights = this.outgoingSkipEdgeWeights[i];

            for (let j = 0; j < degree; ++j) {
                const dest = sEdges[j];
                const weight = sWeights[j];

                const p = Knight.PATH.Memory.parent(dest);
                let index = -1;

                if (p === -1) {
                    index = nUsed;
                    ++nUsed;

                    Knight.PATH.Memory.setParent(dest, index);
                    neighbourIndexes[index] = dest;

                    lowestCostEdgeIndex[index] = j;
                    lowestCost[index] = weight;
                } else {
                    index = p;
                    if (weight < lowestCost[index]) {
                        // Remove existing

                        /*   ________________________________
                         *  |______E__________C____________L_|
                         *   ________________________________
                         *  |______C__________L____________E_|
                         *                                 ^
                         *                                 |
                         *                            nSkipEdges--
                         */
                        this.swapSkipEdges(i, lowestCostEdgeIndex[index], j);
                        this.swapSkipEdges(i, j, degree-1);
                        --j; --degree;

                        // lowestCostEdgeIndex[index] happens to be the same as before.
                        lowestCost[index] = weight;
                    } else {
                        // Remove this.

                        /*   ________________________________
                         *  |______E__________C____________L_|
                         *   ________________________________
                         *  |______E__________L____________C_|
                         *                                 ^
                         *                                 |
                         *                            nSkipEdges--
                         */
                        this.swapSkipEdges(i, j, degree-1);
                        --j; --degree;
                    }
                }
            }
            this.nSkipEdges[i] = degree;

            // Cleanup
            for (let j = 0; j < nUsed; ++j) {
                Knight.PATH.Memory.setParent(neighbourIndexes[j], -1);
            }
        }
    }

    /**
     * @param {Number} v
     * @param {Number} i1
     * @param {Number} i2
     * @memberof ENLSVG
     */
    swapSkipEdges(v, i1, i2) {
        let temp;
        {
            temp = this.outgoingSkipEdges[v][i1];
            this.outgoingSkipEdges[v][i1] = this.outgoingSkipEdges[v][i2];
            this.outgoingSkipEdges[v][i2] = temp;
        }
        {
            temp = this.outgoingSkipEdgeNextNodes[v][i1];
            this.outgoingSkipEdgeNextNodes[v][i1] = this.outgoingSkipEdgeNextNodes[v][i2];
            this.outgoingSkipEdgeNextNodes[v][i2] = temp;
        }
        {
            temp = this.outgoingSkipEdgeNextNodeEdgeIndexes[v][i1];
            this.outgoingSkipEdgeNextNodeEdgeIndexes[v][i1] = this.outgoingSkipEdgeNextNodeEdgeIndexes[v][i2];
            this.outgoingSkipEdgeNextNodeEdgeIndexes[v][i2] = temp;
        }
        {
            temp = this.outgoingSkipEdgeWeights[v][i1];
            this.outgoingSkipEdgeWeights[v][i1] = this.outgoingSkipEdgeWeights[v][i2];
            this.outgoingSkipEdgeWeights[v][i2] = temp;
        }
    }

    /// \\\ /// \\\ /// \\\ /// \\\ /// \\\ ///
    ///    GRAPH CONSTRUCTION PHASE - END   ///
    /// \\\ /// \\\ /// \\\ /// \\\ /// \\\ ///


    /// \\\ /// \\\ /// \\\ /// \\\ ///
    ///  REPURPOSING PHASE - START  ///
    /// \\\ /// \\\ /// \\\ /// \\\ ///

    /**
     * @memberof ENLSVG
     */
    restoreOriginalGraph() {
        // Safeguard against multiple restores
        if (this._startIndex === -1) return;

        this.markEdgesFrom(this._startIndex, false);
        this.markEdgesFrom(this._endIndex, false);
        this.markHasEdgeToGoal(false);
        this.nOutgoingEdges[this._endIndex] = this._endOriginalSize;
        this.nOutgoingEdges[this._startIndex] = this._startOriginalSize;
        this.nEdges = this._originalNEdges;

        this._nNodes = this._originalSize;
        this._startIndex = -1;
        this._endIndex = -1;
    }

    /**
     * Assumption: No edge between start and end.
     * Uses Memory
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @memberof ENLSVG
     */
    addStartAndEnd(sx, sy, ex, ey) {
        // START:
        if (this._nodeIndex[sy*this._sizeXPlusOne + sx] === -1) {
            this._startIndex = this._nNodes;
            this.nOutgoingEdges[this._startIndex] = 0;
            this.xPositions[this._startIndex] = sx;
            this.yPositions[this._startIndex] = sy;
            ++this._nNodes;
        } else {
            this._startIndex = this._nodeIndex[sy*this._sizeXPlusOne + sx];
        }
        this._startOriginalSize = this.nOutgoingEdges[this._startIndex];
        this.addTempEdgesToVisibleNeighbours(this._startIndex, sx, sy);

        // END:
        if (this._nodeIndex[ey*this._sizeXPlusOne + ex] === -1) {
            this._endIndex = this._nNodes;
            this.nOutgoingEdges[this._endIndex] = 0;
            this.xPositions[this._endIndex] = ex;
            this.yPositions[this._endIndex] = ey;
            ++this._nNodes;
        } else {
            this._endIndex = this._nodeIndex[ey*this._sizeXPlusOne + ex];
        }
        this._endOriginalSize = this.nOutgoingEdges[this._endIndex];
        this.addTempEdgesToVisibleNeighbours(this._endIndex, ex, ey);

        this.markHasEdgeToGoal(true);

        this.markEdgesFrom(this._startIndex, true);
        this.markEdgesFrom(this._endIndex, true);
    }

    /**
     * Uses Memory
     * @param {Number} index
     * @param {Number} x
     * @param {Number} y
     * @return {Number}
     * @memberof ENLSVG
     */
    addTempEdgesToVisibleNeighbours(index, x, y) {
        Knight.PATH.Memory.initialize(this._maxSize, Number.POSITIVE_INFINITY, -1, false);
        {
            const nOutgoingEdges = this.nOutgoingEdges[index];
            const outgoingEdges = this.outgoingEdges[index];
            for (let i = 0; i < nOutgoingEdges; ++i) {
                Knight.PATH.Memory.setVisited(outgoingEdges[i], true);
            }
        }

        this._losScanner.computeAllVisibleTautSuccessors(x, y);
        const nSuccessors = Knight.PATH.LineOfSightScanner._nSuccessors;
        for (let i = 0; i < nSuccessors; ++i) {
            const toX = Knight.PATH.LineOfSightScanner._successorsX[i];
            const toY = Knight.PATH.LineOfSightScanner._successorsY[i];
            const targetIndex = this._nodeIndex[toY*this._sizeXPlusOne + toX];
            if (Knight.PATH.Memory.visited(targetIndex)) continue;

            const weight = this._graph.distance(x, y, toX, toY);
            this.addTemporaryEdge(index, targetIndex, weight);
        }

        if (nSuccessors > this.outgoingMarkedEdgeIndexes[index].length) {
            let newLength = Math.max(this.outgoingMarkedEdgeIndexes[index].length, 11);
            while (newLength < nSuccessors) newLength *= 2;
            this.outgoingMarkedEdgeIndexes[index] = Knight.arraysCopyOf(this.outgoingMarkedEdgeIndexes[index], newLength);
        }

        return index;
    }

    /**
     * Adds an edge from node v1 to node v2
     * @param {Number} v1
     * @param {Number} v2
     * @param {Number} weight
     * @memberof ENLSVG
     */
    addTemporaryEdge(v1, v2, weight) {
        // We assume that expansion is never needed.
        const edgeIndex = this.nEdges;

        // add edge to v1
        {
            const index = this.nOutgoingEdges[v1];
            if (index >= this.outgoingEdges[v1].length) {
                const newLength = this.outgoingEdges[v1].length*2;
                this.outgoingEdges[v1] = Knight.arraysCopyOf(this.outgoingEdges[v1], newLength);
                this.outgoingEdgeIndexes[v1] = Knight.arraysCopyOf(this.outgoingEdgeIndexes[v1], newLength);
                this._outgoingEdgeIsMarked[v1] = Knight.arraysCopyOf(this._outgoingEdgeIsMarked[v1], newLength);
            }
            this.outgoingEdges[v1][index] = v2;
            this.outgoingEdgeIndexes[v1][index] = edgeIndex;
            this.nOutgoingEdges[v1]++;
        }

        this.edgeWeights[edgeIndex] = weight;
        this.edgeLevels[edgeIndex] = 0;
        this.isMarked[edgeIndex] = false;
        ++this.nEdges;
    }

    /**
     * @param {Boolean} value
     * @memberof ENLSVG
     */
    markHasEdgeToGoal(value) {
        const outgoingEdges = this.outgoingEdges[this._endIndex];
        const nOutgoingEdges = this.nOutgoingEdges[this._endIndex];
        for (let i = 0; i < nOutgoingEdges; ++i ) {
            this.hasEdgeToGoal[outgoingEdges[i]] = value;
        }
    }

    /**
     * Mark all edges reachable with a path of edges of increasing level from the source.
     * @param {Number} source
     * @param {Boolean} value
     * @memberof ENLSVG
     */
    markEdgesFrom(source, value) {
        this._queueSize = 0;
        {
            const nOutgoingEdges = this.nOutgoingEdges[source];
            const outgoingEdgeIndexes = this.outgoingEdgeIndexes[source];
            const outgoingEdgeIsMarked = this._outgoingEdgeIsMarked[source];

            for (let i = 0; i < nOutgoingEdges; ++i) {
                if (outgoingEdgeIsMarked[i] === value) continue;
                outgoingEdgeIsMarked[i] = value;

                const edgeIndex = outgoingEdgeIndexes[i];
                if (this.isMarked[edgeIndex] !== value) {
                    this.isMarked[edgeIndex] = value;
                    if (value) {
                        this.addToMarkedEdges(source, i);
                    } else {
                        this.clearMarkedEdges(source);
                    }
                }
                this.isMarked[outgoingEdgeIndexes[i]] = value;
                this.addPairToQueue(source, i);
            }
        }

        let currIndex = 0;
        while (currIndex < this._queueSize) {
            const parent = this._queue[currIndex];
            ++currIndex;
            const parentOutgoingIndex = this._queue[currIndex];
            ++currIndex;

            const curr = this.outgoingEdges[parent][parentOutgoingIndex];
            const currLevel = this.edgeLevels[this.outgoingEdgeIndexes[parent][parentOutgoingIndex]];

            const parX = this.xPositions[parent];
            const parY = this.yPositions[parent];
            const currX = this.xPositions[curr];
            const currY = this.yPositions[curr];
            const nOutgoingEdges = this.nOutgoingEdges[curr];
            const outgoingEdges = this.outgoingEdges[curr];
            const outgoingEdgeIndexes = this.outgoingEdgeIndexes[curr];
            const outgoingEdgeIsMarked = this._outgoingEdgeIsMarked[curr];
            const outgoingEdgeOppositeIndexes = this.outgoingEdgeOppositeIndexes[curr];

            for (let i = 0; i < nOutgoingEdges; ++i) {
                const nextLevel = this.edgeLevels[outgoingEdgeIndexes[i]];
                if ((outgoingEdgeIsMarked[i] === value) || (nextLevel !== Knight.PATH.ENLSVG.LEVEL_W && nextLevel <= currLevel)) continue;
                const next = outgoingEdges[i];
                if (!this._graph.isTaut(parX, parY, currX, currY, this.xPositions[next], this.yPositions[next])) continue;
                outgoingEdgeIsMarked[i] = value;

                const edgeIndex = outgoingEdgeIndexes[i];
                if (this.isMarked[edgeIndex] !== value) {
                    this.isMarked[edgeIndex] = value;
                    if (value) {
                        this.addToMarkedEdges(curr, i);
                        this.addToMarkedEdges(next, outgoingEdgeOppositeIndexes[i]);
                    } else {
                        this.clearMarkedEdges(curr);
                        this.clearMarkedEdges(next);
                    }
                }
                this.isMarked[outgoingEdgeIndexes[i]] = value;

                // Only continue marking forward if not a skip vertex.
                if (nextLevel !== Knight.PATH.ENLSVG.LEVEL_W || this.nSkipEdges[next] === 0) {
                    this.addPairToQueue(curr, i);
                }
            }
        }
    }

    /**
     * @param {Number} current
     * @param {Number} outgoingIndex
     * @memberof ENLSVG
     */
    addToMarkedEdges(current, outgoingIndex) {
        this.outgoingMarkedEdgeIndexes[current][this.nMarkedEdges[current]] = outgoingIndex;
        ++this.nMarkedEdges[current];
    }

    /**
     * @param {Number} current
     * @memberof ENLSVG
     */
    clearMarkedEdges(current) {
        this.nMarkedEdges[current] = 0;
    }

    /**
     * @param {Number} parent
     * @param {Number} parentOutgoingIndex
     * @memberof ENLSVG
     */
    addPairToQueue(parent, parentOutgoingIndex) {
        if (this._queueSize+1 >= this._queue.length) {
            this._queue = Knight.arraysCopyOf(this._queue, this._queue.length*2);
        }
        this._queue[this._queueSize] = parent;
        ++this._queueSize;
        this._queue[this._queueSize] = parentOutgoingIndex;
        ++this._queueSize;
    }

    /// \\\ /// \\\ /// \\\ /// \\\ ///
    ///   REPURPOSING PHASE - END   ///
    /// \\\ /// \\\ /// \\\ /// \\\ ///

    /**
     * @return {Number}
     * @memberof ENLSVG
     */
    size() {
        return this._nNodes;
    }

    /**
     * @return {Number}
     * @memberof ENLSVG
     */
    maxSize() {
        return this._maxSize;
    }

    /**
     * @return {Number}
     * @memberof ENLSVG
     */
    startNode() {
        return this._startIndex;
    }

    /**
     * @return {Number}
     * @memberof ENLSVG
     */
    endNode() {
        return this._endIndex;
    }

    /**
     * @return {Number}
     * @memberof ENLSVG
     */
    computeNumSkipEdges() {
        let sumdegrees = 0;
        for (let i = 0; i < this._nNodes; ++i) {
            sumdegrees += this.nSkipEdges[i];
        }
        return sumdegrees / 2;
    }

    /**
     * @memberof ENLSVG
     */
    maybeSaveSnapshot() {
        if (this._saveSnapshot !== null) this._saveSnapshot.run();
    }

    /**
     * @memberof ENLSVG
     */
    static clearMemory() {
        this._storedVisibilityGraph = null;
        this._storedGridGraph = null;
        global.gc();
    }

    /**
     * @param {Number} index
     * @return {String}
     * @memberof ENLSVG
     */
    vertexToStr(index) {
        return `${this.xPositions[index]} , ${this.yPositions[index]}`;
    }
};

Knight.PATH.ENLSVG.LEVEL_W = Number.MAX_VALUE;
Knight.PATH.ENLSVG._storedVisibilityGraph = null;
Knight.PATH.ENLSVG._storedGridGraph = null;
Knight.PATH.ENLSVG._storedLevelLimit = null;
