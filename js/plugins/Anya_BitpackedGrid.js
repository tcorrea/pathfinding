// =============================================================================
// Anya_BitpackedGrid.js
// Version:     1.0
// Date:        2/2/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var

/**
 * A simple uniform-cost lattice / cell grid.
 * This implementation uses a bitpacked representation
 * in order to improve time and space efficiency.
 *
 * @class FibonacciHeapNode
 */
function BitpackedGrid() {
    this.initialize.apply(this, arguments); /* eslint-disable-line */
};

// =============================================================================
// Constants
// =============================================================================

// a very small number; smaller than the smallest step size
// provided no individual grid dimension is larger than epsilon^(-1)
BitpackedGrid.epsilon = 0.0000001;

// we frame the map with some extra obstacles
// this helps us avoid unnecessary boundary testing
BitpackedGrid.padding_ = 3;

// we use single int words when packing the grid;
// each vertex is assigned one bit per word
BitpackedGrid.BITS_PER_WORD = 32;
BitpackedGrid.LOG2_BITS_PER_WORD = 5;
BitpackedGrid.INDEX_MASK = BitpackedGrid.BITS_PER_WORD - 1;

// =============================================================================
// BitpackedGrid
// =============================================================================

/**
 * Creates an instance of BitpackedGrid.
 * @param {Number} width
 * @param {Number} height
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.initialize = function(width, height) {
    // the original dimensions of the gridmap
    this.map_height_original_ = 0;
    this.map_width_original_ = 0;

    // the padded dimensions of the gridmap
    // we round each row to the nearest number of words (+1)
    // and we add one row of padding before the first row
    // one row of padding after the last row
    this.map_height_ = 0;
    this.map_width_ = 0;
    this.map_size_ = 0;
    this.map_width_in_words_ = 0; // for convenience

    // data describing the cell grid
    this.map_cells_ = null;

    // data relating to the discrete set of lattice points that
    // together form the cell grid; the data is redundant but
    // we keep it anyway for better performance.
    this.visible_ = null;
    this.corner_ = null;
    this.double_corner_ = null;

    // there are a finite number of places where an edge can
    // be intersected. This variable stores the smallest
    // distance between any two (adjacent) such points.
    this.smallest_step = 0;
    this.smallest_step_div2 = 0;

    // 1D array to keep track of how many agents are blocking a given cell.
    // They can overlap due to grid dilation and multiple nearby
    // colliders mapping to the same pathfinding cell.
    // Need to store integers, so no bitpacking here.
    this.block_counter_ = null;

    this.init(width, height);
};

/**
 * Convert a GridGraph into a BitpackedGrid. Note that this has to access every
 * cell on the grid, so it can be quite slow for large grids.
 *
 * In mose cases, it's more efficient to create and populate a BitpackedGrid
 * from scratch.
 *
 * @static
 * @param {GridGraph} gridGraph
 * @return {BitpackedGrid}
 */
BitpackedGrid.fromGridGraph = function(gridGraph) {
    const height = gridGraph.sizeY;
    const width = gridGraph.sizeX;
    const bitGrid = new BitpackedGrid(width, height);
    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            bitGrid.set_cell_is_traversable(x, y, !gridGraph.isBlocked(x, y));
        }
    }
    return bitGrid;
};

/**
 * @return {Number}
 */
BitpackedGrid.prototype.width = function() {
    return this.map_width_original_;
};

/**
 * @return {Number}
 */
BitpackedGrid.prototype.height = function() {
    return this.map_height_original_;
};

/**
 * @param {Number} width
 * @param {Number} height
 */
BitpackedGrid.prototype.init = function(width, height) {
    const padded_width = width + BitpackedGrid.padding_ * 2;
    const padded_height = height + BitpackedGrid.padding_ * 2;
    this.map_height_original_ = height;
    this.map_width_original_ = width;
    this.map_width_in_words_ = ((width >> BitpackedGrid.LOG2_BITS_PER_WORD)+1);
    this.map_width_ = this.map_width_in_words_ << BitpackedGrid.LOG2_BITS_PER_WORD;
    this.map_height_ = padded_height;

    this.map_size_ = ((this.map_height_ * this.map_width_) >> BitpackedGrid.LOG2_BITS_PER_WORD);
    this.map_cells_ = new Array(this.map_size_).fill(0);
    this.visible_ = new Array(this.map_size_).fill(0);
    this.corner_ = new Array(this.map_size_).fill(0);
    this.double_corner_ = new Array(this.map_size_).fill(0);

    // TODO: Pack this in fewer bits per block counter index, we don't need this much space
    //       The counter value should never go above 7 or 8, so packing in 4-bit chunks should be safe
    this.block_counter_ = new Array(padded_width * padded_height).fill(0);

    this.smallest_step =
            Math.min(
                1.0 / this.get_padded_width(),
                1.0 / this.get_padded_height());
    this.smallest_step_div2 = this.smallest_step / 2.0;

    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            this.set_bit_value(x, y, true, this.map_cells_);
            this.set_point_is_visible(x, y, true);
        }
    }
};

/**
 * Increase the number of entities occupying a point on the grid.
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}    True when this is the first entity occupying that tile.
 */
BitpackedGrid.prototype.incr_block_counter = function(x, y) {
    x += BitpackedGrid.padding_;
    y += BitpackedGrid.padding_;
    return (++this.block_counter_[this.width()*y + x]) === 1;
};

/**
 * Decrease the number of entities occupying a point on the grid.
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}    True when the tile is now unoccupied.
 */
BitpackedGrid.prototype.decr_block_counter = function(x, y) {
    x += BitpackedGrid.padding_;
    y += BitpackedGrid.padding_;
    return (--this.block_counter_[this.width()*y + x]) === 0;
};

/**
 * Update the number of entities occupying a point on the grid.
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value   True if cell is traversable, false if it's blocked.
 * @return {Boolean}
 */
BitpackedGrid.prototype.update_block_counter = function(x, y, value) {
    return value ? this.decr_block_counter(x, y) : this.incr_block_counter(x, y);
};

/**
 * returns true if the point (x, y) is visible from
 * another discrete point on the grid (i.e. (x, y) is
 * not adjacent to 4 obstacle tiles).
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_point_is_visible = function(x, y) {
    return this.get_bit_value(x, y, this.visible_);
};

/**
 * returns true if the point (x, y) is adjacent to
 * (i) exactly one obstacle tile OR (ii) exactly two
 * diagonally adjacent obstacle tiles
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_point_is_corner = function(x, y) {
    return this.get_bit_value(x, y, this.corner_);
};

/**
 * returns true if the point (x, y) is adjacent to
 * exactly two diagonally adjacent obstacle tiles.
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_point_is_double_corner = function(x, y) {
    return this.get_bit_value(x, y, this.double_corner_);
};

/**
 * returns true if the cell (x, y) is not an obstacle
 * @param {Number} cx
 * @param {Number} cy
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_cell_is_traversable = function(cx, cy) {
    return this.get_bit_value(cx, cy, this.map_cells_);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 */
BitpackedGrid.prototype.set_point_is_visible = function(x, y, value) {
    this.set_bit_value(x, y, value, this.visible_);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 */
BitpackedGrid.prototype.set_point_is_corner = function(x, y, value) {
    this.set_bit_value(x, y, value, this.corner_);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 */
BitpackedGrid.prototype.set_point_is_double_corner = function(x, y, value) {
    this.set_bit_value(x, y, value, this.double_corner_);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_point_is_discrete = function(x, y) {
    return Math.abs(Math.trunc(x + this.smallest_step_div2) - x) < this.smallest_step;
};

/**
 * @param {Number} cx
 * @param {Number} cy
 * @param {Boolean} value
 */
BitpackedGrid.prototype.set_cell_is_traversable = function(cx, cy, value) {
    if (this.update_block_counter(cx, cy, value)) {
        // Only perform bit operations if the cell's "blocked" status actually changed
        this.set_bit_value(cx, cy, value, this.map_cells_);
        this.update_point(cx, cy);
        this.update_point(cx+1, cy);
        this.update_point(cx, cy+1);
        this.update_point(cx+1, cy+1);
    }
};

/**
 * @param {Number} px
 * @param {Number} py
 */
BitpackedGrid.prototype.update_point = function(px, py) {
    const cellNW = this.get_cell_is_traversable(px-1, py-1);
    const cellNE = this.get_cell_is_traversable(px, py-1);
    const cellSW = this.get_cell_is_traversable(px-1, py);
    const cellSE = this.get_cell_is_traversable(px, py);

    const corner =
            ((!cellNW | !cellSE) & cellSW & cellNE) |
            ((!cellNE | !cellSW) & cellNW & cellSE);

    const double_corner =
            ((!cellNW & !cellSE) & cellSW & cellNE) ^
            ((!cellSW & !cellNE) & cellNW & cellSE);

    const visible =
            cellNW | cellNE | cellSW | cellSE;

    this.set_point_is_corner(px, py, corner);
    this.set_point_is_double_corner(px, py, double_corner);
    this.set_point_is_visible(px, py, visible);
};

/**
 * TODO: pass int instead of bool (removes one branch instruction)
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 * @param {Array<Number>} elts
 */
BitpackedGrid.prototype.set_bit_value = function(x, y, value, elts) {
    const map_id = this.get_map_id(x, y);
    const word_index = map_id >> BitpackedGrid.LOG2_BITS_PER_WORD;
    const mask = (1 << ((map_id & BitpackedGrid.INDEX_MASK)));
    const tmp = elts[word_index];
    elts[word_index] = value ? (tmp | mask) : (tmp & ~mask);
};

/**
 * TODO: pass int instead of bool (removes one branch instruction)
 * @param {Number} x
 * @param {Number} y
 * @param {Array<Number>} elts
 * @return {Boolean}
 */
BitpackedGrid.prototype.get_bit_value = function(x, y, elts) {
    const map_id = this.get_map_id(x, y);
    const word_index = map_id >> BitpackedGrid.LOG2_BITS_PER_WORD;
    const mask = 1 << ((map_id & BitpackedGrid.INDEX_MASK));
    return (elts[word_index] & mask) != 0;
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Number}
 */
BitpackedGrid.prototype.get_map_id = function(x, y) {
    x += BitpackedGrid.padding_;
    y += BitpackedGrid.padding_;
    return (y * this.map_width_ + x);
};

/**
 * @param {Number} fromx
 * @param {Number} fromy
 * @param {AnyaVertex.VertexDirections} d
 * @return {Boolean}
 */
BitpackedGrid.prototype.can_step_from_point = function(fromx, fromy, d) {
    // We test whether (fromx, fromy) is a discrete point.
    // Note that we only consider the x-dimension as when
    // traveling along the y-axis we always move from row to row
    const discrete_x = ((fromx + this.smallest_step_div2) - Math.trunc(fromx)) < this.smallest_step;

    // computing traversability differs slightly depending on whether
    // (x, y) is discrete or not. If (x, y) is discrete, we need to look
    // at two adjacent cells and ensure at least one is not an obstacle.
    // On the other hand, if (x, y) is not discrete, we might only need
    // to look a single cell (e.g. moving up from point (x.z, y) depends
    // only on cell (x, y)
    let retval = false;
    let cx;
    let cy;
    switch (d) {
    case VD_LEFT:
        cx = Math.trunc((discrete_x ? fromx - 1 : fromx));
        cy = Math.trunc(fromy);
        retval =
                (this.get_cell_is_traversable(cx, cy) |
                this.get_cell_is_traversable(cx, cy-1));
        break;

    case VD_RIGHT:
        cx = Math.trunc(fromx);
        cy = Math.trunc(fromy);
        retval =
                (this.get_cell_is_traversable(cx, cy) |
                this.get_cell_is_traversable(cx, cy-1));
        break;

    case VD_UP:
        cx = Math.trunc(fromx);
        cy = Math.trunc(fromy-1);
        retval =
                this.get_cell_is_traversable(cx, cy) |
                (discrete_x && this.get_cell_is_traversable(cx-1, cy));
        break;

    case VD_DOWN:
        cx = Math.trunc(fromx);
        cy = Math.trunc(fromy);
        retval =
                this.get_cell_is_traversable(cx, cy) |
                (discrete_x && this.get_cell_is_traversable(cx-1, cy));
        break;
    }
    return retval;
};

/**
 * scan the cells of the grid, starting at (@param x, @param y)
 * and heading in the negative x direction.
 *
 * @param {Number} x
 * @param {Number} y
 * @return {Number} the x-coordinate of the first obstacle node reached by
 * traveling right from (x, y). if @param x is an obstacle, the return
 * value is equal to @param x
 */
BitpackedGrid.prototype.scan_cells_right = function(x, y) {
    const tile_id = this.get_map_id(x, y);
    let t_index = tile_id >> BitpackedGrid.LOG2_BITS_PER_WORD;

    // ignore obstacles in bit positions < (i.e. to the left of) the starting cell
    // (NB: big endian order means the leftmost cell is in the lowest bit)
    let obstacles = ~this.map_cells_[t_index];
    const start_bit_index = tile_id & BitpackedGrid.INDEX_MASK;
    const mask = ~((1 << start_bit_index)-1);
    obstacles &= mask;

    let stop_pos;
    const start_index = t_index;
    while (true) {
        if (obstacles != 0) {
            stop_pos = Knight.Utility.numberOfTrailingZeros(obstacles);
            break;
        }
        t_index++;
        obstacles = ~this.map_cells_[t_index];
    }

    let retval = ((t_index - start_index) * BitpackedGrid.BITS_PER_WORD);
    retval += (stop_pos - start_bit_index);
    return x + retval;
};

/**
 * scan the cells of the grid, starting at (@param x, @param y)
 * and heading in the negative x direction.
 *
 * @param {Number} x
 * @param {Number} y
 * @return {Number} the x-coordinate of the first obstacle node reached by
 * traveling left from (x, y). if @param x is an obstacle, the return
 * value is equal to @param x
 */
BitpackedGrid.prototype.scan_cells_left = function(x, y) {
    const tile_id = this.get_map_id(x, y);
    let t_index = tile_id >> BitpackedGrid.LOG2_BITS_PER_WORD;

    // scan adjacent cells from the current row and the row above
    let obstacles = this.map_cells_[t_index];
    obstacles = ~obstacles;

    // ignore cells in bit positions > (i.e. to the right of) the starting cell
    // (NB: big endian order means the rightmost cell is in the highest bit)
    const start_bit_index = tile_id & BitpackedGrid.INDEX_MASK;
    const opposite_index = (BitpackedGrid.BITS_PER_WORD - (start_bit_index+1));
    let mask = (1 << start_bit_index);
    mask = (mask | (mask-1));
    obstacles &= mask;

    let stop_pos;
    const start_index = t_index;
    while (true) {
        if (obstacles != 0) {
            stop_pos = Math.clz32(obstacles);
            break;
        }
        t_index--;
        obstacles = ~this.map_cells_[t_index];
    }

    let retval = ((start_index - t_index) * BitpackedGrid.BITS_PER_WORD);
    retval += (stop_pos - opposite_index);
    return x - retval;
};

/**
 * scan right along the lattice from location (@param x, @param row).
 *
 * @param {Number} x
 * @param {Number} row
 * @return {Number} the next discrete point that is corner or which is
 * the last traversable point before an obstacle.
 * If no such point exists, the return value is equal to x.
 */
BitpackedGrid.prototype.scan_right = function(x, row) {
    const left_of_x = Math.trunc(x + this.smallest_step_div2);
    const tile_id = this.get_map_id(left_of_x, row);
    let t_index = tile_id >> BitpackedGrid.LOG2_BITS_PER_WORD;
    let ta_index = t_index - this.map_width_in_words_;

    // scan adjacent cells from the current row and the row above
    const cells = this.map_cells_[t_index];
    const cells_above = this.map_cells_[ta_index];

    let obstacles = ~cells & ~cells_above;
    let corners = this.corner_[t_index];

    // ignore corners in bit positions <= (i.e. to the left of) the starting cell
    // (NB: big endian order means the leftmost cell is in the lowest bit)
    const start_bit_index = tile_id & BitpackedGrid.INDEX_MASK;
    const mask = (1 << start_bit_index);
    corners &= ~(mask | (mask-1));

    // ignore obstacles in bit positions < (i.e. strictly left of) the starting cell
    // Because we scan cells (and not corners) we cannot ignore the current location.
    // To do so might result in intervals that pass through obstacles
    // (e.g. current location is a double corner)
    obstacles &= ~(mask-1);

    let stop_pos;
    const start_index = t_index;
    while (true) {
        const value = corners | obstacles;
        if (value != 0) {
            // Each point (x, y) is associated with the top-left
            // corner of tile (x, y). When traveling right (cf. left)
            // we need to stop exactly at the position of the first
            // (corner or obstacle) bit.
            stop_pos = Knight.Utility.numberOfTrailingZeros(value);
            break;
        }
        t_index++;
        ta_index++;
        corners = this.corner_[t_index];
        obstacles = ~this.map_cells_[t_index] & ~this.map_cells_[ta_index];
    }

    let retval = left_of_x + ((t_index - start_index) * BitpackedGrid.BITS_PER_WORD + stop_pos);
    retval -= start_bit_index;
    return retval;
};

/**
 * scan left along the lattice from location (@param x, @param row).
 *
 * @param {Number} x
 * @param {Number} row
 * @return {Number} the next discrete point that is corner or which is the last
 * traversable point before an obstacle.
 * If no such point exists, the return value is equal to @param x.
 */
BitpackedGrid.prototype.scan_left = function(x, row) {
    // early return if the next discrete point
    // left of x is a corner
    const left_of_x = Math.trunc(x);
    if ((x - left_of_x) >= this.smallest_step && this.get_point_is_corner(left_of_x, row)) {
        return left_of_x;
    }

    const tile_id = this.get_map_id(left_of_x, row);
    let t_index = tile_id >> BitpackedGrid.LOG2_BITS_PER_WORD;
    let ta_index = t_index - this.map_width_in_words_;

    // scan adjacent cells from the current row and the row above
    const cells = this.map_cells_[t_index];
    const cells_above = this.map_cells_[ta_index];

    let obstacles = ~cells & ~cells_above;
    let corners = this.corner_[t_index];

    // ignore cells in bit positions >= (i.e. to the right of) the starting cell
    // (NB: big endian order means the rightmost cell is in the highest bit)
    // Because we scan cells (and not just corners) we can safely ignore
    // the current position. The traversability of its associated cell has
    // no impact on deciding whether we can travel left, away from the cell.
    const start_bit_index = tile_id & BitpackedGrid.INDEX_MASK;
    const mask = (1 << start_bit_index) - 1;
    corners &= mask;
    obstacles &= mask;

    let stop_pos;
    const start_index = t_index;
    while (true) {
        const value = corners | obstacles;
        if (value != 0) {
            // Each point (x, y) is associated with the top-left
            // corner of tile (x, y). When counting zeroes to figure
            // out how far we can travel we end up stopping one
            // position before the first set bit. This approach prevents
            // us from traveling through an obstacle (we stop right before)
            // but in in the case of corner tiles, we need to stop exactly
            // at the position of the set bit. Hence, +1 below.
            stop_pos = Math.min(
                Math.clz32(corners)+1,
                Math.clz32(obstacles));
            break;
        }
        t_index--;
        ta_index--;
        corners = this.corner_[t_index];
        obstacles = ~this.map_cells_[t_index] & ~this.map_cells_[ta_index];
    }

    let retval = left_of_x - ((start_index - t_index) * BitpackedGrid.BITS_PER_WORD + stop_pos);
    retval += ((BitpackedGrid.BITS_PER_WORD) - start_bit_index);
    return retval;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_binary_cells = function() {
    let str = '';
    for (let i = 0; i < this.map_size_; i++) {
        str += this.map_cells_[i].toString(2) + " ";
    }
    return str;
};

/**
 * @param {Number} myx
 * @param {Number} myy
 * @return {String}
 */
BitpackedGrid.prototype.debug_cells = function(myx, myy) {
    let str = '';
    for (let y = 0; y < this.height(); ++y) {
        for (let x = 0; x < this.width(); ++x) {
            if (myx === x && myy === y) {
                str += "X";
            } else {
                str += this.get_cell_is_traversable(x, y) ? "." : "@";
            }
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_cells = function() {
    let str = '';
    for (let y = 0; y < this.height(); ++y) {
        for (let x = 0; x < this.width(); ++x) {
            str += this.get_cell_is_traversable(x, y) ? "." : "@";
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_visibility = function() {
    let str = '';
    for (let y = 0; y < this.height()+1; ++y) {
        for (let x = 0; x < this.width()+1; ++x) {
            str += this.get_point_is_visible(x, y) ? "." : "@";
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_corners = function() {
    let str = '';
    for (let y = 0; y < this.height()+1; ++y) {
        for (let x = 0; x < this.width()+1; ++x) {
            str += this.get_point_is_corner(x, y) ? "." : "@";
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_double_corners = function() {
    let str = '';
    for (let y = 0; y < this.height()+1; ++y) {
        for (let x = 0; x < this.width()+1; ++x) {
            str += this.get_point_is_double_corner(x, y) ? "." : "@";
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_block_counter = function() {
    let str = '';
    const padding = BitpackedGrid.padding_;
    for (let y = padding; y < (this.height() + padding); ++y) {
        for (let x = padding; x < (this.width() + padding); ++x) {
            str += `${this.block_counter_[this.width()*y + x].toString()} `;
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {String}
 */
BitpackedGrid.prototype.print_block_counter_with_padding = function() {
    let str = '';
    const padding = BitpackedGrid.padding_;
    const height = this.height() + padding * 2;
    const width = this.width() + padding * 2;
    for (let y = 0; y < height; ++y) {
        for (let x = 0; x < width; ++x) {
            if (y < padding || x < padding || y >= this.height() + padding || x >= this.width() + padding) {
                str +=`X `;
            } else {
                str += `${this.block_counter_[this.width()*y + x].toString()} `;
            }
        }
        str += "\n";
    }
    return str;
};

/**
 * @return {Number}
 */
BitpackedGrid.prototype.get_padded_width = function() {
    return this.map_width_;
};

/**
 * @return {Number}
 */
BitpackedGrid.prototype.get_padded_height = function() {
    return this.map_height_;
};

/**
 * @return {Number}
 */
BitpackedGrid.prototype.get_num_cells = function() {
    return this.map_height_ * this.map_width_;
};

/**
 * print a portion of the grid cells around location (x, y)
 * @param {Number} x
 * @param {Number} y
 * @param {Number} offset   specifies how many cells around (x, y) to print
 * i.e. an offset of 10 prints 21x21 cells with (x,y) in the middle
 * (10 cells above (x, y), 10 below, 10 left and 10 right)
 */
BitpackedGrid.prototype.print_cell_range = function(x, y, offset) {
    for (let j = y-offset; j < y+offset; ++j) {
        console.log(`j `);
        for (let i = x-offset; i < x+offset; ++i) {
            console.log(this.get_cell_is_traversable(i, j) ? "." : "@");
        }
        console.log("\n");
    }
};

/**
 * Performs a dilation operation on the grid, based on an input kernel.
 * The kernel should be a 2D array of booleans. A 3x3 kernel could look like so:
 *
 *   o o .
 *   o o .
 *   . . .
 *
 * Normally, the origin should be the center of the kernel, but it can be anywhere.
 * This kernel would cause every blocked tile to also block every tile above it,
 * to its left and in it's up-left diagonal.
 *
 * This can be used to modify the grid to represent passability for characters that
 * occupy more than a single tile.
 *
 * For more information, see: http://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
 *
 * @param {Set<Object>} blockedTiles
 * @param {Array<Boolean>} kernel
 * @param {Number} kernelSizeX
 * @param {Number} kernelSizeY
 * @param {Number} kernelOriginX
 * @param {Number} kernelOriginY
 * @memberof GridGraph
 */
BitpackedGrid.prototype.dilate = function(blockedTiles, kernel, kernelSizeX, kernelSizeY, kernelOriginX, kernelOriginY) {
    // For each blocked tile, render the kernel to the grid in that tile's position
    for (let y = 0; y < kernelSizeY; ++y) {
        for (let x = 0; x < kernelSizeX; ++x) {
            if (kernel[x][y]) {
                const offsetX = x - kernelOriginX;
                const offsetY = y - kernelOriginY;
                for (const tile of blockedTiles.values()) {
                    const tx = tile.x + offsetX;
                    const ty = tile.y + offsetY;
                    if (this.get_cell_is_traversable(tx, ty)) {
                        this.set_cell_is_traversable(tx, ty, false);
                    };
                }
            }
        }
    }
};

// =============================================================================
// Extra functions & properties for parity with the GridGraph interface
// Because this grid uses padding, most of the boundary testing has been
// removed to improve performance.
// =============================================================================
Object.defineProperty(BitpackedGrid.prototype, 'sizeX', {
    get: function() {
        return this.width();
    },
    configurable: true,
});

Object.defineProperty(BitpackedGrid.prototype, 'sizeY', {
    get: function() {
        return this.height();
    },
    configurable: true,
});

Object.defineProperty(BitpackedGrid.prototype, 'sizeXplusOne', {
    get: function() {
        return this.width() + 1;
    },
    configurable: true,
});

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.setBlocked = function(x, y, value) {
    this.set_cell_is_traversable(x, y, !value);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} value
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.trySetBlocked = function(x, y, value) {
    this.set_cell_is_traversable(x, y, !value);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isBlocked = function(x, y) {
    return !this.get_cell_is_traversable(x, y);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isBlockedRaw = function(x, y) {
    return !this.get_cell_is_traversable(x, y);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isValidCoordinate = function(x, y) {
    return (x <= this.width() &&
            y <= this.height() &&
            x >= 0 &&
            y >= 0);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isValidBlock = function(x, y) {
    return (x < this.width() &&
            y < this.height() &&
            x >= 0 &&
            y >= 0);
};

/**
 * Returns wether a cell is a valid starting or ending point for a path.
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 */
BitpackedGrid.prototype.isValidEndpoint = function(x, y) {
    return this.get_cell_is_traversable(x, y) ||
        this.get_cell_is_traversable(x-1, y) ||
        this.get_cell_is_traversable(x, y-1) ||
        this.get_cell_is_traversable(x-1, y-1);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isOuterCorner = function(x, y) {
    const a = this.isBlocked(x-1, y-1);
    const b = this.isBlocked(x, y-1);
    const c = this.isBlocked(x, y);
    const d = this.isBlocked(x-1, y);

    return ((!a && !c) || (!d && !b)) && (a || b || c || d);
};

/**
 * @param {Number} index
 * @return {Number}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.toTwoDimX = function(index) {
    return index % this.sizeXplusOne;
};

/**
 * @param {Number} index
 * @return {Number}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.toTwoDimY = function(index) {
    return index / this.sizeXplusOne;
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isUnblockedCoordinate = function(x, y) {
    return !this.topRightOfBlockedTile(x, y) ||
            !this.topLeftOfBlockedTile(x, y) ||
            !this.bottomRightOfBlockedTile(x, y) ||
            !this.bottomLeftOfBlockedTile(x, y);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.topRightOfBlockedTile = function(x, y) {
    return this.isBlocked(x-1, y-1);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.topLeftOfBlockedTile = function(x, y) {
    return this.isBlocked(x, y-1);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.bottomRightOfBlockedTile = function(x, y) {
    return this.isBlocked(x-1, y);
};

/**
 * @param {Number} x
 * @param {Number} y
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.bottomLeftOfBlockedTile = function(x, y) {
    return this.isBlocked(x, y);
};

/**
 * x1,y1,x2,y2 refer to the top left corner of the tile.
 * @param {Number} x1 Condition: x1 between 0 and sizeX inclusive.
 * @param {Number} y1 Condition: y1 between 0 and sizeY inclusive.
 * @param {Number} x2 Condition: x2 between 0 and sizeX inclusive.
 * @param {Number} y2 Condition: y2 between 0 and sizeY inclusive.
 * @return {Number} distance
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.distance = function(x1, y1, x2, y2) {
    const xDiff = x2 - x1;
    const yDiff = y2 - y1;

    if (yDiff === 0) {
        return Math.abs(xDiff);
    }
    if (xDiff === 0) {
        return Math.abs(yDiff);
    }
    if (xDiff === yDiff || xDiff === -yDiff) {
        return Knight.PATH.GridGraph.SQRT_TWO * Math.abs(xDiff);
    }

    const squareDistance = xDiff*xDiff + yDiff*yDiff;
    return Math.sqrt(squareDistance);
};

/**
 * Octile distance:
 *   min(dx,dy)*sqrt(2) + (max(dx,dy)-min(dx,dy))
 * = min(dx,dy)*(sqrt(2)-1) + max(dx,dy)
 *
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @return {Number}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.octileDistance = function(x1, y1, x2, y2) {
    let dx = x1 - x2;
    let dy = y1 - y2;
    if (dx < 0) dx = -dx;
    if (dy < 0) dy = -dy;

    let min = dx;
    let max = dy;
    if (dy < dx) {
        min = dy;
        max = dx;
    }

    return min * Knight.PATH.GridGraph.SQRT_TWO_MINUS_ONE + max;
};

/**
 * Same as lineOfSight, but only works with a vertex and its 8 immediate neighbours.
 * Also (x1,y1) != (x2,y2)
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.neighbourLineOfSight = function(x1, y1, x2, y2) {
    if (x1 === x2) {
        if (y1 > y2) {
            return !this.isBlocked(x1, y2) || !this.isBlocked(x1-1, y2);
        } else { // y1 < y2
            return !this.isBlocked(x1, y1) || !this.isBlocked(x1-1, y1);
        }
    } else if (x1 < x2) {
        if (y1 === y2) {
            return !this.isBlocked(x1, y1) || !this.isBlocked(x1, y1-1);
        } else if (y1 < y2) {
            return !this.isBlocked(x1, y1);
        } else { // y2 < y1
            return !this.isBlocked(x1, y2);
        }
    } else { // x2 < x1
        if (y1 === y2) {
            return !this.isBlocked(x2, y1) || !this.isBlocked(x2, y1-1);
        } else if (y1 < y2) {
            return !this.isBlocked(x2, y1);
        } else { // y2 < y1
            return !this.isBlocked(x2, y2);
        }
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @return {Boolean} true iff there is line-of-sight from (x1,y1) to (x2,y2).
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.lineOfSight = function(x1, y1, x2, y2) {
    let dy = y2 - y1;
    let dx = x2 - x1;

    let f = 0;

    let signY = 1;
    let signX = 1;
    let offsetX = 0;
    let offsetY = 0;

    if (dy < 0) {
        dy *= -1;
        signY = -1;
        offsetY = -1;
    }
    if (dx < 0) {
        dx *= -1;
        signX = -1;
        offsetX = -1;
    }

    if (dx >= dy) {
        while (x1 !== x2) {
            f += dy;
            if (f >= dx) {
                if (this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                y1 += signY;
                f -= dx;
            }
            if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
            if (dy === 0 && this.isBlocked(x1 + offsetX, y1) && this.isBlocked(x1 + offsetX, y1 - 1)) return false;

            x1 += signX;
        }
    } else {
        while (y1 !== y2) {
            f += dx;
            if (f >= dy) {
                if (this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                x1 += signX;
                f -= dy;
            }
            if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
            if (dx === 0 && this.isBlocked(x1, y1 + offsetY) && this.isBlocked(x1 - 1, y1 + offsetY)) return false;

            y1 += signY;
        }
    }
    return true;
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} dx
 * @param {Number} dy
 * @return {PIXI.Point}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.findFirstBlockedTile = function(x1, y1, dx, dy) {
    let f = 0;

    let signY = 1;
    let signX = 1;
    let offsetX = 0;
    let offsetY = 0;

    if (dy < 0) {
        dy *= -1;
        signY = -1;
        offsetY = -1;
    }
    if (dx < 0) {
        dx *= -1;
        signX = -1;
        offsetX = -1;
    }

    if (dx >= dy) {
        while (true) {
            f += dy;
            if (f >= dx) {
                if (this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                    return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                }
                y1 += signY;
                f -= dx;
            }
            if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                return new PIXI.Point(x1 + offsetX, y1 + offsetY);
            }
            if (dy === 0 && this.isBlocked(x1 + offsetX, y1) && this.isBlocked(x1 + offsetX, y1 - 1)) {
                return new PIXI.Point(x1 + offsetX, -1);
            }

            x1 += signX;
        }
    } else {
        while (true) {
            f += dx;
            if (f >= dy) {
                if (this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                    return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                }
                x1 += signX;
                f -= dy;
            }
            if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                return new PIXI.Point(x1 + offsetX, y1 + offsetY);
            }
            if (dx === 0 && this.isBlocked(x1, y1 + offsetY) && this.isBlocked(x1 - 1, y1 + offsetY)) {
                return new PIXI.Point(-1, y1 + offsetY);
            }

            y1 += signY;
        }
    }
};

/**
 * Checks whether the path (x1,y1),(x2,y2),(x3,y3) is taut.
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTaut = function(x1, y1, x2, y2, x3, y3) {
    if (x1 < x2) {
        if (y1 < y2) {
            return this.isTautFromBottomLeft(x1, y1, x2, y2, x3, y3);
        } else if (y2 < y1) {
            return this.isTautFromTopLeft(x1, y1, x2, y2, x3, y3);
        } else { // y1 === y2
            return this.isTautFromLeft(x1, y1, x2, y2, x3, y3);
        }
    } else if (x2 < x1) {
        if (y1 < y2) {
            return this.isTautFromBottomRight(x1, y1, x2, y2, x3, y3);
        } else if (y2 < y1) {
            return this.isTautFromTopRight(x1, y1, x2, y2, x3, y3);
        } else { // y1 === y2
            return this.isTautFromRight(x1, y1, x2, y2, x3, y3);
        }
    } else { // x2 === x1
        if (y1 < y2) {
            return this.isTautFromBottom(x1, y1, x2, y2, x3, y3);
        } else if (y2 < y1) {
            return this.isTautFromTop(x1, y1, x2, y2, x3, y3);
        } else { // y1 === y2
            throw new Error("v == u?");
        }
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromBottomLeft = function(x1, y1, x2, y2, x3, y3) {
    if (x3 < x2 || y3 < y2) return false;

    const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
    if (compareGradients < 0) { // m1 < m2
        return this.bottomRightOfBlockedTile(x2, y2);
    } else if (compareGradients > 0) { // m1 > m2
        return this.topLeftOfBlockedTile(x2, y2);
    } else { // m1 == m2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromTopLeft = function(x1, y1, x2, y2, x3, y3) {
    if (x3 < x2 || y3 > y2) return false;

    const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
    if (compareGradients < 0) { // m1 < m2
        return this.bottomLeftOfBlockedTile(x2, y2);
    } else if (compareGradients > 0) { // m1 > m2
        return this.topRightOfBlockedTile(x2, y2);
    } else { // m1 == m2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromBottomRight = function(x1, y1, x2, y2, x3, y3) {
    if (x3 > x2 || y3 < y2) return false;
    const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
    if (compareGradients < 0) { // m1 < m2
        return this.topRightOfBlockedTile(x2, y2);
    } else if (compareGradients > 0) { // m1 > m2
        return this.bottomLeftOfBlockedTile(x2, y2);
    } else { // m1 == m2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromTopRight = function(x1, y1, x2, y2, x3, y3) {
    if (x3 > x2 || y3 > y2) return false;

    const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
    if (compareGradients < 0) { // m1 < m2
        return this.topLeftOfBlockedTile(x2, y2);
    } else if (compareGradients > 0) { // m1 > m2
        return this.bottomRightOfBlockedTile(x2, y2);
    } else { // m1 == m2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromLeft = function(x1, y1, x2, y2, x3, y3) {
    if (x3 < x2) return false;

    const dy = y3 - y2;
    if (dy < 0) { // y3 < y2
        return this.topRightOfBlockedTile(x2, y2);
    } else if (dy > 0) { // y3 > y2
        return this.bottomRightOfBlockedTile(x2, y2);
    } else { // y3 == y2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromRight = function(x1, y1, x2, y2, x3, y3) {
    if (x3 > x2) return false;

    const dy = y3 - y2;
    if (dy < 0) { // y3 < y2
        return this.topLeftOfBlockedTile(x2, y2);
    } else if (dy > 0) { // y3 > y2
        return this.bottomLeftOfBlockedTile(x2, y2);
    } else { // y3 == y2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromBottom = function(x1, y1, x2, y2, x3, y3) {
    if (y3 < y2) return false;

    const dx = x3 - x2;
    if (dx < 0) { // x3 < x2
        return this.topRightOfBlockedTile(x2, y2);
    } else if (dx > 0) { // x3 > x2
        return this.topLeftOfBlockedTile(x2, y2);
    } else { // x3 == x2
        return true;
    }
};

/**
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} x3
 * @param {Number} y3
 * @return {Boolean}
 * @memberof BitpackedGrid
 */
BitpackedGrid.prototype.isTautFromTop = function(x1, y1, x2, y2, x3, y3) {
    if (y3 > y2) return false;

    const dx = x3 - x2;
    if (dx < 0) { // x3 < x2
        return this.bottomRightOfBlockedTile(x2, y2);
    } else if (dx > 0) { // x3 > x2
        return this.bottomLeftOfBlockedTile(x2, y2);
    } else { // x3 == x2
        return true;
    }
};
