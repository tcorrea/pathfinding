var Sinova = Sinova || {}; // eslint-disable-line no-var

/**
 * A point used to detect collisions
 * @class
 */
Sinova.Point = class extends Sinova.Polygon {
    /**
     * @constructor
     * @param {Number} [x = 0] The starting X coordinate
     * @param {Number} [y = 0] The starting Y coordinate
     * @param {Number} [padding = 0] The amount to pad the bounding volume when testing for potential collisions
     */
    constructor(x = 0, y = 0, padding = 0) {
        super(x, y, [[0, 0]], 0, 1, 1, padding);

        /** @private */
        this._point = true;
    }

    /**
     * @return {Object} The shape's bounding box
     */
    getBounds() {
        return {
            min_x: this.x,
            max_x: this.x,
            min_y: this.y,
            max_y: this.y,
        };
    };
};

Sinova.Point.prototype.setPoints = undefined;

