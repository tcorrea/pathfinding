// =============================================================================
// AnyaInterval.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.AnyaInterval = class {
    /**
     * Creates an instance of AnyaInterval.
     * @param {Number} left
     * @param {Number} right
     * @param {Number} row
     * @memberof AnyaInterval
     */
    constructor(left, right, row) {
        this.discrete_left = false;
        this.discrete_right = false;
        this.left_is_root = false;
        this.right_is_root = false;
        this.init(left, right, row);
    }

    /**
     * @param {Number} left
     * @param {Number} right
     * @param {Number} row
     * @memberof AnyaInterval
     */
    init(left, right, row) {
        this.setLeft(left);
        this.setRight(right);
        this.setRow(row);
    }

    /**
     * @param {AnyaInterval} p
     * @return {Boolean}
     */
    equals(p) {
        if (p == null || !(p instanceof Knight.PATH.AnyaInterval)) return false;
        return Math.abs(p.left - this.left) < Knight.PATH.AnyaInterval.DOUBLE_INEQUALITY_THRESHOLD &&
            Math.abs(p.right - this.right) < Knight.PATH.AnyaInterval.DOUBLE_INEQUALITY_THRESHOLD &&
            p.row === this.row;
    }

    /**
     * @param {AnyaInterval} i
     * @return {Boolean}
     */
    covers(i) {
        if (Math.abs(i.left - this.left) < Knight.PATH.AnyaInterval.DOUBLE_INEQUALITY_THRESHOLD &&
            Math.abs(i.right - this.right) < Knight.PATH.AnyaInterval.DOUBLE_INEQUALITY_THRESHOLD &&
            i.row === this.row) {
            return true;
        }

        return (this.left <= i.left && this.right >= i.right && this.row === i.row);
    }

    /**
     * @param {PIXI.Point} p
     * @return {Boolean}
     */
    contains(p) {
        return (Math.trunc(p.y) === this.row) &&
               ((p.x + BitpackedGrid.epsilon) >= this.left) &&
               (p.x <= (this.right + BitpackedGrid.epsilon));
    }

    /**
     * @return {Number}
     */
    getLeft() {
        return this.left;
    }

    /**
     * @return {Number}
     */
    getRight() {
        return this.right;
    }

    /**
     * @return {Number}
     */
    getRow() {
        return this.row;
    }

    /**
     * @param {Number} left
     */
    setLeft(left) {
        this.left = left;
        this.discrete_left = Math.abs(Math.trunc(left + BitpackedGrid.epsilon) - left) < BitpackedGrid.epsilon;
        if (this.discrete_left) {
            this.left = Math.trunc(this.left + BitpackedGrid.epsilon);
        }
    }

    /**
     * @param {Number} right
     */
    setRight(right) {
        this.right = right;
        this.discrete_right = Math.abs(Math.trunc(right + BitpackedGrid.epsilon) - right) < BitpackedGrid.epsilon;
        if (this.discrete_right) {
            this.right = Math.trunc(this.right + BitpackedGrid.epsilon);
        }
    }

    /**
     * @param {Number} row
     */
    setRow(row) {
        this.row = row;
    }

    /**
     * @return {Number}
     */
    range_size() {
        return this.right - this.left;
    }
};

Knight.PATH.AnyaInterval.DOUBLE_INEQUALITY_THRESHOLD = 0.0000001;
