// =============================================================================
// Anya_IntervalProjection.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
//
// This class is used to project intervals from one location on the
// grid onto another. There are two types of projections:
//  - Flat projections
//  - Conical projections
//
// The projection types correspond to the two types of nodes
// used by Anya.
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.IntervalProjection = class {
    /**
     * Creates an instance of IntervalProjection.
     * @memberof IntervalProjection
     */
    constructor() {
        // the actual endpoints of the projected interval
        this.left = 0;
        this.right = 0;

        // the furthest point left (resp. right) which is
        // visible from the the left (resp. right) endpoint
        // of the projected interval.
        this.max_left = 0;
        this.max_right = 0;
        this.row = 0;

        // a projection is valid if it is possible to move the endpoints
        // of the interval to the adjacent row (up or down) without
        // intersecting an obstacle
        this.valid = false;

        // A projection is observable if the projected left endpoint is
        // strictly smaller than the projected right endpoint.
        // NB: a projection can be valid but non-observable.
        this.observable = false;

        // these variables only used for conical projection
        // some terminology:
        // we analyse the cells of this row in order to determine if
        // the successors inside a conical projection are sterile or not.
        this.sterile_check_row = 0;
        this.check_vis_row = 0;

        // used when generating type iii non-observable conical successors
        this.type_iii_check_row = 0;

        // these variables only used for flat projection
        // some terminology:
        // a deadend flat node is one that cannot be projected further.
        // an intermediate flat node is one that does not hug any walls.
        this.deadend = false;
        this.intermediate = false;
    }

    /**
     * @param {AnyaNode} node
     * @param {BitpackedGrid} grid
     */
    projectNode(node, grid) {
        this.project(node.interval.getLeft(), node.interval.getRight(),
            Math.trunc(node.interval.getRow()),
            Math.trunc(node.root.x), Math.trunc(node.root.y), grid);
    }

    /**
     * @param {Number} ileft
     * @param {Number} iright
     * @param {Number} irow
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {BitpackedGrid} grid
     */
    project(ileft, iright, irow, rootx, rooty, grid) {
        this.observable = false;
        this.valid = false;
        if (rooty === irow) {
            this.project_flat(ileft, iright, rootx, rooty, grid);
        } else {
            this.project_cone(ileft, iright, irow, rootx, rooty, grid);
        }
    }

    /**
     * @param {Number} ileft
     * @param {Number} iright
     * @param {Number} irow
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {BitpackedGrid} grid
     */
    project_cone(ileft, iright, irow, rootx, rooty, grid) {
        if (rooty < irow) { // project down
            this.check_vis_row = irow;
            this.sterile_check_row = irow + 1;
            this.row = irow + 1;
            this.type_iii_check_row = irow - 1;
        } else { // project up
            if (!(rooty > irow)) throw new Error("ERROR!");
            this.sterile_check_row = irow - 2;
            this.row = irow - 1;
            this.check_vis_row = irow - 1;
            this.type_iii_check_row = irow;
        }

        this.valid = grid.get_cell_is_traversable(Math.trunc(ileft + grid.smallest_step_div2), this.check_vis_row) &&
                     grid.get_cell_is_traversable(Math.trunc(iright - grid.smallest_step_div2), this.check_vis_row);

        if (!this.valid) return;

        // interpolate the endpoints of the new interval onto the next row.
        // TODO: cache rise, lrun, rrun and y_delta with the root
        // to avoid branch instructions here?
        const rise = Math.abs(irow - rooty);
        const lrun = rootx - ileft;
        const rrun = iright - rootx;

        // clip the interval if visibility from the root is obstructed.
        // NB: +1 because we convert from tile coordinates to point coords
        this.max_left = grid.scan_cells_left(Math.trunc(ileft), this.check_vis_row) + 1;
        this.left = Math.max(ileft - lrun/rise, this.max_left);

        this.max_right = grid.scan_cells_right(Math.trunc(iright), this.check_vis_row);
        this.right = Math.min(iright + rrun/rise, this.max_right);

        this.observable = (this.left < this.right);

        // sanity checking; sometimes an interval cannot be projected
        // all the way to the next row without first hitting an obstacle.
        // in these cases we need to reposition the endpoints appropriately
        if (this.left >= this.max_right) {
            this.left = grid.get_cell_is_traversable(Math.trunc(ileft - grid.smallest_step_div2), this.check_vis_row) ? this.right : this.max_left;
        }
        if (this.right <= this.max_left) {
            this.right = grid.get_cell_is_traversable(Math.trunc(iright), this.check_vis_row) ? this.left : this.max_right;
        }
    }

    /**
     * @param {Number} ileft the left endpoint of the interval being projected
     * @param {Number} iright the right endpoint of the interval being projected
     * @param {Number} rootx the coordinates of the root point
     * @param {Number} rooty the coordinates of the root point
     * @param {BitpackedGrid} grid
     */
    project_flat(ileft, iright, rootx, rooty, grid) {
        if (rootx <= ileft) {
            this.left = iright;
            this.right = grid.scan_right(this.left, rooty);
            this.deadend = !(
                grid.get_cell_is_traversable(Math.trunc(this.right), rooty) &&
                grid.get_cell_is_traversable(Math.trunc(this.right), rooty-1));
        } else {
            this.right = ileft;
            this.left = grid.scan_left(this.right, rooty);
            this.deadend = !(
                grid.get_cell_is_traversable(Math.trunc(this.left - grid.smallest_step_div2), rooty) &&
                grid.get_cell_is_traversable(Math.trunc(this.left - grid.smallest_step_div2), rooty-1));
        }

        this.intermediate =
                grid.get_cell_is_traversable(Math.trunc(this.left), rooty) &&
                grid.get_cell_is_traversable(Math.trunc(this.left), rooty-1);

        this.row = rooty;
        this.valid = (this.left != this.right);
    }

    /**
     * @param {AnyaNode} node
     * @param {BitpackedGrid} grid
     */
    project_f2cNode(node, grid) {
        if (node.interval.getRow() !== node.root.y) throw new Error("ASSERT FAILED!");
        this.project_f2c(node.interval.getLeft(), node.interval.getRight(),
            Math.trunc(node.interval.getRow()),
            Math.trunc(node.root.x), Math.trunc(node.root.y), grid);
    }

    /**
     * @param {Number} ileft
     * @param {Number} iright
     * @param {Number} irow
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {BitpackedGrid} grid
     */
    project_f2c(ileft, iright, irow, rootx, rooty, grid) {
        // look to the right for successors
        // recall that each point (x, y) corresponds to the
        // top-left corner of a tile at location (x, y)
        if (rootx <= ileft) {
            // can we make a valid turn? valid means
            // (i) the path bends around a corner;
            // (ii) we do not step through any obstacles or through
            // double-corner points.
            const can_step =
                    grid.get_cell_is_traversable(Math.trunc(iright), irow) &&
                    grid.get_cell_is_traversable(Math.trunc(iright), irow-1);
            if (!can_step) {
                this.valid = false;
                this.observable = false;
                return;
            }

            // if the tile below is free, we must be going up
            // else we round the corner and go down
            if (!grid.get_cell_is_traversable(Math.trunc(iright)-1, irow)) {
                // going down
                this.sterile_check_row = irow + 1;
                this.row = irow + 1;
                this.check_vis_row = irow;
            } else {
                // going up
                this.row = irow - 1;
                this.check_vis_row = irow - 1;
                this.sterile_check_row = irow - 2;
            }

            this.left = this.max_left = iright;
            this.right = this.max_right = grid.scan_cells_right(Math.trunc(this.left), this.check_vis_row);
        } else {
            // look to the left for successors
            if (rootx < iright) throw new Error("ASSERT FAILED");
            const can_step =
                    grid.get_cell_is_traversable(Math.trunc(ileft)-1, irow) &&
                    grid.get_cell_is_traversable(Math.trunc(ileft)-1, irow-1);
            if (!can_step) {
                this.valid = false;
                this.observable = false;
                return;
            }

            // if the tiles below are free, we must be going up
            // else we round the corner and go down
            if (!grid.get_cell_is_traversable(Math.trunc(ileft), irow)) {
                // going down
                this.check_vis_row = irow;
                this.sterile_check_row = irow + 1;
                this.row = irow + 1;
            } else {
                // going up
                this.row = irow - 1;
                this.check_vis_row = irow - 1;
                this.sterile_check_row = irow - 2;
            }

            this.right = this.max_right = ileft;
            this.left = this.max_left = grid.scan_cells_left(Math.trunc(this.right)-1, this.check_vis_row)+1;
        }
        this.valid = true;
        this.observable = false;
    }

    /**
     * @return {Boolean}
     */
    getValid() {
        return this.valid;
    }
};
