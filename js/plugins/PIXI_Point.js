// =============================================================================
// PIXI_Point.js
// Version:     1.0
// Date:        2/2/2020
// Author:      Kaelan
// =============================================================================

/**
 * Add a distance function to PIXI.Point for convenience
 * @param {PIXI.Point} other
 * @return {Number}
 */
PIXI.Point.prototype.distance = function(other) {
    const dx = this.x - other.x;
    const dy = this.y - other.y;
    return dx * dx + dy * dy;
};
