// =============================================================================
// AnyaSearch.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
//
// An implementation of the Anya search algorithm.
//
// Based on the Java implementation by Daniel Harabor at:
// https://github.com/Ohohcakester/Any-Angle-Pathfinding/tree/master/src/algorithms/anya16
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.AnyaSearch = class {
    /**
     * Creates an instance of AnyaSearch.
     * @param {AnyaExpansionPolicy} expander
     * @memberof AnyaSearch
     */
    constructor(expander) {
        this.roots = new Map();
        this.open = new Knight.PATH.FibonacciHeap;
        this.heuristic = expander.heuristic();
        this.expander = expander;
        this.lastNodeParent = null;
        this.verbose = false;
        this.isRecording = false;
        this.expanded = 0;
        this.insertions = 0;
        this.generated = 0;
        this.heap_ops = 0;

        // these can be set apriori; only used in conjunction with the
        // run method.
        this.mb_start_ = null;
        this.mb_target_ = null;
        this.mb_cost_ = 0;
        this.snapshotInsert = null;
        this.snapshotExpand = null;
    }

    /**
     */
    init() {
        Knight.PATH.AnyaSearch.search_id_counter++;
        this.expanded = 0;
        this.insertions = 0;
        this.generated = 0;
        this.heap_ops = 0;
        this.open.clear();
        this.roots.clear();
    }

    /**
     * @param {SearchNode} current
     */
    print_path(current) {
        if (current.parentNode !== null) {
            this.print_path(current.parentNode);
        }
        console.log(current);
    }

    /**
     * @param {PIXI.Point} p1
     * @param {PIXI.Point} p2
     * @return {Boolean}
     */
    pointsEqual(p1, p2) {
        return Math.trunc(p1.x) === Math.trunc(p2.x) && Math.trunc(p1.y) === Math.trunc(p2.y);
    }

    /**
     * @param {AnyaNode} start
     * @param {AnyaNode} target
     * @return {Path}
     */
    search(start, target) {
        const cost = this.search_costonly(start, target);
        // generate the path
        let path = null;
        if (cost !== -1) {
            path = new Knight.PATH.Path(target, path, 0);
            let node = this.lastNodeParent;

            while (node !== null) {
                if (!this.pointsEqual(path.getVertex().root, node.getData().root)) {
                    path = new Knight.PATH.Path(node.getData(), path, node.getSecondaryKey());
                }
                node = node.parentNode;
            }
        }
        return path;
    }

    /**
     * @param {AnyaNode} start
     * @param {AnyaNode} target
     * @return {Number}
     */
    search_costonly(start, target) {
        this.init();
        let cost = -1;
        if (!this.expander.validate_instance(start, target)) {
            return cost;
        }

        const startNode = this.generate(start);
        startNode.reset();
        this.open.insert(startNode, this.heuristic.getValue(start, target), 0);

        while (!this.open.isEmpty()) {
            const current = this.open.removeMin();
            if (this.isRecording) this.snapshotExpand.accept(current.getData());

            this.expander.expand(current.getData());
            this.expanded++;
            this.heap_ops++;
            if (current.getData().interval.contains(target.root)) {
                // found the goal
                cost = current.getKey();
                this.lastNodeParent = current;

                if (this.verbose) {
                    this.print_path(current);
                }
                break;
            }

            // unique id for the root of the parent node
            const p_hash = this.expander.hash(current.getData());

            // iterate over all neighbours
            while (this.expander.hasNext()) {
                const succ = this.expander.next();
                const neighbour = this.generate(succ);

                let insert = true;
                const root_hash = this.expander.hash(succ);
                const root_rep = this.roots.get(root_hash);
                const new_g_value = current.getSecondaryKey() + this.expander.step_cost();

                // Root level pruning:
                // We prune a node if its g-value is larger than the best
                // distance to its root point. In the case that the g-value
                // is equal to the best known distance, we prune only if the
                // node isn't a sibling of the node with the best distance or
                // if the node with the best distance isn't the immediate parent
                if (root_rep != null) {
                    const root_best_g = root_rep.getSecondaryKey();
                    insert = (new_g_value - root_best_g) <= BitpackedGrid.epsilon;
                    const eq = (new_g_value - root_best_g) >= -BitpackedGrid.epsilon;
                    if (insert && eq) {
                        const p_rep_hash = this.expander.hash(root_rep.parentNode.getData());
                        insert = (root_hash === p_hash) || (p_rep_hash === p_hash);
                    }
                }

                if (insert) {
                    neighbour.reset();
                    neighbour.parentNode = current;

                    this.open.insert(neighbour,
                        new_g_value + this.heuristic.getValue(neighbour.getData(), target),
                        new_g_value);
                    this.roots.set(root_hash, neighbour);

                    if (this.isRecording) this.snapshotInsert.accept(neighbour.getData());

                    this.heap_ops++;
                    this.insertions++;
                } else {
                    if (this.verbose) {
                        console.log(`\told rootg: ${root_rep.getSecondaryKey()}`);
                        console.log(`\tNOT inserting with f= ${neighbour.getKey()} (g= ${new_g_value})`);
                        console.log(neighbour);
                    }
                }
            }
        }
        if (this.verbose) {
            console.log("finishing search;");
        }
        return cost;
    }

    /**
     * @param {AnyaNode} v
     * @return {SearchNode}
     */
    generate(v) {
        const retval = new Knight.PATH.AnyaSearch.SearchNode(v);
        this.generated++;
        return retval;
    }

    /**
     * @return {Number}
     */
    getExpanded() {
        return this.expanded;
    }

    /**
     * @return {Number}
     */
    getGenerated() {
        return this.insertions;
    }

    /**
     * @return {Number}
     */
    getTouched() {
        return this.generated;
    }

    /**
     * @return {Number}
     */
    getHeap_ops() {
        return this.heap_ops;
    }

    /**
     * @param {Number} expanded
     */
    setExpanded(expanded) {
        this.expanded = expanded;
    }

    /**
     * @param {Number} generated
     */
    setGenerated(generated) {
        this.insertions = generated;
    }

    /**
     * @param {Number} touched
     */
    setTouched(touched) {
        this.generated = touched;
    }

    /**
     * @param {Number} heap_ops
     */
    setExpanded(heap_ops) {
        this.heap_ops = heap_ops;
    }

    /**
     */
    run() {
        this.mb_cost_ = this.search_costonly(this.mb_start_, this.mb_target_);
    }

    /**
     */
    cleanUp() {}
};

Knight.PATH.AnyaSearch.search_id_counter = 0;

/**
 * This class holds various bits of data needed to drive the search
 */
Knight.PATH.AnyaSearch.SearchNode = class extends Knight.PATH.FibonacciHeapNode {
    /**
     * @param {AnyaNode} vertex
     * @memberof SearchNode
     */
    constructor(vertex) {
        super(vertex);
        this.search_id = -1; // tracks if the node has been added to open
        this.parentNode = null;  // parent node
        this.closed = false; // tracks if the node has been expanded
    }

    /**
     */
    reset() {
        this.parentNode = null;
        this.search_id = Knight.PATH.AnyaSearch.search_id_counter;
        this.closed = false;
        Knight.PATH.FibonacciHeapNode.prototype.reset.call(this);
    }
};
