// =============================================================================
// SVG_Heap.js
// Version:     1.0
// Date:        1/20/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Reusable indirect binary heap. Used for O(lgn) deleteMin and O(lgn) decreaseKey.
 * @class Heap
 */
Knight.PATH.Heap = class {
    /**
     * Creates an instance of Heap.
     * Runtime: O(1)
     *
     * @param {Number} size
     * @param {Number} memorySize
     * @memberof Heap
     */
    constructor(size, memorySize) {
        Knight.PATH.Heap.initialize(memorySize, Number.POSITIVE_INFINITY);
        this._heapSize = 0;
    }

    /**
     * @static
     * @param {Heap.Context} context
     * @memberof Heap
     */
    static loadContext(context) {
        this._keyList = context.keyList;
        this._inList = context.inList;
        this._outList = context.outList;
        this._ticketCheck = context.ticketCheck;
        this._ticketNumber = context.ticketNumber;
    }

    /**
     * @static
     * @param {Heap.Context} context
     * @memberof Heap
     */
    static saveContext(context) {
        context.keyList = this._keyList;
        context.inList = this._inList;
        context.outList = this._outList;
        context.ticketCheck = this._ticketCheck;
        context.ticketNumber = this._ticketNumber;
    }

    /**
     * @static
     * @param {Number} size
     * @param {Number} defaultKey
     * @memberof Heap
     */
    static initialize(size, defaultKey) {
        this._defaultKey = defaultKey;

        if (this._ticketCheck === null || this._ticketCheck.length !== size) {
            this._keyList = new Array(size).fill(0);
            this._inList = new Array(size).fill(0);
            this._outList = new Array(size).fill(0);
            this._ticketCheck = new Array(size).fill(0);
            this._ticketNumber = 1;
        } else if (this._ticketNumber === -1) {
            this._ticketCheck = new Array(size).fill(0);
            this._ticketNumber = 1;
        } else {
            this._ticketNumber++;
        }
    }

    /**
     * @static
     * @param {Number} index
     * @return {Number}
     * @memberof Heap
     */
    static getKey(index) {
        return this._ticketCheck[index] === this._ticketNumber ? this._keyList[index] : this._defaultKey;
    }

    /**
     * @static
     * @param {Number} index
     * @return {Number}
     * @memberof Heap
     */
    static getIn(index) {
        return this._ticketCheck[index] === this._ticketNumber ? this._inList[index] : index;
    }

    /**
     * @static
     * @param {Number} index
     * @return {Number}
     * @memberof Heap
     */
    static getOut(index) {
        return this._ticketCheck[index] === this._ticketNumber ? this._outList[index] : index;
    }

    /**
     * @static
     * @param {Number} index
     * @param {Number} value
     * @memberof Heap
     */
    static setKey(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._keyList[index] = value;
            this._inList[index] = index;
            this._outList[index] = index;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._keyList[index] = value;
        }
    }

    /**
     * @static
     * @param {Number} index
     * @param {Number} value
     * @memberof Heap
     */
    static setIn(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._keyList[index] = this._defaultKey;
            this._inList[index] = value;
            this._outList[index] = index;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._inList[index] = value;
        }
    }

    /**
     * @static
     * @param {Number} index
     * @param {Number} value
     * @memberof Heap
     */
    static setOut(index, value) {
        if (this._ticketCheck[index] !== this._ticketNumber) {
            this._keyList[index] = this._defaultKey;
            this._inList[index] = index;
            this._outList[index] = value;
            this._ticketCheck[index] = this._ticketNumber;
        } else {
            this._outList[index] = value;
        }
    }

    /**
     * @param {Number} index
     * @memberof Heap
     */
    bubbleUp(index) {
        let parent = Knight.Utility.intDivision(index-1, 2);
        while (index > 0 && Knight.PATH.Heap.getKey(index) < Knight.PATH.Heap.getKey(parent)) {
            // If meets the conditions to bubble up
            this.swapData(index, parent);
            index = parent;
            parent = Knight.Utility.intDivision(index-1, 2);
        }
    }

    /**
     * @param {Number} a
     * @param {Number} b
     * @memberof Heap
     */
    swapData(a, b) {
        // s = Data at a = out[a]
        // t = Data at b = out[b]
        // key[a] <-> key[b]
        // in[s] <-> in[t]
        // out[a] <-> out[b]

        const s = Knight.PATH.Heap.getOut(a);
        const t = Knight.PATH.Heap.getOut(b);

        this.swapKey(a, b);
        this.swapIn(s, t);
        this.swapOut(a, b);
    }

    /**
     * swap integers in list
     * @param {Number} i1
     * @param {Number} i2
     * @memberof Heap
     */
    swapKey(i1, i2) {
        const temp = Knight.PATH.Heap.getKey(i1);
        Knight.PATH.Heap.setKey(i1, Knight.PATH.Heap.getKey(i2));
        Knight.PATH.Heap.setKey(i2, temp);
    }

    /**
     * swap integers in list
     * @param {Number} i1
     * @param {Number} i2
     * @memberof Heap
     */
    swapOut(i1, i2) {
        const temp = Knight.PATH.Heap.getOut(i1);
        Knight.PATH.Heap.setOut(i1, Knight.PATH.Heap.getOut(i2));
        Knight.PATH.Heap.setOut(i2, temp);
    }

    /**
     * swap integers in list
     * @param {Number} i1
     * @param {Number} i2
     * @memberof Heap
     */
    swapIn(i1, i2) {
        const temp = Knight.PATH.Heap.getIn(i1);
        Knight.PATH.Heap.setIn(i1, Knight.PATH.Heap.getIn(i2));
        Knight.PATH.Heap.setIn(i2, temp);
    }

    /**
     * @param {Number} index1
     * @param {Number} index2
     * @return {Number}
     * @memberof Heap
     */
    smallerNode(index1, index2) {
        if (index1 >= this._heapSize) {
            if (index2 >= this._heapSize) return -1;
            return index2;
        }
        if (index2 >= this._heapSize) return index1;

        return Knight.PATH.Heap.getKey(index1) < Knight.PATH.Heap.getKey(index2) ? index1 : index2;
    }

    /**
     * @param {Number} index
     * @memberof Heap
     */
    bubbleDown(index) {
        let leftChild = 2*index+1;
        let rightChild = 2*index+2;
        let smallerChild = this.smallerNode(leftChild, rightChild);

        while (smallerChild !== -1 && Knight.PATH.Heap.getKey(index) > Knight.PATH.Heap.getKey(smallerChild)) {
            // If meets the conditions to bubble down
            this.swapData(index, smallerChild);

            // Recurse
            index = smallerChild;
            leftChild = 2*index+1;
            rightChild = 2*index+2;
            smallerChild = this.smallerNode(leftChild, rightChild);
        }
    }

    /**
     * Runtime: O(lgn)
     *
     * @param {Number} outIndex
     * @param {Number} newKey
     * @memberof Heap
     */
    decreaseKey(outIndex, newKey) {
        // Assume newKey < old key
        let inIndex = Knight.PATH.Heap.getIn(outIndex);

        // Optimisation: Jump the newly set value to the bottom of the heap.
        // Faster if there are a lot of POSITIVE_INFINITY values.
        // This is equivalent to an insert operation.
        if (Knight.PATH.Heap.getKey(inIndex) === Number.POSITIVE_INFINITY) {
            this.swapData(inIndex, this._heapSize);
            inIndex = this._heapSize;
            ++this._heapSize;
        }
        Knight.PATH.Heap.setKey(inIndex, newKey);
        this.bubbleUp(inIndex);
    }

    /**
     * @return {Number}
     * @memberof Heap
     */
    getMinValue() {
        return Knight.PATH.Heap.getKey(0);
    }

    /**
     * Runtime: O(lgn)
     * @return {Number} index of min element
     * @memberof Heap
     */
    popMinIndex() {
        if (this._heapSize === 0) throw new Error("Indirect Heap is empty!");
        else if (this._heapSize === 1) {
            --this._heapSize;
            return Knight.PATH.Heap.getOut(0);
        }
        // nodeList.size() > 1

        // s = Data at 0 = out[0]
        // t = Data at lastIndex = out[lastIndex]
        // key[0] = key[lastIndex], remove key[lastIndex]
        // in[s] = -1
        // in[t] = 0
        // out[0] = out[lastIndex], remove out[lastIndex]
        const s = Knight.PATH.Heap.getOut(0);
        this.swapData(0, this._heapSize-1);

        --this._heapSize;
        this.bubbleDown(0);

        return s;
    }

    /**
     * @return {String}
     * @memberof Heap
     */
    arrayToString() {
        sb = '';
        for (let i = 0; i < Knight.PATH.Heap._ticketCheck.length; ++i) {
            if (i === this._heapSize) sb += '* ';
            sb += `[ ${Knight.PATH.Heap.getOut(i)} `;
            const val = Knight.PATH.Heap.getKey(i);
            sb += (val === Number.POSITIVE_INFINITY) ? '_' : `${val}`;
            sb += '], ';
        }
        return Knight.PATH.Heap.getIn(1).toString() + ` / ` + sb;
    }

    /**
     * @return {Number}
     * @memberof Heap
     */
    size() {
        return this._heapSize;
    }

    /**
     * @return {Number}
     * @memberof Heap
     */
    isEmpty() {
        return this._heapSize <= 0;
    }

    /**
     * @static
     * @memberof Heap
     */
    static clearMemory() {
        this._keyList = null;
        this._inList = null;
        this._outList = null;
        this._ticketCheck = null;
        global.gc();
    }
};

Knight.PATH.Heap._keyList = null;
Knight.PATH.Heap._inList = null;
Knight.PATH.Heap._outList = null;
Knight.PATH.Heap._defaultKey = Number.POSITIVE_INFINITY;
Knight.PATH.Heap._ticketCheck = null;
Knight.PATH.Heap._ticketNumber = 0;

Knight.PATH.Heap.Context = class {
    /**
     *Creates an instance of Heap.Context
     * @memberof Heap.Context
     */
    constructor() {
        this.keyList = null;
        this.inList = null;
        this.outList = null;
        this.ticketCheck = null;
        this.ticketNumber = null;
    }
};
