// =============================================================================
// AnyaHeuristic.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
//
//  A heuristic for evaluating search nodes generated by the
//  Anya algorithm. A main difference between this heuristic
//  and other pathfinding heuristics is that a node in Anya
//  represents a set of locations from an interval, rather than
//  a single (x, y) coordinate.
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.AnyaHeuristic = class {
    /**
     * Creates an instance of AnyaHeuristic.
     * @memberof AnyaHeuristic
     */
    constructor() {
        this.h = new Knight.PATH.EuclideanDistanceHeuristic;
    }

    /**
     * @param {AnyaNode} n
     * @param {AnyaNode} t
     * @return {Number}
     */
    getValue(n, t = null) {
        if (t === null) return 0;

        const check = (t.root.y == t.interval.getRow()) &&
            (t.root.x == t.interval.getLeft()) &&
            (t.root.x == t.interval.getRight());
        if (!check) throw new Error("ASSERT FAILED!");

        const irow = n.interval.getRow();
        const ileft = n.interval.getLeft();
        const iright = n.interval.getRight();
        const targetx = t.root.x;
        let targety = t.root.y;
        const rootx = n.root.x;
        const rooty = n.root.y;

        // root and target must be on opposite sides of the interval
        // (or both on the same row as the interval). we mirror the
        // target through the interval if this is not the case
        if ((rooty < irow && targety < irow)) {
            targety += 2 * (irow - targety);
        } else if (rooty > irow && targety > irow) {
            targety -= 2 * (targety - irow);
        }

        // project the interval endpoints onto the target row
        const rise_root_to_irow = Math.abs(n.root.y - n.interval.getRow());
        const rise_irow_to_target = Math.abs(n.interval.getRow() - t.root.y);
        const lrun = n.root.x - n.interval.getLeft();
        const rrun = n.interval.getRight() - n.root.x;
        const left_proj = n.interval.getLeft() - rise_irow_to_target * (lrun / rise_root_to_irow);
        const right_proj = n.interval.getRight() + rise_irow_to_target * (rrun / rise_root_to_irow);

        if ((t.root.x + BitpackedGrid.epsilon) < left_proj) {
            // pass through the left endpoint
            return this.h.h(rootx, rooty, ileft, irow) + this.h.h(ileft, irow, targetx, targety);
        }

        if (t.root.x > (right_proj + BitpackedGrid.epsilon)) {
            // pass through the right endpoint
            return this.h.h(rootx, rooty, iright, irow) + this.h.h(iright, irow, targetx, targety);
        }

        // representative point is interior to the interval
        return this.h.h(rootx, rooty, targetx, targety);
    }
};
