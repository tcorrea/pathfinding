// =============================================================================
// SVG_Grid.js
// Version:     1.0
// Date:        1/21/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Represents the Grid of blocked/unblocked tiles.
 * @class GridGraph
 */
Knight.PATH.GridGraph = class {
    /**
     *Creates an instance of GridGraph.
     * @param {Number} sizeX
     * @param {Number} sizeY
     * @memberof GridGraph
     */
    constructor(sizeX, sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeXplusOne = this.sizeX + 1;
        this.tiles = new Array(sizeX*sizeY).fill(false); // Flattened 2D array of booleans
    }

    /**
     * @return {Number}
     */
    width() {
        return this.sizeX;
    }

    /**
     * @return {Number}
     */
    height() {
        return this.sizeY;
    }

    /**
     * Performs a dilation operation on the grid, based on an input kernel.
     * The kernel should be a 2D array of booleans. A 3x3 kernel could look like so:
     *
     *   o o .
     *   o o .
     *   . . .
     *
     * Normally, the origin should be the center of the kernel, but it can be anywhere.
     * This kernel would cause every blocked tile to also block every tile above it,
     * to its left and in it's up-left diagonal.
     *
     * This can be used to modify the grid to represent passability for characters that
     * occupy more than a single tile.
     *
     * For more information, see: http://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
     *
     * @param {Set<Object>} blockedTiles
     * @param {Array<Boolean>} kernel
     * @param {Number} kernelSizeX
     * @param {Number} kernelSizeY
     * @param {Number} kernelOriginX
     * @param {Number} kernelOriginY
     * @memberof GridGraph
     */
    dilate(blockedTiles, kernel, kernelSizeX, kernelSizeY, kernelOriginX, kernelOriginY) {
        // For each blocked tile, render the kernel to the grid in that tile's position
        for (let y = 0; y < kernelSizeY; ++y) {
            for (let x = 0; x < kernelSizeX; ++x) {
                if (kernel[x][y]) {
                    const offsetX = x - kernelOriginX;
                    const offsetY = y - kernelOriginY;
                    for (const tile of blockedTiles.values()) {
                        this.trySetBlocked(tile.x + offsetX, tile.y + offsetY, true);
                    }
                }
            }
        }
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @param {Boolean} value
     * @memberof GridGraph
     */
    setBlocked(x, y, value) {
        this.tiles[this.sizeX*y + x] = value;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @param {Boolean} value
     * @memberof GridGraph
     */
    trySetBlocked(x, y, value) {
        if (this.isValidBlock(x, y)) this.tiles[this.sizeX*y + x] = value;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isBlocked(x, y) {
        if (x >= this.sizeX || y >= this.sizeY) return true;
        if (x < 0 || y < 0) return true;
        return this.tiles[this.sizeX*y + x];
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isBlockedRaw(x, y) {
        return this.tiles[this.sizeX*y + x];
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isValidCoordinate(x, y) {
        return (x <= this.sizeX &&
                y <= this.sizeY &&
                x >= 0 &&
                y >= 0);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isValidBlock(x, y) {
        return (x < this.sizeX &&
                y < this.sizeY &&
                x >= 0 &&
                y >= 0);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof GridGraph
     */
    toTwoDimX(index) {
        return index % this.sizeXplusOne;
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof GridGraph
     */
    toTwoDimY(index) {
        return index / this.sizeXplusOne;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isUnblockedCoordinate(x, y) {
        return !this.topRightOfBlockedTile(x, y) ||
               !this.topLeftOfBlockedTile(x, y) ||
               !this.bottomRightOfBlockedTile(x, y) ||
               !this.bottomLeftOfBlockedTile(x, y);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    topRightOfBlockedTile(x, y) {
        return this.isBlocked(x-1, y-1);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    topLeftOfBlockedTile(x, y) {
        return this.isBlocked(x, y-1);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    bottomRightOfBlockedTile(x, y) {
        return this.isBlocked(x-1, y);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    bottomLeftOfBlockedTile(x, y) {
        return this.isBlocked(x, y);
    }

    /**
     * x1,y1,x2,y2 refer to the top left corner of the tile.
     * @param {Number} x1 Condition: x1 between 0 and sizeX inclusive.
     * @param {Number} y1 Condition: y1 between 0 and sizeY inclusive.
     * @param {Number} x2 Condition: x2 between 0 and sizeX inclusive.
     * @param {Number} y2 Condition: y2 between 0 and sizeY inclusive.
     * @return {Number} distance
     * @memberof GridGraph
     */
    distance(x1, y1, x2, y2) {
        const xDiff = x2 - x1;
        const yDiff = y2 - y1;

        if (yDiff === 0) {
            return Math.abs(xDiff);
        }
        if (xDiff === 0) {
            return Math.abs(yDiff);
        }
        if (xDiff === yDiff || xDiff === -yDiff) {
            return Knight.PATH.GridGraph.SQRT_TWO * Math.abs(xDiff);
        }

        const squareDistance = xDiff*xDiff + yDiff*yDiff;
        return Math.sqrt(squareDistance);
    }

    /**
     * Octile distance:
     *   min(dx,dy)*sqrt(2) + (max(dx,dy)-min(dx,dy))
     * = min(dx,dy)*(sqrt(2)-1) + max(dx,dy)
     *
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @return {Number}
     * @memberof GridGraph
     */
    octileDistance(x1, y1, x2, y2) {
        let dx = x1 - x2;
        let dy = y1 - y2;
        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;

        let min = dx;
        let max = dy;
        if (dy < dx) {
            min = dy;
            max = dx;
        }

        return min * Knight.PATH.GridGraph.SQRT_TWO_MINUS_ONE + max;
    }

    /**
     * Same as lineOfSight, but only works with a vertex and its 8 immediate neighbours.
     * Also (x1,y1) != (x2,y2)
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @return {Boolean}
     * @memberof GridGraph
     */
    neighbourLineOfSight(x1, y1, x2, y2) {
        if (x1 === x2) {
            if (y1 > y2) {
                return !this.isBlocked(x1, y2) || !this.isBlocked(x1-1, y2);
            } else { // y1 < y2
                return !this.isBlocked(x1, y1) || !this.isBlocked(x1-1, y1);
            }
        } else if (x1 < x2) {
            if (y1 === y2) {
                return !this.isBlocked(x1, y1) || !this.isBlocked(x1, y1-1);
            } else if (y1 < y2) {
                return !this.isBlocked(x1, y1);
            } else { // y2 < y1
                return !this.isBlocked(x1, y2);
            }
        } else { // x2 < x1
            if (y1 === y2) {
                return !this.isBlocked(x2, y1) || !this.isBlocked(x2, y1-1);
            } else if (y1 < y2) {
                return !this.isBlocked(x2, y1);
            } else { // y2 < y1
                return !this.isBlocked(x2, y2);
            }
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @return {Boolean} true iff there is line-of-sight from (x1,y1) to (x2,y2).
     * @memberof GridGraph
     */
    lineOfSight(x1, y1, x2, y2) {
        let dy = y2 - y1;
        let dx = x2 - x1;

        let f = 0;

        let signY = 1;
        let signX = 1;
        let offsetX = 0;
        let offsetY = 0;

        if (dy < 0) {
            dy *= -1;
            signY = -1;
            offsetY = -1;
        }
        if (dx < 0) {
            dx *= -1;
            signX = -1;
            offsetX = -1;
        }

        if (dx >= dy) {
            while (x1 !== x2) {
                f += dy;
                if (f >= dx) {
                    if (this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                    y1 += signY;
                    f -= dx;
                }
                if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                if (dy === 0 && this.isBlocked(x1 + offsetX, y1) && this.isBlocked(x1 + offsetX, y1 - 1)) return false;

                x1 += signX;
            }
        } else {
            while (y1 !== y2) {
                f += dx;
                if (f >= dy) {
                    if (this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                    x1 += signX;
                    f -= dy;
                }
                if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) return false;
                if (dx === 0 && this.isBlocked(x1, y1 + offsetY) && this.isBlocked(x1 - 1, y1 + offsetY)) return false;

                y1 += signY;
            }
        }
        return true;
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} dx
     * @param {Number} dy
     * @return {PIXI.Point}
     * @memberof GridGraph
     */
    findFirstBlockedTile(x1, y1, dx, dy) {
        let f = 0;

        let signY = 1;
        let signX = 1;
        let offsetX = 0;
        let offsetY = 0;

        if (dy < 0) {
            dy *= -1;
            signY = -1;
            offsetY = -1;
        }
        if (dx < 0) {
            dx *= -1;
            signX = -1;
            offsetX = -1;
        }

        if (dx >= dy) {
            while (true) {
                f += dy;
                if (f >= dx) {
                    if (this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                        return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                    }
                    y1 += signY;
                    f -= dx;
                }
                if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                    return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                }
                if (dy === 0 && this.isBlocked(x1 + offsetX, y1) && this.isBlocked(x1 + offsetX, y1 - 1)) {
                    return new PIXI.Point(x1 + offsetX, -1);
                }

                x1 += signX;
            }
        } else {
            while (true) {
                f += dx;
                if (f >= dy) {
                    if (this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                        return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                    }
                    x1 += signX;
                    f -= dy;
                }
                if (f !== 0 && this.isBlocked(x1 + offsetX, y1 + offsetY)) {
                    return new PIXI.Point(x1 + offsetX, y1 + offsetY);
                }
                if (dx === 0 && this.isBlocked(x1, y1 + offsetY) && this.isBlocked(x1 - 1, y1 + offsetY)) {
                    return new PIXI.Point(-1, y1 + offsetY);
                }

                y1 += signY;
            }
        }
    }

    /**
     * Used by Accelerated A* and MazeAnalysis.
     * leftRange is the number of blocks you can move left before hitting a blocked tile.
     * downRange is the number of blocks you can move down before hitting a blocked tile.
     * For blocked tiles, leftRange, downRange are both -1.
     *
     * How to use the maxRange property:
     *
     *  x,y is the starting point.
     *  k is the number of tiles diagonally up-right of the starting point.
     *  int i = x-y+sizeY;
     *  int j = Math.min(x, y);
     *  return maxRange[i][j + k] - k;
     *
     * @return {Array<Array<Number>>}
     * @memberof GridGraph
     */
    computeMaxDownLeftRanges() {
        const downRange = new Array(this.sizeY+1);
        for (let i = 0; i < downRange.length; ++i) downRange[i] = new Array(this.sizeX+1).fill(0);
        const leftRange = new Array(this.sizeY+1);
        for (let i = 0; i < leftRange.length; ++i) leftRange[i] = new Array(this.sizeX+1).fill(0);

        for (let y = 0; y < this.sizeY; ++y) {
            if (this.isBlocked(0, y)) leftRange[y][0] = -1;
            else leftRange[y][0] = 0;

            for (let x = 1; x < this.sizeX; ++x) {
                if (this.isBlocked(x, y)) {
                    leftRange[y][x] = -1;
                } else {
                    leftRange[y][x] = leftRange[y][x-1] + 1;
                }
            }
        }

        for (let x = 0; x < this.sizeX; ++x) {
            if (this.isBlocked(x, 0)) downRange[0][x] = -1;
            else downRange[0][x] = 0;

            for (let y = 1; y < this.sizeY; ++y) {
                if (this.isBlocked(x, y)) {
                    downRange[y][x] -1;
                } else {
                    downRange[y][x] = downRange[y-1][x] + 1;
                }
            }
        }

        for (let x = 0; x < this.sizeX+1; ++x) {
            downRange[this.sizeY][x] = -1;
            leftRange[this.sizeY][x] = -1;
        }

        for (let y = 0; y < this.sizeY; ++y) {
            downRange[y][this.sizeX] = -1;
            leftRange[y][this.sizeX] = -1;
        }

        maxRanges = new Array(this.sizeX + this.sizeY + 1);
        const maxSize = Math.min(this.sizeX, this.sizeY) + 1;
        for (let i = 0; i < maxRanges.length; ++i) {
            let currSize = Math.min(i+1, maxRanges.length-i);
            if (maxSize < currSize) currSize = maxSize;
            maxRanges[i] = new Array(currSize).fill(0);

            let currX = i - this.sizeY;
            if (currX < 0) currX = 0;
            let currY = currX - i + this.sizeY;
            for (let j = 0; j < maxRanges[i].length; ++j) {
                maxRanges[i][j] = Math.min(downRange[currY][currX], leftRange[currY][currX]);
                currY++;
                currX++;
            }
        }
        return maxRanges;
    }

    /**
     * @return {Number} the percentage of blocked tiles as compared to the total grid size.
     * @memberof GridGraph
     */
    getPercentageBlocked() {
        return this.getNumBlocked() / (this.sizeX * this.sizeY);
    }

    /**
     * @return {Number} the number of blocked tiles in the grid.
     * @memberof GridGraph
     */
    getNumBlocked() {
        let nBlocked = 0;
        for (let y = 0; y < this.sizeY; ++y) {
            for (let x = 0; x < this.sizeX; ++x) {
                if (this.isBlocked(x, y)) nBlocked++;
            }
        }
        return nBlocked;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Boolean}
     * @memberof GridGraph
     */
    isOuterCorner(x, y) {
        const a = this.isBlocked(x-1, y-1);
        const b = this.isBlocked(x, y-1);
        const c = this.isBlocked(x, y);
        const d = this.isBlocked(x-1, y);

        return ((!a && !c) || (!d && !b)) && (a || b || c || d);

        /* NOTE
         *   ___ ___
         *  |   |||||
         *  |...X'''| <-- this is considered a corner in the above definition
         *  |||||___|
         *
         *  The definition below excludes the above case.
         */

        /*let results = 0;
        if(a)results++;
        if(b)results++;
        if(c)results++;
        if(d)results++;
        return (results === 1);*/
    }

    /**
     * Checks whether the path (x1,y1),(x2,y2),(x3,y3) is taut.
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTaut(x1, y1, x2, y2, x3, y3) {
        if (x1 < x2) {
            if (y1 < y2) {
                return this.isTautFromBottomLeft(x1, y1, x2, y2, x3, y3);
            } else if (y2 < y1) {
                return this.isTautFromTopLeft(x1, y1, x2, y2, x3, y3);
            } else { // y1 === y2
                return this.isTautFromLeft(x1, y1, x2, y2, x3, y3);
            }
        } else if (x2 < x1) {
            if (y1 < y2) {
                return this.isTautFromBottomRight(x1, y1, x2, y2, x3, y3);
            } else if (y2 < y1) {
                return this.isTautFromTopRight(x1, y1, x2, y2, x3, y3);
            } else { // y1 === y2
                return this.isTautFromRight(x1, y1, x2, y2, x3, y3);
            }
        } else { // x2 === x1
            if (y1 < y2) {
                return this.isTautFromBottom(x1, y1, x2, y2, x3, y3);
            } else if (y2 < y1) {
                return this.isTautFromTop(x1, y1, x2, y2, x3, y3);
            } else { // y1 === y2
                throw new Error("v == u?");
            }
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromBottomLeft(x1, y1, x2, y2, x3, y3) {
        if (x3 < x2 || y3 < y2) return false;

        const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
        if (compareGradients < 0) { // m1 < m2
            return this.bottomRightOfBlockedTile(x2, y2);
        } else if (compareGradients > 0) { // m1 > m2
            return this.topLeftOfBlockedTile(x2, y2);
        } else { // m1 == m2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromTopLeft(x1, y1, x2, y2, x3, y3) {
        if (x3 < x2 || y3 > y2) return false;

        const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
        if (compareGradients < 0) { // m1 < m2
            return this.bottomLeftOfBlockedTile(x2, y2);
        } else if (compareGradients > 0) { // m1 > m2
            return this.topRightOfBlockedTile(x2, y2);
        } else { // m1 == m2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromBottomRight(x1, y1, x2, y2, x3, y3) {
        if (x3 > x2 || y3 < y2) return false;
        const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
        if (compareGradients < 0) { // m1 < m2
            return this.topRightOfBlockedTile(x2, y2);
        } else if (compareGradients > 0) { // m1 > m2
            return this.bottomLeftOfBlockedTile(x2, y2);
        } else { // m1 == m2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromTopRight(x1, y1, x2, y2, x3, y3) {
        if (x3 > x2 || y3 > y2) return false;

        const compareGradients = (y2-y1)*(x3-x2) - (y3-y2)*(x2-x1); // m1 - m2
        if (compareGradients < 0) { // m1 < m2
            return this.topLeftOfBlockedTile(x2, y2);
        } else if (compareGradients > 0) { // m1 > m2
            return this.bottomRightOfBlockedTile(x2, y2);
        } else { // m1 == m2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromLeft(x1, y1, x2, y2, x3, y3) {
        if (x3 < x2) return false;

        const dy = y3 - y2;
        if (dy < 0) { // y3 < y2
            return this.topRightOfBlockedTile(x2, y2);
        } else if (dy > 0) { // y3 > y2
            return this.bottomRightOfBlockedTile(x2, y2);
        } else { // y3 == y2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromRight(x1, y1, x2, y2, x3, y3) {
        if (x3 > x2) return false;

        const dy = y3 - y2;
        if (dy < 0) { // y3 < y2
            return this.topLeftOfBlockedTile(x2, y2);
        } else if (dy > 0) { // y3 > y2
            return this.bottomLeftOfBlockedTile(x2, y2);
        } else { // y3 == y2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromBottom(x1, y1, x2, y2, x3, y3) {
        if (y3 < y2) return false;

        const dx = x3 - x2;
        if (dx < 0) { // x3 < x2
            return this.topRightOfBlockedTile(x2, y2);
        } else if (dx > 0) { // x3 > x2
            return this.topLeftOfBlockedTile(x2, y2);
        } else { // x3 == x2
            return true;
        }
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof GridGraph
     */
    isTautFromTop(x1, y1, x2, y2, x3, y3) {
        if (y3 > y2) return false;

        const dx = x3 - x2;
        if (dx < 0) { // x3 < x2
            return this.bottomRightOfBlockedTile(x2, y2);
        } else if (dx > 0) { // x3 > x2
            return this.bottomLeftOfBlockedTile(x2, y2);
        } else { // x3 == x2
            return true;
        }
    }
};

Knight.PATH.GridGraph.SQRT_TWO = Math.sqrt(2.0);
Knight.PATH.GridGraph.SQRT_TWO_MINUS_ONE = Math.sqrt(2.0) - 1;
