var Knight = Knight || {}; // eslint-disable-line no-var
Knight.EDITOR = Knight.EDITOR || {};
Knight.EDITOR.isTestPlay = function() {
    return Utils.isNwjs() && Utils.isOptionValid('test');
};

// =============================================================================
// Dependencies
// =============================================================================
if (Knight.EDITOR.isTestPlay()) {
    // =============================================================================
    // Resource Manager
    // =============================================================================
    var Imported = Imported || {}; // eslint-disable-line no-var
    if (!Imported.KNT_ResourceManager) {
        var Knight = Knight || {}; // eslint-disable-line no-var
        Knight.RM = Knight.RM || {};

        //=============================================================================
        // Parameter Variables
        //=============================================================================
        Knight.Param = Knight.Param || {};
        Knight.Param.Menu = Knight.Param.Menu || {};
        Knight.Param.Menu.ImageFolder = "img/gui/";

        //=============================================================================
        // Utility Functions
        //=============================================================================
        Knight.Util = Knight.Util || {};
        Knight.Util.getFilesInFolder = function(folder) {
            if (folder === '') return [];
            const fs = require('fs');
            const results = [];
            const njsPath = require('path');
            const base = njsPath.dirname(process.mainModule.filename);
            const path = njsPath.join(base, folder);
            fs.readdirSync(path).forEach(function(file) {
                name = file;
                file = path + '/' + name;
                const stat = fs.statSync(file);
                if (stat && stat.isDirectory()) {
                    // Do nothing
                } else if (name.match(/.png/g)) {
                    name = name.replace(/.png/g, '');
                    results.push({dir: folder, name: name});
                }
            });
            return results;
        };

        //=============================================================================
        // Show debug window
        //=============================================================================
        nw.Window.get().showDevTools('', () => nw.Window.get().focus());

        //=============================================================================
        // Resources
        //=============================================================================
        Knight.Preload = Knight.Preload || {};
        Knight.Preload.MenuFiles = Knight.Util.getFilesInFolder(Knight.Param.Menu.ImageFolder);

        //=============================================================================
        // Preloading
        //=============================================================================
        Knight.RM.Scene_Boot_initialize = Scene_Boot.prototype.initialize;
        Scene_Boot.prototype.initialize = function() {
            Knight.RM.preloadMenu();
            Knight.RM.Scene_Boot_initialize.call(this);
        };

        Knight.RM.preloadMenu = function() {
            const files = Knight.Preload.MenuFiles;
            for (file of files) {
                ImageManager.reserveBitmap(file.dir, file.name, null, false, ImageManager._systemReservationId);
            }
        };

        ImageManager.loadGui = function(filename, hue) {
            return this.loadBitmap(Knight.Param.Menu.ImageFolder, filename, hue, false);
        };
    };

    // =============================================================================
    // KNT_Window
    // =============================================================================
    if (!Knight.WINDOW) {
        var Knight = Knight || {}; // eslint-disable-line no-var
        Knight.WINDOW = Knight.WINDOW || {};

        //=============================================================================
        // Initiailize logic can't be added via Window_Extension, so add it to
        // Window_Base instead.
        //=============================================================================
        Knight.WINDOW.Window_Base_initialize = Window_Base.prototype.initialize;
        Window_Base.prototype.initialize = function(x, y, width, height) {
            Knight.WINDOW.Window_Base_initialize.call(this, x, y, width, height);
            this._handlers = {};
            this._index = -1;
            this._rects = [];
            this._data = null;
            this._enabled = true;
            this._useMouseInput = false;
        };

        Window_Base.prototype.maxItems = function() {
            return this._data ? this._data.length : 0;
        };

        Window_Base.prototype.item = function() {
            const index = this.index();
            return this._data && index >= 0 ? this._data[index] : null;
        };

        //=============================================================================
        // New class for holding Knight changes to default Window behavior
        //=============================================================================
        Knight.Window_Extension = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_Extension.prototype = Object.create(Object.prototype);
        Knight.Window_Extension.prototype.constructor = Knight.Window_Extension;

        //=============================================================================
        // Override old methods for better default behavior
        //=============================================================================
        // remove drawing of window background
        Knight.Window_Extension.prototype._refreshBack = function() {
        };

        // remove drawing of window frame
        Knight.Window_Extension.prototype._refreshFrame = function() {
        };

        // remove drawing of window cursor
        Knight.Window_Extension.prototype._refreshCursor = function() {
        };

        // remove window border padding so contents size matches window size
        Knight.Window_Extension.prototype.standardPadding = function() {
            return 0;
        };

        // reduce text margin
        Knight.Window_Extension.prototype.textPadding = function() {
            return 4;
        };

        Knight.Window_Extension.prototype.standardBackOpacity = function() {
            return 0;
        };

        // decouple default font face definition for easier modification by subclasses
        Knight.Window_Extension.prototype.standardFontFace = function() {
            if ($gameSystem.isChinese()) {
                return 'SimHei, Heiti TC, sans-serif';
            } else if ($gameSystem.isKorean()) {
                return 'Dotum, AppleGothic, sans-serif';
            } else {
                return this.menuFontFace();
            }
        };

        Knight.Window_Extension.prototype.standardFontSize = function() {
            return 16;
        };

        // turn off text outline by default
        Knight.Window_Extension.prototype.resetFontSettings = function() {
            Window_Base.prototype.resetFontSettings.call(this);
            this.contents.outlineWidth = 0;
            this.contents.fontBold = false;
        };

        // add callback handler logic to every window
        Knight.Window_Extension.prototype.setHandler = function(symbol, method) {
            this._handlers[symbol] = method;
        };

        Knight.Window_Extension.prototype.isHandled = function(symbol) {
            return !!this._handlers[symbol];
        };

        Knight.Window_Extension.prototype.callHandler = function(symbol) {
            if (this.isHandled(symbol)) {
                this._handlers[symbol](this);
            }
        };

        Knight.Window_Extension.prototype.show = function() {
            this.visible = true;
        };

        Knight.Window_Extension.prototype.hide = function() {
            this.visible = false;
        };

        Knight.Window_Extension.prototype.enabled = function() {
            return this._enabled;
        };

        Knight.Window_Extension.prototype.enable = function() {
            this._enabled = true;
        };

        Knight.Window_Extension.prototype.disable = function() {
            this._enabled = false;
        };

        // Counterpart to isOpenAndActive()
        // The active flag is not relevant when primarily using mouse input
        Knight.Window_Extension.prototype.isOpenAndEnabled = function() {
            return this.isOpen() && this.enabled();
        };

        Knight.Window_Extension.prototype.index = function() {
            return this._index;
        };

        Knight.Window_Extension.prototype.itemRect = function(index) {
            const rect = new Rectangle();
            const maxCols = this.maxCols();
            rect.width = this.itemWidth();
            rect.height = this.itemHeight();
            rect.x = index % maxCols * (rect.width + this.spacing()) - this._scrollX;
            rect.y = Math.floor(index / maxCols) * rect.height - this._scrollY;
            return rect;
        };

        // Cache item rects so they don't need to be reallocated every frame
        Knight.Window_Extension.prototype.makeItemRects = function() {
            this._rects = [];
            for (let i = 0; i < this.maxItems(); i++) {
                this._rects.push(this.itemRect(i));
            }
        };

        Knight.Window_Extension.prototype.getItemRect = function(index) {
            return this._rects[index];
        };

        Knight.Window_Extension.prototype.select = function(index) {
            this._index = index;
        };

        //=============================================================================
        // Mouse Input Handling
        //=============================================================================
        Knight.Window_Extension.prototype.update = function() {
            Window_Base.prototype.update.call(this);
            if (this._useMouseInput) this.processHover();
        };

        Knight.Window_Extension.prototype.isMouseInsideFrame = function() {
            const x = this.canvasToLocalX(TouchInput.x);
            const y = this.canvasToLocalY(TouchInput.y);
            return x >= 0 && y >= 0 && x < this.width && y < this.height;
        };

        Knight.Window_Extension.prototype.isMouseInsideRect = function(x, y, rect) {
            return x >= rect.x && y >= rect.y && x < (rect.x + rect.width) && y < (rect.y + rect.height);
        };

        Knight.Window_Extension.prototype.processHover = function() {
            if (this.isOpenAndEnabled()) {
                let mouseOverIndex = null;
                if (this.isMouseInsideFrame()) {
                    const x = this.canvasToLocalX(TouchInput.x);
                    const y = this.canvasToLocalY(TouchInput.y);
                    const topIndex = this.topIndex();
                    for (let i = 0; i < this.maxPageItems(); i++) {
                        const index = topIndex + i;
                        if (index < this.maxItems()) {
                            const rect = this.getItemRect(index);
                            if (this.isMouseInsideRect(x, y, rect)) {
                                mouseOverIndex = index;
                                break;
                            }
                        }
                    }
                }
                this.onHover(mouseOverIndex);
            }
        };

        Knight.Window_Extension.prototype.onHover = function(index) {
            const lastIndex = this.index();
            if (index !== null) {
                this.select(index);
                if (this.isHandled('ok') && TouchInput.isReleased()) {
                    this.processOk();
                }
                if (this.isHandled('cancel') && TouchInput.isCancelled()) {
                    this.processCancel();
                }
            } else {
                this.select(-1);
            }
            if (this.index() !== lastIndex && this.index() >= 0) {
                this.playCursorSound();
            }
        };

        Knight.Window_Extension.prototype.playCursorSound = function() {
            SoundManager.playCursor();
        };

        Knight.Window_Extension.prototype.processOk = function() {
            this.playOkSound();
            this.updateInputData();
            this.callHandler('ok');
        };

        Knight.Window_Extension.prototype.processCancel = function() {
            this.playCancelSound();
            this.updateInputData();
            this.callHandler('cancel');
        };

        Knight.Window_Extension.prototype.playOkSound = function() {
            SoundManager.playOk();
        };

        Knight.Window_Extension.prototype.playCancelSound = function() {
            SoundManager.playCancel();
        };

        Knight.Window_Extension.prototype.updateInputData = function() {
            Input.update();
            TouchInput.update();
        };

        Knight.Window_Extension.prototype.processWheel = function() {
            if (this.isOpenAndEnabled() && this.isMouseInsideFrame()) {
                const threshold = 20;
                if (TouchInput.wheelY >= threshold) {
                    this.scrollDown();
                    this.updateInputData();
                }
                if (TouchInput.wheelY <= -threshold) {
                    this.scrollUp();
                    this.updateInputData();
                }
            }
        };

        //=============================================================================
        // Utility Methods
        //=============================================================================
        Knight.Window_Extension.prototype.menuFontFace = function() {
            return Knight.Param.KEFont;
        };

        Knight.Window_Extension.prototype.drawGui = function(name, x, y, dw = null, dh = null) {
            const bitmap = ImageManager.loadGui(name);
            dw = dw || bitmap.width;
            dh = dh || bitmap.height;
            this.contents.blt(bitmap, 0, 0, bitmap.width, bitmap.height, x, y, dw, dh);
        };

        // Like Window_Base.drawGauge() but using images instead of flat colors, for better looking gauges
        Knight.Window_Extension.prototype.drawImageGauge = function(x, y, rate, backImg, frontImg, reverseFill = false) {
            if (rate >= 0) {
                const backBitmap = ImageManager.loadGui(backImg);
                this.drawGui(backImg, x, y);
                if (rate !== 0) {
                    const dw = backBitmap.width * rate;
                    if (reverseFill) { // Fills right-to-left instead of left-to-right
                        const xoffset = backBitmap.width - dw;
                        const frontBitmap = ImageManager.loadGui(frontImg);
                        this.contents.blt(frontBitmap, xoffset, 0, dw, frontBitmap.height, x + xoffset, y, dw, frontBitmap.height);
                    } else {
                        this.drawGui(frontImg, x, y, dw);
                    }
                }
            }
        };

        // Adds child windows to the scene. Should be overriden by child classes
        // which contain their own internal windows.
        Knight.Window_Extension.prototype.addChildrenToScene = function(scene) {
        };

        //=============================================================================
        // Window_Base
        //-----------------------------------------------------------------------------
        // Add extensions to Window_Base
        //=============================================================================
        Knight.Window_Base = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_Base.prototype = Object.create(Window_Base.prototype);
        Object.assign(Knight.Window_Base.prototype, Knight.Window_Extension.prototype);
        Knight.Window_Base.prototype.constructor = Knight.Window_Base;

        //=============================================================================
        // Window_Selectable
        //-----------------------------------------------------------------------------
        // Add extensions to Window_Selectable
        //=============================================================================
        Knight.Window_Selectable = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_Selectable.prototype = Object.create(Window_Selectable.prototype);
        Object.assign(Knight.Window_Selectable.prototype, Knight.Window_Extension.prototype);
        Knight.Window_Selectable.prototype.constructor = Knight.Window_Selectable;

        Knight.WINDOW.Window_Selectable_update = Knight.Window_Selectable.prototype.update;
        Knight.Window_Selectable.prototype.update = function() {
            Knight.WINDOW.Window_Selectable_update.call(this);
            this.processWheel();
        };

        //=============================================================================
        // Window_ItemList
        //-----------------------------------------------------------------------------
        // Add extensions to Window_ItemList
        //=============================================================================
        Knight.Window_ItemList = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_ItemList.prototype = Object.create(Window_ItemList.prototype);
        Object.assign(Knight.Window_ItemList.prototype, Knight.Window_Extension.prototype);
        Knight.Window_ItemList.prototype.constructor = Knight.Window_ItemList;

        Knight.WINDOW.Window_ItemList_update = Knight.Window_ItemList.prototype.update;
        Knight.Window_ItemList.prototype.update = function() {
            Knight.WINDOW.Window_ItemList_update.call(this);
            this.processWheel();
        };

        //=============================================================================
        // Window_Command
        //-----------------------------------------------------------------------------
        // Add extensions to Window_Command
        //=============================================================================
        Knight.Window_Command = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_Command.prototype = Object.create(Window_Command.prototype);
        Object.assign(Knight.Window_Command.prototype, Knight.Window_Extension.prototype);
        Knight.Window_Command.prototype.constructor = Knight.Window_Command;

        //=============================================================================
        // Window_HorzCommand
        //-----------------------------------------------------------------------------
        // Add extensions to Window_HorzCommand
        //=============================================================================
        Knight.Window_HorzCommand = function(...arguments) {
            this.initialize(...arguments);
        };
        Knight.Window_HorzCommand.prototype = Object.create(Window_HorzCommand.prototype);
        Object.assign(Knight.Window_HorzCommand.prototype, Knight.Window_Extension.prototype);
        Knight.Window_HorzCommand.prototype.constructor = Knight.Window_HorzCommand;

        //=============================================================================
        // WindowLayer
        //-----------------------------------------------------------------------------
        // Reverse update order so overlapping windows handle input correctly. Normally,
        // MV both renders and updates windows in order. That means that windows at the
        // end of the windowLayer draw on top, but windows at the top handle input first.
        // By updating them in reverse order, windows that are drawn on top will handle
        // input first.
        //=============================================================================
        WindowLayer.prototype.update = function() {
            let i = this.children.length;
            while (i--) {
                if (this.children[i].update) {
                    this.children[i].update();
                }
            }
        };
    }
    // =============================================================================
    // KNT_Button
    // =============================================================================
    if (!Knight.Button) {
        var Knight = Knight || {}; // eslint-disable-line no-var
        /**
         * Window representing a clickable button.
         *
         * @class Knight.Button
         * @extends {Knight.Window_Base}
         */
        Knight.Button = class extends Knight.Window_Base {
            /**
             * Creates an instance of Knight.Button.
             *
             * @param {number} x            Window x coordinate
             * @param {number} y            Window y coordinate
             * @param {string} image        Name of the normal button image
             * @param {string} hoverImage   Name of the image displayed when hovering over the button
             * @param {string} text         Text to display on the button. Can be null.
             * @param {number} forceWidth   Forces the button to be a ceirtain width. If NULL, defaults
             *                              to width of the button image.
             * @param {number} forceHeight  Forces the button to be a ceirtain height. If NULL, defaults
             *                              to height of the button image.
             * @param {String} textAlign    Button text alignment
             */
            constructor(x, y, image, hoverImage, text = null, forceWidth = null, forceHeight = null, textAlign = 'center') {
                const bitmap = ImageManager.loadGui(image);
                const width = forceWidth || bitmap.width;
                const height = forceHeight || bitmap.height;
                super(x, y, width, height);
                this._image = image;
                this._hoverImage = hoverImage;
                this._enabled = true;
                this._isHovering = false;
                this._wasHovering = false;
                this._text = text;
                this._useOnOff = false;
                this._textAlign = textAlign;
                this.refresh();
            }

            /**
             * Buttons controlled by on()/off() ignore enable()/disable()
             */
            enable() {
                if (this._useOnOff) return;
                Knight.Window_Base.prototype.enable.call(this);
            }
            /**
             * Buttons controlled by on()/off() ignore enable()/disable()
             */
            disable() {
                if (this._useOnOff) return;
                Knight.Window_Base.prototype.disable.call(this);
            }

            /**
             * Turn button on. Button only accepts input when on.
             */
            on() {
                this._useOnOff = true;
                this._enabled = true;
                this.refresh();
            }

            /**
             * Turn button off. Button doesn't accept input when off.
             */
            off() {
                this._useOnOff = true;
                this._offImage = this._offImage || (this._image + "_Off");
                this._enabled = false;
                this.refresh();
            }

            /**
             * @return {Boolean}
             */
            isOff() {
                return this._offImage && this._enabled === false;
            }

            /**
             * @return {boolean}    Button state
             */
            value() {
                return this._enabled;
            }

            /**
             * Frame update
             */
            update() {
                Window_Base.prototype.update.call(this);
                this.processHover();
                this.processHandling();
                this.processTouch();
            }

            /**
             * @return {Object} Data contained in the Button. Can be null.
             */
            item() {
                return null;
            }

            /**
             * Handle mouse hovering behavior
             */
            processHover() {
                this._wasHovering = this._isHovering;
                this._isHovering = this.isMouseInsideFrame();
                if (this._isHovering !== this._wasHovering) {
                    this.refresh();
                }
            }

            /**
             * Handle keyboard input
             */
            processHandling() {
                if (this.isOpenAndEnabled()) {
                    if (this.isCancelEnabled() && this.isCancelTriggered()) {
                        this.processCancel();
                    }
                }
            }

            /**
             * Handle mouse input
             */
            processTouch() {
                if (this.isOpenAndEnabled()) {
                    if (TouchInput.isReleased() && this.isMouseInsideFrame()) {
                        this.onTouch();
                    }
                }
            }

            /**
             * Handle mouse click events
             */
            onTouch() {
                if (this.isTouchOkEnabled()) {
                    this.processOk();
                }
            }

            /**
             * @return  {boolean}   Whether mouse input can be handled
             */
            isTouchOkEnabled() {
                return this.isOkEnabled();
            }

            /**
             * @return  {boolean}   Whether mouse input can be handled
             */
            isOkEnabled() {
                return this.isHandled('ok');
            }

            /**
             * @return  {boolean}   True when button is triggered
             */
            isOkTriggered() {
                return Input.isRepeated('ok');
            }

            /**
             * @return  {boolean}   Whether cancel command is handled
             */
            isCancelEnabled() {
                return this.isHandled('cancel');
            }

            /**
             * @return  {boolean}   True when cancel command is triggered
             */
            isCancelTriggered() {
                return Input.isRepeated('cancel');
            }

            /**
             * Behavior executed when button is triggered
             */
            processOk() {
                this.playOkSound();
                this.callHandler('ok');
                this.updateInputData();
            }

            /**
             * Button SE
             */
            playOkSound() {
            };

            /**
             * @param {string} name
             */
            setOkSound(name) {
                this._okSound = name;
            };

            /**
             */
            processCancel() {
                this.callHandler('cancel');
                this.updateInputData();
            }

            /**
             * Redraw button
             */
            refresh() {
                if (this.contents) {
                    this.contents.clear();
                    this.drawContents();
                }
            }

            /**
             */
            drawContents() {
                const image = this.isOff() ? this._offImage : (this._isHovering ? this._hoverImage : this._image);
                this.drawGui(image, 0, 0);
                if (this._text) {
                    this.contents.fontBold = true;
                    this.contents.fontSize = 16;
                    const color = this._isHovering ? Knight.COLOR.GREY : this.normalColor();
                    this.changeTextColor(color);
                    const x = this._textAlign === 'center' ? 0 : 30;
                    const y = (this.contents.height - this.lineHeight()) / 2;
                    this.contents.drawText(this._text, x, y, this.contents.width, this.lineHeight(), this._textAlign);
                }
            }
        };
    }
    // =============================================================================
    // KNT_Checkbox
    // =============================================================================
    if (!Knight.Checkbox) {
        var Knight = Knight || {}; // eslint-disable-line no-var
        /**
         * Window representing a checkbox
         *
         * @class Knight.Checkbox
         * @extends {Knight.Button}
         */
        Knight.Checkbox = class extends Knight.Button {
            /**
             * Creates an instance of Knight.Checkbox.
             *
             * @param {number} x            Window x coordinate
             * @param {number} y            Window y coordinate
             * @param {string} onImage      Image shown when checkbox is 'checked'
             * @param {string} offImage     Image shown when checkbox is not 'checked'
             * @param {string} hoverImage   Image shown when hovering over the checkbox
             * @param {boolean} checked     Default value for 'checked'
             * @param {string} label        Checkbox label text
             */
            constructor(x, y, onImage, offImage, hoverImage = null, checked = true, label = '') {
                super(x, y, onImage, hoverImage);
                this._onImage = onImage;
                this._offImage = offImage;
                this._checked = checked;
                this._label = label;
                this.width = ImageManager.loadGui(onImage).width + this.textWidth(label) + 10;
                this.createContents();
                this.refresh();
            }

            /**
             * Toggle checkbox
             */
            toggle() {
                this._checked = !this._checked;
                this.refresh();
                this.onChange();
            }

            /**
             * Set checkbox to 'checked'
             */
            check() {
                this._checked = true;
                this.refresh();
                this.onChange();
            }

            /**
             * Set checkbox to 'unchecked'
             */
            uncheck() {
                this._checked = false;
                this.refresh();
                this.onChange();
            }

            /**
             * @return {boolean}            Whether checkbox is checked
             */
            checked() {
                return this._checked;
            }

            /**
             * Plays a sound when the checkbox is interacted with.
             */
            playOkSound() {
            };

            /**
             * Frame update
             */
            update() {
                Window_Base.prototype.update.call(this);
                this.processHover();
                this.processTouch();
            }

            /**
             * Handle mouse input
             */
            processTouch() {
                if (this.isOpenAndEnabled()) {
                    if (TouchInput.isReleased() && this.isMouseInsideFrame()) {
                        this.onTouch();
                    }
                }
            }

            /**
             * Handle mouse click events
             */
            onTouch() {
                this.playOkSound();
                this.toggle();
                this.updateInputData();
            }

            /**
             * Called whenever the checkbox value changes
             */
            onChange() {
                if (this.isHandled('onChange')) {
                    this._handlers['onChange'](this._checked, this);
                }
            }

            /**
             * Redraw checkbox
             */
            refresh() {
                if (this.contents && this._onImage) {
                    this.contents.clear();
                    this.drawContents();
                }
            }

            /**
             */
            drawContents() {
                this.changeTextColor(Knight.COLOR.BROWN);
                if (this.checked()) {
                    this.drawGui(this._onImage, 0, 0);
                } else {
                    this.drawGui(this._offImage, 0, 0);
                }
                if (this.isMouseInsideFrame() && this._hoverImage) {
                    this.drawGui(this._hoverImage, 0, 0);
                    this.changeTextColor(Knight.COLOR.SEPIA);
                }
                const drawX = ImageManager.loadGui(this._onImage).width + 10;
                this.contents.drawText(this._label, drawX, 0, this.contents.width, this.contents.height, 'left');
            }
        };
    }

    // =============================================================================
    // Knight.EditorDropDownList
    // =============================================================================
    Knight.EditorDropDownList = class extends Knight.Window_Selectable {
        /**
         * Creates an instance of Knight.DropDownList.
         *
         * @param {number} x            Window x coordinate
         * @param {number} y            Window y coordinate
         * @param {number} w            Window width
         * @param {number} h            Window height
         * @param {Array<String>} data  Display text items
         */
        constructor(x, y, w, h, data) {
            super(x, y, w, h);
            this._data = data;
            this._useMouseInput = true;
            this.refresh();
        }

        /**
         * @return {Number}
         */
        maxItems() {
            return this._data ? this._data.length : 0;
        };

        /**
         * @return {Number}
         */
        static itemHeight() {
            return 24;
        }

        /**
         * @return {Number}
         */
        itemHeight() {
            return Knight.EditorDropDownList.itemHeight();
        }

        /**
         * @return {Number}
         */
        lineHeight() {
            return Knight.EditorDropDownList.itemHeight();
        }

        /**
         * Called whenever any value changes
         */
        onChange() {
            if (this.isHandled('onChange')) {
                this._handlers['onChange']();
            }
        }

        /**
         * Selects an item.
         * @param {number} index     Item selection index
         */
        select(index) {
            if (index !== this.index()) {
                super.select(index);
                this.refresh();
            }
        };

        /**
         * Redraw button
         */
        refresh() {
            if (this.contents) {
                this.contents.clear();
                this.resetFontSettings();
                this.makeItemRects();
                this.drawOutline();
                this.drawAllItems();
            }
        }

        /**
         */
        drawOutline() {
            this.contents.fillRect(0, 0, this.contents.width, this.contents.height, Knight.COLOR.BLACK);
        }

        /**
         */
        resetFontSettings() {
            Knight.Window_Base.prototype.resetFontSettings.call(this);
            this.contents.fontBold = false;
            this.contents.fontSize = 16;
        };

        /**
         * @param {Number} index
         */
        drawItem(index) {
            const rect = this.itemRect(index);
            const isSelected = this._index === index;
            const text = this._data[index];

            // Background
            if (isSelected) {
                this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.DARK_GREY);
            }

            // Text
            const ty = rect.y + (rect.height - this.lineHeight()) / 2;
            const margin = 20;
            const textColor = isSelected ? Knight.COLOR.GREY : Knight.COLOR.WHITE;
            this.changeTextColor(textColor);
            this.contents.drawText(text, rect.x + margin, ty, rect.width - margin, this.lineHeight(), "left");
        }
    };

    // =============================================================================
    // KNT_EditorDropDown
    // =============================================================================
    Knight.EditorDropDown = class extends Knight.Button {
        /**
         * Creates an instance of Knight.EditorDropDown
         *
         * @param {number} x            Window x coordinate
         * @param {number} y            Window y coordinate
         * @param {number} w            Window width
         * @param {Array} values        Possible drop-down values.
         * @param {Object} defaultIndex Drop-down default value
         * @param {string} label        Drop-down displayed title.
         * @param {number} maxItems     Max number of options displayed
         */
        constructor(x, y, w, values, defaultIndex = 0, label = null, maxItems = 10) {
            const h = 24 + (label ? 30 : 0);
            super(x, y, null, null, null, w, h);
            this._arrowBitmap = ImageManager.loadGui("EditorDropDownArrow");
            this._dropOpen = false;
            this._values = values;
            this._index = defaultIndex;
            this._label = label;
            this._labelHeight = label ? 30 : 0;
            this._maxItems = maxItems;
            this.createWindows();
            this.toggle(false);
            this.setHandler('ok', this.toggle.bind(this));
        }

        /**
         * Plays a sound when the checkbox is interacted with.
         */
        playOkSound() {
        };

        /**
         * @return {number} Height of the drop-down box, without including the label
         */
        boxHeight() {
            return 24;
        }

        /**
         */
        createWindows() {
            const dx = this.x;
            const dy = this.y + this.height;
            const dw = this.width;
            const dh = Math.min(this._maxItems, this._values.length) * Knight.EditorDropDownList.itemHeight();
            this._list = new Knight.EditorDropDownList(dx, dy, dw, dh, this._values);
            this._list.setHandler('ok', this.select.bind(this));

            this._arrow = new Knight.Button(
                this.x + this.width - this._arrowBitmap.width - 2,
                this.y + this._labelHeight + ((this.boxHeight() - this._arrowBitmap.height) / 2),
                "EditorDropDownArrow", "EditorDropDownArrow_Hover",
            );
            this._arrow.setHandler('ok', this.toggle.bind(this, null));
        }

        /**
         * Repositions child windows. Called when X coordinate changes.
         */
        reposition() {
            this._arrow.x = this.x + this.contents.width - this._arrowBitmap.width - 20;
            this._list.x = this.x;
        }

        /**
         * Adds child windows to parent scene's window layer.
         * @param {Scene_Base} scene    Parent scene
         */
        addWindowsToScene(scene) {
            scene.addChild(this._list);
            scene.addChild(this._arrow);
        };

        /**
         * Shows the window and all child windows.
         */
        show() {
            super.show();
            this._arrow.show();
        }

        /**
         * Hides the window and all child windows.
         */
        hide() {
            if (this._dropOpen) this.toggle(false);
            super.hide();
            this._list.hide();
            this._arrow.hide();
        }

        /**
         * Shows the window and all child windows.
         */
        enable() {
            super.enable();
            this._arrow.enable();
        }

        /**
         * Disable the window and all child windows.
         */
        disable() {
            super.disable();
            this._list.disable();
            this._arrow.disable();
        }

        /**
         * Open/close dropdown
         * @param {boolean} state   True to open, false to close. Null toggles from current state.
         */
        toggle(state = null) {
            if (state !== null) this._dropOpen = state;
            else this._dropOpen = !this._dropOpen;
            if (this._dropOpen) {
                this._list.show();
                this._list.enable();
            } else {
                this._list.hide();
                this._list.disable();
            }
            this.updateInputData();
            this.refresh();
        }

        /**
         * Selects an option and closes the dropdown.
         */
        select() {
            this._index = this._list._index;
            this.toggle(false);
            this.onChange();
        }

        /**
         * Called whenever the dropdown value changes
         */
        onChange() {
            if (this.isHandled('onChange')) {
                this._handlers['onChange']();
            }
        }

        /**
         * @return {Object}            Current drop-down property value
         */
        value() {
            return (this._values) ? this._values[this._index] : null;
        }

        /**
         * @param {String} value
         */
        setValue(value) {
            const index = this._values.indexOf(value);
            if (index > -1) this._index = index;
            this.toggle(false);
            this.refresh();
        }

        /**
         * Frame update
         */
        update() {
            Window_Base.prototype.update.call(this);
            this.processHover();
            this.processTouch();
        }

        /**
         * Handle mouse input
         */
        processTouch() {
            if (this.isOpenAndEnabled() && !this._dropOpen) {
                if (TouchInput.isReleased() && this.isMouseInsideFrame()) {
                    this.onTouch();
                }
            }
        }

        /**
         * Redraw checkbox
         */
        refresh() {
            if (this.contents && this.value() != null) {
                this.createContents();
                this.drawOutline();
                this.drawContents();
            }
        }

        /**
         */
        drawOutline() {
            const oy = this._labelHeight;
            const oh = this.height - this._labelHeight;
            this.contents.fillRect(0, oy, this.contents.width, oh, Knight.COLOR.MID_GREY);
            this.contents.fillRect(1, oy + 1, this.contents.width - 2, oh - 2, Knight.COLOR.BLACK);
        }


        /**
         */
        drawContents() {
            this.contents.fontFace = Knight.Param.KEPanelFont;
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.changeTextColor(Knight.COLOR.LIGHT);
            if (this._label) {
                this.contents.drawText(this._label, 10, 0, this.contents.width, this.lineHeight(), 'left');
            }
            const text = this.value();
            const y = this._labelHeight + (this.boxHeight() - this.lineHeight()) / 2;
            this.contents.drawText(text, 30, y, this.contents.width, this.lineHeight(), 'left');
        }
    };

    // =============================================================================
    // EditorSwitchBox
    // =============================================================================
    Knight.EditorSwitchBox = class extends Knight.Window_Selectable {
        /**
         * Creates an instance of Knight.EditorSwitchBox.
         *
         * @param {number} x            Window x coordinate
         * @param {number} y            Window y coordinate
         * @param {number} w            Window width
         * @param {number} h            Window height
         */
        constructor(x, y, w, h) {
            super(x, y, w, h);
            this._data = [];
            this._useMouseInput = true;
            this.makeButtons();
            this.refresh();
        }

        /**
         * @return {Number}
         */
        maxItems() {
            return this._data ? this._data.length : 0;
        };

        /**
         * @return {Number}
         */
        itemHeight() {
            return Knight.EditorSwitchBox.itemHeight();
        }

        /**
         * @return {Number}
         */
        static itemHeight() {
            return 24;
        }

        /**
         * @return {Number}
         */
        lineHeight() {
            return this.contents.fontSize;
        }

        /**
         * @return {Array<Object>}
         */
        values() {
            return this._data;
        }

        /**
         * Adds child windows to parent scene's window layer.
         * @param {Scene_Base} scene    Parent scene
         */
        addWindowsToScene(scene) {
            this._buttons.forEach((w) => scene.addChild(w));
        };

        /**
         * Shows the window and all child windows.
         */
        show() {
            super.show();
            this._buttons.forEach((w) => w.show());
            this.updateButtonVisibility();
        }

        /**
         * Hides the window and all child windows.
         */
        hide() {
            super.hide();
            this._buttons.forEach((w) => w.hide());
        }

        /**
         * Shows the window and all child windows.
         */
        enable() {
            super.enable();
            this._buttons.forEach((w) => w.enable());
        }

        /**
         * Disable the window and all child windows.
         */
        disable() {
            super.disable();
            this._buttons.forEach((w) => w.disable());
        }

        /**
         */
        makeButtons() {
            this._buttonGroups = [];
            this._buttons = [];
            for (let i = 0; i < this.maxPageRows(); i++) {
                const rect = this.itemRect(i);
                const x = this.x + rect.x;
                const y = this.y + rect.y;
                const deleteButton = new Knight.Button(x + 2, y + 2, 'Close', 'Close_Hover');
                const switchButton = new Knight.Checkbox(x + rect.width - 60, y + 3, 'Switch_On', 'Switch_Off', null, true);
                deleteButton.setHandler('ok', this.remove.bind(this, i));
                switchButton.setHandler('onChange', this.toggle.bind(this, i, switchButton));
                this._buttons.push(deleteButton);
                this._buttons.push(switchButton);
                this._buttonGroups[i] = [];
                this._buttonGroups[i].push(deleteButton);
                this._buttonGroups[i].push(switchButton);
            }
        }

        /**
         */
        updateButtonVisibility() {
            const visibleRows = Math.min(this._data.length, this.maxPageRows());
            for (let i = 0; i < this._buttonGroups.length; ++i) {
                const visible = this.visible && (i < visibleRows);
                for (let j = 0; j < this._buttonGroups[i].length; ++j) {
                    this._buttonGroups[i][j].visible = visible;
                }
            }
        }

        /**
         */
        updateButtonState() {
            const topIndex = this.topIndex();
            for (let i = 0; i < this.maxPageItems(); ++i) {
                const index = topIndex + i;
                const checkbox = this._buttonGroups[i][1];
                if (index < this.maxItems()) {
                    this._data[index].value ? checkbox.check() : checkbox.uncheck();
                }
            }
        }

        /**
         * @param {*} id
         * @param {*} name
         * @param {*} value
         * @return {Object}
         */
        makeData(id, name, value) {
            return {name: name, id: id, value: value};
        }

        /**
         * @param {*} id
         * @param {*} name
         * @param {*} value
         */
        add(id, name, value) {
            const element = this.get(id);
            if (element) {
                element.name = name;
                element.value = value;
                this.refresh();
                this.onChange();
            } else {
                this._data.push(this.makeData(id, name, value));
                this.refresh();
                this.onChange();
            }
        }

        /**
         * @param {Array<Object>} data
         */
        set(data) {
            this._data = data;
            this.setTopRow(0);
            this.refresh();
            this.onChange();
        }

        /**
         * @param {*} id
         * @return {Object}
         */
        get(id) {
            const index = this._data.findIndex(function(element) {
                return element.id === id;
            });
            return index === -1 ? null : this._data[index];
        }

        /**
         * @param {Number} visibleIndex
         */
        remove(visibleIndex) {
            const index = this.topIndex() + visibleIndex;
            if (this._data[index]) {
                this._data.splice(index, 1);
                this.scrollUp();
                this.refresh();
                this.onChange();
            }
        }

        /**
         * @param {Number} visibleIndex
         * @param {Checkbox} checkbox
         */
        toggle(visibleIndex, checkbox) {
            const index = this.topIndex() + visibleIndex;
            if (this._data[index]) {
                this._data[index].value = checkbox.checked();
                this.onChange();
            }
        }

        /**
         * Called whenever any value changes
         */
        onChange() {
            if (this.isHandled('onChange')) {
                this._handlers['onChange']();
            }
        }

        /**
         * Selects an item.
         * @param {number} index     Item selection index
         */
        select(index) {
            if (index !== this.index()) {
                super.select(index);
                this.refresh();
            }
        };

        /**
         * Redraw button
         */
        refresh() {
            if (this.contents) {
                this.contents.clear();
                this.resetFontSettings();
                this.makeItemRects();
                this.drawOutline();
                this.drawAllItems();
                this.updateButtonVisibility();
                this.updateButtonState();
            }
        }

        /**
         */
        drawOutline() {
            this.contents.fillRect(0, 0, this.contents.width, this.contents.height, Knight.COLOR.MID_GREY);
            this.contents.fillRect(1, 1, this.contents.width-2, this.contents.height-2, Knight.COLOR.BLACK);
        }

        /**
         */
        resetFontSettings() {
            Knight.Window_Base.prototype.resetFontSettings.call(this);
            this.contents.fontBold = false;
            this.contents.fontSize = 16;
        };

        /**
         * @param {Number} index
         */
        drawItem(index) {
            const rect = this.itemRect(index);
            const isSelected = this._index === index;
            const data = this._data[index];
            const text = `${data.id.padZero(4)}: ${data.name}`;

            // Background
            if (isSelected) {
                this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.NIGHT);
            }

            // Text
            const ty = rect.y + (rect.height - this.lineHeight()) / 2;
            const bw = this._buttonGroups[0][0].width;
            const margin = bw + 8;
            const textColor = isSelected ? Knight.COLOR.GREY : Knight.COLOR.WHITE;
            this.changeTextColor(textColor);
            this.contents.drawText(text, rect.x + margin, ty, rect.width - margin, this.lineHeight(), "left");
        }
    };

    // =============================================================================
    // EditorPartyBox
    // =============================================================================
    Knight.EditorPartyBox = class extends Knight.EditorSwitchBox {
        /**
         * @return {Number}
         */
        itemHeight() {
            return 60;
        }

        /**
         */
        makeButtons() {
            this._buttonGroups = [];
            this._buttons = [];
            const deleteHeight = 24;
            const switchHeight = 18;
            for (let i = 0; i < this.maxPageRows(); i++) {
                const rect = this.itemRect(i);
                const dx = this.x + rect.x + 2;
                const sx = this.x + rect.x + rect.width - 60;
                const dy = this.y + rect.y + (rect.height - deleteHeight)/2 + 2;
                const sy = this.y + rect.y + (rect.height - switchHeight)/2;
                const deleteButton = new Knight.Button(dx, dy, 'Close', 'Close_Hover');
                const switchButton = new Knight.Checkbox(sx, sy, 'Switch_On', 'Switch_Off', null, true);
                deleteButton.setHandler('ok', this.remove.bind(this, i));
                switchButton.setHandler('onChange', this.toggle.bind(this, i, switchButton));
                this._buttons.push(deleteButton);
                this._buttons.push(switchButton);
                this._buttonGroups[i] = [];
                this._buttonGroups[i].push(deleteButton);
                this._buttonGroups[i].push(switchButton);
            }
        }

        /**
         * @param {Number} index
         */
        drawItem(index) {
            const rect = this.itemRect(index);
            const isSelected = this._index === index;
            const data = this._data[index];
            const text = data.name;

            // Background
            if (isSelected) {
                this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.NIGHT);
            }

            // Character
            const actor = $dataActors[data.id];
            const charHeight = 48;
            const cx = rect.x + charHeight;
            const cy = rect.y + (rect.height - charHeight) / 2 + charHeight;
            this.drawCharacter(actor.characterName, actor.characterIndex, cx, cy);

            // Text
            const tx = cx + 30;
            const ty = rect.y + (rect.height - this.lineHeight()) / 2;
            const textColor = isSelected ? Knight.COLOR.GREY : Knight.COLOR.WHITE;
            this.changeTextColor(textColor);
            this.contents.drawText(text, tx, ty, rect.width - tx, this.lineHeight(), "left");
        }
    };

    // =============================================================================
    // Knight.COLOR
    // =============================================================================
    /* eslint-disable key-spacing */
    if (!Knight.COLOR) {
        Knight.COLOR = {
            WHITE:          '#ffffff',
            LIGHT:          '#fefefe',
            GREY:           '#A9A8A9',
            MID_GREY:       '#343232',
            DARK_GREY:      '#282729',
            NIGHT:          '#222222',
            DARK:           '#111111',
            BLACK:          '#000000',

            BROWN:          '#a58b76',
            SEPIA:          '#e4cbb7',
            ROSE:           '#c85c5c',
            DARK_ROSE:      '#963232',

            PASTEL_GREEN:   '#a3d39c',
            LIGHT_GREEN:    '#7fff7f',
            GREEN:          '#00a651',
            DARK_GREEN:     '#32965e',
            AQUA:           '#00a99d',
            LIGHT_BLUE:     '#00bff3',
            PASTEL_BLUE:    '#0072bc',
            NAVY_BLUE:      '#0054a6',
            DARK_BLUE:      '#325396',
            BLUE:           '#0000ff',

            YELLOW:         '#fff200',
            DARK_YELLOW:    '#ffd800',
            LIGHT_ORANGE:   '#f7941d',
            ORANGE:         '#ff7f00',
            DARK_ORANGE:    '#ff4000',
            RED:            '#ff0000',
            IMPERIAL_RED:   '#ed1c24',
            BLOOD_RED:      '#bf0000',
            BRIGHT_PINK:    '#ed145b',
            LIGHT_PINK:     '#f06eaa',
            HOT_PINK:       '#ff00d8',
            PINK:           '#ff00ff',
            PASTEL_PURPLE:  '#a864a8',
        };
    }
    /* eslint-enable key-spacing */

    // =============================================================================
    // Knight.Container
    // =============================================================================
    if (!Knight.Container) {
        Knight.Container = class extends PIXI.Container {
            /**
             */
            update() {
                let i = this.children.length;
                while (i--) {
                    if (this.children[i].update) {
                        this.children[i].update();
                    }
                }
            }
        };
    }
}
