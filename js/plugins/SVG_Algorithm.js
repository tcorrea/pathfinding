// =============================================================================
// SVG_Algorithm.js
// Version:     1.0
// Date:        1/11/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class ENLSVGAlgorithm
 */
Knight.PATH.ENLSVGAlgorithm = class extends Knight.PATH.AStarStaticMemory { // eslint-disable-line no-unused-vars
    /**
     * Creates an instance of ENLSVGAlgorithm.
     * @param {*} graph
     * @param {*} sx
     * @param {*} sy
     * @param {*} ex
     * @param {*} ey
     * @memberof ENLSVGAlgorithm
     */
    constructor(graph, sx, sy, ex, ey) {
        super(graph, sx, sy, ex, ey);
        this._visibilityGraph = null;
        this._levelLimit = Number.MAX_VALUE;
        this._reuseGraph = false;
    }

    /**
     * @param {GridGraph} graph
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @return {ENLSVGAlgorithm}
     * @memberof ENLSVGAlgorithm
     */
    static graphReuse(graph, sx, sy, ex, ey) {
        const algo = new Knight.PATH.ENLSVGAlgorithm(graph, sx, sy, ex, ey);
        algo._reuseGraph = true;
        return algo;
    }

    /**
     * @static
     * @param {Number} levelLimit
     * @return {Function}
     * @memberof ENLSVGAlgorithm
     */
    static withLevelLimit(levelLimit) {
        return function(graph, sx, sy, ex, ey) {
            const algo = new Knight.PATH.ENLSVGAlgorithm(graph, sx, sy, ex, ey);
            algo._reuseGraph = true;
            algo._levelLimit = levelLimit;
            return algo;
        };
    }

    /**
     * @return {ENLSVG}
     * @memberof ENLSVGAlgorithm
     */
    getVisibilityGraph() {
        return this._visibilityGraph;
    }

    /**
     * @memberof ENLSVGAlgorithm
     */
    computePath() {
        this.setupVisibilityGraph();
        const size = this._visibilityGraph.size();
        const memorySize = this._visibilityGraph.maxSize();
        this._pq = new Knight.PATH.Heap(size, memorySize);
        this.initializeMemory(memorySize, Number.POSITIVE_INFINITY, -1, false);

        this.initialize(this._visibilityGraph.startNode());
        const finish = this._visibilityGraph.endNode();

        if (this._graph.lineOfSight(this._sx, this._sy, this._ex, this._ey)) {
            if (this._sx !== this._ex || this._sy !== this._ey) {
                this.setParent(finish, this._visibilityGraph.startNode());
            }
            return;
        }

        while (!this._pq.isEmpty()) {
            const current = this._pq.popMinIndex();
            this.setVisited(current, true);
            this.resolveSkipEdgeNextNode(current);

            if (current === finish) {
                break;
            }

            const outgoingEdges = this._visibilityGraph.outgoingEdges[current];
            const outgoingEdgeIndexes = this._visibilityGraph.outgoingEdgeIndexes[current];

            // Scan through marked edges to neighbours
            const nMarkedEdges = this._visibilityGraph.nMarkedEdges[current];
            const outgoingMarkedEdgeIndexes = this._visibilityGraph.outgoingMarkedEdgeIndexes[current];

            for (let i = 0; i < nMarkedEdges; ++i) {
                const index = outgoingMarkedEdgeIndexes[i];
                const dest = outgoingEdges[index];
                const edgeIndex = outgoingEdgeIndexes[index];
                const weight = this._visibilityGraph.edgeWeights[edgeIndex];
                if (!Knight.PATH.Memory.visited(dest) && this.relax(current, dest, weight)) {
                    const destX = this._visibilityGraph.xPositions[dest];
                    const destY = this._visibilityGraph.yPositions[dest];

                    this._pq.decreaseKey(dest, this.distance(dest) + this._graph.distance(destX, destY, this._ex, this._ey));
                }
            }

            // Scan through skip edges to neighbours
            const nSkipEdges = this._visibilityGraph.nSkipEdges[current];
            const outgoingSkipEdges = this._visibilityGraph.outgoingSkipEdges[current];
            const outgoingSkipEdgeWeights = this._visibilityGraph.outgoingSkipEdgeWeights[current];
            const outgoingSkipEdgeNextNodes = this._visibilityGraph.outgoingSkipEdgeNextNodes[current];

            for (let i = 0; i < nSkipEdges; ++i) {
                const dest = outgoingSkipEdges[i];
                const nextNode = outgoingSkipEdgeNextNodes[i];
                const edgeWeight = outgoingSkipEdgeWeights[i];

                if (!Knight.PATH.Memory.visited(dest) && this.relaxViaSkipEdge(current, dest, nextNode, edgeWeight)) {
                    const destX = this._visibilityGraph.xPositions[dest];
                    const destY = this._visibilityGraph.yPositions[dest];

                    this._pq.decreaseKey(dest, this.distance(dest) + this._graph.distance(destX, destY, this._ex, this._ey));
                }
            }

            if (this._visibilityGraph.hasEdgeToGoal[current]) {
                const currX = this._visibilityGraph.xPositions[current];
                const currY = this._visibilityGraph.yPositions[current];

                const weight = this._graph.distance(currX, currY, this._ex, this._ey);
                if (this.relax(current, finish, weight)) {
                    this._pq.decreaseKey(finish, this.distance(finish));
                }
            }

            this.maybeSaveSearchSnapshot();
        }
        this.resolveFinalPath();
    }

    /**
     * @memberof ENLSVGAlgorithm
     */
    setupVisibilityGraph() {
        if (this._reuseGraph) {
            this._visibilityGraph = Knight.PATH.ENLSVG.initializeNew(this._graph, this._levelLimit);
        } else {
            Knight.PATH.ENLSVG.clearMemory();
            this._visibilityGraph = Knight.PATH.ENLSVG.initializeNew(this._graph, this._levelLimit);
        }

        if (this.isRecording()) {
            this._visibilityGraph.setSaveSnapshotFunction(this.saveVisibilityGraphSnapshotFalse);
            this.saveVisibilityGraphSnapshot(false);
            this._visibilityGraph.addStartAndEnd(this._sx, this._sy, this._ex, this._ey);
            this.saveVisibilityGraphSnapshot(false);
            this.saveVisibilityGraphSnapshot(true);
        } else {
            this._visibilityGraph.addStartAndEnd(this._sx, this._sy, this._ex, this._ey);
        }
    }

    /**
     * @param {Number} u
     * @param {Number} v
     * @param {Number} weightUV
     * @return {Boolean}
     * @memberof ENLSVGAlgorithm
     */
    relax(u, v, weightUV) {
        // return true iff relaxation is done
        const newWeight = this.distance(u) + weightUV;

        if (newWeight < this.distance(v)) {
            let p = this.parent(u);
            if (p !== -1) {
                const xPositions = this._visibilityGraph.xPositions;
                const yPositions = this._visibilityGraph.yPositions;
                p = this.getNextNodeIndex(p);
                const x1 = xPositions[p];
                const y1 = yPositions[p];
                const x2 = xPositions[u];
                const y2 = yPositions[u];
                const x3 = xPositions[v];
                const y3 = yPositions[v];

                if (!this._graph.isTaut(x1, y1, x2, y2, x3, y3)) return false;
            }
            this.setDistance(v, newWeight);
            this.setParent(v, u);
            return true;
        }
        return false;
    }

    /**
     * Skip Edge Parents
     *
     *            .--'''''-. _ n
     *          .'          (_)
     *        _/               '-._
     *       (_)                 (_) v
     *      u
     *
     * We use a hack to store the parent vertices of skip edges.
     * Non-skip-edge parents are stored with an index >= 0.
     *
     * For skip edge parents,  [Suppose we are relaxing v from u, and the node before v ("next node") is n]
     *
     *   When v is first relaxed, we store u + MAX_INT as v's parent.
     *   When v is generated (visited), we replace v's parent with n + MAX_INT
     *
     * Thus when checking taut paths, vertex n will be used instead.
     *
     * When setting up the final path, the path if Level-W edges from v -> n will be followed till the next skip node (u).
     *
     * @param {Number} u
     * @param {Number} v
     * @param {Number} nextNode
     * @param {Number} weightUV
     * @return {Boolean}
     * @memberof ENLSVGAlgorithm
     */
    relaxViaSkipEdge(u, v, nextNode, weightUV) {
        // return true iff relaxation is done
        const newWeight = this.distance(u) + weightUV;
        if (newWeight < this.distance(v)) {
            let p = this.parent(u);
            if (p !== -1) {
                p = this.getNextNodeIndex(p);
                const x1 = this._visibilityGraph.xPositions[p];
                const y1 = this._visibilityGraph.yPositions[p];
                const x2 = this._visibilityGraph.xPositions[u];
                const y2 = this._visibilityGraph.yPositions[u];
                const x3 = this._visibilityGraph.xPositions[nextNode];
                const y3 = this._visibilityGraph.yPositions[nextNode];

                if (!this._graph.isTaut(x1, y1, x2, y2, x3, y3)) return false;
            }
            this.setDistance(v, newWeight);
            this.setParent(v, u + (-Number.MAX_SAFE_INTEGER));
            return true;
        }
        return false;
    }

    /**
     * @param {Number} v
     * @memberof ENLSVGAlgorithm
     */
    resolveSkipEdgeNextNode(v) {
        let parent = Knight.PATH.Memory.parent(v);
        if (parent >= -1) return;
        parent -= (-Number.MAX_SAFE_INTEGER);
        const nSkipEdges = this._visibilityGraph.nSkipEdges[v];
        const outgoingSkipEdges = this._visibilityGraph.outgoingSkipEdges[v];

        for (let i = 0; i < nSkipEdges; ++i) {
            if (outgoingSkipEdges[i] === parent) {
                Knight.PATH.Memory.setParent(v, this._visibilityGraph.outgoingSkipEdgeNextNodes[v][i] + (-Number.MAX_SAFE_INTEGER));
                return;
            }
        }
        throw new Error('SHOULD NOT REACH HERE!');
    }

    /**
     * @param {Number} p
     * @return {Number}
     * @memberof ENLSVGAlgorithm
     */
    getNextNodeIndex(p) {
        return (p >= -1) ? p : (p - (-Number.MAX_SAFE_INTEGER));
    }

    /**
     * @memberof ENLSVGAlgorithm
     */
    resolveFinalPath() {
        let current = this._visibilityGraph.endNode();
        let previous = -1;
        const edgeLevels = this._visibilityGraph.edgeLevels;

        while (current !== -1) {
            if (current < -1) {
                current -= (-Number.MAX_SAFE_INTEGER);
                Knight.PATH.Memory.setParent(previous, current);

                if (this._visibilityGraph.nSkipEdges[current] !== 0) {
                    previous = current;
                    current = Knight.PATH.Memory.parent(current);
                    continue;
                }

                const nOutgoingEdges = this._visibilityGraph.nOutgoingEdges[current];
                const outgoingEdges = this._visibilityGraph.outgoingEdges[current];
                const outgoingEdgeIndexes = this._visibilityGraph.outgoingEdgeIndexes[current];

                let done = false;
                for (let i = 0; i < nOutgoingEdges; ++i) {
                    if (edgeLevels[outgoingEdgeIndexes[i]] !== Knight.PATH.ENLSVG.LEVEL_W) continue;
                    if (outgoingEdges[i] === previous) continue;

                    const next = outgoingEdges[i];
                    Knight.PATH.Memory.setParent(current, next + (-Number.MAX_SAFE_INTEGER));
                    done = true;
                    break;
                }
                if (!done) throw new Error("SS");
            }
            previous = current;
            current = Knight.PATH.Memory.parent(current);
        }
    }

    /**
     * @return {Number}
     * @memberof ENLSVGAlgorithm
     */
    pathLength() {
        let length = 0;
        let current = this._visibilityGraph.endNode();
        while (current !== -1) {
            current = this.parent(current);
            length++;
        }
        return length;
    }

    /**
     * @return {Array<Array<Number>>}
     * @memberof ENLSVGAlgorithm
     */
    getPath() {
        const length = this.pathLength();
        const path = new Array(length);
        let current = this._visibilityGraph.endNode();

        let index = length-1;
        while (current !== -1) {
            const x = this._visibilityGraph.xPositions[current];
            const y = this._visibilityGraph.yPositions[current];

            path[index] = [x, y];

            index--;
            current = this.parent(current);
        }
        return path;
    }

    /**
     * @return {Number}
     * @memberof ENLSVGAlgorithm
     */
    goalParentIndex() {
        return this._visibilityGraph.endNode();
    }

    /**
     * @param {Number} endIndex
     * @return {Array<Number>}
     * @memberof ENLSVGAlgorithm
     */
    snapshotEdge(endIndex) {
        let startIndex = this.parent(endIndex);
        if (startIndex < -1) {
            startIndex -= (-Number.MAX_SAFE_INTEGER);
            const nSkipEdges = this._visibilityGraph.nSkipEdges[endIndex];
            const outgoingSkipEdgeNextNodes = this._visibilityGraph.outgoingSkipEdgeNextNodes[endIndex];
            for (let i = 0; i < nSkipEdges; ++i) {
                if (outgoingSkipEdgeNextNodes[i] === startIndex) {
                    startIndex = this._visibilityGraph.outgoingSkipEdges[endIndex][i];
                    break;
                }
            }
        }

        const edge = [
            this._visibilityGraph.xPositions[startIndex],
            this._visibilityGraph.yPositions[startIndex],
            this._visibilityGraph.xPositions[endIndex],
            this._visibilityGraph.yPositions[endIndex],
        ];
        return edge;
    }

    /**
     * @param {Number} index
     * @return {Array<Number>}
     * @memberof ENLSVGAlgorithm
     */
    snapshotVertex(index) {
        if (this.selected(index)) {
            const edge = new Array[2];
            edge[0] = this._visibilityGraph.xPositions[index];
            edge[1] = this._visibilityGraph.yPositions[index];
            return edge;
        }
        return null;
    }

    /**
     * @param {Boolean} showMarked
     * @memberof ENLSVGAlgorithm
     */
    saveVisibilityGraphSnapshot(showMarked) {
        const size = this._visibilityGraph.size();
        snapshotItemList = new Array(size);

        const xPositions = this._visibilityGraph.xPositions;
        const yPositions = this._visibilityGraph.yPositions;

        sortedSnapshots = new TreeMap();
        for (let i = 0; i < size; ++i) {
            const x1 = xPositions[i];
            const y1 = yPositions[i];

            const nOutgoingEdges = this._visibilityGraph.nOutgoingEdges[i];
            const outgoingEdges = this._visibilityGraph.outgoingEdges[i];
            const outgoingEdgeIndexes = this._visibilityGraph.outgoingEdgeIndexes[i];

            for (let j = 0; j < nOutgoingEdges; ++j) {
                const neighbour = outgoingEdges[j];
                const edgeIndex = outgoingEdgeIndexes[j];
                x2 = xPositions[neighbour];
                y2 = yPositions[neighbour];

                const path = [x1, y1, x2, y2];

                let color = null;
                let colourIndex = null;

                if (showMarked && this._visibilityGraph.isMarked[edgeIndex]) {
                    colourIndex = 0;
                    color = Knight.PATH.ENLSVGAlgorithm._vertexColours[colourIndex];
                } else if (this._visibilityGraph.edgeLevels[edgeIndex] === Knight.PATH.ENLSVG.LEVEL_W) {
                    colourIndex = Knight.PATH.ENLSVGAlgorithm._vertexColours.length;
                    color = Knight.PATH.ENLSVGAlgorithm._levelWColour;
                } else {
                    colourIndex = Math.min(this._visibilityGraph.edgeLevels[edgeIndex], Knight.PATH.ENLSVGAlgorithm._vertexColours.length-1);
                    color = Knight.PATH.ENLSVGAlgorithm._vertexColours[colourIndex];
                }

                snapshotItem = Knight.PATH.SnapshotItem.generate(path, color);
                if (!sortedSnapshots.containsKey(colourIndex)) {
                    sortedSnapshots.put(colourIndex, []);
                }
                sortedSnapshots.get(colourIndex).push(snapshotItem);
            }

            vert = [x1, y1];

            snapshotItem = null;
            if (this._visibilityGraph.nSkipEdges[i] === 0) {
                // Regular vertex.
                snapshotItem = Knight.PATH.SnapshotItem.generate(vert, Knight.COLOR.BLUE);
            } else {
                snapshotItem = Knight.PATH.SnapshotItem.generate(vert, Knight.COLOR.WHITE);
            }
            snapshotItemList.push(snapshotItem);
        }

        const transfer = sortedSnapshots.remove(0);
        sortedSnapshots.put(Knight.PATH.ENLSVGAlgorithm._vertexColours.length+1, transfer);

        sortedSnapshots.forEach(function(list) {
            if (list !== null) {
                for (const item of list) {
                    snapshotItemList.push(item);
                }
            }
        }, this);

        for (let i = 0; i < size; ++i) {
            const x1 = xPositions[i];
            const y1 = yPositions[i];

            const nSkipEdges = this._visibilityGraph.nSkipEdges[i];
            const outgoingSkipEdges = this._visibilityGraph.outgoingSkipEdges[i];

            for (let j = 0; j < nSkipEdges; ++j) {
                const neighbour = outgoingSkipEdges[j];
                const x2 = xPositions[neighbour];
                const y2 = yPositions[neighbour];

                const path = [x1, y1, x2, y2];

                snapshotItem = Knight.PATH.SnapshotItem.generate(path, Knight.PATH.ENLSVGAlgorithm._skipEdgeColour);
                snapshotItemList.push(snapshotItem);
            }
        }
        this.addSnapshot(snapshotItemList);
    }

    /**
     * @memberof ENLSVGAlgorithm
     */
    printStatistics() {
        console.log(`Nodes: ${this._visibilityGraph.size()}`);
        console.log(`Edges: ${this._visibilityGraph.nEdges}`);

        // Print edge distribution
        const edgeLevels = this._visibilityGraph.edgeLevels;

        let maxLevel = 0;
        for (let i = 0; i < this._visibilityGraph.nEdges; ++i) {
            const level = edgeLevels[i];
            if (level !== Knight.PATH.ENLSVG.LEVEL_W && level > maxLevel) maxLevel = level;
        }

        let countLevelW = 0;
        const counts = new Array(maxLevel+1).fill(0);
        for (let i = 0; i < this._visibilityGraph.nEdges; ++i) {
            const level = edgeLevels[i];

            if (level === Knight.PATH.ENLSVG.LEVEL_W) {
                countLevelW++;
            } else {
                counts[level]++;
            }
        }

        for (let i = 0; i <= maxLevel; ++i) {
            console.log(`Level ${i} : ${counts[i]}`);
        }
        console.log(`Level W : ${countLevelW}`);
        console.log(`Skip Edges: ${this._visibilityGraph.computeNumSkipEdges()}`);
    }

    /**
     * @memberof ENLSVGAlgorithm
     */
    saveVisibilityGraphSnapshotFalse() {
        this.saveVisibilityGraphSnapshot(false);
    }
};

Knight.PATH.ENLSVGAlgorithm._skipEdgeColour = '#ff00ff';
Knight.PATH.ENLSVGAlgorithm._levelWColour = '#0000ff';
Knight.PATH.ENLSVGAlgorithm._vertexColours = [
    '#000000',
    '#7fff7f',
    '#ffd800',
    '#ff7f00',
    '#ff4000',
    '#ff0000',
    '#bf0000',
];
