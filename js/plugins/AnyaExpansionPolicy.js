// =============================================================================
// AnyaExpansionPolicy.js
// Version:     1.0
// Date:        1/27/2020
// Author:      Kaelan
//
// Expands a given vertex v (from a graph G) in order to generate
// its successors. The set of successors are iterated over
// using the functions ::next and ::hasNext.
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class ExpansionPolicy
 */
Knight.PATH.ExpansionPolicy = class {
    /**
     * Creates an instance of AnyaNode.
     * @param {BitpackedGrid} grid
     * @param {Boolean} prune
     * @memberof AnyaNode
     */
    constructor(grid, prune = true) {
        this._grid = grid;
        this._prune = prune; // reduces branching by eliminating nodes that cannot have successors
        this._successors = [];
        this._heuristic = new Knight.PATH.AnyaHeuristic;
        this._euclidean = new Knight.PATH.EuclideanDistanceHeuristic;
        this._start = null;
        this._target = null;
        this._idx_succ = null;
        this._cnode = null;
        this._csucc = null;
        this._tx = null; // the location of the target
        this._ty = null;
    }

    /**
     * Need to work with nodes, not vertices and edges
     * @param {AnyaNode} vertex
     */
    expand(vertex) {
        this._cnode = vertex;
        this._csucc = null;
        this._idx_succ = 0;
        this._successors = [];
        if (vertex.equals(this._start)) {
            this.generate_start_successors(this._cnode, this._successors);
        } else {
            this.generate_successors(this._cnode, this._successors);
        }
    }

    /**
     * @return {AnyaNode}
     */
    next() {
        this._csucc = null;
        if (this._idx_succ < this._successors.length) {
            this._csucc = this._successors[this._idx_succ++];
        }
        return this._csucc;
    }

    /**
     * @return {Boolean}
     */
    hasNext() {
        return this._idx_succ < this._successors.length;
    }

    /**
     * @return {Number}
     */
    step_cost() {
        if (this._cnode === null || this._csucc === null) throw new Error("ERROR!");
        return this._euclidean.h(this._cnode.root.x, this._cnode.root.y,
            this._csucc.root.x, this._csucc.root.y);
    }

    /**
     * @return {Heuristic}
     */
    heuristic() {
        return this._heuristic;
    }

    /**
     * We require that the cells (s.x, s.y) and (t.x, t.y) are
     * non-obstacle locations; i.e. the instances are valid
     * both for the corner graph that we search and also on
     * the cell-based graph representation of the grid.
     * We make this decision to keep compatibility with
     * Nathan Sturtevant's benchmarks (http://movingai.com)
     * i.e. every benchmark problem should be solvable and
     * any that isn't should also fail here
     *
     * @param {AnyaNode} start
     * @param {AnyaNode} target
     * @return {Boolean}
     */
    validate_instance_old(start, target) {
        this._start = start;
        this._target = target;
        this._tx = target.root.x;
        this._ty = target.root.y;
        return this._grid.get_cell_is_traversable(Math.trunc(start.root.x), Math.trunc(start.root.y)) &&
               this._grid.get_cell_is_traversable(Math.trunc(target.root.x), Math.trunc(target.root.y));
    }

    /**
     * Revised validation to validate cases only based on the corner graph.
     *
     * @param {AnyaNode} start
     * @param {AnyaNode} target
     * @return {Boolean}
     */
    validate_instance(start, target) {
        this._start = start;
        this._target = target;
        this._tx = target.root.x;
        this._ty = target.root.y;

        const startX = Math.trunc(start.root.x);
        const startY = Math.trunc(start.root.y);
        const targetX = Math.trunc(target.root.x);
        const targetY = Math.trunc(target.root.y);

        const startResult = this._grid.get_cell_is_traversable(startX, startY) ||
            this._grid.get_cell_is_traversable(startX-1, startY) ||
            this._grid.get_cell_is_traversable(startX, startY-1) ||
            this._grid.get_cell_is_traversable(startX-1, startY-1);

        const targetResult = this._grid.get_cell_is_traversable(targetX, targetY) ||
            this._grid.get_cell_is_traversable(targetX-1, targetY) ||
            this._grid.get_cell_is_traversable(targetX, targetY-1) ||
            this._grid.get_cell_is_traversable(targetX-1, targetY-1);

        return startResult && targetResult;
    }

    /**
     * @return {BitpackedGrid}
     */
    getGrid() {
        return this._grid;
    }

    /**
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     */
    generate_successors(node, retval) {
        const projection = new Knight.PATH.IntervalProjection;

        if (node.root.y === node.interval.getRow()) {
            projection.projectNode(node, this._grid);
            this.flat_node_obs(node, retval, projection);
            projection.project_f2cNode(node, this._grid);
            this.flat_node_nobs(node, retval, projection);
        } else {
            projection.projectNode(node, this._grid);
            this.cone_node_obs(node, retval, projection);
            this.cone_node_nobs(node, retval, projection);
        }
    }

    /**
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     */
    generate_start_successors(node, retval) {
        const check = node.interval.getLeft() === node.interval.getRight() &&
            node.interval.getLeft() === node.root.x &&
            node.interval.getRow() === node.root.y;
        if (!check) throw new Error("ERROR!");

        // certain successors will be ignored if the start is a double-corner
        const start_dc = this._grid.get_point_is_double_corner(Math.trunc(node.root.x), Math.trunc(node.root.y));

        // certain start locations are ambiguous; we don't try to solve these

        const rootx = Math.trunc(node.root.x);
        const rooty = Math.trunc(node.root.y);

        // generate flat observable successors left of the start point
        // NB: hacky implementation; we use a fake root for the projection
        const projection = new Knight.PATH.IntervalProjection;
        if (!start_dc) {
            projection.project(rootx, rootx, rooty, rootx+1, rooty, this._grid);
            this.generate_observable_flat__(projection, rootx, rooty, node, retval);
        }

        // generate flat observable successors right of the start point
        // NB: hacky implementation; we use a fake root for the projection
        projection.project(rootx, rootx, rooty, rootx-1, rooty, this._grid);
        this.generate_observable_flat__(projection, rootx, rooty, node, retval);

        // generate conical observable successors below the start point
        let max_left = this._grid.scan_cells_left(rootx-1, rooty)+1;
        let max_right = this._grid.scan_cells_right(rootx, rooty);
        if (max_left !== rootx && !start_dc) {
            this.split_interval_make_successors(max_left, rootx, rooty+1, rootx, rooty, rooty+1, node, retval);
        }
        if (max_right !== rootx) {
            this.split_interval_make_successors(rootx, max_right, rooty+1, rootx, rooty, rooty+1, node, retval);
        }

        // generate conical observable successors above the start point
        max_left = this._grid.scan_cells_left(rootx-1, rooty-1)+1;
        max_right = this._grid.scan_cells_right(rootx, rooty-1);
        if (max_left != rootx && !start_dc) {
            this.split_interval_make_successors(max_left, rootx, rooty-1, rootx, rooty, rooty-2, node, retval);
        }
        if (max_right != rootx) {
            this.split_interval_make_successors(rootx, max_right, rooty-1, rootx, rooty, rooty-2, node, retval);
        }
    }

    /**
     * @param {Number} max_left
     * @param {Number} max_right
     * @param {Number} irow
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {Number} sterile_check_row
     * @param {AnyaNode} parent
     * @param {Array<AnyaNode>} retval
     */
    split_interval_make_successors(max_left, max_right, irow, rootx, rooty, sterile_check_row, parent, retval) {
        if (max_left === max_right) return;

        let succ_left = max_right;
        let succ_right;
        const num_successors = retval.length;
        const target_node = this.contains_target(max_left, max_right, irow);
        const forced_succ = !this._prune || target_node;

        let successor = null;
        do {
            succ_right = succ_left;
            succ_left = this._grid.scan_left(succ_right, irow);
            if (forced_succ || !this.sterile(succ_left, succ_right, sterile_check_row)) {
                successor = new Knight.PATH.AnyaNode(parent,
                    new Knight.PATH.AnyaInterval(succ_left, succ_right, irow),
                    rootx, rooty);
                successor.interval.setLeft(succ_left < max_left ? max_left : succ_left);
                retval.push(successor);
            }
        } while ((succ_left !== succ_right) && (succ_left > max_left));

        // TODO: recurse over every node (NB: intermediate check includes goal check)
        // TODO: recurse until we start heading e.g. up instead of down (flat is ok)
        if (!forced_succ && retval.length === (num_successors+1) &&
            this.intermediate(successor.interval, rootx, rooty)) {
            retval.pop();
            // TODO: optimise this new call out?
            const proj = new Knight.PATH.IntervalProjection;
            proj.project_cone(
                successor.interval.getLeft(),
                successor.interval.getRight(),
                successor.interval.getRow(),
                rootx, rooty, this._grid);
            if (proj.valid && proj.observable) {
                this.split_interval_make_successors(proj.left, proj.right, proj.row,
                    rootx, rooty, proj.sterile_check_row, parent, retval);
            }
        }
    }

    /**
     * return true if the non-discrete points of the interval
     * [@param left, @param right] on @param row sit adjacent to
     * any obstacle cells.
     *
     * TODO: can we do this better/faster by keeping track of whether the
     * interval endpoints are open or closed? i.e. only generate
     * intervals with two closed endpoints and ignore the rest unless
     * they contain the start or goal. Should work because for every semi-open
     * interval (a, b] we can create two new intervals (a, b) and [b, c]
     *
     * @param {Number} left
     * @param {Number} right
     * @param {Number} row
     * @return {Boolean}
     */
    sterile(left, right, row) {
        const r = Math.trunc(right - BitpackedGrid.epsilon);
        const l = Math.trunc(left + BitpackedGrid.epsilon);
        return !(
            (this._grid.get_cell_is_traversable(l, row) &&
            this._grid.get_cell_is_traversable(r, row)));
    }

    /**
     * return true if the interval [@param left, @param right] on
     * has no adjacent successors on @param row.
     * NB: This code is not inside IntervalProjection because the
     * area inside a projection needs to be split into individual
     * intervals.
     *
     * @param {*} interval
     * @param {*} rootx
     * @param {*} rooty
     * @return {Boolean}
     */
    intermediate(interval, rootx, rooty) {
        // intermediate nodes have intervals that are not taut; i.e.
        // their endpoints are not adjacent to any location that cannot be
        // directly observed from the root.

        const left = interval.getLeft();
        const right = interval.getRight();
        const row = interval.getRow();

        const tmp_left = Math.trunc(left);
        const tmp_right = Math.trunc(right);
        const discrete_left = interval.discrete_left;
        const discrete_right = interval.discrete_right;

        const rightroot = ((tmp_right - rootx) >>> 31) === 1;
        const leftroot = ((rootx - tmp_left) >>> 31) === 1;
        let right_turning_point = false;
        let left_turning_point = false;

        if (rooty < row) {
            left_turning_point = discrete_left &&
                this._grid.get_point_is_corner(tmp_left, row) &&
                (!this._grid.get_cell_is_traversable(tmp_left-1, row-1) || leftroot);
            right_turning_point = discrete_right &&
                this._grid.get_point_is_corner(tmp_right, row) &&
                (!this._grid.get_cell_is_traversable(tmp_right, row-1) || rightroot);
        } else {
            left_turning_point = discrete_left &&
                this._grid.get_point_is_corner(tmp_left, row) &&
                (!this._grid.get_cell_is_traversable(tmp_left-1, row) || leftroot);
            right_turning_point = discrete_right &&
                this._grid.get_point_is_corner(tmp_right, row) &&
                (!this._grid.get_cell_is_traversable(tmp_right, row) || rightroot);
        }

        return !((discrete_left && left_turning_point) || (discrete_right && right_turning_point));
    }

    /**
     * @param {Number} left
     * @param {Number} right
     * @param {Number} row
     * @return {Boolean}
     */
    contains_target(left, right, row) {
        return (row == this._ty) &&
            (this._tx >= left - BitpackedGrid.epsilon) &&
            (this._tx <= right + BitpackedGrid.epsilon);
    }

    /**
     * TODO: assumes vertical move to the next row is always valid.
     * there is an inductive argument here: if the move is not valid
     * the node should have been pruned. check this is always true.
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     * @param {IntervalProjection} projection
     */
    cone_node_obs(node, retval, projection) {
        if (node.root.y === node.interval.getRow()) throw new Error("ERROR!");

        const root = node.root;
        this.generate_observable_cone__(projection, Math.trunc(root.x), Math.trunc(root.y), node, retval);
    }

    /**
     * @param {IntervalProjection} projection
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {AnyaNode} parent
     * @param {Array<AnyaNode>} retval
     */
    generate_observable_cone__(projection, rootx, rooty, parent, retval) {
        if (!(projection.valid && projection.observable)) return;
        this.split_interval_make_successors(projection.left, projection.right,
            Math.trunc(projection.row), rootx, rooty, projection.sterile_check_row, parent, retval);
    }

    /**
     * there are two kinds of non-observable successors
     * (i) conical successors that are adjacent to an observable projection
     * (ii) flat successors that are adjacent to the current interval
     * (iii) conical successors that are not ajdacent to to any observable
     * projection or the current interval (i.e the angle from the root
     * to the interval is too low to observe any point from the next row)
     * TODO: seems like too many branching statements in this function. consolidate?
     *
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     * @param {IntervalProjection} projection
     */
    cone_node_nobs(node, retval, projection) {
        if (!projection.valid) return;

        const ileft = node.interval.getLeft();
        const iright = node.interval.getRight();
        const irow = Math.trunc(node.interval.getRow());

        // non-observable successor type (iii)
        if (!projection.observable) {
            if (node.root.x > iright && node.interval.discrete_right && this._grid.get_point_is_corner(Math.trunc(iright), irow)) {
                this.split_interval_make_successors(
                    projection.max_left, iright, projection.row,
                    Math.trunc(iright), irow, projection.sterile_check_row, node, retval);
            } else if (node.root.x < ileft && node.interval.discrete_left && this._grid.get_point_is_corner(Math.trunc(ileft), irow)) {
                this.split_interval_make_successors(
                    ileft, projection.max_right, projection.row,
                    Math.trunc(ileft), irow, projection.sterile_check_row, node, retval);
            }

            // non-observable successors to the left of the current interval
            if (node.interval.discrete_left &&
                !this._grid.get_cell_is_traversable(Math.trunc(ileft)-1, projection.type_iii_check_row) &&
                this._grid.get_cell_is_traversable(Math.trunc(ileft)-1, projection.check_vis_row)) {
                projection.project_flat(ileft - this._grid.smallest_step_div2, ileft, Math.trunc(ileft), Math.trunc(irow), this._grid);
                this.generate_observable_flat__(projection, Math.trunc(ileft), irow, node, retval);
            }

            // non-observable successors to the right of the current interval
            if (node.interval.discrete_right &&
                !this._grid.get_cell_is_traversable(Math.trunc(iright), projection.type_iii_check_row) &&
                this._grid.get_cell_is_traversable(Math.trunc(iright), projection.check_vis_row)) {
                projection.project_flat(iright, iright + this._grid.smallest_step_div2, Math.trunc(iright), Math.trunc(irow), this._grid); // NB: dummy root
                this.generate_observable_flat__(projection, Math.trunc(iright), irow, node, retval);
            }

            return;
        }

        // non-observable successors type (i) and (ii)
        const flatprj = new Knight.PATH.IntervalProjection;
        const corner_row = irow - ((Math.trunc(node.root.y) - irow) >>> 31);

        // non-observable successors to the left of the current interval
        if (node.interval.discrete_left && this._grid.get_point_is_corner(Math.trunc(ileft), irow)) {
            // flat successors from the interval row
            if (!this._grid.get_cell_is_traversable(Math.trunc(ileft - 1), corner_row)) {
                flatprj.project(ileft - BitpackedGrid.epsilon, iright, Math.trunc(irow), Math.trunc(ileft), Math.trunc(irow), this._grid);
                this.generate_observable_flat__(flatprj, Math.trunc(ileft), irow, node, retval);
            }

            // conical successors from the projected row
            this.split_interval_make_successors(projection.max_left, projection.left, projection.row,
                Math.trunc(ileft), irow, projection.sterile_check_row, node, retval);
        }

        // non-observable successors to the right of the current interval
        if (node.interval.discrete_right && this._grid.get_point_is_corner(Math.trunc(iright), irow)) {
            // flat successors from the interval row
            if (!this._grid.get_cell_is_traversable(Math.trunc(iright), corner_row)) {
                flatprj.project(ileft, iright + BitpackedGrid.epsilon, Math.trunc(irow), Math.trunc(ileft), Math.trunc(irow), this._grid);
                this.generate_observable_flat__(flatprj, Math.trunc(iright), irow, node, retval);
            }

            // conical successors from the projected row
            this.split_interval_make_successors(projection.right, projection.max_right, projection.row,
                Math.trunc(iright), irow, projection.sterile_check_row, node, retval);
        }
    }

    /**
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     * @param {IntervalProjection} projection
     */
    flat_node_obs(node, retval, projection) {
        const root = node.root;
        this.generate_observable_flat__(projection, Math.trunc(root.x), Math.trunc(root.y), node, retval);
    }

    /**
     * @param {IntervalProjection} projection
     * @param {Number} rootx
     * @param {Number} rooty
     * @param {AnyaNode} parent
     * @param {Array<AnyaNode>} retval
     */
    generate_observable_flat__(projection, rootx, rooty, parent, retval) {
        if (projection.row !== rooty) throw new Error("ERROR");
        if (!projection.valid) return;

        let goal_interval = this.contains_target(projection.left, projection.right, projection.row);
        if (projection.intermediate && this._prune && !goal_interval) {
            // ignore intermediate nodes and project further along the row
            projection.project(projection.left, projection.right, projection.row, rootx, rooty, this._grid);

            // check if the projection contains the goal
            goal_interval = this.contains_target(projection.left, projection.right, projection.row);
        }

        if (!projection.deadend || !this._prune || goal_interval) {
            retval.push(new Knight.PATH.AnyaNode(parent,
                new Knight.PATH.AnyaInterval(projection.left, projection.right, projection.row),
                rootx, rooty));
        }
    }

    /**
     * @param {AnyaNode} node
     * @param {Array<AnyaNode>} retval
     * @param {IntervalProjection} projection
     */
    flat_node_nobs(node, retval, projection) {
        if (!projection.valid) return;

        let new_rootx;
        const new_rooty = node.interval.getRow();
        if (node.root.x <= node.interval.getLeft()) {
            new_rootx = Math.trunc(node.interval.getRight());
        } else {
            new_rootx = Math.trunc(node.interval.getLeft());
        }

        this.split_interval_make_successors(projection.left, projection.right, projection.row,
            new_rootx, new_rooty, projection.sterile_check_row, node, retval);
    }

    /**
     * @param {AnyaNode} n
     * @return {Number}
     */
    hash(n) {
        const x = Math.trunc(n.root.x);
        const y = Math.trunc(n.root.y);
        return y * this._grid.get_padded_width() + x;
    }
};
