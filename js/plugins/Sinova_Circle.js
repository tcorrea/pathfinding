var Sinova = Sinova || {}; // eslint-disable-line no-var

/**
 * A circle used to detect collisions
 * @class
 */
Sinova.Circle = class extends Sinova.Body {
    /**
     * @constructor
     * @param {Number} [x = 0] The starting X coordinate
     * @param {Number} [y = 0] The starting Y coordinate
     * @param {Number} [radius = 0] The radius
     * @param {Number} [scale = 1] The scale
     * @param {Number} [padding = 0] The amount to pad the bounding volume when testing for potential collisions
     */
    constructor(x = 0, y = 0, radius = 0, scale = 1, padding = 0) {
        super(x, y, padding);

        /** @private */
        this._circle = true;

        /**
         * @desc
         * @type {Number}
         */
        this.radius = radius;

        /**
         * @desc
         * @type {Number}
         */
        this.scale = scale;
    }

    /**
     * Draws the circle to a CanvasRenderingContext2D's current path
     * @param {CanvasRenderingContext2D} context The context to add the arc to
     */
    draw(context) {
        const radius = this.radius * this.scale;
        context.moveTo(radius, 0);
        context.arc(0, 0, radius, 0, Math.PI * 2);
    }

    /**
     * @return {Object} The shape's bounding box
     */
    getBounds() {
        const radius = this.radius * this.scale;
        return {
            min_x: this.x - radius,
            max_x: this.x + radius,
            min_y: this.y - radius,
            max_y: this.y + radius,
        };
    };
};
