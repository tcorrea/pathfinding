// =============================================================================
// Anya.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
// =============================================================================

//=============================================================================
/*:
 * @plugindesc
 * @version 1.0.0
 * @author Kaelan
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * Javascript implementation of the Anya16 algorithm.
 *
 * Anya is a fast any-angle pathfinding algorithm that doesn't require any
 * preprocessing. Anya16 refers to the improved version of the algorithm
 * developed by Daniel Harabor, as described in the following paper:
 * http://harabor.net/data/papers/hgoa-oaapip-16.pdf
 *
 * Based on the Java implementation by Daniel Harabor at:
 * https://github.com/Ohohcakester/Any-Angle-Pathfinding/tree/master/src/algorithms/anya16
 *
 * The original implementation is released under the MIT License.
 */

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class Anya
 */
Knight.PATH.Anya = class extends Knight.PATH.Algorithm {
    /**
     * Creates an instance of Anya.
     * @param {BitpackedGrid} grid
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @memberof Anya
     */
    constructor(grid, sx, sy, ex, ey) {
        super(grid, grid.width(), grid.height(), sx, sy, ex, ey);
        this._pathStartNode = null;
        this.currSnapshot = [];
        Knight.PATH.Anya.initialize(grid);

        Knight.PATH.Anya._anya.snapshotExpand = this.snapshotExpand;
        Knight.PATH.Anya._anya.snapshotInsert = this.snapshotInsert;
        Knight.PATH.Anya._anya.isRecording = false;

        const start = new Knight.PATH.AnyaNode(null, new Knight.PATH.AnyaInterval(0, 0, 0), 0, 0);
        const target = new Knight.PATH.AnyaNode(null, new Knight.PATH.AnyaInterval(0, 0, 0), 0, 0);
        Knight.PATH.Anya._anya.mb_start_ = start;
        Knight.PATH.Anya._anya.mb_target_ = target;

        start.root.set(sx, sy);
        start.interval.init(sx, sx, sy);
        target.root.set(ex, ey);
        target.interval.init(ex, ex, ey);
    }

    /**
     * @static
     * @param {BitpackedGrid} grid
     */
    static initialize(grid) {
        if (grid === Knight.PATH.Anya._storedGraph) return;
        try {
            Knight.PATH.Anya._anya = new Knight.PATH.AnyaSearch(new Knight.PATH.ExpansionPolicy(grid));
            Knight.PATH.Anya._storedGraph = grid;
        } catch (e) {
            console.log(e);
        }
        Knight.PATH.Anya._anya.isRecording = false;
    }

    /**
     * Call this to compute the path
     */
    computePath() {
        this._pathStartNode = Knight.PATH.Anya._anya.search(
            Knight.PATH.Anya._anya.mb_start_,
            Knight.PATH.Anya._anya.mb_target_,
        );
    }

    /**
     * @return {Array<Array<Number>>} retrieve the path computed by the algorithm
     */
    getPath() {
        let length = 0;
        let current = this._pathStartNode;
        while (current !== null) {
            current = current.getNext();
            ++length;
        }
        const path = new Array(length);

        current = this._pathStartNode;
        let i = 0;
        while (current !== null) {
            const p = current.getVertex().root;
            path[i] = [p.x, p.y];
            current = current.getNext();
            ++i;
        }
        return path;
    }

    /**
     * @return {Number} directly get path length without computing path.
     * Has to run fast, unlike getPath.
     */
    getPathLength() {
        return Knight.PATH.Anya._anya.mb_cost_;
    }

    /**
     */
    startRecording() {
        Knight.PATH.Algorithm.startRecording.call(this);
        Knight.PATH.Anya._anya.isRecording = true;
    }

    /**
     */
    stopRecording() {
        Knight.PATH.Algorithm.stopRecording.call(this);
        Knight.PATH.Anya._anya.isRecording = false;
    }

    /**
     * @param {AnyaNode} anyaNode
     */
    snapshotInsert(anyaNode) {
        const interval = anyaNode.interval;

        const line = new Array(7);
        line[0] = interval.getRow();
        line[1] = Math.trunc(interval.getLeft() * Knight.PATH.Anya._RES);
        line[2] = Knight.PATH.Anya._RES;
        line[3] = Math.trunc(interval.getRight() * Knight.PATH.Anya._RES);
        line[4] = Knight.PATH.Anya._RES;
        line[5] = Math.trunc(anyaNode.root.x);
        line[6] = Math.trunc(anyaNode.root.y);
        this.currSnapshot.add(Knight.PATH.SnapshotItem.generate(line));

        this.maybeSaveSearchSnapshot();
    }

    /**
     * @param {AnyaNode} anyaNode
     */
    snapshotExpand(anyaNode) {
        const interval = anyaNode.interval;

        const line = new Array(5);
        line[0] = interval.getRow();
        line[1] = Math.trunc(interval.getLeft() * Knight.PATH.Anya._RES);
        line[2] = Knight.PATH.Anya._RES;
        line[3] = Math.trunc(interval.getRight() * Knight.PATH.Anya._RES);
        line[4] = Knight.PATH.Anya._RES;
        this.currSnapshot.add(Knight.PATH.SnapshotItem.generate(line));

        this.maybeSaveSearchSnapshot();
    }

    /**
     * @return {Array<SnapshotItem>}
     */
    computeSearchSnapshot() {
        return this.currSnapshot.clone();
    }

    /**
     * @static
     */
    static clearMemory() {
        this._anya = null;
        this._storedGraph = null;
        global.gc();
    }
};

Knight.PATH.Anya._RES = 10000;
Knight.PATH.Anya._anya = null;
Knight.PATH.Anya._storedGraph = null;
