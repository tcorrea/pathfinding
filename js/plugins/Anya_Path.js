// =============================================================================
// Anya_Path.js
// Version:     1.0
// Date:        1/26/2020
// Author:      Kaelan
//
// Describes a path in a graph in terms of its constituent vertices
// and their associated cumulative cost. i.e. the cost to step from
// the start vertex to the current vertex, i.
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.Path = class {
    /**
     * Creates an instance of Path.
     * @param {AnyaNode} vertex
     * @param {Path} next
     * @param {Number} path_cost
     * @memberof Path
     */
    constructor(vertex, next, path_cost) {
        this.vertex = vertex;
        this.path_cost = path_cost;
        this.next = next;
        if (next !== null) next.prev = this;
        this.prev = null;
    }

    /**
     * @return {Number}
     */
    getPathCost() {
        return this.path_cost;
    }

    /**
     * @return {Path}
     */
    getNext() {
        return this.next;
    }

    /**
     * @return {Path}
     */
    getPrev() {
        return this.prev;
    }

    /**
     * @return {AnyaNode}
     */
    getVertex() {
        return this.vertex;
    }
};
