// =============================================================================
// SVG_LOSScanner.js
// Version:     1.0
// Date:        1/21/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Singleton. Do not make multiple simultaneous copies of this class or use in parallel code.
 * @class LineOfSightScanner
 */
Knight.PATH.LineOfSightScanner = class {
    /* eslint-disable require-jsdoc */
    // Double API
    toDouble(n, d) {
        return Knight.Utility.intDivision(n / d);
    }
    isLessThanOrEqual(a, b) {
        return a < b + Knight.PATH.LineOfSightScanner._EPSILON;
    }
    isLessThan(a, b) {
        return a < b - Knight.PATH.LineOfSightScanner._EPSILON;
    }
    isEqualTo(a, b) {
        return Math.abs(a - b) < Knight.PATH.LineOfSightScanner._EPSILON;
    }
    // These are mathematically wrong, but it's what the code in the reference implementation
    // uses. Fixing this causes undesired behavior, so I've ported them as-is.
    isWholeNumber(n) {
        return Math.abs(n - Math.trunc(n + 0.5)) < Knight.PATH.LineOfSightScanner._EPSILON;
    }
    floor(n) {
        return Math.trunc(n + Knight.PATH.LineOfSightScanner._EPSILON);
    }
    ceil(n) {
        return Math.trunc(n + 1 - Knight.PATH.LineOfSightScanner._EPSILON);
    }
    round(n) {
        return Math.trunc(n + 0.5);
    }
    /* eslint-enable require-jsdoc */

    /**
     * @static
     * @param {GridGraph} graph
     * @memberof LineOfSightScanner
     */
    static initializeExtents(graph) {
        // Don't reinitialise if graph is the same size as the last time.
        if (this._rightDownExtents !== null &&
            graph.sizeY+2 === this._rightDownExtents.length &&
            graph.sizeX+1 === this._rightDownExtents[0].length) {
            return;
        }

        this._rightDownExtents = new Array(graph.sizeY+2);
        this._leftDownExtents = new Array(graph.sizeY+2);
        for (let y = 0; y < graph.sizeY+2; ++y) {
            this._rightDownExtents[y] = new Array(graph.sizeX+1).fill(0);
            this._leftDownExtents[y] = new Array(graph.sizeX+1).fill(0);
        }
    }

    /**
     * @static
     * @memberof LineOfSightScanner
     */
    static initializeStack() {
        if (this._intervalStack !== null) return;
        this._intervalStack = new Array(11);
        this._intervalStackSize = 0;
    }

    /**
     * @static
     * @memberof LineOfSightScanner
     */
    static initialiseSuccessorList() {
        if (this._successorsX !== null) return;
        this._successorsX = new Array(11).fill(0);
        this._successorsY = new Array(11).fill(0);
        this._nSuccessors = 0;
    }

    /**
     * @static
     * @memberof LineOfSightScanner
     */
    static clearSuccessors() {
        this._nSuccessors = 0;
    }

    /**
     * @static
     * @param {LOSInterval} interval
     * @memberof LineOfSightScanner
     */
    static stackPush(interval) {
        if (this._intervalStackSize >= this._intervalStack.length) {
            this._intervalStack = Knight.arraysCopyOf(this._intervalStack, this._intervalStack.length*2);
        }
        this._intervalStack[this._intervalStackSize] = interval;
        ++this._intervalStackSize;

        // this.addToSnapshot(interval); //Uncomment for debugging
    }

    /**
     * @static
     * @param {LOSInterval} interval
     * @memberof LineOfSightScanner
     */
    static addToSnapshot(interval) {
        const RES = 100000;
        const xLn = Math.trunc(interval.xL*RES);
        const xRn = Math.trunc(interval*xR*RES);

        const path = [interval.y, xLn, RES, xRn, RES, this._snapshot_sx, this._snapshot_sy];
        this._snapshots.push(Knight.PATH.SnapshotItem.generate(path, Color.GREEN));
        this.snapshotList.push(this._snapshots.clone());
    }

    /**
     * @static
     * @memberof LineOfSightScanner
     */
    static clearSnapshots() {
        this.snapshotList = [];
        this._snapshots = [];
    }

    /**
     * @static
     * @return {LOSInterval}
     * @memberof LineOfSightScanner
     */
    static stackPop() {
        const temp = this._intervalStack[this._intervalStackSize-1];
        --this._intervalStackSize;
        this._intervalStack[this._intervalStackSize] = null;
        return temp;
    }

    /**
     * @static
     * @memberof LineOfSightScanner
     */
    static clearStack() {
        this._intervalStackSize = 0;
    }

    /**
     * @static
     * @param {Number} x
     * @param {Number} y
     * @memberof LineOfSightScanner
     */
    static addSuccessor(x, y) {
        if (this._nSuccessors >= this._successorsX.length) {
            this._successorsX = Knight.arraysCopyOf(this._successorsX, this._successorsX.length*2);
            this._successorsY = Knight.arraysCopyOf(this._successorsY, this._successorsY.length*2);
        }
        this._successorsX[this._nSuccessors] = x;
        this._successorsY[this._nSuccessors] = y;
        ++this._nSuccessors;
    }

    /**
     * Creates an instance of Knight.PATH.LineOfSightScanner.
     * @param {GridGraph} gridGraph
     * @memberof LineOfSightScanner
     */
    constructor(gridGraph) {
        Knight.PATH.LineOfSightScanner.initializeExtents(gridGraph);
        Knight.PATH.LineOfSightScanner.initialiseSuccessorList();
        Knight.PATH.LineOfSightScanner.initializeStack();

        this._graph = gridGraph;
        this._sizeX = this._graph.sizeX;
        this._sizeY = this._graph.sizeY;
        this.computeExtents();
    }

    /**
     * @memberof LineOfSightScanner
     */
    computeExtents() {
        // graph.isBlocked(x,y) is the same as graph.bottomLeftOfBlockedTile(x,y)
        Knight.PATH.LineOfSightScanner.initializeExtents(this._graph);

        for (let y = 0; y < this._sizeY+2; ++y) {
            let lastIsBlocked = true;
            let lastX = -1;
            for (let x = 0; x <= this._sizeX; ++x) {
                Knight.PATH.LineOfSightScanner._leftDownExtents[y][x] = lastX;
                if (this._graph.isBlocked(x, y-1) !== lastIsBlocked) {
                    lastX = x;
                    lastIsBlocked = !lastIsBlocked;
                }
            }
            lastIsBlocked = true;
            lastX = this._sizeX+1;
            for (let x = this._sizeX; x >= 0; --x) {
                Knight.PATH.LineOfSightScanner._rightDownExtents[y][x] = lastX;
                if (this._graph.isBlocked(x-1, y-1) !== lastIsBlocked) {
                    lastX = x;
                    lastIsBlocked = !lastIsBlocked;
                }
            }
        }
    }

    /**
     * Stores results in successorsX, successorsY and nSuccessors.
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    computeAllVisibleSuccessors(sx, sy) {
        Knight.PATH.LineOfSightScanner._snapshot_sx = sx;
        Knight.PATH.LineOfSightScanner._snapshot_sy = sy;
        Knight.PATH.LineOfSightScanner.clearSuccessors();
        Knight.PATH.LineOfSightScanner.clearStack();

        this.generateStartingStates(sx, sy);
        this.exploreStatesNonTaut(sx, sy);
    }

    /**
     * Stores results in successorsX, successorsY and nSuccessors.
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    computeAllVisibleTautSuccessors(sx, sy) {
        Knight.PATH.LineOfSightScanner._snapshot_sx = sx;
        Knight.PATH.LineOfSightScanner._snapshot_sy = sy;
        Knight.PATH.LineOfSightScanner.clearSuccessors();
        Knight.PATH.LineOfSightScanner.clearStack();

        this.generateStartingStates(sx, sy);
        this.exploreStates(sx, sy);
    }

    /**
     * Stores results in successorsX, successorsY and nSuccessors.
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    computeAllVisibleTwoWayTautSuccessors(sx, sy) {
        Knight.PATH.LineOfSightScanner._snapshot_sx = sx;
        Knight.PATH.LineOfSightScanner._snapshot_sy = sy;
        Knight.PATH.LineOfSightScanner.clearSuccessors();
        Knight.PATH.LineOfSightScanner.clearStack();

        this.generateTwoWayTautStartingStates(sx, sy);
        this.exploreStates(sx, sy);
    }

    /**
     * Stores results in successorsX, successorsY and nSuccessors.
     * We are moving in direction dx, dy
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} dx
     * @param {Number} dy
     * @memberof LineOfSightScanner
     */
    computeAllVisibleIncrementalTautSuccessors(sx, sy, dx, dy) {
        Knight.PATH.LineOfSightScanner._snapshot_sx = sx;
        Knight.PATH.LineOfSightScanner._snapshot_sy = sy;
        Knight.PATH.LineOfSightScanner.clearSuccessors();
        Knight.PATH.LineOfSightScanner.clearStack();

        this.generateIncrementalTautStartingStates(sx, sy, dx, dy);
        this.exploreStates(sx, sy);
    }

    /**
     * Assumption: We are at an outer corner. One of six cases:
     *   BR        BL        TR        TL       TRBL      TLBR
     * XXX|         |XXX      :         :         |XXX   XXX|
     * XXX|...   ...|XXX   ___:...   ...:___   ___|XXX   XXX|___
     *    :         :      XXX|         |XXX   XXX|         |XXX
     *    :         :      XXX|         |XXX   XXX|         |XXX
     *
     * Assumption: We are also entering from a taut direction.
     * dx > 0, dy > 0 : BR TL
     * dx > 0, dy < 0 : BL TR
     * dx < 0, dy < 0 : BR TL
     * dx < 0, dy > 0 : BL TR
     *
     * @param {*} sx
     * @param {*} sy
     * @param {*} dx
     * @param {*} dy
     * @memberof LineOfSightScanner
     */
    generateIncrementalTautStartingStates(sx, sy, dx, dy) {
        let rightwardsSearch = false;
        let leftwardsSearch = false;

        if (dx > 0) {
            // Moving rightwards
            if (dy > 0) {
                //    P
                //   /
                //  B
                const brOfBlocked = this._graph.bottomRightOfBlockedTile(sx, sy);
                const tlOfBlocked = this._graph.topLeftOfBlockedTile(sx, sy);

                const rightBound = this.rightUpExtent(sx, sy);
                let leftExtent;
                let rightExtent;

                if (brOfBlocked && tlOfBlocked) {
                    //  |
                    //  |___

                    leftExtent = sx;
                    rightExtent = rightBound;

                    rightwardsSearch = true;
                } else if (brOfBlocked) {
                    //  | /
                    //  |/

                    leftExtent = sx;
                    rightExtent = this.toDouble(sx*dy + dy, dy);
                    if (!this.isLessThanOrEqual(rightExtent, rightBound)) { // rightBound < rightExtent
                        rightExtent = rightBound;
                    }
                } else { // tlOfBlocked
                    //   /
                    //  /__

                    leftExtent = this.toDouble(sx*dy + dx, dy);
                    rightExtent = rightBound;

                    rightwardsSearch = true;
                }

                if (this.isLessThanOrEqual(leftExtent, rightExtent)) {
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }
            } else if (dy < 0) {
                //  B
                //   \
                //    P
                const trOfBlocked = this._graph.topRightOfBlockedTile(sx, sy);
                const blOfBlocked = this._graph.bottomLeftOfBlockedTile(sx, sy);

                const rightBound = this.rightDownExtent(sx, sy);
                let leftExtent;
                let rightExtent;

                if (trOfBlocked && blOfBlocked) {
                    //  ____
                    //  |
                    //  |

                    leftExtent = sx;
                    rightExtent = rightBound;

                    rightwardsSearch = true;
                } else if (trOfBlocked) {
                    //  .
                    //  |\
                    //  | \

                    leftExtent = sx;
                    rightExtent = this.toDouble(sx*-dy + dx, -dy);
                    if (!this.isLessThanOrEqual(rightExtent, rightBound)) { // rightBound < rightExtent
                        rightExtent = rightBound;
                    }
                } else { // blOfBlocked
                    //  ___
                    //  \
                    //   \
                    leftExtent = this.toDouble(sx*-dy + dx, -dy);
                    rightExtent = rightBound;

                    rightwardsSearch = true;
                }

                if (this.isLessThanOrEqual(leftExtent, rightExtent)) {
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }
            } else { // dy == 0
                // B--P

                if (this._graph.bottomRightOfBlockedTile(sx, sy)) {
                    // |
                    // |___

                    const leftExtent = sx;
                    const rightExtent = this.rightUpExtent(sx, sy);
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                } else if (this._graph.topRightOfBlockedTile(sx, sy)) { // topRightOfBlockedTile
                    // ____
                    // |
                    // |

                    const leftExtent = sx;
                    const rightExtent = this.rightDownExtent(sx, sy);
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }

                rightwardsSearch = true;
            }
        } else if (dx < 0) {
            // Moving leftwards

            if (dy > 0) {
                //  B
                //   \
                //    P
                const blOfBlocked = this._graph.bottomLeftOfBlockedTile(sx, sy);
                const trOfBlocked = this._graph.topRightOfBlockedTile(sx, sy);

                const leftBound = this.leftUpExtent(sx, sy);
                let leftExtent;
                let rightExtent;

                if (blOfBlocked && trOfBlocked) {
                    //     |
                    //  ___|

                    leftExtent = leftBound;
                    rightExtent = sx;

                    leftwardsSearch = true;
                } else if (blOfBlocked) {
                    //  \ |
                    //   \|

                    leftExtent = this.toDouble(sx*dy + dx, dy);
                    rightExtent = sx;
                    if (this.isLessThan(leftExtent, leftBound)) { // leftExtent < leftBound
                        leftExtent = leftBound;
                    }
                } else { // trOfBlocked
                    //   \
                    //  __\

                    leftExtent = leftBound;
                    rightExtent = this.toDouble(sx*dy + dx, dy);

                    leftwardsSearch = true;
                }

                if (this.isLessThanOrEqual(leftExtent, rightExtent)) {
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }
            } else if (dy < 0) {
                //    B
                //   /
                //  P
                const tlOfBlocked = this._graph.topLeftOfBlockedTile(sx, sy);
                const brOfBlocked = this._graph.bottomRightOfBlockedTile(sx, sy);

                const leftBound = this.leftDownExtent(sx, sy);
                let leftExtent;
                let rightExtent;

                if (tlOfBlocked && brOfBlocked) {
                    //  ____
                    //     |
                    //     |

                    leftExtent = leftBound;
                    rightExtent = sx;

                    leftwardsSearch = true;
                } else if (tlOfBlocked) {
                    //   /|
                    //  / |

                    leftExtent = this.toDouble(sx*-dy + dx, -dy);
                    rightExtent = sx;
                    if (this.isLessThan(leftExtent, leftBound)) { // leftExtent < leftBound
                        leftExtent = leftBound;
                    }
                } else { // brOfBlocked
                    //  ___
                    //    /
                    //   /

                    leftExtent = leftBound;
                    rightExtent = this.toDouble(sx*-dy + dx, -dy);

                    leftwardsSearch = true;
                }

                if (this.isLessThanOrEqual(leftExtent, rightExtent)) {
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }
            } else { // dy == 0
                // P--B

                if (this._graph.bottomLeftOfBlockedTile(sx, sy)) {
                    //    |
                    // ___|

                    const leftExtent = this.leftUpExtent(sx, sy);
                    const rightExtent = sx;
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                } else if (this._graph.topLeftOfBlockedTile(sx, sy)) {
                    // ____
                    //    |
                    //    |

                    const leftExtent = this.leftDownExtent(sx, sy);
                    const rightExtent = sx;
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
                }

                leftwardsSearch = true;
            }
        } else { // dx == 0
            // Direct upwards or direct downwards.
            if (dy > 0) {
                // Direct upwards

                //  P
                //  |
                //  B

                if (this._graph.topLeftOfBlockedTile(sx, sy)) {
                    // |
                    // |___

                    const leftExtent = sx;
                    const rightExtent = this.rightUpExtent(sx, sy);
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);

                    rightwardsSearch = true;
                } else if (this._graph.topRightOfBlockedTile(sx, sy)) {
                    //    |
                    // ___|

                    const leftExtent = this.leftUpExtent(sx, sy);
                    const rightExtent = sx;
                    this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);

                    leftwardsSearch = true;
                } else {
                    const x = sx;
                    Knight.PATH.LineOfSightScanner.stackPush(new Knight.PATH.LOSInterval(sy+1, x, x, Knight.PATH.LOSInterval.BOTH_INCLUSIVE));
                }
            } else { // dy < 0
                // Direct downwards

                //  B
                //  |
                //  P

                if (this._graph.bottomLeftOfBlockedTile(sx, sy)) {
                    // ____
                    // |
                    // |

                    const leftExtent = sx;
                    const rightExtent = this.rightDownExtent(sx, sy);
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);

                    rightwardsSearch = true;
                } else if (this._graph.bottomRightOfBlockedTile(sx, sy)) {
                    // ____
                    //    |
                    //    |

                    const leftExtent = this.leftDownExtent(sx, sy);
                    const rightExtent = sx;
                    this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);

                    leftwardsSearch = true;
                } else {
                    const x = sx;
                    Knight.PATH.LineOfSightScanner.stackPush(new Knight.PATH.LOSInterval(sy-1, x, x, Knight.PATH.LOSInterval.BOTH_INCLUSIVE));
                }
            }
        }

        // Direct Search Left
        if (leftwardsSearch) {
            // Direct Search Left
            // Assumption: Not blocked towards left.
            Knight.PATH.LineOfSightScanner.addSuccessor(this.leftAnyExtent(sx, sy), sy);
        }

        if (rightwardsSearch) {
            // Direct Search Right
            // Assumption: Not blocked towards right.
            Knight.PATH.LineOfSightScanner.addSuccessor(this.rightAnyExtent(sx, sy), sy);
        }
    }

    /**
     * Assumption: We are at an outer corner. One of six cases:
     *   BR        BL        TR        TL       TRBL      TLBR
     * XXX|         |XXX      :         :         |XXX   XXX|
     * XXX|...   ...|XXX   ___:...   ...:___   ___|XXX   XXX|___
     *    :         :      XXX|         |XXX   XXX|         |XXX
     *    :         :      XXX|         |XXX   XXX|         |XXX
     *
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    generateTwoWayTautStartingStates(sx, sy) {
        const bottomLeftOfBlocked = this._graph.bottomLeftOfBlockedTile(sx, sy);
        const bottomRightOfBlocked = this._graph.bottomRightOfBlockedTile(sx, sy);
        const topLeftOfBlocked = this._graph.topLeftOfBlockedTile(sx, sy);
        const topRightOfBlocked = this._graph.topRightOfBlockedTile(sx, sy);

        // Generate up-left direction
        if (topRightOfBlocked || bottomLeftOfBlocked) {
            const leftExtent = this.leftUpExtent(sx, sy);
            const rightExtent = sx;

            this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Generate up-right direction
        if (bottomRightOfBlocked || topLeftOfBlocked) {
            const leftExtent = sx;
            const rightExtent = this.rightUpExtent(sx, sy);

            this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Generate down-left direction
        if (bottomRightOfBlocked || topLeftOfBlocked) {
            const leftExtent = this.leftDownExtent(sx, sy);
            const rightExtent = sx;

            this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Generate down-right direction
        if (topRightOfBlocked || bottomLeftOfBlocked) {
            const leftExtent = sx;
            const rightExtent = this.rightDownExtent(sx, sy);

            this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Search leftwards
        if (!topRightOfBlocked || !bottomRightOfBlocked) {
            const x = this.leftAnyExtent(sx, sy);
            const y = sy;
            if (!(this._graph.topRightOfBlockedTile(x, y) && this._graph.bottomRightOfBlockedTile(x, y))) {
                Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
            }
        }

        // Search rightwards
        if (!topLeftOfBlocked || !bottomLeftOfBlocked) {
            const x = this.rightAnyExtent(sx, sy);
            const y = sy;
            if (!(this._graph.topLeftOfBlockedTile(x, y) && this._graph.bottomLeftOfBlockedTile(x, y))) {
                Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
            }
        }
    }

    /**
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    generateStartingStates(sx, sy) {
        const bottomLeftOfBlocked = this._graph.bottomLeftOfBlockedTile(sx, sy);
        const bottomRightOfBlocked = this._graph.bottomRightOfBlockedTile(sx, sy);
        const topLeftOfBlocked = this._graph.topLeftOfBlockedTile(sx, sy);
        const topRightOfBlocked = this._graph.topRightOfBlockedTile(sx, sy);

        // Generate up
        if (!bottomLeftOfBlocked || !bottomRightOfBlocked) {
            let leftExtent;
            let rightExtent;

            if (bottomLeftOfBlocked) {
                // Explore up-left
                leftExtent = this.leftUpExtent(sx, sy);
                rightExtent = sx;
            } else if (bottomRightOfBlocked) {
                // Explore up-right
                leftExtent = sx;
                rightExtent = this.rightUpExtent(sx, sy);
            } else {
                // Explore up-left-right
                leftExtent = this.leftUpExtent(sx, sy);
                rightExtent = this.rightUpExtent(sx, sy);
            }

            this.generateUpwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Generate down
        if (!topLeftOfBlocked || !topRightOfBlocked) {
            let leftExtent;
            let rightExtent;

            if (topLeftOfBlocked) {
                // Explore down-left
                leftExtent = this.leftDownExtent(sx, sy);
                rightExtent = sx;
            } else if (topRightOfBlocked) {
                // Explore down-right
                leftExtent = sx;
                rightExtent = this.rightDownExtent(sx, sy);
            } else {
                // Explore down-left-right
                leftExtent = this.leftDownExtent(sx, sy);
                rightExtent = this.rightDownExtent(sx, sy);
            }

            this.generateDownwards(leftExtent, rightExtent, sx, sy, sy, true, true);
        }

        // Search leftwards
        if (!topRightOfBlocked || !bottomRightOfBlocked) {
            const x = this.leftAnyExtent(sx, sy);
            const y = sy;
            if (!(this._graph.topRightOfBlockedTile(x, y) && this._graph.bottomRightOfBlockedTile(x, y))) {
                Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
            }
        }

        // Search rightwards
        if (!topLeftOfBlocked || !bottomLeftOfBlocked) {
            const x = this.rightAnyExtent(sx, sy);
            const y = sy;
            if (!(this._graph.topLeftOfBlockedTile(x, y) && this._graph.bottomLeftOfBlockedTile(x, y))) {
                Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
            }
        }
    }

    /**
     * @param {Number} sx
     * @param {Number} sy
     * @memberof LineOfSightScanner
     */
    exploreStates(sx, sy) {
        while (Knight.PATH.LineOfSightScanner._intervalStackSize > 0) {
            const currState = Knight.PATH.LineOfSightScanner.stackPop();
            let leftInclusive = (currState.inclusive & Knight.PATH.LOSInterval.LEFT_INCLUSIVE) != 0;
            let rightInclusive = (currState.inclusive & Knight.PATH.LOSInterval.RIGHT_INCLUSIVE) != 0;

            const zeroLengthInterval = this.isEqualTo(currState.xR, currState.xL);

            if (currState.y > sy) {
                // Upwards

                // Insert endpoints if integer.
                if (leftInclusive && this.isWholeNumber(currState.xL)) {
                    /* The two cases   _
                     *  _             |X|
                     * |X|'.           ,'
                     *      '.       ,'
                     *        B     B
                     */

                    const x = this.round(currState.xL);
                    const y = currState.y;
                    const topRightOfBlockedTile = this._graph.topRightOfBlockedTile(x, y);
                    const bottomRightOfBlockedTile = this._graph.bottomRightOfBlockedTile(x, y);

                    if (x <= sx && topRightOfBlockedTile && !bottomRightOfBlockedTile) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        leftInclusive = false;
                    } else if (sx <= x && bottomRightOfBlockedTile && !topRightOfBlockedTile) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        leftInclusive = false;
                    }
                }
                if (rightInclusive && this.isWholeNumber(currState.xR)) {
                    /*   _   The two cases
                     *  |X|             _
                     *  '.           ,'|X|
                     *    '.       ,'
                     *      B     B
                     */

                    const x = this.round(currState.xR);
                    const y = currState.y;
                    const bottomLeftOfBlockedTile = this._graph.bottomLeftOfBlockedTile(x, y);
                    const topLeftOfBlockedTile = this._graph.topLeftOfBlockedTile(x, y);

                    if (x <= sx && bottomLeftOfBlockedTile && !topLeftOfBlockedTile) {
                        if (leftInclusive || !zeroLengthInterval) {
                            Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                            rightInclusive = false;
                        }
                    } else if (sx <= x && topLeftOfBlockedTile && !bottomLeftOfBlockedTile) {
                        if (leftInclusive || !zeroLengthInterval) {
                            Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                            rightInclusive = false;
                        }
                    }
                }

                // Generate Upwards
                /*
                 * =======      =====    =====
                 *  \   /       / .'      '. \
                 *   \ /   OR  /.'    OR    '.\
                 *    B       B                B
                 */

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                const dy = currState.y - sy;
                let leftProjection = (currState.xL - sx)*(dy+1)/dy + sx;

                let leftBound = this.leftUpExtent(this.ceil(currState.xL), currState.y);
                if (this.isWholeNumber(currState.xL) &&
                    this._graph.bottomRightOfBlockedTile(this.round(currState.xL), currState.y)) {
                    leftBound = this.round(currState.xL);
                }

                if (this.isLessThan(leftProjection, leftBound)) { // leftProjection < leftBound
                    leftProjection = leftBound;
                    leftInclusive = true;
                }

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                let rightProjection = (currState.xR - sx)*(dy+1)/dy + sx;

                let rightBound = this.rightUpExtent(this.floor(currState.xR), currState.y);
                if (this.isWholeNumber(currState.xR) &&
                    this._graph.bottomLeftOfBlockedTile(this.round(currState.xR), currState.y)) {
                    rightBound = this.round(currState.xR);
                }

                if (!this.isLessThanOrEqual(rightProjection, rightBound)) { // rightBound < rightProjection
                    rightProjection = rightBound;
                    rightInclusive = true;
                }

                // Call Generate
                if (leftInclusive && rightInclusive) {
                    if (this.isLessThanOrEqual(leftProjection, rightProjection)) {
                        this.generateUpwards(leftProjection, rightProjection, sx, sy, currState.y, true, true);
                    }
                } else if (this.isLessThan(leftProjection, rightProjection)) {
                    this.generateUpwards(leftProjection, rightProjection, sx, sy, currState.y, leftInclusive, rightInclusive);
                }
            } else {
                // Upwards

                // Insert endpoints if integer.
                if (leftInclusive && this.isWholeNumber(currState.xL)) {
                    /* The two cases
                     *        B     B
                     *  _   ,'       '.
                     * |X|.'           '.
                     *                |X|
                     */

                    const x = this.round(currState.xL);
                    const y = currState.y;
                    const bottomRightOfBlockedTile = this._graph.bottomRightOfBlockedTile(x, y);
                    const topRightOfBlockedTile = this._graph.topRightOfBlockedTile(x, y);

                    if (x <= sx && bottomRightOfBlockedTile && !topRightOfBlockedTile) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        leftInclusive = false;
                    } else if (sx <= x && topRightOfBlockedTile && !bottomRightOfBlockedTile) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        leftInclusive = false;
                    }
                }
                if (rightInclusive && this.isWholeNumber(currState.xR)) {
                    /*       The two cases
                     *      B     B
                     *    .'       '.   _
                     *  .'           '.|X|
                     *  |X|
                     */

                    const x = this.round(currState.xR);
                    const y = currState.y;
                    const topLeftOfBlockedTile = this._graph.topLeftOfBlockedTile(x, y);
                    const bottomLeftOfBlockedTile = this._graph.bottomLeftOfBlockedTile(x, y);

                    if (x <= sx && topLeftOfBlockedTile && !bottomLeftOfBlockedTile) {
                        if (leftInclusive || !zeroLengthInterval) {
                            Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                            rightInclusive = false;
                        }
                    } else if (sx <= x && bottomLeftOfBlockedTile && !topLeftOfBlockedTile) {
                        if (leftInclusive || !zeroLengthInterval) {
                            Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                            rightInclusive = false;
                        }
                    }
                }

                // Generate downwards
                /*
                 *    B       B                B
                 *   / \   OR  \'.    OR    .'/
                 *  /   \       \ '.      .' /
                 * =======      =====    =====
                 */

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                const dy = sy - currState.y;
                let leftProjection = (currState.xL - sx)*(dy+1)/dy + sx;

                let leftBound = this.leftDownExtent(this.ceil(currState.xL), currState.y);
                if (this.isWholeNumber(currState.xL) &&
                    this._graph.topRightOfBlockedTile(this.round(currState.xL), currState.y)) {
                    leftBound = this.round(currState.xL);
                }

                if (this.isLessThan(leftProjection, leftBound)) { // leftProjection < leftBound
                    leftProjection = leftBound;
                    leftInclusive = true;
                }

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                let rightProjection = (currState.xR - sx)*(dy+1)/dy + sx;

                let rightBound = this.rightDownExtent(this.floor(currState.xR), currState.y);
                if (this.isWholeNumber(currState.xR) &&
                    this._graph.topLeftOfBlockedTile(this.round(currState.xR), currState.y)) {
                    rightBound = this.round(currState.xR);
                }

                if (!this.isLessThanOrEqual(rightProjection, rightBound)) { // rightBound < rightProjection
                    rightProjection = rightBound;
                    rightInclusive = true;
                }

                // Call Generate
                if (leftInclusive && rightInclusive) {
                    if (this.isLessThanOrEqual(leftProjection, rightProjection)) {
                        this.generateDownwards(leftProjection, rightProjection, sx, sy, currState.y, true, true);
                    }
                } else if (this.isLessThan(leftProjection, rightProjection)) {
                    this.generateDownwards(leftProjection, rightProjection, sx, sy, currState.y, leftInclusive, rightInclusive);
                }
            }
        }
    }

    /**
     * @param {Normal} sx
     * @param {Normal} sy
     * @memberof LineOfSightScanner
     */
    exploreStatesNonTaut(sx, sy) {
        while (Knight.PATH.LineOfSightScanner._intervalStackSize > 0) {
            const currState = stackPop();
            let leftInclusive = (currState.inclusive & Knight.PATH.LOSInterval.LEFT_INCLUSIVE) != 0;
            let rightInclusive = (currState.inclusive & Knight.PATH.LOSInterval.RIGHT_INCLUSIVE) != 0;

            // const zeroLengthInterval = this.isEqualTo(currState.xR, currState.xL);

            if (currState.y > sy) {
                // Upwards

                // Insert endpoints if integer.
                if (leftInclusive && this.isWholeNumber(currState.xL)) {
                    /* The two cases   _
                     *  _             |X|
                     * |X|'.           ,'
                     *      '.       ,'
                     *        B     B
                     */

                    const x = this.round(currState.xL);
                    const y = currState.y;

                    if (this._graph.isOuterCorner(x, y)) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        //leftInclusive = false;
                    }
                }
                if (rightInclusive && this.isWholeNumber(currState.xR)) {
                    /*   _   The two cases
                     *  |X|             _
                     *  '.           ,'|X|
                     *    '.       ,'
                     *      B     B
                     */

                    const x = this.round(currState.xR);
                    const y = currState.y;

                    if (this._graph.isOuterCorner(x, y)) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        //rightInclusive = false;
                    }
                }

                // Generate Upwards
                /*
                 * =======      =====    =====
                 *  \   /       / .'      '. \
                 *   \ /   OR  /.'    OR    '.\
                 *    B       B                B
                 */

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                const dy = currState.y - sy;
                let leftProjection = (currState.xL - sx)*(dy+1)/dy + sx;

                let leftBound = this.leftUpExtent(this.ceil(currState.xL), currState.y);
                if (this.isWholeNumber(currState.xL) &&
                    this._graph.bottomRightOfBlockedTile(this.round(currState.xL), currState.y)) {
                    leftBound = this.round(currState.xL);
                }

                if (this.isLessThan(leftProjection, leftBound)) { // leftProjection < leftBound
                    leftProjection = leftBound;
                    leftInclusive = true;
                }

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                let rightProjection = (currState.xR - sx)*(dy+1)/dy + sx;

                let rightBound = this.rightUpExtent(this.floor(currState.xR), currState.y);
                if (this.isWholeNumber(currState.xR) &&
                    this._graph.bottomLeftOfBlockedTile(this.round(currState.xR), currState.y)) {
                    rightBound = this.round(currState.xR);
                }

                if (!this.isLessThanOrEqual(rightProjection, rightBound)) { // rightBound < rightProjection
                    rightProjection = rightBound;
                    rightInclusive = true;
                }

                // Call Generate
                if (leftInclusive && rightInclusive) {
                    if (this.isLessThanOrEqual(leftProjection, rightProjection)) {
                        this.generateUpwards(leftProjection, rightProjection, sx, sy, currState.y, true, true);
                    }
                } else if (this.isLessThan(leftProjection, rightProjection)) {
                    this.generateUpwards(leftProjection, rightProjection, sx, sy, currState.y, leftInclusive, rightInclusive);
                }
            } else {
                // Upwards

                // Insert endpoints if integer.
                if (leftInclusive && this.isWholeNumber(currState.xL)) {
                    /* The two cases
                     *        B     B
                     *  _   ,'       '.
                     * |X|.'           '.
                     *                |X|
                     */

                    const x = this.round(currState.xL);
                    const y = currState.y;

                    if (this._graph.isOuterCorner(x, y)) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        //leftInclusive = false;
                    }
                }
                if (rightInclusive && this.isWholeNumber(currState.xR)) {
                    /*       The two cases
                     *      B     B
                     *    .'       '.   _
                     *  .'           '.|X|
                     *  |X|
                     */

                    const x = this.round(currState.xR);
                    const y = currState.y;
                    if (this._graph.isOuterCorner(x, y)) {
                        Knight.PATH.LineOfSightScanner.addSuccessor(x, y);
                        //rightInclusive = false;
                    }
                }

                // Generate downwards
                /*
                 *    B       B                B
                 *   / \   OR  \'.    OR    .'/
                 *  /   \       \ '.      .' /
                 * =======      =====    =====
                 */

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                const dy = sy - currState.y;
                let leftProjection = (currState.xL - sx)*(dy+1)/dy + sx;

                let leftBound = this.leftDownExtent(this.ceil(currState.xL), currState.y);
                if (this.isWholeNumber(currState.xL) &&
                    this._graph.topRightOfBlockedTile(this.round(currState.xL), currState.y)) {
                    leftBound = this.round(currState.xL);
                }

                if (this.isLessThan(leftProjection, leftBound)) { // leftProjection < leftBound
                    leftProjection = leftBound;
                    leftInclusive = true;
                }

                // (Px-Bx)*(Py-By+1)/(Py-By) + Bx
                let rightProjection = (currState.xR - sx)*(dy+1)/dy + sx;

                let rightBound = this.rightDownExtent(this.floor(currState.xR), currState.y);
                if (this.isWholeNumber(currState.xR) &&
                    this._graph.topLeftOfBlockedTile(this.round(currState.xR), currState.y)) {
                    rightBound = this.round(currState.xR);
                }

                if (!this.isLessThanOrEqual(rightProjection, rightBound)) { // rightBound < rightProjection
                    rightProjection = rightBound;
                    rightInclusive = true;
                }

                // Call Generate
                if (leftInclusive && rightInclusive) {
                    if (this.isLessThanOrEqual(leftProjection, rightProjection)) {
                        this.generateDownwards(leftProjection, rightProjection, sx, sy, currState.y, true, true);
                    }
                } else if (this.isLessThan(leftProjection, rightProjection)) {
                    this.generateDownwards(leftProjection, rightProjection, sx, sy, currState.y, leftInclusive, rightInclusive);
                }
            }
        }
    }

    /**
     * @param {Number} xL
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    leftUpExtent(xL, y) {
        return xL > this._sizeX ? this._sizeX : Knight.PATH.LineOfSightScanner._leftDownExtents[y+1][xL];
    }

    /**
     * @param {Number} xL
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    leftDownExtent(xL, y) {
        return xL > this._sizeX ? this._sizeX : Knight.PATH.LineOfSightScanner._leftDownExtents[y][xL];
    }

    /**
     * @param {Number} xL
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    leftAnyExtent(xL, y) {
        return Math.max(
            Knight.PATH.LineOfSightScanner._leftDownExtents[y][xL],
            Knight.PATH.LineOfSightScanner._leftDownExtents[y+1][xL]);
    }

    /**
     * @param {Number} xR
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    rightUpExtent(xR, y) {
        return xR < 0 ? 0 : Knight.PATH.LineOfSightScanner._rightDownExtents[y+1][xR];
    }

    /**
     * @param {Number} xR
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    rightDownExtent(xR, y) {
        return xR < 0 ? 0 : Knight.PATH.LineOfSightScanner._rightDownExtents[y][xR];
    }

    /**
     * @param {Number} xR
     * @param {Number} y
     * @return {Number}
     * @memberof LineOfSightScanner
     */
    rightAnyExtent(xR, y) {
        return Math.min(
            Knight.PATH.LineOfSightScanner._rightDownExtents[y][xR],
            Knight.PATH.LineOfSightScanner._rightDownExtents[y+1][xR]);
    }

    /**
     * @param {Number} leftBound
     * @param {Number} rightBound
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} currY
     * @param {Boolean} leftInclusive
     * @param {Boolean} rightInclusive
     * @memberof LineOfSightScanner
     */
    generateUpwards(leftBound, rightBound, sx, sy, currY, leftInclusive, rightInclusive) {
        this.generateAndSplitIntervals(
            currY + 2, currY + 1,
            sx, sy,
            leftBound, rightBound,
            leftInclusive, rightInclusive);
    }

    /**
     * @param {Number} leftBound
     * @param {Number} rightBound
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} currY
     * @param {Boolean} leftInclusive
     * @param {Boolean} rightInclusive
     * @memberof LineOfSightScanner
     */
    generateDownwards(leftBound, rightBound, sx, sy, currY, leftInclusive, rightInclusive) {
        this.generateAndSplitIntervals(
            currY - 1, currY - 1,
            sx, sy,
            leftBound, rightBound,
            leftInclusive, rightInclusive);
    }

    /**
     * Called by generateUpwards / Downwards.
     * Note: Unlike Anya, 0-length intervals are possible.
     * @param {Number} checkY
     * @param {Number} newY
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} leftBound
     * @param {Number} rightBound
     * @param {Boolean} leftInclusive
     * @param {Boolean} rightInclusive
     * @memberof LineOfSightScanner
     */
    generateAndSplitIntervals(checkY, newY, sx, sy, leftBound, rightBound, leftInclusive, rightInclusive) {
        let left = leftBound;
        let leftFloor = this.floor(left);

        // Up: !bottomRightOfBlockedTile && bottomLeftOfBlockedTile
        if (leftInclusive && this.isWholeNumber(left) &&
            !this._graph.isBlocked(leftFloor-1, checkY-1) &&
            this._graph.isBlocked(leftFloor, checkY-1)) {
            const interval = new Knight.PATH.LOSInterval(newY, left, left, Knight.PATH.LOSInterval.BOTH_INCLUSIVE);
            Knight.PATH.LineOfSightScanner.stackPush(interval);
        }

        // Divide up the intervals.
        while (true) {
            // it's actually rightDownExtents for exploreDownwards. (thus we use checkY = currY - 2)
            const right = Knight.PATH.LineOfSightScanner._rightDownExtents[checkY][leftFloor];
            if (this.isLessThanOrEqual(rightBound, right)) break; // right < rightBound

            // Only push unblocked ( bottomRightOfBlockedTile )
            if (!this._graph.isBlocked(right-1, checkY-1)) {
                const inclusive = leftInclusive ? Knight.PATH.LOSInterval.BOTH_INCLUSIVE : Knight.PATH.LOSInterval.RIGHT_INCLUSIVE;
                const interval = new Knight.PATH.LOSInterval(newY, left, right, inclusive);
                Knight.PATH.LineOfSightScanner.stackPush(interval);
            }

            leftFloor = right;
            left = leftFloor;
            leftInclusive = true;
        }

        // The last interval will always be here.
        // if !bottomLeftOfBlockedTile(leftFloor, checkY)
        if (!this._graph.isBlocked(leftFloor, checkY-1)) {
            const inclusive = (leftInclusive ? Knight.PATH.LOSInterval.LEFT_INCLUSIVE : 0) | (rightInclusive ? Knight.PATH.LOSInterval.RIGHT_INCLUSIVE : 0);
            const interval = new Knight.PATH.LOSInterval(newY, left, rightBound, inclusive);
            Knight.PATH.LineOfSightScanner.stackPush(interval);
        } else {
            // The possibility of there being one degenerate interval at the end. ( !bottomLeftOfBlockedTile(xR, checkY) )
            if (rightInclusive && this.isWholeNumber(rightBound) &&
                !this._graph.isBlocked(this.round(rightBound), checkY-1)) {
                const interval = new Knight.PATH.LOSInterval(newY, rightBound, rightBound, Knight.PATH.LOSInterval.BOTH_INCLUSIVE);
                Knight.PATH.LineOfSightScanner.stackPush(interval);
            }
        }
    }

    /**
     * @memberof LineOfSightScanner
     */
    static clearMemory() {
        this.snapshotList = [];
        this._snapshots = [];
        this._rightDownExtents = null;
        this._leftDownExtents = null;
        this._intervalStack = null;
        this._successorsX = null;
        this._successorsY = null;
        global.gc();
    }
};

Knight.PATH.LineOfSightScanner._EPSILON = 0.0000001;
Knight.PATH.LineOfSightScanner.snapshotList = [];
Knight.PATH.LineOfSightScanner._snapshots = [];
Knight.PATH.LineOfSightScanner._snapshot_sx = null;
Knight.PATH.LineOfSightScanner._snapshot_sy = null;

Knight.PATH.LineOfSightScanner._rightDownExtents = null;
Knight.PATH.LineOfSightScanner._leftDownExtents = null;
Knight.PATH.LineOfSightScanner._intervalStack = null;
Knight.PATH.LineOfSightScanner._intervalStackSize = null;

Knight.PATH.LineOfSightScanner._successorsX = null;
Knight.PATH.LineOfSightScanner._successorsY = null;
Knight.PATH.LineOfSightScanner._nSuccessors = null;

/**
 * @class LOSInterval
 */
Knight.PATH.LOSInterval = class {
    /**
     * Creates an instance of LOSInterval.
     * @param {Number} y
     * @param {Number} xL
     * @param {Number} xR
     * @param {Number} inclusive
     * @memberof LOSInterval
     */
    constructor(y, xL, xR, inclusive) {
        this.y = y;
        this.xL = xL;
        this.xR = xR;
        this.inclusive = inclusive;
    }

    /**
     * @return {String}
     * @memberof LOSInterval
     */
    toString() {
        return ((this.inclusive & Knight.PATH.LOSInterval.LEFT_INCLUSIVE) === 0 ? "(" : "[") +
            `${this.xL}, ${this.xR}` +
            ((this.inclusive & Knight.PATH.LOSInterval.RIGHT_INCLUSIVE) === 0 ? ")" : "]") +
            `|${this.y}`;
    }
};

Knight.PATH.LOSInterval.BOTH_EXCLUSIVE = 0x0;
Knight.PATH.LOSInterval.LEFT_INCLUSIVE = 0x1;
Knight.PATH.LOSInterval.RIGHT_INCLUSIVE = 0x2;
Knight.PATH.LOSInterval.BOTH_INCLUSIVE = 0x3;
