// =============================================================================
// AnyaNode.js
// Version:     1.0
// Date:        1/27/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class AnyaNode
 */
Knight.PATH.AnyaNode = class {
    /**
     * Creates an instance of AnyaNode.
     * @param {AnyaNode} parent
     * @param {AnyaInterval} interval
     * @param {Number} rootx
     * @param {Number} rooty
     * @memberof AnyaNode
     */
    constructor(parent, interval, rootx, rooty) {
        this.parentNode = parent;
        this.interval = interval;
        this.root = new PIXI.Point(rootx, rooty);
        this.g;
        this.f = 0;

        if (parent === null) {
            this.g = 0;
        } else {
            this.g = parent.g + parent.root.distance(this.root);
        }
    }

    /**
     * @return {Number}
     */
    getF() {
        return this.f;
    }

    /**
     * @param {Number} f
     */
    setF(f) {
        this.f = f;
    }

    /**
     * @return {Number}
     */
    getG() {
        return this.g;
    }

    /**
     * @param {Number} g
     */
    setG(g) {
        this.g = g;
    }

    /**
     * @param {AnyaNode} n
     * @return {Boolean}
     */
    equals(n) {
        if (!(n instanceof Knight.PATH.AnyaNode)) return false;

        if (!n.interval.equals(this.interval)) return false;
        if (n.root.x !== this.root.x || n.root.y !== this.root.y) return false;

        return true;
    }

    /**
     * @return {AnyaNode}
     */
    getParentNode() {
        return this.parentNode;
    }

    /**
     * @param {AnyaNode} parentNode
     */
    setParentNode(parentNode) {
        this.parentNode = parentNode;
    }

    /**
     * @return {AnyaInterval}
     */
    getInterval() {
        return this.interval;
    }

    /**
     * @param {AnyaInterval} interval
     */
    setInterval(interval) {
        this.interval = interval;
    }

    /**
     * @return {PIXI.Point}
     */
    getRoot() {
        return this.root;
    }

    /**
     * @param {PIXI.Point} root
     */
    setRoot(root) {
        this.root = root;
    }

    /**
     * @param {Array<AnyaNode>} nodeList
     * @param {AnyaNode} node
     */
    addNodeToList(nodeList, node) {
        const found = nodeList.find(function(element) {
            return element.getParentNode() === node.getParentNode() &&
                   element.getInterval().getRight() === node.getInterval().getRight() &&
                   element.getInterval().getLeft() === node.getInterval().getLeft() &&
                   element.getRoot() === node.getRoot();
        }, this);
        if (found === -1) nodeList.push(node);
    }

    /**
     * @param {Array<AnyaNode>} dest
     * @param {Array<AnyaNode>} source
     */
    addNodeListToList(dest, source) {
        for (node of source) {
            this.addNodeToList(dest, n);
        }
    }
};
