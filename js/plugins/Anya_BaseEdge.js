// =============================================================================
// Anya_BaseEdge.js
// Version:     1.0
// Date:        1/27/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

Knight.PATH.BaseEdge = class {
    /**
     * Creates an instance of BaseEdge.
     * @param {Number} id
     * @param {BaseVertex} start
     * @param {BaseVertex} end
     * @param {Number} weight
     * @memberof AnyaVertex
     */
    constructor(id, start, end, weight) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.weight = weight;
        this.otherCost = 0;
        this.zoneCost = 0;
    }

    /**
     * @param {BaseEdge} o
     * @return {Boolean}
     */
    equals(o) {
        return id === o.id;
    }

    /**
     * Returns the euclidian length of the edge
     */
    getLength() {};

    /**
     * Returns the weight of the edge which is the cost of traversal
     * @return {Number}
     */
    getEdgeWeight() {
        return this.weight;
    }

    /**
     * Compares to another edge
     * Java compare(long, long) definition:
     * "The compare(long x, long y) method of Long class returns the
     *  value 0 if x == y; a value less than 0 if x < y; and a value
     *  greater than 0 if x > y"
     * @param {BaseEdge} o
     * @return {Number}
     */
    compareTo(o) {
        return o.id - this.id;
    }
};
