// =============================================================================
// SVG_AStarStaticMemory.js
// Version:     1.0
// Date:        1/11/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * @class AStarStaticMemory
 * @extends {Algorithm}
 */
Knight.PATH.AStarStaticMemory = class extends Knight.PATH.Algorithm {
    /**
     *Creates an instance of AStarStaticMemory.
     * @param {GridGraph} graph
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @memberof AStarStaticMemory
     */
    constructor(graph, sx, sy, ex, ey) {
        super(graph, graph.sizeX, graph.sizeY, sx, sy, ex, ey);
        this._postSmoothingOn = false;
        this._repeatedPostSmooth = false;
        this._heuristicWeight = 1.0;
        this._pq = null;
        this._finish = null;
    }

    /**
     * @static
     * @param {GridGraph} graph
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @return {AStarStaticMemory}
     * @memberof AStarStaticMemory
     */
    static postSmooth(graph, sx, sy, ex, ey) {
        aStar = new Knight.PATH.AStarStaticMemory(graph, sx, sy, ex, ey);
        aStar.postSmoothingOn = true;
        aStar.repeatedPostSmooth = false;
        return aStar;
    }

    /**
     * @static
     * @param {GridGraph} graph
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @return {AStarStaticMemory}
     * @memberof AStarStaticMemory
     */
    static repeatedPostSmooth(graph, sx, sy, ex, ey) {
        aStar = new Knight.PATH.AStarStaticMemory(graph, sx, sy, ex, ey);
        aStar.postSmoothingOn = true;
        aStar.repeatedPostSmooth = true;
        return aStar;
    }

    /**
     * @static
     * @param {GridGraph} graph
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @return {AStarStaticMemory}
     * @memberof AStarStaticMemory
     */
    static dijkstra(graph, sx, sy, ex, ey) {
        aStar = new Knight.PATH.AStarStaticMemory(graph, sx, sy, ex, ey);
        aStar.heuristicWeight = 0;
        return aStar;
    }

    /**
     * @memberof AStarStaticMemory
     */
    computePath() {
        const totalSize = (this._graph.sizeX+1) * (this._graph.sizeY+1);

        const start = this.toOneDimIndex(this._sx, this._sy);
        this._finish = this.toOneDimIndex(this._ex, this._ey);

        this._pq = new Knight.PATH.Heap(totalSize);
        this.initializeMemory(totalSize, Number.POSITIVE_INFINITY, -1, false);

        this.initialize(start);

        // let lastDist = -1; // unused
        while (!this._pq.isEmpty()) {
            // const dist = this._pq.getMinValue(); // unused
            const current = this._pq.popMinIndex();

            this.maybeSaveSearchSnapshot();

            if (current === this._finish || this.distance(current) === Number.POSITIVE_INFINITY) {
                this.maybeSaveSearchSnapshot();
                break;
            }
            this.setVisited(current, true);

            const x = this.toTwoDimX(current);
            const y = this.toTwoDimY(current);

            this.tryRelaxNeighbour(current, x, y, x-1, y-1);
            this.tryRelaxNeighbour(current, x, y, x, y-1);
            this.tryRelaxNeighbour(current, x, y, x+1, y-1);

            this.tryRelaxNeighbour(current, x, y, x-1, y);
            this.tryRelaxNeighbour(current, x, y, x+1, y);

            this.tryRelaxNeighbour(current, x, y, x-1, y+1);
            this.tryRelaxNeighbour(current, x, y, x, y+1);
            this.tryRelaxNeighbour(current, x, y, x+1, y+1);
        }
        this.maybePostSmooth();
    }

    /**
     * @param {Number} current
     * @param {Number} currentX
     * @param {Number} currentY
     * @param {Number} x
     * @param {Number} y
     * @memberof AStarStaticMemory
     */
    tryRelaxNeighbour(current, currentX, currentY, x, y) {
        if (!this._graph.isValidCoordinate(x, y)) return;

        const destination = this.toOneDimIndex(x, y);
        if (this.visited(destination)) return;
        if (!this._graph.neighbourLineOfSight(currentX, currentY, x, y)) return;

        if (this.relax(current, destination, this.weight(currentX, currentY, x, y))) {
            this._pq.decreaseKey(destination, this.distance(destination) + this.heuristic(x, y));
        }
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    heuristic(x, y) {
        return this._heuristicWeight * this._graph.distance(x, y, this._ex, this._ey);
    }

    /**
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    weight(x1, y1, x2, y2) {
        return this._graph.distance(x1, y1, x2, y2);
    }

    /**
     * @param {Number} u
     * @param {Number} v
     * @param {Number} weightUV
     * @return {Boolean} True if relaxation is done
     * @memberof AStarStaticMemory
     */
    relax(u, v, weightUV) {
        const newWeight = this.distance(u) + weightUV;
        if (newWeight < this.distance(v)) {
            this.setDistance(v, newWeight);
            this.setParent(v, u);
            return true;
        }
        return false;
    }

    /**
     * @param {Number} s
     * @memberof AStarStaticMemory
     */
    initialize(s) {
        this._pq.decreaseKey(s, 0.0);
        Knight.PATH.Memory.setDistance(s, 0.0);
    }

    /**
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    pathLength() {
        let length = 0;
        let current = this._finish;
        while (current !== -1) {
            current = this.parent(current);
            length++;
        }
        return length;
    }

    /**
     * @param {Number} node1
     * @param {Number} node2
     * @return {Boolean}
     * @memberof AStarStaticMemory
     */
    lineOfSight(node1, node2) {
        x1 = this.toTwoDimX(node1);
        y1 = this.toTwoDimY(node1);
        x2 = this.toTwoDimX(node2);
        y2 = this.toTwoDimY(node2);
        return this._graph.lineOfSight(x1, y1, x2, y2);
    }

    /**
     * @param {Number} node1
     * @param {Number} node2
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    physicalDistance(node1, node2) {
        x1 = this.toTwoDimX(node1);
        y1 = this.toTwoDimY(node1);
        x2 = this.toTwoDimX(node2);
        y2 = this.toTwoDimY(node2);
        return this._graph.distance(x1, y1, x2, y2);
    }

    /**
     * @param {Number} x1
     * @param {Number} y2
     * @param {Number} node2
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    physicalDistance(x1, y2, node2) {
        x2 = this.toTwoDimX(node2);
        y2 = this.toTwoDimY(node2);
        return this._graph.distance(x1, y1, x2, y2);
    }

    /**
     * @memberof AStarStaticMemory
     */
    maybePostSmooth() {
        if (this._postSmoothingOn) {
            if (this._repeatedPostSmooth) {
                while (this.postSmooth());
            } else {
                this.postSmooth();
            }
        }
    }

    /**
     * @return {Boolean}
     * @memberof AStarStaticMemory
     */
    postSmooth() {
        let didSomething = false;

        let current = this._finish;
        while (current !== -1) {
            let next = this.parent(current); // we can skip checking this one as it always has LoS to current.
            if (next !== -1) {
                next = this.parent(next);
                while (next !== -1) {
                    if (this.lineOfSight(current, next)) {
                        this.setParent(current, next);
                        next = this.parent(next);
                        didSomething = true;
                        this.maybeSaveSearchSnapshot();
                    } else {
                        next = -1;
                    }
                }
            }
            current = this.parent(current);
        }
        return didSomething;
    }

    /**
     * @return {Array<Array<Number>>}
     * @memberof AStarStaticMemory
     */
    getPath() {
        const length = this.pathLength();
        const path = new Array(length);
        let current = this._finish;

        let index = length-1;
        while (current !== -1) {
            const x = this.toTwoDimX(current);
            const y = this.toTwoDimY(current);

            path[index] = [x, y];

            index--;
            current = this.parent(current);
        }
        return path;
    }

    /**
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    getPathLength() {
        let current = this._finish;
        if (current === -1) return -1;

        let pathLength = 0;

        let prevX = this.toTwoDimX(current);
        let prevY = this.toTwoDimY(current);
        current = this.parent(current);

        while (current !== -1) {
            const x = this.toTwoDimX(current);
            const y = this.toTwoDimY(current);

            pathLength += this._graph.distance(x, y, prevX, prevY);

            current = this.parent(current);
            prevX = x;
            prevY = y;
        }
        return pathLength;
    }

    /**
     * @param {Number} index
     * @return {Boolean}
     * @memberof AStarStaticMemory
     */
    selected(index) {
        return this.visited(index);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    parent(index) {
        return Knight.PATH.Memory.parent(index);
    }

    /**
     * @param {Number} index
     * @param {Number} value
     * @memberof AStarStaticMemory
     */
    setParent(index, value) {
        Knight.PATH.Memory.setParent(index, value);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof AStarStaticMemory
     */
    distance(index) {
        return Knight.PATH.Memory.distance(index);
    }

    /**
     * @param {Number} index
     * @param {Number} value
     * @memberof AStarStaticMemory
     */
    setDistance(index, value) {
        Knight.PATH.Memory.setDistance(index, value);
    }

    /**
     * @param {Number} index
     * @return {Boolean}
     * @memberof AStarStaticMemory
     */
    visited(index) {
        return Knight.PATH.Memory.visited(index);
    }

    /**
     * @param {Number} index
     * @param {Boolean} value
     * @memberof AStarStaticMemory
     */
    setVisited(index, value) {
        Knight.PATH.Memory.setVisited(index, value);
    }
};

