// =============================================================================
// KNT_Movement
// Version:     1.0
// Date:        1/7/2020
// Author:      Kaelan
// =============================================================================

//=============================================================================
/*:
 * @plugindesc Enables pixel movement
 * @version 1.0.0
 * @author Kaelan
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * Enables pixel movement
 */

var Imported = Imported || {}; // eslint-disable-line no-var
Imported.KNT_Movement = '1.0.0';

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.MOVEMENT = Knight.MOVEMENT || {};

//=============================================================================
// Parameter Variables
//=============================================================================
Knight.MOVEMENT.TileSize = 48;
Knight.MOVEMENT.MaxPathAttempts = 6;

//=============================================================================
// Utility Functions
//=============================================================================
Knight.MOVEMENT.adjustRadian = function(radian) {
    while (radian < 0) {
        radian += Math.PI * 2;
    }
    while (radian > Math.PI * 2) {
        radian -= Math.PI * 2;
    }
    return radian;
};

// counter-clockwise: 90 degrees is North
Knight.MOVEMENT.dirToRad = {
    6: 0,
    9: Math.PI * 1 / 4,
    8: Math.PI * 2 / 4,
    7: Math.PI * 3 / 4,
    4: Math.PI * 4 / 4,
    1: Math.PI * 5 / 4,
    2: Math.PI * 6 / 4,
    3: Math.PI * 7 / 4,
    5: 0,
};

// counter-clockwise
Knight.MOVEMENT.radToDir = [
    6, // E
    9, // NE
    8, // N
    7, // NW
    4, // W
    1, // SW
    2, // S
    3, // SE
];

Knight.MOVEMENT.dirToVec = [
    null,
    new PIXI.Vector(-1, 1), // 1
    new PIXI.Vector( 0, 1), // 2
    new PIXI.Vector( 1, 1), // 3
    new PIXI.Vector(-1, 0), // 4
    new PIXI.Vector( 0, 0), // 5
    new PIXI.Vector( 1, 0), // 6
    new PIXI.Vector(-1, -1), // 7
    new PIXI.Vector( 0, -1), // 8
    new PIXI.Vector( 1, -1), // 9
];

Knight.degToRad = function(degrees) {
    return degrees * Math.PI / 180;
};

Knight.radToDeg = function(radians) {
    return radians * 180 / Math.PI;
};

//=============================================================================
// Game_Map
//=============================================================================
Game_Map.prototype.adjustPX = function(px) {
    return this.adjustX(px / Knight.MOVEMENT.TileSize) * Knight.MOVEMENT.TileSize;
};

Game_Map.prototype.adjustPY = function(py) {
    return this.adjustY(py / Knight.MOVEMENT.TileSize) * Knight.MOVEMENT.TileSize;
};

Game_Map.prototype.roundPX = function(px) {
    return this.isLoopHorizontal() ? px.mod(this.width() * Knight.MOVEMENT.TileSize) : px;
};

Game_Map.prototype.roundPY = function(py) {
    return this.isLoopVertical() ? py.mod(this.height() * Knight.MOVEMENT.TileSize) : py;
};

Game_Map.prototype.pxWithDirection = function(px, d, dist) {
    return px + (d === 6 ? dist : d === 4 ? -dist : 0);
};

Game_Map.prototype.pyWithDirection = function(py, d, dist) {
    return py + (d === 2 ? dist : d === 8 ? -dist : 0);
};

Game_Map.prototype.roundPXWithDirection = function(px, d, dist) {
    return this.roundPX(px + (d === 6 ? dist : d === 4 ? -dist : 0));
};

Game_Map.prototype.roundPYWithDirection = function(py, d, dist) {
    return this.roundPY(py + (d === 2 ? dist : d === 8 ? -dist : 0));
};

Game_Map.prototype.deltaPX = function(px1, px2) {
    let result = px1 - px2;
    if (this.isLoopHorizontal() && Math.abs(result) > (this.width() * Knight.MOVEMENT.TileSize) / 2) {
        if (result < 0) {
            result += this.width() * Knight.MOVEMENT.TileSize;
        } else {
            result -= this.width() * Knight.MOVEMENT.TileSize;
        }
    }
    return result;
};

Game_Map.prototype.deltaPY = function(py1, py2) {
    let result = py1 - py2;
    if (this.isLoopVertical() && Math.abs(result) > (this.height() * Knight.MOVEMENT.TileSize) / 2) {
        if (result < 0) {
            result += this.height() * Knight.MOVEMENT.TileSize;
        } else {
            result -= this.height() * Knight.MOVEMENT.TileSize;
        }
    }
    return result;
};

Game_Map.prototype.canvasToMapPX = function(px) {
    const originX = this.displayX() * this.tileWidth();
    return this.roundPX(originX + px);
};

Game_Map.prototype.canvasToMapPY = function(py) {
    const originY = this.displayY() * this.tileHeight();
    return this.roundPY(originY + py);
};

Game_Map.prototype.isMouseDestinationWalkable = function() {
    const x = $gameMap.canvasToMapPX(TouchInput.x);
    const y = $gameMap.canvasToMapPY(TouchInput.y);
    const gridXY = Knight.COLLISION.pointToGridTile(x, y);
    return this.pathGrid().isValidEndpoint(gridXY.x, gridXY.y);
};

//=============================================================================
// Game_CharacterBase
//=============================================================================
Object.defineProperties(Game_CharacterBase.prototype, {
    px: {
        get: function() {
            return this._px;
        },
        configurable: true,
    },
    py: {
        get: function() {
            return this._py;
        },
        configurable: true,
    },
});

Knight.MOVEMENT.Game_CharacterBase_initMembers = Game_CharacterBase.prototype.initMembers;
Game_CharacterBase.prototype.initMembers = function() {
    Knight.MOVEMENT.Game_CharacterBase_initMembers.call(this);
    this._px = 0;
    this._py = 0;
    this._realPX = 0;
    this._realPY = 0;
    this._radian = this.directionToRadian(this._direction);
    this._forwardRadian = this.directionToRadian(this._direction);
    this._diagonal = false;
    this._isMoving = false;
    this._isSelected = false;
    this._pathData = {
        destinationX: null,
        destinationY: null,
        destinationEvent: null,
        path: null,
        retries: 0,
        retryCounter: 0,
        retryWait: 0,
        retryingPath: false,
    };
};

Game_CharacterBase.prototype.select = function() {
    this._isSelected = true;
};

Game_CharacterBase.prototype.deselect = function() {
    this._isSelected = false;
};

Game_CharacterBase.prototype.isSelected = function() {
    return this._isSelected;
};

Game_CharacterBase.prototype.setDestination = function(px, py) {
    this._pathData.destinationX = px;
    this._pathData.destinationY = py;
};

Game_CharacterBase.prototype.setDestinationEvent = function(event) {
    this._pathData.destinationEvent = event;
};

Game_CharacterBase.prototype.clearDestination = function() {
    if (this.hasPath() || this._pathData.retryingPath) return;
    this._pathData.destinationX = null;
    this._pathData.destinationY = null;
    this._pathData.destinationEvent = null;
};

Game_CharacterBase.prototype.destinationX = function() {
    return this._pathData.destinationX;
};

Game_CharacterBase.prototype.destinationY = function() {
    return this._pathData.destinationY;
};

Game_CharacterBase.prototype.destinationEvent = function() {
    return this._pathData.destinationEvent;
};

Game_CharacterBase.prototype.isDestinationEvent = function(event) {
    return event === this._pathData.destinationEvent;
};

Game_CharacterBase.prototype.path = function() {
    return this._pathData.path;
};

Game_CharacterBase.prototype.isDestinationValid = function() {
    return this._pathData.destinationX !== null;
};

Game_CharacterBase.prototype.directionToRadian = function(dir) {
    return Knight.MOVEMENT.dirToRad[dir || this._direction];
};

Game_CharacterBase.prototype.radianToDirection = function(radian) {
    radian = Knight.MOVEMENT.adjustRadian(radian);
    return Knight.MOVEMENT.radToDir[Math.round( 8 * radian / (2 * Math.PI) + 8 ) % 8];
};

Game_CharacterBase.prototype.direction8 = function(horz, vert) {
    if (horz === 4 && vert === 8) return 7;
    if (horz === 4 && vert === 2) return 1;
    if (horz === 6 && vert === 8) return 9;
    if (horz === 6 && vert === 2) return 3;
    return 5;
};

Game_CharacterBase.prototype.directionToVector = function() {
    return Knight.MOVEMENT.dirToVec[this._direction];
};

Game_CharacterBase.prototype.cx = function(grid) {
    const collider = this.getCollider('collision');
    const x = collider ? collider.x : this.x;
    return grid ? (x / Knight.MOVEMENT.TileSize) : x;
};

Game_CharacterBase.prototype.cy = function(grid) {
    const collider = this.getCollider('collision');
    const y = collider ? collider.y : this.y;
    return grid ? (y / Knight.MOVEMENT.TileSize) : y;
};

Game_CharacterBase.prototype.isMoving = function() {
    return this._isMoving;
};

Game_CharacterBase.prototype.startedMoving = function() {
    return this._realPX !== this._px || this._realPY !== this._py;
};

Game_CharacterBase.prototype.stop = function() {
    this._px = this._realPX;
    this._py = this._realPY;
};

Game_CharacterBase.prototype.isDiagonal = function() {
    return this._diagonal;
};

Game_CharacterBase.prototype.setPixelPosition = function(px, py) {
    this.setPosition(px / Knight.MOVEMENT.TileSize, py / Knight.MOVEMENT.TileSize);
};

Knight.MOVEMENT.Game_CharacterBase_setPosition = Game_CharacterBase.prototype.setPosition;
Game_CharacterBase.prototype.setPosition = function(x, y) {
    Knight.MOVEMENT.Game_CharacterBase_setPosition.call(this, x, y);
    this._px = this._realPX = x * Knight.MOVEMENT.TileSize;
    this._py = this._realPY = y * Knight.MOVEMENT.TileSize;
    if (this.hasCollider()) this.updateColliderPositions();
};

Game_CharacterBase.prototype.pixelLocate = function(px, py) {
    this.setPixelPosition(px, py);
    this.straighten();
    this.refreshBushDepth();
};

Knight.MOVEMENT.Game_CharacterBase_setDirection = Game_CharacterBase.prototype.setDirection;
Game_CharacterBase.prototype.setDirection = function(d) {
    if (d) this._radian = this.directionToRadian(d);
    if (!this.isDirectionFixed() && d) {
        if ([1, 3, 7, 9].contains(d)) {
            this._diagonal = d;
            const horz = [1, 7].contains(d) ? 4 : 6;
            const vert = [1, 3].contains(d) ? 2 : 8;
            if (this._direction === this.reverseDir(horz)) {
                this._direction = horz;
            }
            if (this._direction === this.reverseDir(vert)) {
                this._direction = vert;
            }
            this.resetStopCount();
            return;
        } else {
            this._diagonal = false;
        }
    }
    Knight.MOVEMENT.Game_CharacterBase_setDirection.call(this, d);
};

Game_CharacterBase.prototype.setRadian = function(radian) {
    radian = Knight.MOVEMENT.adjustRadian(radian);
    this.setDirection(this.radianToDirection(radian));
    this._radian = radian;
};

Game_CharacterBase.prototype.frameSpeed = function(multiplier = 1) {
    return this.distancePerFrame() * Knight.MOVEMENT.TileSize * Math.abs(multiplier);
};

Game_CharacterBase.prototype.forwardV = function() {
    return {
        x: Math.cos(this._forwardRadian) * this.frameSpeed(),
        y: Math.sin(this._forwardRadian) * this.frameSpeed(),
    };
};

Game_CharacterBase.prototype.terrainTag = function() {
    return $gameMap.terrainTag(Math.floor(this.cx(true)), Math.floor(this.cy(true)));
};

Game_CharacterBase.prototype.regionId = function() {
    return $gameMap.regionId(Math.floor(this.cx(true)), Math.floor(this.cy(true)));
};

Game_CharacterBase.prototype.update = function() {
    this.updatePath();
    this.runPath();
    const prevX = this._realPX;
    const prevY = this._realPY;
    if (this.startedMoving()) {
        this._isMoving = true;
    } else {
        this.updateStop();
    }
    if (this.isJumping()) {
        this.updateJump();
    } else if (this.isMoving()) {
        this.updateMove();
    }
    this.updateAnimation();
    if (prevX !== this._realPX || prevY !== this._realPY) {
        this.updateColliderPositions();
        this.refreshBushDepth();
        this.removeFromPathGrid();
        this.addToPathGrid();
    } else {
        this._isMoving = false;
    }
    this.updateDebugTiles();
};

Game_CharacterBase.prototype.updateMove = function() {
    let xSpeed = 1;
    let ySpeed = 1;
    if (this._adjustFrameSpeed) {
        xSpeed = Math.cos(this._radian);
        ySpeed = Math.sin(this._radian);
    }
    if (this._px < this._realPX) {
        this._realPX = Math.max(this._realPX - this.frameSpeed(xSpeed), this._px);
    }
    if (this._px > this._realPX) {
        this._realPX = Math.min(this._realPX + this.frameSpeed(xSpeed), this._px);
    }
    if (this._py < this._realPY) {
        this._realPY = Math.max(this._realPY - this.frameSpeed(ySpeed), this._py);
    }
    if (this._py > this._realPY) {
        this._realPY = Math.min(this._realPY + this.frameSpeed(ySpeed), this._py);
    }
    this._x = this._realX = this._realPX / Knight.MOVEMENT.TileSize;
    this._y = this._realY = this._realPY / Knight.MOVEMENT.TileSize;
};

Knight.MOVEMENT.Game_CharacterBase_updateJump = Game_CharacterBase.prototype.updateJump;
Game_CharacterBase.prototype.updateJump = function() {
    Knight.MOVEMENT.Game_CharacterBase_updateJump.call(this);
    this._px = this._realPX = this._x * Knight.MOVEMENT.TileSize;
    this._py = this._realPY = this._y * Knight.MOVEMENT.TileSize;
};

Game_CharacterBase.prototype.refreshBushDepth = function() {
    if (this.isNormalPriority() && !this.isObjectCharacter() &&
        this.isOnBush() && !this.isJumping()) {
        if (!this.startedMoving()) {
            this._bushDepth = 12;
        }
    } else {
        this._bushDepth = 0;
    }
};

Game_CharacterBase.prototype.moveStraight = function(dir, dist = Knight.MOVEMENT.TileSize) {
    if (dir === 5) dir = this.direction();
    if (dir === 0) dir = this.reverseDir(this.direction());
    let horz = dir;
    let vert = dir;
    let dir8 = dir;
    if ([1, 3, 7, 9].contains(dir)) {
        horz = (dir === 1 || dir === 7) ? 4 : 6;
        vert = (dir === 1 || dir === 3) ? 2 : 8;
        dir8 = this.direction8(horz, vert);
    }
    this.setDirection(dir8);
    this._forwardRadian = this.directionToRadian(dir8);
    this._adjustFrameSpeed = false;
    this._px = $gameMap.roundPXWithDirection(this._px, horz, dist);
    this._py = $gameMap.roundPYWithDirection(this._py, vert, dist);
    this._realPX = $gameMap.pxWithDirection(this._px, this.reverseDir(horz), dist);
    this._realPY = $gameMap.pyWithDirection(this._py, this.reverseDir(vert), dist);
    this.increaseSteps();
};

Game_CharacterBase.prototype.moveDiagonally = function(horz, vert, dist = Knight.MOVEMENT.TileSize) {
    const dir = this.direction8(horz, vert);
    this.moveStraight(dir, dist);
};

Game_CharacterBase.prototype.moveRadian = function(radian, dist = Knight.MOVEMENT.TileSize) {
    const xAxis = Math.cos(radian);
    const yAxis = Math.sin(radian);
    const horzSteps = Math.abs(xAxis) * dist;
    const vertSteps = Math.abs(yAxis) * dist;
    const horz = xAxis > 0 ? 6 : xAxis < 0 ? 4 : 0;
    // Unit circle Y positive is up, but RPG Maker Y positive is down, so we have to flip the sign
    const vert = yAxis > 0 ? 8 : yAxis < 0 ? 2 : 0;
    const x2 = $gameMap.roundPXWithDirection(this._px, horz, horzSteps);
    const y2 = $gameMap.roundPYWithDirection(this._py, vert, vertSteps);
    this.setRadian(radian);
    this._forwardRadian = Knight.MOVEMENT.adjustRadian(radian);
    this._adjustFrameSpeed = true;
    this._px = x2;
    this._py = y2;
    this._realPX = $gameMap.pxWithDirection(this._px, this.reverseDir(horz), horzSteps);
    this._realPY = $gameMap.pyWithDirection(this._py, this.reverseDir(vert), vertSteps);
    this.increaseSteps();
};

Game_CharacterBase.prototype.setPath = function(path, startX, startY, destX, destY) {
    this._pathData.path = new Knight.COLLISION.RunnablePath(path, startX, startY, destX, destY);
};

Game_CharacterBase.prototype.clearPath = function() {
    this._pathData.path = null;
    this._pathData.retries = 0;
    this._pathData.retryCounter = 0;
    this._pathData.retryWait = 0;
    this._pathData.retryingPath = false;
    this.stop();
    this.clearDestination();
};

Game_CharacterBase.prototype.hasPath = function() {
    return !!this.path();
};

Game_CharacterBase.prototype.destinationChanged = function(dx, dy) {
    if (!this.hasPath()) return true;
    return this.path().destinationChanged(dx, dy);
};

Game_CharacterBase.prototype.moveToDestination = function() {
    const dx = this.destinationX();
    const dy = this.destinationY();
    if (this._pathData.retryingPath) {
        this.updatePathRetry(dx, dy);
    } else if (this.isDestinationValid() && this.destinationChanged(dx, dy)) {
        this.pathfindTo(dx, dy);
    }
};

Game_CharacterBase.prototype.pathfindTo = function(dx, dy, isRetry = false) {
    if (!isRetry) this._pathData.retries = 0;

    this.setupGridForPath();
    const start = Knight.COLLISION.pointToGridTile(this.cx(), this.cy());
    const end = Knight.COLLISION.pointToGridTile(dx, dy);

    // If we're stuck on the starting point and can't move because we're too close to an object,
    // try pathfinding on the non-dilated grid. This will give a bad path, but will "unstick" the
    // character. From there, a retry on the normal grid should get a good path to the destination
    let grid = $gameMap.pathGrid();
    if (!grid.isValidEndpoint(start.x, start.y)) {
        console.log(`Character got stuck! => Pathing on permissive grid to fix`);
        grid = $gameMap.collisionGrid();
    }

    const path = Knight.Utility.computeOptimalPathOnline(grid, start.x, start.y, end.x, end.y);
    if (path.length > 1) {
        this.stop();
        this.setPath(path, this.cx(), this.cy(), dx, dy);
    } else {
        // invalid path (can't reach, destination is impassable, etc.)
        this.clearPath();
    }
    this.restorePathGrid();
};

Game_CharacterBase.prototype.setupGridForPath = function() {
    // Remove self from grid while pathfinding
    this.removeFromPathGrid();
    const destinationEvent = this.destinationEvent();
    if (destinationEvent) destinationEvent.removeFromPathGrid();
};

Game_CharacterBase.prototype.restorePathGrid = function() {
    // Restore the grid to its original state
    this.addToPathGrid();
    const destinationEvent = this.destinationEvent();
    if (destinationEvent) destinationEvent.addToPathGrid();
};

Game_CharacterBase.prototype.retryPath = function(wait) {
    if (!this._pathData.retryingPath) {
        this._pathData.retryingPath = true;
        this._pathData.path = null;
        this._pathData.retryWait = wait ? 1 + Math.randomInt(20) : 0;
        this.stop();
    }
};

Game_CharacterBase.prototype.updatePathRetry = function(dx, dy) {
    if (this._pathData.retries < (Knight.MOVEMENT.MaxPathAttempts)) {
        this._pathData.retryCounter++;
        if (this._pathData.retryCounter > this._pathData.retryWait) {
            this._pathData.retryCounter = 0;
            this._pathData.retryingPath = false;
            this._pathData.retries++;
            const name = this.actor ? this.actor().name() : '';
            console.log(`${name} | Retrying Path! | Attempt #${this._pathData.retries}`);
            this.pathfindTo(dx, dy, true);
        }
    } else {
        const name = this.actor ? this.actor().name() : '';
        console.log(`${name} | Too Many Path Retries! Breaking Path`);
        this.clearPath();
    }
};

// Offset path destinations by character anchor position
// We're anchoring to center of character's bottom tile, since
// that looks pretty good when using 2-tile tall characters.
Game_CharacterBase.prototype.offsetPathDest = function(dest) {
    return {
        x: dest.x - ($gameMap.tileWidth() / 2),
        y: dest.y - ($gameMap.tileHeight() / 2),
    };
};

Game_CharacterBase.prototype.runPath = function() {
    const path = this.path();
    if (this.startedMoving() || !this.hasPath() || path.isEmpty()) return;

    let dest = this.offsetPathDest(path.curr());
    if (this._realPX === dest.x && this._realPY === dest.y) {
        const next = path.next();
        if (next === null) { // Path is done
            this.clearPath();
            return;
        }
        dest = this.offsetPathDest(next);
    }

    // Move to current destination in a straight line
    const dist = this.realPixelDistanceFrom(dest.x, dest.y);
    const dx = $gameMap.deltaPX(this._realPX, dest.x);
    const dy = $gameMap.deltaPY(this._realPY, dest.y);
    const radian = Knight.MOVEMENT.adjustRadian(Math.atan2(dy, -dx));
    this.moveRadian(radian, dist);
};

Game_CharacterBase.prototype.updatePath = function() {
    if (this.canMove()) this.moveToDestination();
};

Game_CharacterBase.prototype.canMove = function() {
    if ($gameMap.isEventRunning() || $gameMessage.isBusy()) {
        return false;
    }
    return true;
};

//=============================================================================
// Game_Character
//=============================================================================
Game_CharacterBase.prototype.canMove = function() {
    if ($gameMap.isEventRunning() || $gameMessage.isBusy()) {
        return false;
    }
    if (this.isMoveRouteForcing()) {
        return false;
    }
    return true;
};

Game_Character.prototype.deltaPXFrom = function(px) {
    return $gameMap.deltaPX(this.cx(), px);
};

Game_Character.prototype.deltaPYFrom = function(py) {
    return $gameMap.deltaPY(this.cy(), py);
};

// Uses center of character's collider as character's position
Game_Character.prototype.pixelDistanceFrom = function(px, py) {
    const dx = this.deltaPXFrom(px);
    const dy = this.deltaPYFrom(py);
    return Math.sqrt(dx*dx + dy*dy);
};

// Uses top-left pixel of character's tile as character's position
Game_Character.prototype.realPixelDistanceFrom = function(px, py) {
    const dx = $gameMap.deltaPX(this._realPX, px);
    const dy = $gameMap.deltaPY(this._realPY, py);
    return Math.sqrt(dx*dx + dy*dy);
};

Game_Character.prototype.moveRandom = function() {
    this.moveStraight(Math.randomInt(10));
};

Game_Character.prototype.moveTowardCharacter = function(character) {
    const dx = this.deltaPXFrom(character.cx());
    const dy = this.deltaPYFrom(character.cy());
    const radian = Knight.MOVEMENT.adjustRadian(Math.atan2(dy, -dx));
    this.moveRadian(radian);
};

Game_Character.prototype.moveAwayFromCharacter = function(character) {
    const dx = this.deltaPXFrom(character.cx());
    const dy = this.deltaPYFrom(character.cy());
    const radian = Knight.MOVEMENT.adjustRadian(Math.atan2(-dy, dx));
    this.moveRadian(radian);
};

Game_Character.prototype.turnTowardCharacter = function(character) {
    const dx = this.deltaPXFrom(character.cx());
    const dy = this.deltaPYFrom(character.cy());
    this.setRadian(Math.atan2(-dy, -dx));
};

Game_Character.prototype.turnAwayFromCharacter = function(character) {
    const dx = this.deltaPXFrom(character.cx());
    const dy = this.deltaPYFrom(character.cy());
    this.setRadian(Math.atan2(dy, dx));
};

Game_Character.prototype.turnRight90 = function() {
    switch (this.direction()) {
    case 2:
        this.setDirection(4);
        break;
    case 4:
        this.setDirection(8);
        break;
    case 6:
        this.setDirection(2);
        break;
    case 8:
        this.setDirection(6);
        break;
    case 1:
        this.setDirection(7);
        break;
    case 3:
        this.setDirection(1);
        break;
    case 7:
        this.setDirection(9);
        break;
    case 9:
        this.setDirection(3);
        break;
    }
};

Game_Character.prototype.turnLeft90 = function() {
    switch (this.direction()) {
    case 2:
        this.setDirection(6);
        break;
    case 4:
        this.setDirection(2);
        break;
    case 6:
        this.setDirection(8);
        break;
    case 8:
        this.setDirection(4);
        break;
    case 1:
        this.setDirection(3);
        break;
    case 3:
        this.setDirection(9);
        break;
    case 7:
        this.setDirection(1);
        break;
    case 9:
        this.setDirection(7);
        break;
    }
};

// Swaps this character's position with another's
Game_Character.prototype.swap = function(character) {
    const newDir = character._direction;
    const newX = character._realPX;
    const newY = character._realPY;
    character.setDirection(this._direction);
    this.setDirection(newDir);
    character.pixelLocate(this._realPX, this._realPY);
    this.pixelLocate(newX, newY);
    Knight.Utility.swapValue(this, character, "_isSelected");
    character.removeFromPathGrid();
    character.addToPathGrid();
    this.removeFromPathGrid();
    this.addToPathGrid();
};

//=============================================================================
// Game_Event
//=============================================================================
Game_Event.prototype.pixelLocate = function(px, py) {
    Game_Character.prototype.pixelLocate.call(this, px, py);
    this._prelockDirection = 0;
};

//=============================================================================
// Game_Player
//=============================================================================
Knight.MOVEMENT.Game_Player_initMembers = Game_Player.prototype.initMembers;
Game_Player.prototype.initMembers = function() {
    Knight.MOVEMENT.Game_Player_initMembers.call(this);
    this._distanceMoved = 0;
    this._isSelected = true;
    this._party = [this].concat(this.followers()._data);
};

Game_Player.prototype.party = function() {
    return this._party;
};

Game_Player.prototype.visibleParty = function() {
    return this._party.filter((character) => character.actor() && !character.isHidden());
};

Game_Player.prototype.selectedParty = function() {
    return this._party.filter((character) => character.isSelected());
};

Game_Player.prototype.pixelLocate = function(px, py) {
    Game_Character.prototype.pixelLocate.call(this, px, py);
    this.center(this.x, this.y);
    this.makeEncounterCount();
    if (this.isInVehicle()) {
        this.vehicle().refresh();
    }
};

Game_Player.prototype.movedOneTile = function() {
    return this._distanceMoved >= Knight.MOVEMENT.TileSize;
};

Knight.MOVEMENT.Game_Player_updateMove = Game_Player.prototype.updateMove;
Game_Player.prototype.updateMove = function() {
    Knight.MOVEMENT.Game_Player_updateMove.call(this);
    this._distanceMoved += this.frameSpeed();
};

Game_Player.prototype.update = function(sceneActive) {
    const lastScrolledX = this.scrolledX();
    const lastScrolledY = this.scrolledY();
    const wasMoving = this.isMoving();
    this.updateDashing();
    if (sceneActive) {
        this.moveByInput();
    }
    Game_Character.prototype.update.call(this);
    this.updateScroll(lastScrolledX, lastScrolledY);
    this.updateVehicle();
    if (!this.startedMoving()) this.updateNonmoving(wasMoving);
    this._followers.update();
};

Game_Player.prototype.updatePath = function() {
    // Does nothing
    // moveToDestination() is called inside moveByInput instead
};

Game_Player.prototype.moveByInput = function() {
    const startedMoving = this.startedMoving();
    if (this.canMove()) {
        if (!startedMoving && this.triggerAction()) return;
        const direction = Input.dir8;
        if (direction > 0) {
            // Break the path when moving with the keyboard
            if (this.hasPath()) {
                this.clearPath();
            }
            if (!startedMoving) {
                this.moveStraight(direction, this.frameSpeed());
            }
        } else {
            this.moveToDestination();
        }
    }
};

Game_Player.prototype.updateNonmoving = function(wasMoving) {
    if (!$gameMap.isEventRunning()) {
        if (wasMoving) {
            if (this.movedOneTile()) $gameParty.onPlayerWalk();
            this.checkEventTriggerHere([1, 2]);
            if ($gameMap.setupStartingEvent()) return;
        }
        if (this.triggerAction()) return;
        if (wasMoving) {
            if (this.movedOneTile()) {
                this.updateEncounterCount();
                this._distanceMoved = 0;
            }
        } else if (!this.isMoving() && !this.hasPath()) {
            this.clearDestination();
        }
    }
};

Game_Player.prototype.updateDashing = function() {
    if (this.canMove() && !this.isInVehicle() && !$gameMap.isDashDisabled()) {
        this._dashing = this.isDashButtonPressed();
    } else {
        this._dashing = false;
    }
};

Game_Player.prototype.placeFollowers = function() {
    this._followers.synchronize(this.x, this.y, this.direction(), true);
};

Game_Player.prototype.setupGridForPath = function() {
    Game_CharacterBase.prototype.setupGridForPath.call(this);
    this._followers.forEach(function(follower) {
        if (follower.startedMoving()) follower.removeFromPathGrid();
    });
};

Game_Player.prototype.restorePathGrid = function() {
    Game_CharacterBase.prototype.restorePathGrid.call(this);
    this._followers.forEach(function(follower) {
        if (follower.startedMoving()) follower.addToPathGrid();
    });
};

//=============================================================================
// Game_Follower
//=============================================================================
Game_Follower.prototype.setupGridForPath = function() {
    Game_CharacterBase.prototype.setupGridForPath.call(this);
    $gamePlayer.party().forEach(function(character) {
        if (character.startedMoving()) character.removeFromPathGrid();
    });
};

Game_Follower.prototype.restorePathGrid = function() {
    Game_CharacterBase.prototype.restorePathGrid.call(this);
    $gamePlayer.party().forEach(function(character) {
        if (this !== character && character.startedMoving()) character.addToPathGrid();
    });
};

//=============================================================================
// Game_Party
//=============================================================================
Knight.MOVEMENT.Game_Party_initialize = Game_Party.prototype.initialize;
Game_Party.prototype.initialize = function() {
    Knight.MOVEMENT.Game_Party_initialize.call(this);
    this._formation = this.makeFormation();
};

Game_Party.prototype.steps = function() {
    return Math.floor(this._steps);
};

Game_Party.prototype.increaseSteps = function() {
    this._steps += $gamePlayer.frameSpeed() / Knight.MOVEMENT.TileSize;
};

Game_Party.prototype.getActorIndex = function(actorId) {
    return this._actors.indexOf(actorId);
};

Game_Party.prototype.setLeader = function(character) {
    if (character !== $gamePlayer) {
        $gamePlayer.swap(character);
        Knight.Utility.swapValue($gamePlayer, character, "_pathData");
        const actorId = character.actor().actorId();
        const index = this.getActorIndex(actorId);
        this.swapOrder(0, index);
        $gamePlayer.select();
    }
};

/* eslint-disable no-multi-spaces */
Game_Party.prototype.makeFormation = function() {
    const refPoint = new PIXI.Vector(0, 0);
    const r = 24; // Approximate character half-width (i.e. collider radius)

    // Default: Box formation
    //      1P  2
    //
    //      3   4
    // P = Reference point (0,0) (I.e. mouse click destination)
    // Don't let characters stand too close to each other or the pathfinding may fail!
    // There should always be enough space between two characters in a formation for a
    // third to pass through.
    const p1 = new PIXI.Vector(refPoint.x,         refPoint.y);
    const p2 = new PIXI.Vector(refPoint.x + 3.5*r, refPoint.y);
    const p3 = new PIXI.Vector(refPoint.x,         refPoint.y + 3.5*r);
    const p4 = new PIXI.Vector(refPoint.x + 3.5*r, refPoint.y + 3.5*r);
    return [p1, p2, p3, p4];
};
/* eslint-enable no-multi-spaces */

Game_Party.prototype.getPosInFormation = function(id, dx, dy) {
    const grid = $gameMap.pathGrid();
    const heading = new PIXI.Vector(dx - $gamePlayer._px, dy - $gamePlayer._py);
    const angle = heading.unsignedAngle() + 0.5 * Math.PI;
    const pos = new PIXI.Vector(this._formation[id].x, this._formation[id].y);
    pos.rotate(angle);
    pos.x += dx;
    pos.y += dy;
    pos.x = pos.x.clamp(0, Knight.COLLISION.gridPixelWidth()-1);
    pos.y = pos.y.clamp(0, Knight.COLLISION.gridPixelHeight()-1);
    const gridPos = Knight.COLLISION.pointToGridTile(pos.x, pos.y);
    if (!grid.isValidEndpoint(gridPos.x, gridPos.y)) {
        const gridDest = Knight.COLLISION.pointToGridTile(dx, dy);
        console.time('=====> getNearestValidDestination');
        const ret = this.getNearestValidDestination(gridPos, gridDest);
        console.timeEnd('=====> getNearestValidDestination');
        return ret;
    }
    return pos;
};

Game_Party.prototype.getPosInFormationAfterTransfer = function(id) {
    const heading = $gamePlayer.directionToVector();
    const angle = heading.unsignedAngle() + 0.5 * Math.PI;
    const pos = new PIXI.Vector(this._formation[id].x, this._formation[id].y);
    pos.rotate(angle);
    pos.x += $gamePlayer._px;
    pos.y += $gamePlayer._py;
    pos.x = pos.x.clamp(0, Knight.COLLISION.gridPixelWidth()-1);
    pos.y = pos.y.clamp(0, Knight.COLLISION.gridPixelHeight()-1);
    return pos;
};

// Find the nearest valid pathfinding destination, given that the current destination is blocked.
Game_Party.prototype.getNearestValidDestination = function(start, dest) {
    const openList = [];
    const grid = $gameMap.pathGrid();
    const visited = new Array(grid.width() * grid.height()).fill(false);
    const minSqDist = Math.pow(48 / Knight.COLLISION.tileSizeX(), 2);

    // init open list
    for (let j = -1; j <= 1; ++j) {
        for (let i = -1; i <= 1; ++i) {
            const x = start.x + i;
            const y = start.y + j;
            if (grid.isValidEndpoint(x, y)) {
                const dx = x - dest.x;
                const dy = y - dest.y;
                const sqDist = dx*dx + dy*dy;
                openList.push({x: x, y: y, sqDist: sqDist});
                visited[grid.width()*y + x] = true;
            }
        }
    }
    openList.sort((a, b) => a.sqDist - b.sqDist);

    // Depth-first search, sorted by shortest distance to destination
    while (openList.length > 0) {
        const curr = openList.shift();
        if (grid.isValidEndpoint(curr.x, curr.y) &&
            grid.lineOfSight(curr.x, curr.y, dest.x, dest.y)) { // Done
            return Knight.COLLISION.gridTileToPoint(curr.x, curr.y);
        }

        // Add all adjacent unvisited nodes to the list
        for (let j = -1; j <= 1; ++j) {
            for (let i = -1; i <= 1; ++i) {
                const x = curr.x + i;
                const y = curr.y + j;
                if (grid.isValidBlock(x, y) && !visited[grid.width()*y + x]) {
                    const dx = x - dest.x;
                    const dy = y - dest.y;
                    const sqDist = dx*dx + dy*dy;
                    if (sqDist > minSqDist) {
                        openList.push({x: x, y: y, sqDist: sqDist});
                        visited[grid.width()*y + x] = true;
                    }
                }
            }
        }
        openList.sort((a, b) => a.sqDist - b.sqDist);
    }

    // Couldn't find anywhere to go
    return Knight.COLLISION.gridTileToPoint(start.x, start.y);
};

//=============================================================================
// Game_Followers
//=============================================================================
Knight.MOVEMENT.Game_Followers_synchronize = Game_Followers.prototype.synchronize;
Game_Followers.prototype.synchronize = function(x, y, d, usePathGrid = false) {
    if (!usePathGrid) {
        return Knight.MOVEMENT.Game_Followers_synchronize.call(this, x, y, d);
    }

    console.time('Synchronize');
    const grid = $gameMap.pathGrid();
    const placed = [$gamePlayer];
    const followersLeft = this._data.filter((follower) => follower.actor() && !follower.isHidden());
    if (followersLeft.length === 0) return;
    followersLeft.forEach((follower) => follower.removeFromPathGrid());

    // Try placing in formation first
    let formationId = 1;
    while (followersLeft.length > 0) {
        const follower = followersLeft[0];
        let pos = $gameParty.getPosInFormationAfterTransfer(formationId);
        const gridPos = Knight.COLLISION.pointToGridTile(pos.x, pos.y);
        if (!this.isValidFollowerPlacement(follower, gridPos, grid, placed)) {
            pos = this.getNearestValidPosition(follower, grid, placed);
        }
        followersLeft.shift();
        follower.pixelLocate(pos.x, pos.y);
        follower.setDirection(d);
        follower.addToPathGrid();
        placed.push(follower);
        ++formationId;
    }
    console.timeEnd('Synchronize');
};

// Use a breadth-first search to find the nearest open spot within a minimum
// distance away from the player and all other followers
Game_Followers.prototype.getNearestValidPosition = function(follower, grid, placed) {
    const openList = Array.from($gamePlayer._lastOccupiedTiles);
    const visited = new Array(grid.width() * grid.height()).fill(false);

    while (openList.length > 0) {
        const curr = openList.shift();
        if (this.isValidFollowerPlacement(follower, curr, grid, placed)) {
            return Knight.COLLISION.gridTileToPoint(curr.x, curr.y);
        }

        // Mark current node as visited
        visited[grid.width()*curr.y + curr.x] = true;

        // Add all adjacent unblocked, unvisited nodes to the list
        for (let j = -1; j <= 1; ++j) {
            for (let i = -1; i <= 1; ++i) {
                const x = curr.x + i;
                const y = curr.y + j;
                if (grid.isValidBlock(x, y) && !visited[grid.width()*y + x] && !grid.isBlockedRaw(x, y)) {
                    openList.push({x: x, y: y});
                }
            }
        }
    }

    // No reachable open spots available for one or more followers. This shouldn't ever
    // happen, but if it does, we could try repeating the above search but not prevent
    // blocked tiles from being added to the openList. So followers would end up being
    // placed potentially on the other side of a wall, but at least they would have a.
    // valid position. The other alternative is to put them on top of the player and find
    // some way to deal with the collision.
    throw new Error("ERROR: Couldn't place all followers!");
};

// Node is valid if all nodes follower would occupy are unblocked and further than
// sqrt(min_square_dist) away from all already placed characters.
Game_Followers.prototype.isValidFollowerPlacement = function(follower, node, grid, placedCharacters) {
    // Early exit
    if (grid.isBlocked(node.x, node.y)) return false;

    // Check distance
    const colliderRadius = follower.getCollider('collision').radius;
    const minDist = colliderRadius * 2 + 4;
    const minSquareDist = minDist*minDist;
    const nodePos = Knight.COLLISION.gridTileToPoint(node.x, node.y);
    let dx; let dy; let sqDist;
    for (character of placedCharacters) {
        dx = character._px - nodePos.x;
        dy = character._py - nodePos.y;
        sqDist = dx*dx + dy*dy;
        if (sqDist < minSquareDist) return false;
    }

    // Check if there's room to move there
    const bounds = {
        min_x: nodePos.x - colliderRadius,
        max_x: nodePos.x + colliderRadius,
        min_y: nodePos.y - colliderRadius,
        max_y: nodePos.y + colliderRadius,
    };
    const minTile = Knight.COLLISION.pointToGridTile(bounds.min_x, bounds.min_y);
    const maxTile = Knight.COLLISION.pointToGridTile(bounds.max_x, bounds.max_y);
    for (let y = minTile.y; y <= maxTile.y; ++y) {
        for (let x = minTile.x; x <= maxTile.x; ++x) {
            if (grid.isBlocked(x, y)) return false;
        }
    }

    return true;
};

Game_Followers.prototype.updateMove = function() {
    // Turn off the chase behavior
};

//=============================================================================
// Scene_Map
//=============================================================================
Knight.MOVEMENT.Scene_Map_initialize = Scene_Map.prototype.initialize;
Scene_Map.prototype.initialize = function() {
    Knight.MOVEMENT.Scene_Map_initialize.call(this);
    this._pathfindQueue = [];
};

Scene_Map.prototype.updateCallMenu = function() {
    if (this.isMenuEnabled()) {
        if (this.isMenuCalled()) {
            this.menuCalling = true;
        }
        if (this.menuCalling) { // Remove check for player moving
            this.callMenu();
        }
    } else {
        this.menuCalling = false;
    }
};

Scene_Map.prototype.processMapTouch = function() {
    if (TouchInput.isReleased() && $gamePlayer.canMove()) {
        const character = $gameMap.getMouseCollision();
        if (character && !character.isHidden()) {
            if (character instanceof Game_Event || character instanceof Sprite_Doodad) {
                // Set event as destination
                const x = $gameMap.canvasToMapPX(TouchInput.x);
                const y = $gameMap.canvasToMapPY(TouchInput.y);
                this.queuePathfind(x, y, character);
            } else if (character instanceof Game_Follower) {
                // Set follower as new party leader
                $gamePlayer.party().forEach((character) => character.deselect());
                $gameParty.setLeader(character);
            } else if (character instanceof Game_Player) {
                // Re-center camera on player
                $gamePlayer.followers().forEach((follower) => follower.deselect());
                $gamePlayer.select();
                $gamePlayer.center($gamePlayer.x, $gamePlayer.y);
            }
            // Ignore vehicles
        } else if ($gameMap.isMouseDestinationWalkable()) {
            // Set mouse location as destination
            const x = $gameMap.canvasToMapPX(TouchInput.x);
            const y = $gameMap.canvasToMapPY(TouchInput.y);
            this.queuePathfind(x, y);
        } else {
            console.log('Bad Path Destination!');
            $gamePlayer.party().forEach(function(character) {
                if (character.isSelected()) character.clearPath();
            });
        }
    }
};

Scene_Map.prototype.queuePathfind = function(dx, dy, event = null) {
    const party = $gamePlayer.selectedParty();
    this._pathfindQueue = [];
    for (i = 0; i < party.length; ++i) {
        this._pathfindQueue.push({
            char: party[i],
            dest: (i === 0 && event) ? {x: dx, y: dy} : $gameParty.getPosInFormation(i, dx, dy),
            event: event,
        });
        // Remove characters from the pathfinding grid so they move together instead of
        // trying to walk around each other
        party[i].removeFromPathGrid();
    }
};

Scene_Map.prototype.updatePathQueue = function() {
    if (this._pathfindQueue.length > 0) {
        const data = this._pathfindQueue.shift();
        data.char.setDestination(data.dest.x, data.dest.y);
        data.char.setDestinationEvent(data.event);
    }
};

Knight.MOVEMENT.Scene_Map_updateDestination = Scene_Map.prototype.updateDestination;
Scene_Map.prototype.updateDestination = function() {
    Knight.MOVEMENT.Scene_Map_updateDestination.call(this);
    this.updatePathQueue();
};

//=============================================================================
// Spriteset_Map
//=============================================================================
Spriteset_Map.prototype.createDestination = function() {
    // Moved to createDestinations
};

Spriteset_Map.prototype.createDestinations = function() {
    // https://pixiplayground.com/#/edit/889Ns6-7acEVEYJXlwnv9
    this._destinationSprites = [];
    const graphics = new PIXI.Graphics();
    graphics.position.set(0, 0);

    // Draw an ellipse with 4 small arrows inside of it
    const a = 12;       // Ellipse horizontal half-width
    const b = 6;        // Ellipse vertical half-height
    const aLen = 0.60;  // Arrow length. Lower numbers => bigger arrows.
    const color = 0xFF0000; //0x37D613;
    const points = [
        {x: a*aLen, y: 0},
        Knight.Utility.getEllipsePoint(a, b, 360 - 11.25),
        Knight.Utility.getEllipsePoint(a, b, 0 + 11.25),

        {x: 0, y: b*aLen},
        Knight.Utility.getEllipsePoint(a, b, 90 - 22.5),
        Knight.Utility.getEllipsePoint(a, b, 90 + 22.5),

        {x: -a*aLen, y: 0},
        Knight.Utility.getEllipsePoint(a, b, 180 - 11.25),
        Knight.Utility.getEllipsePoint(a, b, 180 + 11.25),

        {x: 0, y: -b*aLen},
        Knight.Utility.getEllipsePoint(a, b, 270 - 22.5),
        Knight.Utility.getEllipsePoint(a, b, 270 + 22.5),
    ];

    graphics.lineStyle(1, color, 1);
    graphics.beginFill(0x000000, 0);
    graphics.drawEllipse(0, 0, a, b);
    graphics.endFill();

    graphics.beginFill(color, 1);
    for (let i = 0; i < points.length; i += 3) {
        const path = [points[i].x, points[i].y, points[i+1].x, points[i+1].y, points[i+2].x, points[i+2].y];
        graphics.drawPolygon(path);
    }
    graphics.endFill();

    const texture = Graphics._renderer.generateTexture(graphics);
    for (const character of $gamePlayer.party()) {
        const sprite = new Sprite_Pixel_Destination(texture, character);
        this._destinationSprites.push(sprite);
        this._tilemap.addChild(sprite);
    }
    graphics.destroy(true);
};

Spriteset_Map.prototype.createSelection = function() {
    const graphics = new PIXI.Graphics();
    const color = 0x37D613;
    this._selectionSprites = [];
    this._selectionSpriteFilter = new PIXI.filters.GlowFilter(5, 2, 0, color, 0.3);
    graphics.position.set(0, 0);
    graphics.lineStyle(2, color, 1);
    graphics.beginFill('0x000000', 0);
    graphics.drawEllipse(0, 0, 24, 12);
    graphics.endFill();
    const texture = Graphics._renderer.generateTexture(graphics);
    for (const character of $gamePlayer.party()) {
        const sprite = new Sprite_Selection(texture, character);
        sprite.filters = [this._selectionSpriteFilter];
        this._selectionSprites.push(sprite);
        this._tilemap.addChild(sprite);
    }
    graphics.destroy(true);
};

Knight.MOVEMENT.Spriteset_Map_createCharacters = Spriteset_Map.prototype.createCharacters;
Spriteset_Map.prototype.createCharacters = function() {
    this.createDestinations();
    this.createSelection();
    Knight.MOVEMENT.Spriteset_Map_createCharacters.call(this);
};

Spriteset_Map.prototype.free = function() {
    this.removeChild(this._selectionBox);
    this._selectionBox.free();
    this._destinationSprites.forEach(function(sprite) {
        this._tilemap.removeChild(sprite);
        sprite.destroy(true);
    }, this);
    this._selectionSprites.forEach(function(sprite) {
        this._tilemap.removeChild(sprite);
        sprite.destroy(true);
    }, this);
};

//=============================================================================
// Sprite_Pixel_Destination
//=============================================================================
/**
 * @class Sprite_Pixel_Destination
 * @extends {Sprite}
 */
class Sprite_Pixel_Destination extends PIXI.Sprite {
    /**
     * Creates an instance of Sprite_Pixel_Destination.
     * @param {PIXI.Texture} texture
     * @param {Game_Character} character
     * @memberof Sprite_Pixel_Destination
     */
    constructor(texture, character) {
        super(texture);
        this._character = character;
        this._frameCount = 0;
        this._scaleIncr = 0.01;
    }

    /**
     * @memberof Sprite_Pixel_Destination
     */
    update() {
        if (this._character.isDestinationValid()) {
            this.updatePosition();
            this.updateAnimation();
            this.visible = true;
        } else {
            this._frameCount = 0;
            this.scale.x = 1;
            this.scale.y = 1;
            this.visible = false;
        }
    }

    /**
     * @memberof Sprite_Pixel_Destination
     */
    updatePosition() {
        const tileWidth = $gameMap.tileWidth();
        const tileHeight = $gameMap.tileHeight();
        const x = this._character.destinationX() - this.width/2;
        const y = this._character.destinationY() - this.height/2;
        const screenX = Math.round(x - ($gameMap._displayX * tileWidth));
        const screenY = Math.round(y - ($gameMap._displayY * tileHeight));
        this.position.set(screenX, screenY);
        // this.x = ($gameMap.adjustX(x) + 0.5) * tileWidth;
        // this.y = ($gameMap.adjustY(y) + 0.5) * tileHeight;
    }

    /**
     * @memberof Sprite_Pixel_Destination
     */
    updateAnimation() {
        this.scale.x += this._scaleIncr;
        this.scale.y = this.scale.x;
        if (this.scale.x > 1.25 || this.scale.x < 1) {
            this._scaleIncr *= -1;
        }
    }
};

//=============================================================================
// Sprite_Selection
//=============================================================================
/**
 * @class Sprite_Selection
 * @extends {Sprite}
 */
class Sprite_Selection extends PIXI.Sprite {
    /**
     * Creates an instance of Sprite_Selection.
     * @param {PIXI.Texture} texture
     * @param {Game_Character} character
     * @memberof Sprite_Selection
     */
    constructor(texture, character) {
        super(texture);
        this._character = character;
    }

    /**
     * @memberof Sprite_Selection
     */
    update() {
        this.visible = this._character.isSelected();
        const x = this._character._realPX + 48 - this.width - 0.5;
        const y = this._character._realPY + 48 - this.height;
        const screenX = Math.round(x - ($gameMap._displayX * $gameMap.tileWidth()));
        const screenY = Math.round(y - ($gameMap._displayY * $gameMap.tileHeight()));
        this.position.set(screenX, screenY);
    }
};
