// =============================================================================
// Knight Engine Plugins - Knight Editor
// KNT_Editor.js
// =============================================================================

//=============================================================================
/*:
 * @plugindesc A Point-and-click UI written for Yanfly's Doodads plugin.
 * @version 1.0.0
 * @author Kaelan
 *
 * @param ---General---
 * @default
 *
 * @param Editor Toggle Key
 * @parent ---General---
 * @desc Key used to toggle the editor. F10 by default.
 * @type text
 * @default F10
 *
 * @param Doodad Selection Effect
 * @parent ---General---
 * @desc How should the editor highlight the currently selected doodad?
 * @type select
 * @option No Change
 * @option Outline (Red)
 * @option Outline (Black)
 * @option Increase Brightness
 * @option Increase Contrast
 * @default Increase Brightness
 *
 * @param ---Font---
 * @default
 *
 * @param Editor
 * @parent ---Font---
 * @desc The font used in the toolbar & doodad selection windows.
 * @type text
 * @default GameFont
 *
 * @param Edit Panel
 * @parent ---Font---
 * @desc The font used in the Doodad Edit Panel.
 * @type text
 * @default GameFont
 *
 * @param ---SE---
 * @default
 *
 * @param Toggle
 * @parent ---SE---
 * @desc Sound clip played when the editor is toggled.
 * @type file
 * @dir audio/se/
 * @default Book1
 *
 * @param Place
 * @parent ---SE---
 * @desc Sound clip played when a new doodad is placed.
 * @type file
 * @dir audio/se/
 * @default Key
 *
 * @param Minimize
 * @parent ---SE---
 * @desc Sound clip played when the editor is minimized.
 * @type file
 * @dir audio/se/
 * @default Cursor1
 *
 * @param Undo
 * @parent ---SE---
 * @desc Sound clip played when the undo button is used.
 * @type file
 * @dir audio/se/
 * @default Cancel2
 *
 * @param Reset Settings
 * @parent ---SE---
 * @desc Sound clip played when the revert to default button is used.
 * @type file
 * @dir audio/se/
 * @default Cancel2
 *
 * @param Delete
 * @parent ---SE---
 * @desc Sound clip played when the delete button is used.
 * @type file
 * @dir audio/se/
 * @default Cancel2
 *
 * @param Delete All
 * @parent ---SE---
 * @desc Sound clip played when the delete all button is used.
 * @type file
 * @dir audio/se/
 * @default Cancel2
 *
 * @param Save
 * @parent ---SE---
 * @desc Sound clip played when the save button is used.
 * @type file
 * @dir audio/se/
 * @default Save
 *
 * @param Copy
 * @parent ---SE---
 * @desc Sound clip played when the copy button is used.
 * @type file
 * @dir audio/se/
 * @default Decision1
 *
 * @param Region Toggle
 * @parent ---SE---
 * @desc Sound clip played when the region overlay toggle button is used.
 * @type file
 * @dir audio/se/
 * @default Cursor1
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 * Provides a point-and-click UI for use with Yanfly's Grid Free Doodads plugin.
 *
 * ============================================================================
 * Usage Requirements
 * ============================================================================
 *
 * For this plugin to work, you must do the following:
 *
 * 1. Install Yanfly's Grid Free Doodads plugin, as per the plugin instructions.
 *    Make sure it works correctly before continuing.
 *
 * 2. Copy the 'gui' folder that comes with this plugin into your project's 'img'
 *    folder.
 *
 * 3. Copy the 'pixi-filters.js' file that comes with this plugin into your
 *    project's '/js/libs' folder.
 *
 * 4. Replace your project's 'index.html' file with the one that comes with this
 *    plugin. Alternatively, if you'd like to keep your current 'index.html' file,
 *    insert the following line inside the <body></body> tag of your index file,
 *    after all other pixi-___.js files:
 *    <script type="text/javascript" src="js/libs/pixi-filters.js"></script>
 *
 * 5. Place this plugin below Yanfly's plugin in your load order and configure
 *    the plugin parameters to your liking.
 *
 * ============================================================================
 * Additional Notes and Restrictions
 * ============================================================================
 *
 * Please note that there are a few differences when using this plugin, when
 * compared to Yanfly's built-in editor:
 *
 * 1. Subfolders are not supported. All doodads must be in a folder of the form
 *    "doodads/category/doodad.png". So, "doodads/Plants/Tree1.png" works, but
 *     not "doodads/Plants/Trees/Tree1.png". In this case, please move the
 *     Tree folder, so it's directly under 'doodads/'.
 *
 * 2. Switches & Party Member Checks from Yanfly's 'Extended Doodads Pack 1'
 *    plugin are currently not supported.
 *
 * ============================================================================
 * Instructions - Controls
 * ============================================================================
 *
 * This plugin changes Yanfly's default Doodad editor to function via mouse
 * point-and-click controls.
 *
 * To place a Doodad, press F10 to open the editor, click on one of the Doodad
 * folders on the bottom right window, then click on the image of any Doodad
 * you'd like to place on the map.
 *
 * With the editor open, the following commands are available:
 *
 *  Editor Toggle (Hotkey: F10)
 *  - Use this to bring up the editor while in Play-Test mode. Press it again
 *    to make the editor disappear.
 *
 *  Map Scroll (Hokeys: W A S D)
 *  - Scrolls the map in the given direction.
 *
 *  Edit Doodad
 *  - When no Doodads are selected, left-click on an existing Doodad to bring up the Doodad
 *    Edit Panel.
 *
 *  Move Doodad
 *  - When no Doodads are selected, click-and-drag an existing Doodad to move it. Alternatively,
 *    you can also click on it, then move it with the arrow keys. Using the arrow keys moves the
 *    Doodad one pixel at a time, for precise placement. Holding Shift while pressing an arrow
 *    key allows for continous movement.
 *
 *  Window Scroll (Hotkey: Mouse Wheel)
 *  - When hovering the mouse over either the Doodad Selection window, or the Doodad Folder window,
 *    use the mouse wheel to scroll the contents of the window, if you have more Doodads or Folders
 *    than the window can display at once.
 *
 *  Import (Hotkey: I)
 *  - Imports Doodads from another map. Helpful when making many similar maps.
 *    Can also be used by clicking on the Import icon on the toolbar.
 *
 *  Minimize (Hotkey: N)
 *  - Hides all of the editor windows, except the toolbar. This is helpful when
 *    trying to place doodads in a part of your map that is behind one of the
 *    editor windows. Can also be used by clicking on the Minimize icon on
 *    the toolbar. Clicking the button again causes windows to re-appear.
 *
 *  Save (Hotkey: Ctrl + S)
 *  - Save your current doodad settings to your Doodads.json file. Can also be
 *    used by clicking on the Save icon on the toolbar.
 *
 *  Undo (Hokey: Ctrl + Z)
 *  - Reverses the most recent change. This can also reverse the effects of the
 *    'Delete', 'Delete All' and 'Import' buttons. The editor only keeps track
 *    of a single change at a time - pressing the button multiple times has no
 *    effect. Can also be used by clicking the Undo icon on the toolbar.
 *
 *  Delete All
 *  - Deletes all Doodads on the map. Can only be used by clicking the Delete All
 *    icon on the toolbar. There's no hotkey for this for safety reasons. If
 *    you accidentally press the Delete All button, you can bring your Doodads
 *    back by pressing Undo (Ctrl + Z).
 *
 *  Toggle Region Overlay (Hotkey: R)
 *  - Toggles the region ovelay on or off. This is the same as Yanfly's. Can also
 *    be used by clicking the Region icon on the toolbar.
 *
 * Additionally, the following commands can be used when you have a Doodad selected:
 *
 *  Place Doodad
 *  - With a Doodad on your cursor, left-click on the map to place it.
 *
 *  Cancel Doodad Placement
 *  - With a Doodad on your cursor, right-click to unselect it.
 *
 *  Revert to Default Settings (Hotkey: Backspace)
 *  - Restores the default settings for the selected Doodad. Can also be used by clicking
 *    on the Revert icon on the top-right corner of the Doodad Edit Panel.
 *
 *  Copy Doodad (Hotkey: C)
 *  - Makes a copy of the currently selected Doodad and places it on your mouse cursor,
 *    so you can place more of them on the map. Useful if you want to place a Doodad, spend
 *    time tweaking it, then make many copies of it. Can also be used by clicking on the
 *    Copy icon on the top-right corner of the Doodad Edit Panel.
 *
 *  Delete Doodad (Hotkey: Del)
 *  - Deletes the currently selected Doodad. Can also be used by clicking on the Delete icon
 *    on the top-right corner of the Doodad Edit Panel.
 *
 *  Change Layer (Hotkey: Mouse Wheel)
 *  - Scroll the mouse wheel up or down to change the layer of the currently selected Doodad.
 *    This is the same as moving the Layer slider in the Doodad Edit Panel.
 *
 *  Change Opacity (Hotkey: Number Keys)
 *  - Number keys from 1 to 0 will change the currently selected Doodad's opacity. '1' sets opacity
 *    to 0 (fully transparent), while '0' sets it to 255 (fully visible).
 *
 *  Change Scale (Hotkey: Q/E)
 *  - E increases the currently selected Doodad's size by 0.1 (i.e. 10%), while Q decreases it by
 *    the same amount. Both X and Y scale are changed together. Holding Shift while pressing Q/E
 *    will change the scale by 1.0 (i.e. 100%) at a time.
 *
 *  Toggle Grid Lock Mode (Hotkey: G)
 *  - Toggles whether Doodads should be moved freely or along grid tiles. You can adjust the size
 *    of the grid in the Doodad Edit Panel. This is the same as clicking the Grid Lock box in
 *    the Doodad Edit Panel.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * The 'Grid Free Doodads' plugin was originally written by Yanfly:
 * http://www.yanfly.moe/wiki/Grid-Free_Doodads_(YEP)
 *
 * Icons used under the free licence from:
 * https://icons8.com
 *
 * pixi-filters is provided under the MIT license:
 * https://github.com/pixijs/pixi-filters/blob/master/LICENSE
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 * This plugin may be freely used in any free or commercial project. You are
 * free to modify or distribute this plugin, as long you credit the original
 * author when doing so.
 *
 * If you use this plguin to develop your project, credits are appreciated but
 * not required.
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.10:
 * - Added support for switches and party member checks from YEP_X_ExtDoodadPack1
 * - Fixed a bug in original YEP code when a Doodad is loaded requiring a switch
 *   or party member that no longer exists in the database.
 * - Fixed an issue with Doodad position outside Test Play mode.
 *
 * Version 1.00:
 * - First Version!
 */

var Imported = Imported || {}; // eslint-disable-line no-var
Imported.KNT_Editor = '1.1.0';

if (!Imported.YEP_GridFreeDoodads) {
    alert('Error: Knight Object Editor requires YEP_GridFreeDoodads to work.');
    throw new Error('Error: Knight Object Editor requires YEP_GridFreeDoodads to work.');
}

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.EDITOR = Knight.EDITOR || {};
Knight.INPUT = Knight.INPUT || {};
Knight.Editor = Knight.Editor || {};

//=============================================================================
// Parameter Variables
//=============================================================================
Knight.Parameters = PluginManager.parameters('KNT_Editor');
Knight.Param = Knight.Param || {};

Knight.Param.KEFilterType = String(Knight.Parameters['Doodad Selection Effect']);
Knight.Param.KEFont = String(Knight.Parameters['Editor']);
Knight.Param.KEPanelFont = String(Knight.Parameters['Edit Panel']);
Knight.Param.KEToggleKey = String(Knight.Parameters['Editor Toggle Key']).toLowerCase();

Knight.Param.KESEToggle = String(Knight.Parameters['Toggle']);
Knight.Param.KESEPlace = String(Knight.Parameters['Place']);
Knight.Param.KESEMinimize = String(Knight.Parameters['Minimize']);
Knight.Param.KESEUndo = String(Knight.Parameters['Undo']);
Knight.Param.KESEReset = String(Knight.Parameters['Reset Settings']);
Knight.Param.KESEDelete = String(Knight.Parameters['Delete']);
Knight.Param.KESEClear = String(Knight.Parameters['Delete All']);
Knight.Param.KESESave = String(Knight.Parameters['Save']);
Knight.Param.KESECopy = String(Knight.Parameters['Copy']);
Knight.Param.KESERegion = String(Knight.Parameters['Region Toggle']);

//=============================================================================
// Utility Functions
//=============================================================================
Knight.Editor.isActive = function() {
    return SceneManager.sceneIs(Scene_Map) &&
        SceneManager._scene._editor &&
        SceneManager._scene._editor.active();
};

//=============================================================================
// SceneManager
//=============================================================================
SceneManager.sceneIs = function(sceneClass) {
    return this._scene && this._scene.constructor === sceneClass;
};

//=============================================================================
// TouchInput
// - Add a version of isLongPressed that takes the wait as a parameter
// - Update TouchInput x/y when mouse moves
// - Fix the mouse wheel preventDefault bug in Chrome.
//=============================================================================
TouchInput.isLongPressedFor = function(keyRepeatWait) {
    return this.isPressed() && this._pressedTime >= keyRepeatWait;
};

TouchInput._onMouseMove = function(event) {
    const x = Graphics.pageToCanvasX(event.pageX);
    const y = Graphics.pageToCanvasY(event.pageY);
    this._onMove(x, y);
};

TouchInput._setupEventHandlers = function() {
    var isSupportPassive = Utils.isSupportPassiveEvent(); /* eslint-disable-line no-var */
    document.addEventListener('mousedown', this._onMouseDown.bind(this));
    document.addEventListener('mousemove', this._onMouseMove.bind(this));
    document.addEventListener('mouseup', this._onMouseUp.bind(this));
    document.addEventListener('wheel', this._onWheel.bind(this), isSupportPassive ? {passive: false} : false);
    document.addEventListener('touchstart', this._onTouchStart.bind(this), isSupportPassive ? {passive: false} : false);
    document.addEventListener('touchmove', this._onTouchMove.bind(this), isSupportPassive ? {passive: false} : false);
    document.addEventListener('touchend', this._onTouchEnd.bind(this));
    document.addEventListener('touchcancel', this._onTouchCancel.bind(this));
    document.addEventListener('pointerdown', this._onPointerDown.bind(this));
};

//=============================================================================
// Yanfly Overwrites
//=============================================================================
Sprite_Doodad.prototype.initData = function() {
    if (this._data.scaleX > 10 || this._data.scaleX < -10) this._data.scaleX /= 100;
    if (this._data.scaleY > 10 || this._data.scaleY < -10) this._data.scaleY /= 100;
    this._data.toneRed = this._data.toneRed || 0;
    this._data.toneGreen = this._data.toneGreen || 0;
    this._data.toneBlue = this._data.toneBlue || 0;
    this._data.toneGrey = this._data.toneGrey || 0;
    this._currentCount = 0;
    this.x = this._data.x;
    this.y = this._data.y;
    this.z = this._data.z;
    this._iconIndex = this._data.iconIndex || 0;
    this._xFrames = this._data.xFrames || 1;
    this._yFrames = this._data.yFrames || 1;
    this._index = this._xFrames * this._yFrames - 1;
    this._frameUpdate = this._data.frameUpdate || 15;
    this.anchor.x = this._data.anchorX;
    this.anchor.y = this._data.anchorY;
    this.scale.x = this._data.scaleX;
    this.scale.y = this._data.scaleY;
    if (this.scale.x <= 0) {
        if (this.anchor.x === 0) {
            this.anchor.x = 1;
        } else if (this.anchor.x === 1) {
            this.anchor.x = 0;
        }
    }
    if (this.scale.y <= 0) {
        if (this.anchor.y === 0) {
            this.anchor.y = 1;
        } else if (this.anchor.y === 1) {
            this.anchor.y = 0;
        }
    }
    this.blendMode = this._data.blend || 0;
    this.opacity = this._data.opacity || 0;
    const folder = this._data.folder || '';
    const path = folder + this._data.bitmap;
    this.bitmap = ImageManager.loadDoodad(path, 0, this._data.smooth || false);
    const toneRed = this._data.toneRed;
    const toneGreen = this._data.toneGreen;
    const toneBlue = this._data.toneBlue;
    const toneGrey = this._data.toneGrey;
    this.setColorTone([toneRed, toneGreen, toneBlue, toneGrey]);
    this.updatePosition();
    this.updateFrame();
    this.initSwitches();
    this._loadedData = true;
};

Sprite_Doodad.prototype.initSwitches = function() {
    if (Imported.YEP_X_ExtDoodadPack1) {
        // Fix a bug with Yanfly's original code: When a switch or actor requirement is saved to Doodad data,
        // then later removed from the Database, the Doodad will continue checking for it even though it no
        // longer exists, causing the check to always fail but not allowing you to change Doodad settings via
        // menus. To fix, filter for valid switches/actorIDs on load.
        this.switchOn = this._data.switchOn ? this._data.switchOn.filter((id) => id < $dataSystem.switches.length) : [];
        this.switchOff = this._data.switchOff ? this._data.switchOff.filter((id) => id < $dataSystem.switches.length) : [];
        this.partyHave = this._data.partyHave ? this._data.partyHave.filter((id) => id < $dataActors.length) : [];
        this.partyMiss = this._data.partyMiss ? this._data.partyMiss.filter((id) => id < $dataActors.length) : [];
    }
};

Sprite_Doodad.prototype.update = function() {
    Sprite_Base.prototype.update.call(this);
    this.updatePosition();
    if (!this._loadedData) return;
    this.updateCustomA();
    this.updateFrame();
    this.updateCustomZ();
    if (!this._loadedData) return;
    this.updateSwitches();
};

Sprite_Doodad.prototype.updateSwitches = function() {
    if (Imported.YEP_X_ExtDoodadPack1 && !Knight.Editor.isActive()) {
        this.updateCustomEDP1Z();
    }
};

//=============================================================================
// Spriteset_Map
//=============================================================================
Spriteset_Map.prototype.doodads = function() {
    return this._doodads;
};

//=============================================================================
// Play Test Only
//=============================================================================
if (Knight.EDITOR.isTestPlay()) {
    //=============================================================================
    // Declarations & Utility Functions
    //=============================================================================
    if (!Imported.KNT_Movement) {
        Game_Map.prototype.roundPX = function(px) {
            return this.isLoopHorizontal() ? px.mod(this.width() * Knight.MOVEMENT.TileSize) : px;
        };

        Game_Map.prototype.roundPY = function(py) {
            return this.isLoopVertical() ? py.mod(this.height() * Knight.MOVEMENT.TileSize) : py;
        };

        Game_Map.prototype.canvasToMapPX = function(px) {
            const originX = this.displayX() * this.tileWidth();
            return Math.round(this.roundPX(originX + px));
        };

        Game_Map.prototype.canvasToMapPY = function(py) {
            const originY = this.displayY() * this.tileHeight();
            return Math.round(this.roundPY(originY + py));
        };
    }

    //=============================================================================
    // Input
    // Add input checking for various keys, with compatibility for QInput key codes.
    // Delete key isn't included in QInput for some reason, so we add it here too.
    //=============================================================================
    Knight.EDITOR.KEYS = {
        8: 'backspace', 9: 'tab', 13: 'enter', 16: 'shift', 17: 'ctrl', 18: 'alt',
        27: 'esc', 32: 'space', 33: 'pageup', 34: 'pagedown', 37: 'left',
        38: 'up', 39: 'right', 40: 'down', 45: 'escape', 46: 'del',
        48: '0', 49: '1', 50: '2', 51: '3', 52: '4', 53: '5', 54: '6',
        55: '7', 56: '8', 57: '9',
        96: 'num0', 97: 'num1', 98: 'num2', 99: 'num3', 100: 'num4',
        101: 'num5', 102: 'num6', 103: 'num7', 104: 'num8', 105: 'num9',
        65: 'a', 66: 'b', 67: 'c', 68: 'd', 69: 'e', 70: 'f', 71: 'g',
        72: 'h', 73: 'i', 74: 'j', 75: 'k', 76: 'l', 77: 'm', 78: 'n',
        79: 'o', 80: 'p', 81: 'q', 82: 'r', 83: 's', 84: 't', 85: 'u',
        86: 'v', 87: 'w', 88: 'x', 89: 'y', 90: 'z',
        112: 'f1', 113: 'f2', 114: 'f3', 115: 'f4', 116: 'f5', 117: 'f6',
        118: 'f7', 119: 'f8', 120: 'f9', 121: 'f10', 122: 'f11', 123: 'f12',
        186: 'semicolon', 187: 'equal', 188: 'comma', 189: 'minus', 190: 'period',
        191: 'slash', 192: 'grave', 219: 'openbracket', 220: 'backslash',
        221: 'closedbracket', 222: 'singlequote',
    };
    if (!Object.values(Knight.EDITOR.KEYS).includes(Knight.Param.KEToggleKey)) {
        Knight.Param.KEToggleKey = 'f10';
    }
    if (Imported.QInput) {
        for (keyCode in Knight.EDITOR.KEYS) {
            if (Knight.EDITOR.KEYS.hasOwnProperty(keyCode)) {
                const keyName = Knight.EDITOR.KEYS[keyCode];
                if (!QInput.keys.hasOwnProperty(keyCode)) QInput.keys[keyCode] = keyName;
                Knight.INPUT[keyName] = '#' + keyName;
            }
        }
    } else {
        for (keyCode in Knight.EDITOR.KEYS) {
            if (Knight.EDITOR.KEYS.hasOwnProperty(keyCode)) {
                const keyName = Knight.EDITOR.KEYS[keyCode];
                if (!Input.keyMapper.hasOwnProperty(keyCode)) {
                    Input.keyMapper[keyCode] = keyName;
                    Knight.INPUT[keyName] = keyName;
                } else {
                    Knight.INPUT[keyName] = Input.keyMapper[keyCode];
                }
            }
        }
    }

    // Compatibility Patch for Yanfly's Keyboard Config
    // Yanfly overwrites the default Input.keyMapper, so I need to re-insert
    // the editor's trigger key in his keyMapper every time he loads it.
    if (Imported.YEP_KeyboardConfig) {
        Knight.EDITOR.ConfigManager_applyKeyConfig = ConfigManager.applyKeyConfig;
        ConfigManager.applyKeyConfig = function() {
            for (keyCode in Knight.EDITOR.KEYS) {
                if (Knight.EDITOR.KEYS.hasOwnProperty(keyCode)) {
                    const keyName = Knight.EDITOR.KEYS[keyCode];
                    if (Knight.Param.KEToggleKey === keyName) {
                        if (this.keyMapper.hasOwnProperty(keyCode)) {
                            Knight.Param.KEToggleKey = this.keyMapper[keyCode];
                            Knight.INPUT[Knight.Param.KEToggleKey] = Knight.Param.KEToggleKey;
                        } else {
                            this.keyMapper[keyCode] = keyName;
                        }
                    }
                }
            }
            Knight.EDITOR.ConfigManager_applyKeyConfig.call(this);
        };
    }

    //=============================================================================
    // Game_Player
    //=============================================================================
    Knight.EDITOR.Game_Player_canMove = Game_Player.prototype.canMove;
    Game_Player.prototype.canMove = function() {
        if (Knight.Editor.isActive()) return false;
        return Knight.EDITOR.Game_Player_canMove.call(this);
    };

    //=============================================================================
    // Game_Event
    //=============================================================================
    Knight.EDITOR.Game_Event_updateSelfMovement = Game_Event.prototype.updateSelfMovement;
    Game_Event.prototype.updateSelfMovement = function() {
        if (Knight.Editor.isActive()) return;
        Knight.EDITOR.Game_Event_updateSelfMovement.call(this);
    };

    //=============================================================================
    // Game_Map
    //=============================================================================
    Knight.EDITOR.Game_Map_isEventRunning = Game_Map.prototype.isEventRunning;
    Game_Map.prototype.isEventRunning = function() {
        if (Knight.Editor.isActive()) return true;
        return Knight.EDITOR.Game_Map_isEventRunning.call(this);
    };

    //=============================================================================
    // Scene_Map
    //=============================================================================
    if (Imported.KNT_Collision) {
        Knight.EDITOR.Scene_Map_updateCharacterSelection = Scene_Map.prototype.updateCharacterSelection;
        Scene_Map.prototype.updateCharacterSelection = function() {
            if (Knight.Editor.isActive()) return;
            Knight.EDITOR.Scene_Map_updateCharacterSelection.call(this);
        };
    }

    Knight.EDITOR.Scene_Map_isDebugCalled = Scene_Map.prototype.isDebugCalled;
    Scene_Map.prototype.isDebugCalled = function() {
        if (Knight.Editor.isActive()) return false;
        return Knight.EDITOR.Scene_Map_isDebugCalled.call(this);
    };


    Knight.EDITOR.Scene_Map_update = Scene_Map.prototype.update;
    Scene_Map.prototype.update = function() {
        Knight.EDITOR.Scene_Map_update.call(this);
        this._editor.update();
    };

    Knight.EDITOR.Scene_Map_createAllWindows = Scene_Map.prototype.createAllWindows;
    Scene_Map.prototype.createAllWindows = function() {
        Knight.EDITOR.Scene_Map_createAllWindows.call(this);
        this.createEditor();
    };

    Scene_Map.prototype.createEditor = function() {
        this._editor = new Knight.Editor.KnightEditor(this);
    };

    Knight.EDITOR.Scene_Map_isMenuEnabled = Scene_Map.prototype.isMenuEnabled;
    Scene_Map.prototype.isMenuEnabled = function() {
        return Knight.EDITOR.Scene_Map_isMenuEnabled.call(this) && !Knight.Editor.isActive();
    };

    Scene_Map.prototype.currentDoodad = function() {
        return this._editor._currentDoodad;
    };

    Knight.EDITOR.Scene_Map_createDisplayObjects = Scene_Map.prototype.createDisplayObjects;
    Scene_Map.prototype.createDisplayObjects = function() {
        Knight.EDITOR.Scene_Map_createDisplayObjects.call(this);
        if (Imported.KNT_CollisionViewer) this.addChild(this.debugContainer());
        if (Imported.KNT_Editor) this.addChild(this.editorContainer());
    };

    Scene_Map.prototype.debugContainer = function() {
        this._debugContainer = this._debugContainer || new Knight.Container();
        return this._debugContainer;
    };

    Scene_Map.prototype.editorContainer = function() {
        this._editorContainer = this._editorContainer || new Knight.Container();
        return this._editorContainer;
    };

    //=============================================================================
    // Knight Editor
    //=============================================================================
    Knight.EDITOR.SELECTION_MODE = 0;
    Knight.EDITOR.PLACEMENT_MODE = 1;
    /**
     * A manager class that handles all of the editor logic, so we don't have to
     * write it directly in the Scene.
     *
     * @class KnightEditor
     */
    Knight.Editor.KnightEditor = class {
        /**
         *Creates an instance of KnightEditor.
        * @memberof KnightEditor
        *
        * @param {Scene_Base} scene  Current scene
        */
        constructor(scene) {
            this.initData(scene);
            this.createWindows();
            this.setInputHandlers();
            this.addWindowsToScene(scene);
            this.updateUndoQueue(null);
        }

        /**
         * @param {Scene} scene
         */
        initData(scene) {
            this._active = false;
            this._windows = [];
            this._buttons = [];
            this._windowGroups = {};
            this._handlers = {};
            this._managers = {};
            const windowGroups = Knight.Editor.KnightEditor.windowGroups();
            windowGroups.forEach(function(group) {
                this._windowGroups[group] = [];
                this._handlers[group] = {};
                this._managers[group] = null;
            }, this);
            this._activeGroup = windowGroups[0];
            this._spriteset = scene._spriteset;
            this._mouseOnUi = false;
            this._mode = Knight.EDITOR.SELECTION_MODE;
            this._outlineFilter = this.makeOutlineFilter();
            this._draggingDoodad = false;
            this._minimized = false;

            // Doodads
            this._currentDoodad = null;
            this._currentDoodadSprite = null;
            this._hoverDoodadSprite = null;
            this._managers['Doodads'] = DoodadManager;
        }

        /**
         * @param {String} group        Name of the window group the handler belongs to
         * @param {String} symbol       Name of the command to be handled
         * @param {Function} method     Function that will be called to handle the command when it is received
         */
        setHandler(group, symbol, method) {
            this._handlers[group][symbol] = method;
        };

        /**
         * @param {String} symbol
         * @return {Boolean}
         */
        isHandled(symbol) {
            return this._handlers[this._activeGroup] && !!this._handlers[this._activeGroup][symbol];
        };

        /**
         * @param {String} symbol
         */
        callHandler(symbol) {
            if (this.isHandled(symbol)) {
                this._handlers[this._activeGroup][symbol](this);
            }
        };

        /**
         * @param {Window} window
         */
        closeWindow(window) {
            window.hide();
            window.deactivate();
            window.disable();
        }

        /**
         * @param {Window} window
         */
        openWindow(window) {
            window.show();
            window.activate();
            window.enable();
        }

        /**
         */
        openCommandWindow() {
            this.openWindow(this._commandWindow);
            this._buttons.forEach((b) => this.openWindow(b));
        }

        /**
         * @return {PIXI.filter}
         */
        makeOutlineFilter() {
            let filter = null;
            switch (Knight.Param.KEFilterType) {
            case 'Outline (Red)':
                filter = new PIXI.filters.GlowFilter(4, 3, 0, 0xff0000, 0.1);
                break;
            case 'Outline (Black)':
                filter = new PIXI.filters.GlowFilter(4, 3, 0, 0x000000, 0.1);
                break;
            case 'Increase Brightness':
                filter = new PIXI.filters.AdjustmentFilter;
                filter.brightness = 1.5;
                break;
            case 'Increase Contrast':
                filter = new PIXI.filters.AdjustmentFilter;
                filter.contrast = 2;
                break;
            }
            return filter;
        }

        /**
         * @return {Array<PIXI.filter>}
         */
        outlineFilter() {
            return this._outlineFilter ? [this._outlineFilter] : [];
        }

        /**
         * @static
         * @return {Array<String>}
         */
        static windowGroups() {
            return ["Doodads"];
        }

        /**
         * @return {Array<String>}
         */
        buttons() {
            return ['Import', 'Minimize', 'Save', 'Undo', 'Clear', 'Region'];
        }

        /**
         * @return {Boolean}
         */
        inPlacementMode() {
            return this._mode === Knight.EDITOR.PLACEMENT_MODE;
        }

        /**
         * @return {Boolean}
         */
        inSelectionMode() {
            return this._mode === Knight.EDITOR.SELECTION_MODE;
        }

        /**
         * @param {String} group
         * @return {Boolean}
         */
        isActiveGroup(group) {
            return this._activeGroup === group;
        }

        /**
         * @return {Boolean}
         */
        isDoodadGroup() {
            return this._activeGroup === 'Doodads';
        }

        /**
         * @param {String} group    Manager window group. If null, returns the active group's manager.
         * @return {Object}
         */
        manager(group = null) {
            return group ? this._managers[group] : this._managers[this._activeGroup];
        }

        /**
         * @return {*}
         */
        currentObject() {
            if (this.isDoodadGroup()) {
                return this._currentDoodad;
            }
        }

        /**
         * @param {*} name
         */
        playSound(name) {
            AudioManager.playSe({name: name, pan: 0, pitch: 100, volume: 90});
        }

        /**
         * @return {Sprite_Doodad}
         */
        currentDoodadSprite() {
            return this.inPlacementMode() ? this._spriteset._doodadCursor : this._currentDoodadSprite;
        }

        /**
         * @param {*} scene
         * @memberof KnightEditor
         */
        createWindows() {
            this.createDoodadWindows();
        }

        /**
         * @param {*} scene
         */
        addWindowsToScene(scene) {
            const container = scene.editorContainer();
            this._windows.forEach(function(w) {
                w.addToScene ? w.addToScene(container) : container.addChild(w);
                this.closeWindow(w);
            }, this);
        }

        /**
         * @memberof KnightEditor
         */
        createDoodadWindows() {
            const groupNames = Knight.Editor.KnightEditor.windowGroups();

            // Doodads
            const cy = Graphics.height - Knight.Editor.Window_EditorCommand.windowHeight();
            this._commandWindow = new Knight.Editor.Window_EditorCommand(0, cy);
            this._windows.push(this._commandWindow);
            this._windowGroups[groupNames[0]].push(this._commandWindow);
            groupNames.forEach((name) => this._windowGroups[name].push(this._commandWindow));
            this._commandWindow.setHandler('ok', this.updateWindowGroup.bind(this));

            const fw = 350;
            const lw = Graphics.width - fw;
            const lh = 180;
            const ly = cy - lh;
            this._doodadWindow = new Knight.Editor.Window_DoodadList(0, ly, lw, lh);
            this._windows.push(this._doodadWindow);
            this._windowGroups[groupNames[0]].push(this._doodadWindow);
            this._doodadWindow.setHandler('ok', this.setDoodad.bind(this));

            const fh = lh;
            const fy = cy - lh;
            this._folderWindow = new Knight.Editor.Window_FolderList(Graphics.width - fw, fy, fw, fh);
            this._windows.push(this._folderWindow);
            this._windowGroups[groupNames[0]].push(this._folderWindow);
            this._folderWindow.setHandler('ok', this.setFolder.bind(this));

            const pw = fw;
            const ph = fy;
            this._doodadEditWindow = new Knight.Editor.Window_DoodadProperties(Graphics.width - pw, 0, pw, ph, this);
            this._windows.push(this._doodadEditWindow);

            const iw = 240;
            const ih = 120;
            this._importWindow = new Knight.Editor.Window_DoodadImport(Graphics.width/2 - iw/2, Graphics.height/2 - ih/2 - 50, iw, ih, this);
            this._windows.push(this._importWindow);
            this._importWindow.setHandler('ok', this.importConfirm.bind(this));
            this._importWindow.setHandler('cancel', this.closeWindow.bind(this, this._importWindow));

            // Buttons
            const buttonSpacing = 64;
            const buttonY = this._commandWindow.y + this._commandWindow.height - 46;
            const buttonNames = this.buttons();
            let buttonX = this._commandWindow.x + this._commandWindow.width - (buttonSpacing * buttonNames.length);
            for (let i = 0; i < buttonNames.length; ++i) {
                const name = buttonNames[i];
                const button = new Knight.Button(buttonX, buttonY, name, `${name}_Hover`);
                button.setOkSound(null);
                this._windows.push(button);
                this._buttons.push(button);
                groupNames.forEach((name) => this._windowGroups[name].push(button));
                button.setHandler('ok', this[name.toLowerCase()].bind(this));
                buttonX += buttonSpacing;
            }

            // Default Button States
            this.updateAllButtonStates();
        }

        /**
         * @memberof KnightEditor
         */
        setInputHandlers() {
            const group = 'Doodads';
            this.setHandler(group, 'handlePlacementInput', this.handlePlacementInput.bind(this));
            this.setHandler(group, 'handleSelectionInput', this.handleSelectionInput.bind(this));
            this.setHandler(group, 'scrollUp', this.scrollUp.bind(this));
            this.setHandler(group, 'scrollDown', this.scrollDown.bind(this));
            this.setHandler(group, 'scrollLeft', this.scrollLeft.bind(this));
            this.setHandler(group, 'scrollRight', this.scrollRight.bind(this));
            this.setHandler(group, 'moveUp', this.moveUp.bind(this));
            this.setHandler(group, 'moveDown', this.moveDown.bind(this));
            this.setHandler(group, 'moveLeft', this.moveLeft.bind(this));
            this.setHandler(group, 'moveRight', this.moveRight.bind(this));
            this.setHandler(group, 'layerIncr', this.layerIncr.bind(this));
            this.setHandler(group, 'layerDecr', this.layerDecr.bind(this));
            this.setHandler(group, 'scaleIncr', this.scaleIncr.bind(this));
            this.setHandler(group, 'scaleDecr', this.scaleDecr.bind(this));
            this.setHandler(group, 'defaultSettings', this.defaultSettings.bind(this));
            this.setHandler(group, 'toggleGridLock', this.toggleGridLock.bind(this));
            this.setHandler(group, 'setManualMove', this.setManualMove.bind(this));
            this.setHandler(group, 'setOpacity', this.setOpacity.bind(this));
        };

        /**
         */
        update() {
            this.updateToggle();
            if (this.active() && !this._importWindow.visible) {
                this.updateActiveGroup();
                this.handleInput();
            }
        }

        /**
         */
        updateToggle() {
            if (Input.isTriggered(Knight.INPUT[Knight.Param.KEToggleKey])) {
                this.toggle();
            }
        }

        /**
         */
        updateActiveGroup() {
            if (this.isDoodadGroup()) {
                this.updateDoodadPosition();
            }
        }

        /**
         */
        updateDoodadPosition() {
            if (this._draggingDoodad || (this.inPlacementMode() && !DoodadManager.usingManualMove())) {
                const oldX = this._currentDoodad.x;
                const oldY = this._currentDoodad.y;
                this._currentDoodad.x = this._draggingDoodad ? this._spriteset.doodadDragX() : this._spriteset.currentDoodadX();
                this._currentDoodad.y = this._draggingDoodad ? this._spriteset.doodadDragY() : this._spriteset.currentDoodadY();
                if (this._currentDoodad.x !== oldX || this._currentDoodad.y !== oldY) {
                    this._doodadEditWindow.refresh();
                    if (this._draggingDoodad) this.setDirty(true);
                }
            }
        }

        /**
         */
        handleInput() {
            this._mouseOnUi = this._windows.some(function(window) {
                return window.visible && window.isMouseInsideFrame();
            });
            if (!this._mouseOnUi && !this._draggingDoodad) {
                this.inPlacementMode() ? this.callHandler('handlePlacementInput') : this.callHandler('handleSelectionInput');
            }
            if (this._draggingDoodad && TouchInput.isReleased()) this._draggingDoodad = false;
            if (Input.isPressed(Knight.INPUT['w'])) this.callHandler('scrollUp');
            else if (Input.isPressed(Knight.INPUT['a'])) this.callHandler('scrollLeft');
            else if (Input.isPressed(Knight.INPUT['d'])) this.callHandler('scrollRight');
            else if (Input.isPressed(Knight.INPUT['s']) && !Input.isPressed(Knight.INPUT['ctrl'])) this.callHandler('scrollDown');

            if (this.currentObject()) {
                const up = Input.isRepeated(Knight.INPUT['up']) || (Input.isPressed(Knight.INPUT['up']) && Input.isPressed(Knight.INPUT['shift']));
                const down = Input.isRepeated(Knight.INPUT['down']) || (Input.isPressed(Knight.INPUT['down']) && Input.isPressed(Knight.INPUT['shift']));
                const left = Input.isRepeated(Knight.INPUT['left']) || (Input.isPressed(Knight.INPUT['left']) && Input.isPressed(Knight.INPUT['shift']));
                const right = Input.isRepeated(Knight.INPUT['right']) || (Input.isPressed(Knight.INPUT['right']) && Input.isPressed(Knight.INPUT['shift']));
                if (up) this.callHandler('moveUp');
                else if (down) this.callHandler('moveDown');
                if (left) this.callHandler('moveLeft');
                else if (right) this.callHandler('moveRight');

                if (!this._mouseOnUi) {
                    if (TouchInput.wheelY > 0) this.callHandler('layerDecr');
                    else if (TouchInput.wheelY < 0) this.callHandler('layerIncr');
                }
                if (Input.isRepeated(Knight.INPUT['q'])) this.callHandler('scaleDecr');
                if (Input.isRepeated(Knight.INPUT['e'])) this.callHandler('scaleIncr');
                if (Input.isTriggered(Knight.INPUT['backspace'])) this.callHandler('defaultSettings');
                if (Input.isTriggered(Knight.INPUT['g'])) this.callHandler('toggleGridLock');
                this.callHandler('setOpacity');
            }
            if (TouchInput.isMoved()) this.callHandler('setManualMove');
            if (Input.isPressed(Knight.INPUT['ctrl']) && Input.isTriggered(Knight.INPUT['s'])) this.save();
            if (Input.isPressed(Knight.INPUT['ctrl']) && Input.isTriggered(Knight.INPUT['z'])) this.undo();
            if (Input.isTriggered(Knight.INPUT['n'])) this.minimize();
            if (Input.isTriggered(Knight.INPUT['r'])) this.region();
            if (Input.isTriggered(Knight.INPUT['i'])) this.import();
        }

        /**
         */
        handlePlacementInput() {
            if (TouchInput.isTriggered()) this.placeDoodad();
            if (TouchInput.isCancelled()) this.clearDoodad();
        }

        /**
         */
        handleSelectionInput() {
            const doodadSprite = DoodadManager.isMouseOnDoodads();
            const doodad = doodadSprite ? doodadSprite._data : null;
            // Highlight Doodads on mouseover
            if (doodadSprite !== this._hoverDoodadSprite && this._currentDoodad === null) {
                if (this._hoverDoodadSprite) this._hoverDoodadSprite.filters = [];
                this._hoverDoodadSprite = doodadSprite;
                if (this._hoverDoodadSprite) this._hoverDoodadSprite.filters = this.outlineFilter();
            }
            if (doodad) {
                if (TouchInput.isTriggered()) {
                    this._currentDoodad = doodad;
                    if (this._currentDoodadSprite) this._currentDoodadSprite.filters = [];
                    this._currentDoodadSprite = doodadSprite;
                    this._currentDoodadSprite.filters = this.outlineFilter();
                    this.updateEditWindow(this._currentDoodad);
                }
                if (TouchInput.isLongPressedFor(8)) {
                    this._draggingDoodad = true;
                }
            }
            if (TouchInput.isCancelled()) this.clearDoodad();
            if (Input.isTriggered(Knight.INPUT['del'])) this.delete();
            if (Input.isTriggered(Knight.INPUT['c'])) this.copy();
        }

        /**
         * @memberof KnightEditor
         */
        scrollUp() {
            $gameMap.startScroll(8, 1, 6);
            DoodadManager.setManualMove(false);
        }

        /**
         * @memberof KnightEditor
         */
        scrollDown() {
            $gameMap.startScroll(2, 1, 6);
            DoodadManager.setManualMove(false);
        }

        /**
         * @memberof KnightEditor
         */
        scrollLeft() {
            $gameMap.startScroll(4, 1, 6);
            DoodadManager.setManualMove(false);
        }

        /**
         * @memberof KnightEditor
         */
        scrollRight() {
            $gameMap.startScroll(6, 1, 6);
            DoodadManager.setManualMove(false);
        }

        /**
         * @memberof KnightEditor
         */
        moveUp() {
            DoodadManager.manualMoveUp();
            this._currentDoodad.y = DoodadManager._manualY;
            this._doodadEditWindow.refresh();
        }

        /**
         * @memberof KnightEditor
         */
        moveDown() {
            DoodadManager.manualMoveDown();
            this._currentDoodad.y = DoodadManager._manualY;
            this._doodadEditWindow.refresh();
        }

        /**
         * @memberof KnightEditor
         */
        moveLeft() {
            DoodadManager.manualMoveLeft();
            this._currentDoodad.x = DoodadManager._manualX;
            this._doodadEditWindow.refresh();
        }

        /**
         * @memberof KnightEditor
         */
        moveRight() {
            DoodadManager.manualMoveRight();
            this._currentDoodad.x = DoodadManager._manualX;
            this._doodadEditWindow.refresh();
        }

        /**
         * @memberof KnightEditor
         */
        layerIncr() {
            this._doodadEditWindow.layerIncr();
        }

        /**
         * @memberof KnightEditor
         */
        layerDecr() {
            this._doodadEditWindow.layerDecr();
        }

        /**
         * @memberof KnightEditor
         */
        scaleIncr() {
            this._doodadEditWindow.scaleIncr(Input.isPressed(Knight.INPUT['shift']));
        }

        /**
         * @memberof KnightEditor
         */
        scaleDecr() {
            this._doodadEditWindow.scaleDecr(Input.isPressed(Knight.INPUT['shift']));
        }

        /**
         * @memberof KnightEditor
         */
        defaultSettings() {
            this._doodadEditWindow.default();
        }

        /**
         * @memberof KnightEditor
         */
        toggleGridLock() {
            this._doodadEditWindow.toggleGridLock();
        }

        /**
         * @memberof KnightEditor
         */
        setManualMove() {
            DoodadManager.setManualMove(false);
        }

        /**
         * @memberof KnightEditor
         */
        setOpacity() {
            for (let i = 0; i < 10; ++i) {
                const keyCode = `${i}`;
                if (Input.isTriggered(Knight.INPUT[keyCode])) {
                    this._currentDoodad.opacity = i === 0 ? 255 : Math.floor(255 * i / 10);
                    this._doodadEditWindow.updateAllValues();
                }
            }
        }

        /**
         * @memberof KnightEditor
         */
        toggle() {
            if (SceneManager._scene._debugActive) return;
            this.active() ? this.deactivate() : this.activate();
            this.playSound(Knight.Param.KESEToggle);
        }

        /**
         * @return {Boolean}
         * @memberof KnightEditor
         */
        active() {
            return this._active;
        }

        /**
         * @return {Array<Window>}
         */
        windows() {
            return this._windowGroups[this._activeGroup];
        }

        /**
         * @memberof KnightEditor
         */
        activate() {
            this._active = true;
            if (!this._minimized) this.windows().forEach((w) => this.openWindow(w));
            this.openCommandWindow();
        }

        /**
         * @memberof KnightEditor
         */
        deactivate() {
            this._active = false;
            this._windows.forEach((w) => this.closeWindow(w));
            this.clearDoodad();
            this._spriteset.closeRegionOverlayWindow();
            $gameMap.centerScreenPlayer();
        }

        /**
         * @param {String} group
         */
        setWindowGroup(group) {
            if (this._activeGroup !== group) {
                this.deactivate();
                this._activeGroup = group;
                this.activate();
                this.updateAllButtonStates();
            }
        }

        /**
         */
        updateWindowGroup() {
            this.setWindowGroup(this._commandWindow.currentSymbol());
        }

        /**
         */
        setDoodad() {
            this.clearDoodad();
            if (this._doodadWindow.isIconset()) {
                this._currentDoodad = DoodadManager.getTemplate('', 'IconSet');
                this._currentDoodad.iconIndex = this._doodadWindow.index();
            } else {
                const name = this._doodadWindow.item();
                const folder = this._doodadWindow.folder();
                this._currentDoodad = DoodadManager.getTemplate(folder, name);
                this._currentDoodad.xFrames = DoodadManager.getXFrames(name);
                this._currentDoodad.yFrames = DoodadManager.getYFrames(name);
            }
            this.enterPlacementMode();
        }

        /**
         */
        placeDoodad() {
            this.playSound(Knight.Param.KESEPlace);
            const doodad = DoodadManager.currentCopy();
            doodad.x = this._spriteset.currentDoodadX();
            doodad.y = this._spriteset.currentDoodadY();
            DoodadManager.addNew(doodad);
            this.setDirty(true);
            this.updateButtonState('Clear');
        }

        /**
         */
        clearDoodad() {
            this._currentDoodad = null;
            if (this._hoverDoodadSprite) {
                this._hoverDoodadSprite.filters = [];
                this._hoverDoodadSprite = null;
            }
            if (this._currentDoodadSprite) {
                this._currentDoodadSprite.filters = [];
                this._currentDoodadSprite = null;
            }
            this.exitPlacementMode();
        }

        /**
         * @param {*} object
         */
        updateEditWindow(object) {
            if (this.isDoodadGroup()) {
                const doodad = object;
                this._doodadEditWindow.setDoodad(doodad);
                this.closeWindow(this._doodadEditWindow);
                if (doodad && !this._minimized) {
                    this.openWindow(this._doodadEditWindow);
                    this.updateUndoQueue($dataDoodads);
                }
            }
        }

        /**
         */
        setFolder() {
            const folder = this._folderWindow.item();
            this._doodadWindow.setFolder(folder);
        }

        /**
         */
        enterPlacementMode() {
            this._mode = Knight.EDITOR.PLACEMENT_MODE;
            if (this.isDoodadGroup()) {
                this._spriteset.setDoodadCursor(this.currentObject());
            }
            this.updateEditWindow(this.currentObject());
        };

        /**
         */
        exitPlacementMode() {
            this._mode = Knight.EDITOR.SELECTION_MODE;
            if (this.isDoodadGroup()) {
                this._spriteset.clearDoodadCursor();
            }
            this.updateEditWindow(null);
        };

        /**
         */
        onPropertyChange() {
            if (this.isDoodadGroup()) {
                if (this.inPlacementMode()) {
                    this._spriteset._doodadCursor.refreshSettings();
                } else if (this.inSelectionMode()) {
                    this._currentDoodadSprite.initData();
                    this.setDirty(true);
                }
            }
        }

        /**
         * @param {Boolean} isDirty
         */
        setDirty(isDirty) {
            const manager = this.manager();
            if (manager.isDirty() !== isDirty) {
                manager.setDirty(isDirty);
                this._commandWindow.refresh();
                this.updateButtonState('Save');
            }
        }

        /**
         * @param {Boolean} isDirty
         */
        setAllDirty(isDirty) {
            Object.values(this._managers).forEach((manager) => manager.setDirty(isDirty));
            this._commandWindow.refresh();
            this.updateButtonState('Save');
        }

        /**
         * @param {*} data
         */
        updateUndoQueue(data) {
            $gameTemp._prevDoodadSettings = data ? JsonEx.makeDeepCopy(data) : null;
            this.updateButtonState('Undo');
        }

        /**
         */
        minimize() {
            this._minimized = !this._minimized;
            if (this._minimized) {
                this._windows.forEach((w) => this.closeWindow(w));
            } else {
                this.windows().forEach((w) => this.openWindow(w));
                if (this.isDoodadGroup()) {
                    this._doodadEditWindow._doodad ? this.openWindow(this._doodadEditWindow) : this.closeWindow(this._doodadEditWindow);
                }
                this.closeWindow(this._importWindow);
            }
            this.openCommandWindow(); // Command window should always be visible
            this.playSound(Knight.Param.KESEMinimize);
        }

        /**
         */
        save() {
            StorageManager.saveDoodadSettings();
            this.playSound(Knight.Param.KESESave);
            this.setAllDirty(false);
        }

        /**
         */
        copy() {
            if (this.isDoodadGroup() && this._currentDoodad) {
                if (this._hoverDoodadSprite) {
                    this._hoverDoodadSprite.filters = [];
                    this._hoverDoodadSprite = null;
                }
                this._currentDoodadSprite = null;
                this._currentDoodad = JsonEx.makeDeepCopy(this._currentDoodad);
                this.enterPlacementMode();
                this.playSound(Knight.Param.KESECopy);
            }
        }

        /**
         */
        undo() {
            if (this.isDoodadGroup() && $gameTemp._prevDoodadSettings) {
                const index = DoodadManager.doodads().indexOf(this._currentDoodad);
                $dataDoodads = JsonEx.makeDeepCopy($gameTemp._prevDoodadSettings);
                DoodadManager.refresh();
                if (this._hoverDoodadSprite) {
                    this._hoverDoodadSprite.filters = [];
                    this._hoverDoodadSprite = null;
                }
                if (this._currentDoodadSprite) {
                    this._currentDoodadSprite.filters = [];
                    this._currentDoodadSprite = null;
                }
                if (this._doodadEditWindow.visible) {
                    this._currentDoodad = DoodadManager.doodads()[index];
                    this._doodadEditWindow.setDoodad(this._currentDoodad);
                    const doodadSprites = this._spriteset._doodads;
                    if (doodadSprites) {
                        this._currentDoodadSprite = doodadSprites.find((sprite) => sprite._data === this._currentDoodad);
                        if (this._currentDoodadSprite) this._currentDoodadSprite.filters = this.outlineFilter();
                    }
                }
                this.playSound(Knight.Param.KESEUndo);
                this.updateUndoQueue(null);
                this.setDirty(true);
                this.updateButtonState('Clear');
            }
        }

        /**
         */
        delete() {
            if (this.isDoodadGroup() && this._currentDoodad) {
                DoodadManager.delete(this._currentDoodad);
                this.clearDoodad();
                this.playSound(Knight.Param.KESEDelete);
                this.setDirty(true);
                this.updateButtonState('Clear');
            };
        }

        /**
         */
        clear() {
            if (this.isDoodadGroup() && DoodadManager.doodads().length > 0) {
                this.updateUndoQueue($dataDoodads);
                DoodadManager.clearMap();
                this.clearDoodad();
                this.playSound(Knight.Param.KESEClear);
                this.setDirty(true);
                this.updateButtonState('Clear');
            }
        }

        /**
         */
        region() {
            this._spriteset.toggleRegionOverlayWindow();
            this.playSound(Knight.Param.KESERegion);
        }

        /**
         */
        import() {
            this.openWindow(this._importWindow);
        }

        /**
         */
        importConfirm() {
            const mapId = this._importWindow.mapId();
            if (mapId) {
                this.clearDoodad();
                this.updateUndoQueue($dataDoodads);
                $dataDoodads[mapId] = $dataDoodads[mapId] || [];
                const data = JsonEx.makeDeepCopy($dataDoodads[mapId]);
                $dataDoodads[DoodadManager.mapId()] = data;
                DoodadManager.refresh();
                this.closeWindow(this._importWindow);
                this.setAllDirty(true);
            }
            this.updateButtonState('Clear');
        }

        /**
         * @param {String} buttonName
         */
        updateButtonState(buttonName) {
            const index = this.buttons().indexOf(buttonName);
            if (index < 0) return;
            const button = this._buttons[index];
            switch (buttonName) {
            case "Save":
                this.manager().isDirty() ? button.on() : button.off();
                break;
            case "Clear":
                this.manager().size() > 0 ? button.on() : button.off();
                break;
            case "Undo":
                (this.isDoodadGroup() && $gameTemp._prevDoodadSettings != null) ? button.on() : button.off();
                break;
            }
        };

        /**
         */
        updateAllButtonStates() {
            this.updateButtonState('Clear');
            this.updateButtonState('Save');
            this.updateButtonState('Undo');
        };
    };

    //=============================================================================
    // Editor Modes
    //=============================================================================
    Knight.Editor.Window_EditorCommand = class extends Knight.Window_HorzCommand {
        /**
         *Creates an instance of Window_EditorCommand.
        *
        * @param {Number} x
        * @param {Number} y
        */
        constructor(x, y) {
            super(x, y);
            this._useMouseInput = true;
            this._activeIndex = 0;
        }

        /**
         * @return {Number}
         */
        maxCols() {
            return Knight.Editor.KnightEditor.windowGroups().length;
        };

        /**
         * @return {Number}
         */
        itemWidth() {
            return 200;
        };

        /**
         * @return {Number}
         */
        windowWidth() {
            return Graphics.width;
        };

        /**
         * @return {Number}
         */
        windowHeight() {
            return Knight.Editor.Window_EditorCommand.windowHeight();
        };

        /**
         * @static
         * @return {Number}
         */
        static windowHeight() {
            return 38;
        }

        /**
         * Called when the OK handler is triggered
         */
        processOk() {
            this._activeIndex = this.index();
            super.processOk();
            this.refresh();
        };

        /**
         * Turn off window cursor sound
         */
        playCursorSound() {
        };

        /**
         * Plays a sound when interacted with.
         */
        playOkSound() {
            SoundManager.playCursor();
        };

        /**
         * @param {*} scene
         */
        addToScene(scene) {
            scene.addChild(this);
        }

        /**
         */
        makeCommandList() {
            const windowGroups = Knight.Editor.KnightEditor.windowGroups();
            windowGroups.forEach((group) => this.addCommand(group, group));
            this.makeItemRects();
        };

        /**
         * @param {*} index
         */
        select(index) {
            if (index !== this._index) {
                Knight.Window_HorzCommand.prototype.select.call(this, index);
                Window_Selectable.prototype.refresh.call(this);
            }
        }

        /**
         */
        resetFontSettings() {
            this.contents.fontFace = this.standardFontFace();
            this.contents.fontSize = 20;
            this.contents.outlineWidth = 0;
        }

        /**
         */
        drawAllItems() {
            this.drawBackground();
            Window_HorzCommand.prototype.drawAllItems.call(this);
        }

        /**
         */
        drawBackground() {
            this.contents.fillRect(0, 0, this.contents.width, this.contents.height, Knight.COLOR.DARK_GREY);
        }

        /**
         * @param {Number} index
         */
        drawItem(index) {
            const rect = this.itemRectForText(index);
            const align = this.itemTextAlign();
            if (this._activeIndex === index) {
                this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.BLACK);
                this.changeTextColor(this.normalColor());
            } else {
                this.changeTextColor(Knight.COLOR.GREY);
            }

            let text = this.commandName(index);

            const editor = SceneManager._scene._editor;
            if (editor) {
                const manager = SceneManager._scene._editor.manager(text);
                if (manager && manager.isDirty()) text += " *";
            }
            this.drawText(text, rect.x, rect.y, rect.width, align);
        };
    };

    //=============================================================================
    // Doodad List
    //=============================================================================
    Knight.Editor.Window_DoodadList = class extends Knight.Window_ItemList {
        /**
         *Creates an instance of Window_DoodadList.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Number} width
        * @param {Number} height
        */
        constructor(x, y, width, height) {
            super(x, y, width, height);
            this._folder = '';
            this._useMouseInput = true;
            this.refresh();
        }

        /**
         * @return {Number}
         */
        itemWidth() {
            return 60;
        };

        /**
         * @return {Number}
         */
        itemHeight() {
            return 60;
        };

        /**
         * @return {Number}
         */
        maxCols() {
            return Math.floor(this.width / (this.itemWidth() + this.spacing()));
        };

        /**
         * @return {Number}
         */
        spacing() {
            return 10;
        };

        /**
         * @param {*} item
         * @return {Boolean}
         */
        isEnabled(item) {
            return true;
        }

        /**
         * @return {Boolean}
         */
        isIconset() {
            return this._folder === 'Iconset';
        }

        /**
         * Turn off window cursor sound
         */
        playCursorSound() {
        };

        /**
         * Plays a sound when interacted with.
         */
        playOkSound() {
            SoundManager.playCursor();
        };

        /**
         * @param {*} scene
         */
        addToScene(scene) {
            scene.addChild(this);
        }

        /**
         * @param {*} index
         */
        select(index) {
            if (index !== this._index) {
                Knight.Window_ItemList.prototype.select.call(this, index);
                Window_Selectable.prototype.refresh.call(this);
            }
        }

        /**
         *
         *
         */
        update() {
            Knight.Window_ItemList.prototype.update.call(this);
        }

        /**
         */
        makeItemList() {
            this._data = this.getFileList();
            this.makeItemRects();
        };

        /**
         * @param {String} folderName
         */
        setFolder(folderName) {
            if (this._folder !== folderName) {
                this._folder = folderName;
                this.deselect();
                this.resetScroll();
                this.refresh();
            }
        };

        /**
         */
        drawAllItems() {
            this.contents.fontSize = 20;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.drawBackground();
            Window_ItemList.prototype.drawAllItems.call(this);
        }

        /**
         */
        drawBackground() {
            this.changePaintOpacity(false);
            this.contents.fillRect(0, 0, this.contents.width, this.contents.height, Knight.COLOR.BLACK);
            this.changePaintOpacity(true);
        }

        /**
         * @param {Rect} rect
         */
        drawSelectionCursor(rect) {
            this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.DARK);
            this.contents.paintOpacity = 212;
            this.contents.fillRect(rect.x + 2, rect.y + 2, rect.width - 4, rect.height - 4, Knight.COLOR.WHITE);
            this.changePaintOpacity(true);
        }

        /**
         * @param {number} index
         */
        drawItem(index) {
            if (this.isIconset()) {
                const rect = this.getItemRect(index);
                if (index === this.index()) this.drawSelectionCursor(rect);
                const x = rect.x + rect.width/2 - Window_Base._iconWidth/2;
                const y = rect.y + rect.height/2 - Window_Base._iconHeight/2;
                this.drawIcon(index, x, y);
            } else {
                this.drawDoodadImage(index, this._data[index]);
            }
        }

        /**
         * @param {*} index
         * @param {*} filename
         * @return {*}
         */
        drawDoodadImage(index, filename) {
            const bitmap = ImageManager.loadDoodad(this._folder + '/' + filename, 0, true);
            if (bitmap.width <= 0) {
                return setTimeout(this.drawDoodadImage.bind(this, index, filename), 5);
            }
            const rect = this.itemRectForText(index);
            const xframes = DoodadManager.getXFrames(filename);
            const yframes = DoodadManager.getYFrames(filename);
            const pw = Math.floor(bitmap.width / xframes);
            const ph = Math.floor(bitmap.height / yframes);
            const maxWidth = this.itemWidth() - 2;
            const maxHeight = this.itemHeight() - 2;
            let dw = pw;
            let dh = ph;
            if (dw > maxWidth) {
                const rate = maxWidth / dw;
                dw *= rate;
                dh *= rate;
            }
            if (dh > maxHeight) {
                const rate = maxHeight / dh;
                dw *= rate;
                dh *= rate;
            }
            const dx = rect.x + 2 + (maxWidth - dw) / 2;
            const dy = rect.y + 2 + (maxHeight - dh) / 2;
            if (index === this.index()) {
                const sRect = new Rectangle(dx - 8, dy - 8, dw + 16, dh + 16);
                this.drawSelectionCursor(sRect);
            }
            this.contents.blt(bitmap, 0, 0, pw, ph, dx, dy, dw, dh);
        };

        /**
         */
        updateHelp() {
        }

        /**
         * @return {String}
         */
        getLocalPath() {
            const path = require('path');
            const base = path.dirname(process.mainModule.filename);
            return path.join(base, Yanfly.Param.GFDFolder);
        };

        /**
         * @return {Array<String>}
         */
        getFileList() {
            if (this._folder === '') return [];
            if (this.isIconset()) return this.iconData();
            const fs = require('fs');
            const results = [];
            const path = this.getLocalPath() + this._folder;
            fs.readdirSync(path).forEach(function(file) {
                name = file;
                file = path + '/' + name;
                const stat = fs.statSync(file);
                if (stat && stat.isDirectory()) {
                    // Do nothing
                } else if (name.match(/.png/g)) {
                    name = name.replace(/.png/g, '');
                    results.push(name);
                }
            });
            return results;
        };

        /**
         * @return {String}
         */
        folder() {
            return this._folder + '/';
        }

        /**
         * @return {Array}
         */
        iconData() {
            const bitmap = ImageManager.loadSystem('IconSet');
            const rows = Math.floor(bitmap.height / Window_Base._iconHeight);
            const length = rows * 16;
            return new Array(length);
        }
    };

    //=============================================================================
    // Folder List
    //=============================================================================
    Knight.Editor.Window_FolderList = class extends Knight.Editor.Window_DoodadList {
        /**
         * @return {Number}
         */
        itemWidth() {
            return 60;
        };

        /**
         * @return {Number}
         */
        itemHeight() {
            return 60;
        };

        /**
         * @return {Number}
         */
        spacing() {
            return 10;
        };

        /**
         * @param {Rect} rect
         */
        drawSelectionCursor(rect) {
            this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.DARK);
            this.contents.paintOpacity = 64;
            this.contents.fillRect(rect.x + 2, rect.y + 2, rect.width - 4, rect.height - 4, Knight.COLOR.WHITE);
            this.changePaintOpacity(true);
        }

        /**
         */
        drawAllItems() {
            this.contents.fontSize = 14;
            this.contents.outlineWidth = 2;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.drawBackground();
            Window_ItemList.prototype.drawAllItems.call(this);
        }

        /**
         * @param {number} index
         */
        drawItem(index) {
            const folderName = this._data[index];
            const rect = this.getItemRect(index);
            let image = 'Folder_Closed';
            if (index === this.index()) {
                image = 'Folder_Open';
                this.drawSelectionCursor(rect);
            }
            if (folderName === "Iconset") image = 'Iconset';
            this.drawGui(image, rect.x + rect.width/2 - Window_Base._iconWidth/2, rect.y + 2);
            this.drawText(folderName, rect.x, rect.y + Window_Base._iconHeight - 4, rect.width, 'center');
        }

        /**
         */
        makeItemList() {
            this._data = this.getFolderList();
            this.makeItemRects();
        };

        /**
         * @return {Array<String>}
         */
        getFolderList() {
            const fs = require('fs');
            const results = ['Iconset'];
            const path = this.getLocalPath();
            fs.readdirSync(path).forEach(function(file) {
                name = file;
                file = path + '/' + name;
                const stat = fs.statSync(file);
                if (stat && stat.isDirectory()) {
                    results.push(name);
                }
            });
            return results;
        };
    };

    //=============================================================================
    // Edit Window Pages
    //=============================================================================
    Knight.Editor.Window_EditorPageCommand = class extends Knight.Editor.Window_EditorCommand {
        /**
         *Creates an instance of Window_EditorPageCommand.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Window} propertyWindow
        */
        constructor(x, y, propertyWindow) {
            super(x, y);
            this._propertyWindow = propertyWindow;
        }

        /**
         * @return {Number}
         */
        maxCols() {
            return this._propertyWindow ? this._propertyWindow.windowGroups().length : Knight.Editor.Window_DoodadProperties.windowGroups().length;
        };

        /**
         * @return {Number}
         */
        spacing() {
            return 0;
        }

        /**
         * @return {Number}
         */
        lineHeight() {
            return this.contents.fontSize;
        }

        /**
         * @return {Number}
         */
        itemHeight() {
            return this.windowHeight();
        };

        /**
         * @return {Number}
         */
        itemWidth() {
            return Math.floor(this.windowWidth() / this.maxCols());
        };

        /**
         * @return {Number}
         */
        windowWidth() {
            return 350;
        };

        /**
         * @return {Number}
         */
        windowHeight() {
            return Knight.Editor.Window_EditorPageCommand.windowHeight();
        };

        /**
         * @static
         * @return {Number}
         */
        static windowHeight() {
            return 30;
        }

        /**
         */
        makeCommandList() {
            const windowGroups = this._propertyWindow ? this._propertyWindow.windowGroups() : Knight.Editor.Window_DoodadProperties.windowGroups();
            windowGroups.forEach((group) => this.addCommand(group, group));
            this.makeItemRects();
        };

        /**
         */
        resetFontSettings() {
            this.contents.fontFace = this.standardFontFace();
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 0;
        }

        /**
         * @param {Number} index
         */
        drawItem(index) {
            const rect = this.getItemRect(index);
            const align = this.itemTextAlign();
            if (this._activeIndex === index) {
                this.contents.fillRect(rect.x, rect.y, rect.width, rect.height, Knight.COLOR.BLACK);
                this.changeTextColor(this.normalColor());
            } else {
                this.changeTextColor(Knight.COLOR.GREY);
            }
            const y = rect.y + rect.height/2 - this.lineHeight()/2;
            this.drawText(this.commandName(index), rect.x, y, rect.width, align);
        };
    };

    //=============================================================================
    // Doodad Property Window
    //=============================================================================
    Knight.Editor.Window_DoodadProperties = class extends Knight.Window_Base {
        /**
         *Creates an instance of Window_DoodadProperties.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Number} width
        * @param {Number} height
        * @param {KnightEditor} editor
        */
        constructor(x, y, width, height, editor) {
            super(x, y, width, height);
            this._doodad = null;
            this._editor = editor;
            this._windows = [];
            this._buttons = [];
            this._windowGroups = {};
            const windowGroups = this.windowGroups();
            windowGroups.forEach(function(group) {
                this._windowGroups[group] = [];
            }, this);
            this._activeGroup = windowGroups[0];
            this._toneColors = this.getToneColors();
            this.createWindows();
            this.refresh();
        }

        /**
         * @return {Boolean}
         */
        compactMode() {
            return Graphics.height < 700;
        }

        /**
         * @static
         * @return {Array<String>}
         */
        windowGroups() {
            return Knight.Editor.Window_DoodadProperties.windowGroups();
        }

        /**
         * @static
         * @return {Array<String>}
         */
        static windowGroups() {
            if (!this._windowGroupNames) {
                this._windowGroupNames = ["Position", "Display"];
                if (Imported.YEP_X_ExtDoodadPack1) this._windowGroupNames.push("Conditions");
            }
            return this._windowGroupNames;
        }

        /**
         * @return {Object}
         */
        getToneColors() {
            return {
                'Custom': {},
                'Normal': {red: 0, green: 0, blue: 0, grey: 0},
                'Grey': {red: 0, green: 0, blue: 0, grey: 255},
                'Red': {red: 127, green: 0, blue: 0, grey: 0},
                'Orange': {red: 127, green: 64, blue: 0, grey: 0},
                'Yellow': {red: 127, green: 127, blue: 0, grey: 0},
                'Lime': {red: 68, green: 127, blue: 0, grey: 0},
                'Green': {red: 0, green: 127, blue: 0, grey: 0},
                'Turquoise': {red: 0, green: 127, blue: 68, grey: 0},
                'Cyan': {red: 0, green: 127, blue: 127, grey: 0},
                'Sky': {red: 0, green: 68, blue: 127, grey: 0},
                'Blue': {red: 0, green: 0, blue: 127, grey: 0},
                'Purple': {red: 68, green: 0, blue: 127, grey: 0},
                'Magenta': {red: 127, green: 0, blue: 127, grey: 0},
                'Pink': {red: 127, green: 0, blue: 68, grey: 0},
                'Dark': {red: -68, green: -68, blue: -68, grey: 0},
                'Sepia': {red: 34, green: -34, blue: -68, grey: 170},
                'Sunset': {red: 68, green: -34, blue: -34, grey: 0},
                'Night': {red: -68, green: -68, blue: 0, grey: 68},
            };
        }

        /**
         * @param {*} scene
         */
        addToScene(scene) {
            scene.addChild(this);
            this._windows.forEach(function(w) {
                scene.addChild(w);
                if (w.addWindowsToScene) w.addWindowsToScene(scene);
            });
        }

        /**
         * @return {Array<Window>}
         */
        windows() {
            return this._windowGroups[this._activeGroup];
        }

        /**
         * @param {String} group
         */
        setWindowGroup(group) {
            if (this._activeGroup !== group) {
                this.updateWindowGroupVisibility(group);
                this.refresh();
            }
        }

        /**
         * @param {String} group
         */
        updateWindowGroupVisibility(group) {
            this.windows().forEach(function(w) {
                w.hide();
                w.deactivate();
                w.disable();
            });
            this._activeGroup = group;
            this.windows().forEach(function(w) {
                w.show();
                w.activate();
                w.enable();
            });
            if (!this.isAnimated() && this._frameSpeed) {
                this._frameSpeed.hide();
                this._frameSpeed.deactivate();
                this._frameSpeed.disable();
            }
        }

        /**
         */
        updateWindowGroup() {
            this.setWindowGroup(this._commandWindow.currentSymbol());
        }

        /**
         * @param {Object} doodad
         */
        setDoodad(doodad) {
            if (this._doodad !== doodad) {
                this._doodad = doodad;
                this._tonePreset.setValue(this._doodad && this._doodad.toneRed ? 0 : 1);
                this.updateAllValues();
                this.refresh();
            }
        }

        /**
         */
        updateAllValues() {
            if (this._doodad) {
                this._layer.setValue(this._doodad.z);
                this._scaleX.setValue(this._doodad.scaleX * 10 + 100);
                this._scaleY.setValue(this._doodad.scaleY * 10 + 100);
                this._opacityWindow.setValue(this._doodad.opacity);
                this._blend.setValue(this._doodad.blend);
                this._anchorX.setValue(this._doodad.anchorX * 10);
                this._anchorY.setValue(this._doodad.anchorY * 10);
                this._frameSpeed.setValue(this._doodad.frameUpdate);
                this._doodad.smooth ? this._smoothing.check(false) : this._smoothing.uncheck(false);
                DoodadManager._gridLockMode ? this._gridLock.check(false) : this._gridLock.uncheck(false);
                this._gridLockX.setValue(DoodadManager.gridLockX());
                this._gridLockY.setValue(DoodadManager.gridLockY());
                this._toneR.setValue(this._doodad.toneRed + 255);
                this._toneG.setValue(this._doodad.toneGreen + 255);
                this._toneB.setValue(this._doodad.toneBlue + 255);
                this._toneGrey.setValue(this._doodad.toneGrey);
                if (Imported.YEP_X_ExtDoodadPack1) {
                    const switchList = [];
                    const maxSwitches = $dataSystem.switches.length;
                    this._doodad.switchOn = this._doodad.switchOn || [];
                    this._doodad.switchOff = this._doodad.switchOff || [];
                    this._doodad.partyHave = this._doodad.partyHave || [];
                    this._doodad.partyMiss = this._doodad.partyMiss || [];

                    this._doodad.switchOn.forEach(function(id) {
                        if (id < maxSwitches) switchList.push(this._switchBox.makeData(id, $dataSystem.switches[id], true));
                    }, this);
                    this._doodad.switchOff.forEach(function(id) {
                        if (id < maxSwitches) switchList.push(this._switchBox.makeData(id, $dataSystem.switches[id], false));
                    }, this);
                    switchList.sort((a, b) => a.id - b.id);
                    this._switchBox.set(switchList);

                    const partyList = [];
                    const maxActors = $dataActors.length;
                    this._doodad.partyHave.forEach(function(id) {
                        if (id < maxActors) partyList.push(this._partyBox.makeData(id, $dataActors[id].name, true));
                    }, this);
                    this._doodad.partyMiss.forEach(function(id) {
                        if (id < maxActors) partyList.push(this._partyBox.makeData(id, $dataActors[id].name, false));
                    }, this);
                    partyList.sort((a, b) => a.id - b.id);
                    this._partyBox.set(partyList);
                }
            }
        }

        /**
         * @param {*} name
         * @param {*} widget
         */
        updateValue(name, widget) {
            switch (name) {
            case 'layer':
                this._doodad.z = widget.value();
                break;
            case 'scalex':
                this._doodad.scaleX = widget.value();
                break;
            case 'scaley':
                this._doodad.scaleY = widget.value();
                break;
            case 'opacity':
                this._doodad.opacity = widget.value();
                break;
            case 'anchorx':
                this._doodad.anchorX = widget.value();
                break;
            case 'anchory':
                this._doodad.anchorY = widget.value();
                break;
            case 'blend':
                this._doodad.blend = widget.value();
                break;
            case 'smooth':
                this._doodad.smooth = widget.checked();
                break;
            case 'frameSpeed':
                this._doodad.frameUpdate = widget.value();
                break;
            case 'gridLock':
                DoodadManager.setGridLockMode(widget.checked());
                break;
            case 'gridx':
                DoodadManager._gridLockX = widget.value();
                break;
            case 'gridy':
                DoodadManager._gridLockY = widget.value();
                break;
            case 'tonePreset':
                const index = widget.value();
                if (index > 0) {
                    const tone = Object.values(this._toneColors)[index];
                    this._doodad.toneRed = tone.red;
                    this._doodad.toneGreen = tone.green;
                    this._doodad.toneBlue = tone.blue;
                    this._doodad.toneGrey = tone.grey;
                    this.updateAllValues();
                }
                break;
            case 'toneR':
                this._doodad.toneRed = widget.value();
                this._tonePreset.setValue(0);
                break;
            case 'toneG':
                this._doodad.toneGreen = widget.value();
                this._tonePreset.setValue(0);
                break;
            case 'toneB':
                this._doodad.toneBlue = widget.value();
                this._tonePreset.setValue(0);
                break;
            case 'toneGrey':
                this._doodad.toneGrey = widget.value();
                this._tonePreset.setValue(0);
                break;
            case 'switch':
                this._doodad.switchOn = [];
                this._doodad.switchOff = [];
                widget.values().forEach(function(s) {
                    const arr = s.value ? this._doodad.switchOn : this._doodad.switchOff;
                    arr.push(s.id);
                }, this);
                break;
            case 'actor':
                this._doodad.partyHave = [];
                this._doodad.partyMiss = [];
                widget.values().forEach(function(s) {
                    const arr = s.value ? this._doodad.partyHave : this._doodad.partyMiss;
                    arr.push(s.id);
                }, this);
                break;
            }
            this.onChange();
        }

        /**
         */
        layerDecr() {
            this._layer.setValue(this._doodad.z - 1);
            this._doodad.z = this._layer.value();
            this.onChange();
        }

        /**
         */
        layerIncr() {
            this._layer.setValue(this._doodad.z + 1);
            this._doodad.z = this._layer.value();
            this.onChange();
        }

        /**
         * @param {Boolean} byTen   When true, changes values in steps of 10. Otherwise, in steps of 1.
         */
        scaleDecr(byTen) {
            const increment = byTen ? 10 : 1;
            this._scaleX.setValue(this._doodad.scaleX * 10 + 100 - increment);
            this._doodad.scaleX = this._scaleX.value();
            this._scaleY.setValue(this._doodad.scaleY * 10 + 100 - increment);
            this._doodad.scaleY = this._scaleY.value();
            this.onChange();
        }

        /**
         * @param {Boolean} byTen   When true, changes values in steps of 10. Otherwise, in steps of 1.
         */
        scaleIncr(byTen) {
            const increment = byTen ? 10 : 1;
            this._scaleX.setValue(this._doodad.scaleX * 10 + 100 + increment);
            this._doodad.scaleX = this._scaleX.value();
            this._scaleY.setValue(this._doodad.scaleY * 10 + 100 + increment);
            this._doodad.scaleY = this._scaleY.value();
            this.onChange();
        }

        /**
         * Called whenever a widget value changes
         */
        onChange() {
            this._editor.onPropertyChange();
        }

        /**
         */
        createWindows() {
            const groupNames = this.windowGroups();

            // Pages
            const cy = this.y + (this.compactMode() ? 40 : 90);
            this._commandWindow = new Knight.Editor.Window_EditorPageCommand(this.x, cy, this);
            this._commandWindow.refresh();
            this._windows.push(this._commandWindow);
            for (const group in this._windowGroups) {
                if (this._windowGroups.hasOwnProperty(group)) this._windowGroups[group].push(this._commandWindow);
            }
            this._commandWindow.setHandler('ok', this.updateWindowGroup.bind(this));

            const spacing = this.compactMode() ? 54 : 60;
            const x = this.x + 15;
            let y = this._commandWindow.y + this._commandWindow.height + (this.compactMode() ? 5 : 10);

            //////////////////////////////////////////////////////////////////
            // Page 1: Position
            //////////////////////////////////////////////////////////////////
            let page = groupNames[0];

            // layer
            const layerNames = ["Lowest", "Low", "Below", "Same", "Above", "5", "6", "7", "8", "9", "Highest"];
            this._layer = new Knight.EditorSlider(x, y, 0, layerNames.length-1, 5, "Layer", layerNames, null, true);
            this._layer.setHandler('onChange', this.updateValue.bind(this, 'layer', this._layer));
            this._windows.push(this._layer);
            this._windowGroups[page].push(this._layer);
            y += spacing;

            // scale
            const scaleMin = 0;
            const scaleMax = 200;
            const scaleNames = [];
            for (let i = scaleMin; i <= scaleMax; ++i) {
                scaleNames[i] = ((i - 100) / 10).toString();
            }
            const scaleValueFn = function(value) {
                return (value - 100) / 10;
            };
            this._scaleX = new Knight.EditorSlider(x, y, scaleMin, scaleNames.length-1, 110, "Scale", scaleNames, scaleValueFn, true);
            this._scaleY = new Knight.EditorSlider(x, y + 52, scaleMin, scaleNames.length-1, 110, null, scaleNames, scaleValueFn, true);
            this._scaleX.setHandler('onChange', this.updateValue.bind(this, 'scalex', this._scaleX));
            this._scaleY.setHandler('onChange', this.updateValue.bind(this, 'scaley', this._scaleY));
            this._windows.push(this._scaleX);
            this._windows.push(this._scaleY);
            this._windowGroups[page].push(this._scaleX);
            this._windowGroups[page].push(this._scaleY);
            y += spacing * 1.5;

            // anchor
            const anchorMin = 0;
            const anchorMax = 10;
            const anchorNames = [];
            for (let i = anchorMin; i <= anchorMax; ++i) {
                anchorNames[i] = (i / 10).toString();
            }
            const anchorValueFn = function(value) {
                return value / 10;
            };
            this._anchorX = new Knight.EditorSlider(x, y, anchorMin, anchorNames.length-1, 5, "Anchor", anchorNames, anchorValueFn, true);
            this._anchorY = new Knight.EditorSlider(x, y + 52, anchorMin, anchorNames.length-1, 10, null, anchorNames, anchorValueFn, true);
            this._anchorX.setHandler('onChange', this.updateValue.bind(this, 'anchorx', this._anchorX));
            this._anchorY.setHandler('onChange', this.updateValue.bind(this, 'anchory', this._anchorY));
            this._windows.push(this._anchorX);
            this._windows.push(this._anchorY);
            this._windowGroups[page].push(this._anchorX);
            this._windowGroups[page].push(this._anchorY);
            y += spacing * 1.5;

            // grid lock
            const gridMin = 1;
            const gridMax = 240;
            const gridNames = [];
            for (let i = gridMin; i <= gridMax; ++i) {
                gridNames[i] = i.toString();
            }
            this._gridLock = new Knight.EditorCheckbox(x, y, 'Check_On', 'Check_Off', 'Check_Hover', true, 'Grid Lock');
            this._gridLock.width = 350;
            this._gridLock.createContents();
            this._gridLock.uncheck();
            this._gridLock.refresh();
            this._gridLock.setHandler('onChange', this.updateValue.bind(this, 'gridLock', this._gridLock));
            this._windows.push(this._gridLock);
            this._windowGroups[page].push(this._gridLock);
            this._gridLockX = new Knight.EditorSlider(x, y + 22, gridMin, gridNames.length-1, Yanfly.Param.GFDGridWidth, null, gridNames, null, true);
            this._gridLockY = new Knight.EditorSlider(x, y + 44, gridMin, gridNames.length-1, Yanfly.Param.GFDGridHeight, null, gridNames, null, true);
            this._gridLockX.setHandler('onChange', this.updateValue.bind(this, 'gridx', this._gridLockX));
            this._gridLockY.setHandler('onChange', this.updateValue.bind(this, 'gridy', this._gridLockY));
            this._windows.push(this._gridLockX);
            this._windows.push(this._gridLockY);
            this._windowGroups[page].push(this._gridLockX);
            this._windowGroups[page].push(this._gridLockY);
            y += spacing * 2;

            //////////////////////////////////////////////////////////////////
            // Page 2: Display
            //////////////////////////////////////////////////////////////////
            y = this._commandWindow.y + this._commandWindow.height + (this.compactMode() ? 5 : 10);
            page = groupNames[1];

            // opacity
            this._opacityWindow = new Knight.EditorSlider(x, y, 0, 255, 255, "Opacity", null, null, true);
            this._opacityWindow.setHandler('onChange', this.updateValue.bind(this, 'opacity', this._opacityWindow));
            this._windows.push(this._opacityWindow);
            this._windowGroups[page].push(this._opacityWindow);
            y += spacing;

            // blend mode
            // WebGL only accepts NORMAL, ADD, MULTIPLY and SCREEN
            const blendNames = DoodadManager.blendNames();
            this._blend = new Knight.EditorSlider(x, y, 0, blendNames.length-1, 0, "Blend Mode", blendNames, null, true);
            this._blend.setHandler('onChange', this.updateValue.bind(this, 'blend', this._blend));
            this._windows.push(this._blend);
            this._windowGroups[page].push(this._blend);
            y += spacing;

            // smoothing
            this._smoothing = new Knight.EditorCheckbox(x, y, 'Check_On', 'Check_Off', 'Check_Hover', true, 'Smoothing');
            this._smoothing.width = 350;
            this._smoothing.createContents();
            this._smoothing.refresh();
            this._smoothing.setHandler('onChange', this.updateValue.bind(this, 'smooth', this._smoothing));
            this._windows.push(this._smoothing);
            this._windowGroups[page].push(this._smoothing);
            y += spacing * 0.5;

            // tone
            const toneMin = 0;
            const toneMax = 255*2;
            const toneNames = [];
            for (let i = toneMin; i <= toneMax; ++i) {
                toneNames[i] = (i - 255).toString();
            }
            const toneValueFn = function(value) {
                return value - 255;
            };
            const toneGreyNames = [];
            for (let i = toneMin; i <= 255; ++i) {
                toneGreyNames[i] = i.toString();
            }
            const colorNames = Object.keys(this._toneColors);
            this._tonePreset = new Knight.EditorSlider(x, y, 0, colorNames.length-1, 1, "Color Tone", colorNames, null, true);
            this._toneR = new Knight.EditorSlider(x, y + 52, toneMin, toneNames.length-1, 255, null, toneNames, toneValueFn, true);
            this._toneG = new Knight.EditorSlider(x, y + 74, toneMin, toneNames.length-1, 255, null, toneNames, toneValueFn, true);
            this._toneB = new Knight.EditorSlider(x, y + 96, toneMin, toneNames.length-1, 255, null, toneNames, toneValueFn, true);
            this._toneGrey = new Knight.EditorSlider(x, y + 118, toneMin, toneGreyNames.length-1, 0, null, toneGreyNames, null, true);
            this._tonePreset.setHandler('onChange', this.updateValue.bind(this, 'tonePreset', this._tonePreset));
            this._toneR.setHandler('onChange', this.updateValue.bind(this, 'toneR', this._toneR));
            this._toneG.setHandler('onChange', this.updateValue.bind(this, 'toneG', this._toneG));
            this._toneB.setHandler('onChange', this.updateValue.bind(this, 'toneB', this._toneB));
            this._toneGrey.setHandler('onChange', this.updateValue.bind(this, 'toneGrey', this._toneGrey));
            this._windows.push(this._tonePreset);
            this._windows.push(this._toneR);
            this._windows.push(this._toneG);
            this._windows.push(this._toneB);
            this._windows.push(this._toneGrey);
            this._windowGroups[page].push(this._tonePreset);
            this._windowGroups[page].push(this._toneR);
            this._windowGroups[page].push(this._toneG);
            this._windowGroups[page].push(this._toneB);
            this._windowGroups[page].push(this._toneGrey);
            y += spacing * (this.compactMode() ? 2.5 : 2.3);

            // frame speed
            this._frameSpeed = new Knight.EditorSlider(x, y, 1, 60, 20, "Frame Speed", null, null, true);
            this._frameSpeed.setHandler('onChange', this.updateValue.bind(this, 'frameSpeed', this._frameSpeed));
            this._windows.push(this._frameSpeed);
            this._windowGroups[page].push(this._frameSpeed);
            y += spacing;

            //////////////////////////////////////////////////////////////////
            // Page 3: Conditions
            //////////////////////////////////////////////////////////////////
            if (Imported.YEP_X_ExtDoodadPack1) {
                y = this._commandWindow.y + this._commandWindow.height + (this.compactMode() ? 5 : 10);
                page = groupNames[2];

                // Switches
                const dropWidth = 245;
                this._switchDropDownIds = new Map();
                const switchNames = $dataSystem.switches.map(function(name, index) {
                    const text = `${index.padZero(4)}: ${name}`;
                    this._switchDropDownIds.set(text, index);
                    return text;
                }, this);
                switchNames.shift(); // Remove element 0
                this._switchDropDown = new Knight.EditorDropDown(x, y, dropWidth, switchNames, 0, "Switches");
                this._switchDropDown.setHandler('onChange', this.addSwitch.bind(this, this._switchDropDown));
                y += spacing;

                // switch box
                const maxBoxRows = this.compactMode() ? 3 : 4;
                const switchBoxHeight = maxBoxRows * Knight.EditorSwitchBox.itemHeight();
                this._switchBox = new Knight.EditorSwitchBox(x, y, this._switchDropDown.width + 80, switchBoxHeight);
                this._switchBox.setHandler('onChange', this.updateValue.bind(this, 'switch', this._switchBox));
                this._windows.push(this._switchBox);
                this._windowGroups[page].push(this._switchBox);
                y += this._switchDropDown.height + (this.compactMode() ? spacing * 0.25 : spacing * 0.75);

                // Party Members
                this._partyDropDownIds = new Map();
                const actorNames = $dataActors.map(function(data, index) {
                    const text = index > 0 ? `${index.padZero(4)}: ${data.name}` : null;
                    this._partyDropDownIds.set(text, index);
                    return text;
                }, this);
                actorNames.shift(); // Remove element 0
                this._partyDropDown = new Knight.EditorDropDown(x, y, dropWidth, actorNames, 0, "Party Members");
                this._partyDropDown.setHandler('onChange', this.addActor.bind(this, this._partyDropDown));
                y += spacing;

                // party box
                const maxPartyBoxRows = this.compactMode() ? 2 : 3;
                const partyBoxHeight = maxPartyBoxRows * 60;
                this._partyBox = new Knight.EditorPartyBox(x, y, this._partyDropDown.width + 80, partyBoxHeight);
                this._partyBox.setHandler('onChange', this.updateValue.bind(this, 'actor', this._partyBox));
                this._windows.push(this._partyBox);
                this._windowGroups[page].push(this._partyBox);
                y += this._partyDropDown.height + spacing;
            }

            //////////////////////////////////////////////////////////////////
            // Buttons
            //////////////////////////////////////////////////////////////////
            const buttonSpacing = 28;
            const buttonY = this.y + 6;
            const buttonNames = ['Default', 'Copy', 'Delete'];
            let buttonX = this.x + this.width - (buttonSpacing * buttonNames.length);
            for (let i = 0; i < buttonNames.length; ++i) {
                const name = buttonNames[i];
                const button = new Knight.Button(buttonX, buttonY, name, `${name}_Hover`);
                button.setOkSound(null);
                this._windows.push(button);
                this._buttons.push(button);
                button.setHandler('ok', this[name.toLowerCase()].bind(this));
                buttonX += buttonSpacing;
            }

            // Add Drop-downs last so they draw above other windows
            if (Imported.YEP_X_ExtDoodadPack1) {
                this._windows.push(this._partyDropDown);
                this._windowGroups[page].push(this._partyDropDown);
                this._windows.push(this._switchDropDown);
                this._windowGroups[page].push(this._switchDropDown);
            }
        }

        /**
         * @return {Boolean}
         */
        isAnimated() {
            if (!this._doodad) return false;
            if (this._doodad.xFrames > 1) return true;
            if (this._doodad.yFrames > 1) return true;
            return false;
        };

        /**
         */
        copy() {
            this._editor.copy();
        }

        /**
         */
        default() {
            const defaultDoodad = DoodadManager.getTemplate(this._doodad.folder, this._doodad.bitmap);
            const ignoreList = ["x", "y", "xFrames", "yFrames", "iconIndex"];
            for (const property in defaultDoodad) {
                if (ignoreList.includes(property)) continue;
                if (this._doodad.hasOwnProperty(property)) {
                    this._doodad[property] = defaultDoodad[property];
                }
            }
            DoodadManager.setGridLockMode(false);
            DoodadManager._gridLockX = Yanfly.Param.GFDGridWidth;
            DoodadManager._gridLockY = Yanfly.Param.GFDGridHeight;
            this._tonePreset.setValue(1);
            this.updateAllValues();
            this.onChange();
            this._editor.playSound(Knight.Param.KESEReset);
        }

        /**
         */
        delete() {
            this._editor.delete();
        }

        /**
         */
        toggleGridLock() {
            DoodadManager.setGridLockMode(!DoodadManager._gridLockMode);
            DoodadManager._gridLockMode ? this._gridLock.check(false) : this._gridLock.uncheck(false);
            SoundManager.playCursor();
        }

        /**
         */
        addSwitch() {
            const id = this._switchDropDownIds.get(this._switchDropDown.value());
            this._switchBox.add(id, $dataSystem.switches[id], true);
        }

        /**
         */
        addActor() {
            const id = this._partyDropDownIds.get(this._partyDropDown.value());
            this._partyBox.add(id, $dataActors[id].name, true);
        }

        /**
         */
        show() {
            Knight.Window_Base.prototype.show.call(this);
            this.windows().forEach((w) => w.show());
            this._buttons.forEach(function(b) {
                if (!this._editor.inPlacementMode() || b === this._buttons[0]) b.show();
            }, this);
            if (!this.isAnimated() && this._frameSpeed) this._frameSpeed.hide();
        }

        /**
         */
        hide() {
            Knight.Window_Base.prototype.hide.call(this);
            this._windows.forEach((w) => w.hide());
        }

        /**
         */
        enable() {
            Knight.Window_Base.prototype.enable.call(this);
            this.windows().forEach((w) => w.enable());
            this._buttons.forEach(function(b) {
                if (!this._editor.inPlacementMode() || b === this._buttons[0]) b.enable();
            }, this);
            if (!this.isAnimated() && this._frameSpeed) this._frameSpeed.disable();
        }

        /**
         */
        disable() {
            Knight.Window_Base.prototype.disable.call(this);
            this._windows.forEach((w) => w.disable());
        }

        /**
         */
        activate() {
            Knight.Window_Base.prototype.activate.call(this);
            this.windows().forEach((w) => w.activate());
            this._buttons.forEach(function(b) {
                if (!this._editor.inPlacementMode() || b === this._buttons[0]) b.activate();
            }, this);
            if (!this.isAnimated() && this._frameSpeed) this._frameSpeed.deactivate();
        }

        /**
         */
        deactivate() {
            Knight.Window_Base.prototype.deactivate.call(this);
            this._windows.forEach((w) => w.deactivate());
        }

        /**
         */
        refresh() {
            if (this.contents) {
                this.contents.clear();
                this.drawBackground();
                if (this._doodad) {
                    this.drawProperties();
                }
            }
        }

        /**
         */
        drawBackground() {
            this.contents.fillRect(0, 0, this.contents.width, this.contents.height, Knight.COLOR.DARK_GREY);
            this.contents.fillRect(0, this._commandWindow.y + this._commandWindow.height,
                this.contents.width, 2, Knight.COLOR.BLACK);
        }

        /**
         */
        drawProperties() {
            this.contents.fontFace = Knight.Param.KEPanelFont;
            this.contents.fontSize = 20;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.drawText(`${this._doodad.bitmap}`, 0, this.compactMode() ? 0 : 20, this.width, 'center');

            if (this._doodad.iconIndex) {
                this.drawIcon(this._doodad.iconIndex, 30, 30);
            } else if (!this.compactMode()) {
                this.drawDoodadImage(20, 10, 60, 60, this._doodad.folder, this._doodad.bitmap);
            }

            this.contents.fontSize = this.compactMode() ? 12 : 16;
            this.drawText(`X: ${this._doodad.x}`, this.compactMode() ? 10 : 110, this.compactMode() ? 0 : 50, this.width/2, 'left');
            this.drawText(`Y: ${this._doodad.y}`, this.compactMode() ? 50 : 180, this.compactMode() ? 0 : 50, this.width/2, 'left');

            let x; let y;
            if (this.windows().includes(this._scaleX)) {
                x = this._scaleX.x - this.x - 3;
                y = this._scaleX.y - this.y + this._scaleX.barTextY();
                this.drawText('X', x, y, this.width, 'left');
                x = this._scaleY.x - this.x - 3;
                y = this._scaleY.y - this.y + this._scaleY.barTextY();
                this.drawText('Y', x, y, this.width, 'left');
            }

            if (this.windows().includes(this._anchorX)) {
                x = this._anchorX.x - this.x - 3;
                y = this._anchorX.y - this.y + this._anchorX.barTextY();
                this.drawText('X', x, y, this.width, 'left');
                x = this._anchorY.x - this.x - 3;
                y = this._anchorY.y - this.y + this._anchorY.barTextY();
                this.drawText('Y', x, y, this.width, 'left');
            }

            if (this.windows().includes(this._gridLock)) {
                x = this._gridLockX.x - this.x - 3;
                y = this._gridLockX.y - this.y + this._gridLockX.barTextY();
                this.drawText('W', x, y, this.width, 'left');
                x = this._gridLockY.x - this.x - 3;
                y = this._gridLockY.y - this.y + this._gridLockY.barTextY();
                this.drawText('H', x, y, this.width, 'left');
            }

            if (this.windows().includes(this._toneR)) {
                x = this._toneR.x - this.x - 3;
                y = this._toneR.y - this.y + this._toneR.barTextY();
                this.changeTextColor(Knight.COLOR.DARK_ROSE);
                this.drawText('R', x, y, this.width, 'left');
                x = this._toneG.x - this.x - 3;
                y = this._toneG.y - this.y + this._toneG.barTextY();
                this.changeTextColor(Knight.COLOR.DARK_GREEN);
                this.drawText('G', x, y, this.width, 'left');
                x = this._toneB.x - this.x - 3;
                y = this._toneB.y - this.y + this._toneB.barTextY();
                this.changeTextColor(Knight.COLOR.DARK_BLUE);
                this.drawText('B', x, y, this.width, 'left');
                x = this._toneGrey.x - this.x - 3;
                y = this._toneGrey.y - this.y + this._toneGrey.barTextY();
                this.changeTextColor(Knight.COLOR.GREY);
                this.drawText('G', x, y, this.width, 'left');
                this.changeTextColor(this.normalColor());
            }
        }

        /**
         * @param {Number} x
         * @param {Number} y
         * @param {Number} maxWidth
         * @param {Number} maxHeight
         * @param {String} folder
         * @param {String} filename
         * @return {*}
         */
        drawDoodadImage(x, y, maxWidth, maxHeight, folder, filename) {
            const bitmap = ImageManager.loadDoodad(folder + '/' + filename, 0, true);
            if (bitmap.width <= 0) {
                return setTimeout(this.drawDoodadImage.bind(this, x, y, maxWidth, maxHeight, folder, filename), 5);
            }
            const xframes = DoodadManager.getXFrames(filename);
            const yframes = DoodadManager.getYFrames(filename);
            const pw = Math.floor(bitmap.width / xframes);
            const ph = Math.floor(bitmap.height / yframes);
            let dw = pw;
            let dh = ph;
            if (dw > maxWidth) {
                const rate = maxWidth / dw;
                dw *= rate;
                dh *= rate;
            }
            if (dh > maxHeight) {
                const rate = maxHeight / dh;
                dw *= rate;
                dh *= rate;
            }
            const dx = x + 2 + (maxWidth - dw) / 2;
            const dy = y + 2 + (maxHeight - dh) / 2;
            this.contents.blt(bitmap, 0, 0, pw, ph, dx, dy, dw, dh);
        };
    };

    //=============================================================================
    // Editor Checkbox
    //=============================================================================
    /**
     * Modified version of the default Checkbox for use with the Editor.
     *
     * @class Knight.EditorCheckbox
     * @extends {Knight.Checkbox}
     */
    Knight.EditorCheckbox = class extends Knight.Checkbox {
        /**
         * Plays a sound when the checkbox is interacted with.
         */
        playOkSound() {
            SoundManager.playCursor();
        };

        /**
         * Set checkbox to 'checked'
         * @param {Boolean} callOnChange
         */
        check(callOnChange = true) {
            this._checked = true;
            this.refresh();
            if (callOnChange) this.onChange();
        }

        /**
         * Set checkbox to 'unchecked'
         * @param {Boolean} callOnChange
         */
        uncheck(callOnChange = true) {
            this._checked = false;
            this.refresh();
            if (callOnChange) this.onChange();
        }

        /**
         */
        drawContents() {
            this.contents.fontFace = Knight.Param.KEPanelFont;
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.changeTextColor(Knight.COLOR.LIGHT);
            this.contents.drawText(this._label, 0, 0, this.contents.width, this.contents.height, 'left');
            const drawX = this.textWidth(this._label) + 50;
            if (this.checked()) {
                this.drawGui(this._onImage, drawX, 0);
            } else {
                this.drawGui(this._offImage, drawX, 0);
            }
            if (this.isMouseInsideFrame() && this._hoverImage) {
                this.drawGui(this._hoverImage, drawX, 0);
            }
        }
    };

    //=============================================================================
    // Doodad Editor Slider
    //=============================================================================
    /**
     * Slider widget used in the object editor.
     *
     * @class Knight.EditorSlider
     * @extends {Knight.Button}
     */
    Knight.EditorSlider = class extends Knight.Button {
        /**
         * Creates an instance of Knight.Slider.
         *
         * @param {number} x                Window x coordinate
         * @param {number} y                Window y coordinate
         * @param {number} min              Slider min value. Must be an integer.
         * @param {number} max              Slider max value. Must be an integer.
         * @param {number} value            Slider starting value, in the range [min, max].
         * @param {string} label            Slider displayed title.
         * @param {string} labelMap         Optional map that converts values to label names. So you can have
         *                                  an internal value of "1" but display "Medium".
         * @param {string} valueFn          Optional function to be applied to return values. Can be used to map
         *                                  the stored integer values into any other value.
         * @param {boolean} snapToValue     When true, the slider position snaps to the nearest integer value, so
         *                                  the bar moves in "steps" when dragging it. When false, there is no
         *                                  rounding, so the bar moves smoothly. This only affects the UI display,
         *                                  the value() returned by the slider is always rounded. Always set to true
         *                                  when a labelMap is provided.
         */
        constructor(x, y, min, max, value, label = null, labelMap = null, valueFn = null, snapToValue = false) {
            const bgBitmap = ImageManager.loadGui('EditorSlider_Bar');
            const buttonBitmap = ImageManager.loadGui('EditorSlider_Handler');
            const labelHeight = label ? 60 : 0;
            const width = 350;
            const height = bgBitmap.height + labelHeight;
            super(x, y, null, null, null, width, height);
            this._min = min;
            this._max = max;
            this._value = value.clamp(min, max);
            this._buttonBitmap = buttonBitmap;
            this._barImageName = 'EditorSlider_Bar';
            this._fillImageName = 'EditorSlider_Fill';
            this._sliderStartX = 18;
            this._sliderEndX = this._sliderStartX + ImageManager.loadGui(this._barImageName).width;
            this._draggingDoodad = false;
            this._snapToValue = labelMap ? true : snapToValue;
            this._label = label;
            this._labelHeight = labelHeight;
            this._labelMap = labelMap;
            this._valueFn = valueFn;
            this.createWindows();
            this.refresh();
        }

        /**
         * @return {boolean}
         */
        hasTooltips() {
            return false;
        };

        /**
         * @return {number} Slider value. Rounds to nearest integer. Applies the value conversion
         *                  function, if one was provided when creating the slider.
         */
        value() {
            const v = Math.round(this._value);
            return this._valueFn ? this._valueFn(v) : v;
        }

        /**
         * @param {Number} newValue
         */
        setValue(newValue) {
            if (newValue >= this._min && newValue <= this._max) {
                this._value = newValue;
                if (this._snapToValue) {
                    this._handler.x = this.handlerX();
                } else {
                    this._handler.x = destX - (this._buttonBitmap.width / 2);
                }
                this.refresh();
            }
        }

        /**
         * @return {number} Data used for tooltip display
         */
        item() {
            return this._descriptionId;
        }

        /**
         * @return {number} Y position of the slider, without including the label.
         */
        widgetY() {
            return this._label ? this._labelHeight : 0;
        }

        /**
         * @return {number} Y position of slider bar.
         */
        barY() {
            return (this.contents.height - ImageManager.loadGui(this._barImageName).height) / 2;
        }

        /**
         * @return {number} Y position of slider bar text
         */
        barTextY() {
            const gaugeHeight = ImageManager.loadGui(this._barImageName).height;
            return this.barY() + (gaugeHeight / 2) - (this.lineHeight() / 2);
        }

        /**
         * @return {number} How full the slider is. A number between 0 and 1.
         */
        rate() {
            return (this._value - this._min) / (this._max - this._min);
        }

        /**
         * @return {number} X position of handler based on current value.
         */
        handlerX() {
            const diff = (this._sliderEndX - this._buttonBitmap.width - 4) - this._sliderStartX;
            return this.x + this._sliderStartX + 2 + Math.round(diff * this.rate());
        }

        /**
         */
        createWindows() {
            this._handler = new Knight.Button(
                this.handlerX(),
                this.y + (this.contents.height - this._buttonBitmap.height) / 2,
                "EditorSlider_Handler", "EditorSlider_Handler_Hover");
        }

        /**
         * Repositions child windows. Called when X coordinate changes.
         */
        reposition() {
            this._handler.x = this.handlerX();
            this._tooltipRect = new Rectangle(this.x, this.y, this.width, this.height);
            this._tooltipPosition = (this.x < (Graphics.width / 2)) ? 6 : 4;
        }

        /**
         * Adds child windows to parent scene's window layer.
         * @param {Scene_Base} scene    Parent scene
         */
        addWindowsToScene(scene) {
            scene.addChild(this._handler);
        };

        /**
         * Shows the window and all child windows.
         */
        show() {
            super.show();
            this._handler.show();
        }


        /**
         * Hides the window and all child windows.
         */
        hide() {
            super.hide();
            this._handler.hide();
        }

        /**
         * Shows the window and all child windows.
         */
        enable() {
            super.enable();
            this._handler.enable();
        }

        /**
         * Disable the window and all child windows.
         */
        disable() {
            super.disable();
            this._handler.disable();
        }

        /**
         * Frame update
         */
        update() {
            Window_Base.prototype.update.call(this);
            this.processHover();
            this.processTouch();
            this.processDrag();
        }

        /**
         * Handle mouse input
         */
        processTouch() {
            if (this.isOpenAndEnabled()) {
                if (TouchInput.isTriggered() && this._handler.isMouseInsideFrame()) {
                    this._draggingDoodad = true;
                } else if (TouchInput.isReleased()) {
                    this._draggingDoodad = false;
                }
            } else {
                this._draggingDoodad = false;
            }
        }

        /**
         * Handle mouse input
         */
        processDrag() {
            if (this._draggingDoodad) {
                const oldValue = this._value;
                const minX = this.x + this._sliderStartX;
                const maxX = this.x + this._sliderEndX;
                const destX = TouchInput.x.clamp(minX, maxX);
                const rate = (destX - minX) / (maxX - minX);
                this._value = this._min + (this._max - this._min) * rate;
                if (this._snapToValue) {
                    this._value = Math.round(this._value);
                    this._handler.x = this.handlerX();
                } else {
                    this._handler.x = destX - (this._buttonBitmap.width / 2);
                }
                if (this._value !== oldValue) {
                    this.onChange();
                    this.refresh();
                }
            }
        }

        /**
         * Called whenever the slider value changes
         */
        onChange() {
            if (this.isHandled('onChange')) {
                this._handlers['onChange']();
            }
        }

        /**
         * Redraw checkbox
         */
        refresh() {
            if (this.contents && this._value != null) {
                this.contents.clear();
                this.drawContents();
            }
        }

        /**
         */
        drawContents() {
            this.contents.fontFace = Knight.Param.KEPanelFont;
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.changeTextColor(Knight.COLOR.LIGHT);

            // Draw slider label
            if (this._label) {
                this.contents.drawText(this._label, 0, 0, this.contents.width, this.lineHeight(), 'left');
            }
            // Draw slider gauge
            // this.drawImageGauge(this._sliderStartX, this.barY(), this.rate(), this._barImageName, this._fillImageName);
            const x = this._sliderStartX;
            const y = this.barY();
            this.drawGui(this._barImageName, x, y);
            const rate = this.rate();
            if (rate > 0) {
                const backBitmap = ImageManager.loadGui(this._fillImageName);
                const dw = backBitmap.width * rate;
                this.drawGui(this._fillImageName, x + 2, y + 2, dw);
            }

            // Draw slider value
            const value = Math.round(this._value);
            const vx = this._sliderEndX + 10;
            const vy = this.barTextY();
            const valueText = this._labelMap ? this._labelMap[value] : value;
            this.contents.drawText(valueText, vx, vy, this.contents.width - vx, this.lineHeight(), 'left');
        }
    };

    //=============================================================================
    // Import From Map Window
    //=============================================================================
    Knight.Editor.Window_DoodadImport = class extends Knight.Window_Base {
        /**
         *Creates an instance of Window_DoodadImport.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Number} width
        * @param {Number} height
        * @param {KnightEditor} editor
        */
        constructor(x, y, width, height, editor) {
            super(x, y, width, height);
            this._editor = editor;
            this._windows = [];
            this.createWindows();
            this.refresh();
        }

        /**
         * @param {*} scene
         */
        addToScene(scene) {
            scene.addChild(this);
            this._windows.forEach(function(w) {
                scene.addChild(w);
                if (w.addWindowsToScene) w.addWindowsToScene(scene);
            });
        }

        /**
         */
        playOkSound() {
            this._mapIdBox.hasValidValue() ? SoundManager.playOk() : SoundManager.playBuzzer();
        };

        /**
         * @return {Number}
         */
        lineHeight() {
            return this.contents.fontSize;
        }

        /**
         * @return {Number}
         */
        mapId() {
            return this._mapIdBox.value();
        }

        /**
         */
        createWindows() {
            // Name Entry Box
            const validMapIds = [];
            for (const info of $dataMapInfos) {
                if (info && info.id !== DoodadManager.mapId()) validMapIds.push(info.id);
            }
            this._mapIdBox = new Knight.Editor.Window_NumberInput(this.x + 14, this.y + 38, 214, 27, 'Type a map ID...');
            this._mapIdBox.setValidValues(validMapIds);
            this._windows.push(this._mapIdBox);

            // Buttons
            // Close
            let buttonX = this.x + this.width - 30;
            let buttonY = this.y + 7;
            this._closeButton = new Knight.Button(buttonX, buttonY, 'Close', 'Close_Hover');
            this._closeButton.setOkSound(null);
            this._closeButton.setHandler('ok', this.processCancel.bind(this));
            this._windows.push(this._closeButton);

            const buttonSpacing = 20;
            const buttonWidth = 80;
            const buttonHeight = 30;
            const buttonAreaWidth = buttonWidth*2 + buttonSpacing;
            buttonX = this.x + this.width/2 - buttonAreaWidth/2;
            buttonY = this.y + this.height - buttonHeight - 12;

            // Confirm
            this._okButton = new Knight.Button(buttonX, buttonY, 'Button', 'Button_Hover', 'Confirm');
            this._okButton.setOkSound(null);
            this._okButton.setHandler('ok', this.processOk.bind(this));
            this._windows.push(this._okButton);

            // Cancel
            buttonX += buttonWidth + buttonSpacing;
            this._cancelButton = new Knight.Button(buttonX, buttonY, 'Button', 'Button_Hover', 'Cancel');
            this._cancelButton.setOkSound(null);
            this._cancelButton.setHandler('ok', this.processCancel.bind(this));
            this._windows.push(this._cancelButton);
        }

        /**
         */
        show() {
            Knight.Window_Base.prototype.show.call(this);
            this._windows.forEach((w) => w.show());
        }

        /**
         */
        hide() {
            Knight.Window_Base.prototype.hide.call(this);
            this._windows.forEach((w) => w.hide());
        }

        /**
         */
        enable() {
            Knight.Window_Base.prototype.enable.call(this);
            this._windows.forEach((w) => w.enable());
        }

        /**
         */
        disable() {
            Knight.Window_Base.prototype.disable.call(this);
            this._windows.forEach((w) => w.disable());
        }

        /**
         */
        activate() {
            Knight.Window_Base.prototype.activate.call(this);
            this._windows.forEach((w) => w.activate());
        }

        /**
         */
        deactivate() {
            Knight.Window_Base.prototype.deactivate.call(this);
            this._windows.forEach((w) => w.deactivate());
        }

        /**
         */
        refresh() {
            if (this.contents) {
                this.contents.clear();
                this.drawBackground();
            }
        }

        /**
         */
        drawBackground() {
            this.contents.fontFace = Knight.Param.KEFont;
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.changeTextColor(Knight.COLOR.LIGHT);
            this.drawGui('Import_Window', 0, 0);
            this.drawText('Import From Map', 0, 10, this.contents.width, 'center');
        }
    };

    //=============================================================================
    // Yanfly Overwrites
    //=============================================================================
    Knight.Editor.Window_NumberInput = class extends Knight.Window_Base {
        /**
         *Creates an instance of Window_NumberInput.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Number} width
        * @param {Number} height
        * @param {String} title
        */
        constructor(x, y, width, height, title) {
            super(x, y, width, height);
            this._text = '';
            this._title = title;
            this._index = 0;
            this._maxLength = 3;
            this._maxValue = Number.MAX_SAFE_INTEGER;
            this._validValues = null;
            this._keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            this.refresh();
        };

        /**
         * @return {Number}
         */
        lineHeight() {
            return this.height;
        }

        /**
         */
        update() {
            Knight.Window_Base.prototype.update.call(this);
            this.processHandling();
        };

        /**
         */
        processHandling() {
            if (this.isOpenAndEnabled()) {
                if (Input.isRepeated(Knight.INPUT['backspace'])) {
                    if (this.back()) {
                        Input.clear();
                        SoundManager.playCursor();
                    }
                    return;
                }
                this._keys.forEach(function(key) {
                    if (Input.isRepeated(Knight.INPUT[key])) {
                        this.add(key);
                    }
                }, this);
            }
        };

        /**
         * @param {Array<Number>} valueList
         */
        setValidValues(valueList) {
            this._validValues = valueList;
            this.refresh();
        }

        /**
         * @param {Number} value
         */
        setMaxValue(value) {
            this._text = '';
            this._index = 0;
            this._maxValue = value;
            this._maxLength = (value === 0) ? 1 : Math.floor(Math.log10(value)) + 1;
            this.refresh();
        }

        /**
         * @return {Boolean}
         */
        hasValue() {
            return this._text !== '';
        }

        /**
         * @return {Boolean}
         */
        hasValidValue() {
            return this.hasValue() && !this._validValues || this._validValues.includes(this.toNumber());
        }

        /**
         * @return {Number}
         */
        toNumber() {
            return this.hasValue() ? Math.min(parseInt(this._text, 10), this._maxValue) : null;
        }

        /**
         * @return {Number}
         */
        value() {
            return this.hasValidValue() ? this.toNumber() : null;
        };

        /**
         * @param {String} char
         * @return {Boolean}
         */
        add(char) {
            if (this._index < this._maxLength) {
                this._text += char;
                this._index++;
                this.refresh();
                return true;
            } else {
                return false;
            }
        };

        /**
         * @return {Boolean}
         */
        back() {
            if (this._index > 0) {
                this._index--;
                this._text = this._text.slice(0, this._index);
                this.refresh();
                return true;
            } else {
                return false;
            }
        };

        /**
         * @return {Number}
         */
        charWidth() {
            const text = $gameSystem.isJapanese() ? '\uff21' : 'A';
            return this.textWidth(text);
        };

        /**
         * @return {Number}
         */
        left() {
            const nameCenter = this.contentsWidth() / 2;
            const nameWidth = (this._maxLength + 1) * this.charWidth();
            return Math.min(nameCenter - nameWidth / 2, this.contentsWidth() - nameWidth);
        };

        /**
         * @param {Number} index
         * @return {Object}
         */
        itemRect(index) {
            return {
                x: this.left() + index * this.charWidth(),
                y: 0,
                width: this.charWidth(),
                height: this.lineHeight(),
            };
        };

        /**
         * @param {Number} index
         */
        drawChar(index) {
            const rect = this.itemRect(index);
            this.drawText(this._text[index] || '', rect.x, rect.y);
        };

        /**
         */
        refresh() {
            this.contents.clear();
            this.contents.fontFace = Knight.Param.KEFont;
            this.contents.fontSize = 16;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            if (this._text === '') {
                this.changeTextColor(Knight.COLOR.MID_GREY);
                this.drawText(this._title, 10, 0);
                this.setCursorRect(0, 0, 0, 0);
            } else {
                this.hasValidValue() ? this.changeTextColor(Knight.COLOR.LIGHT) : this.changeTextColor(Knight.COLOR.ROSE);
                for (let j = 0; j < this._text.length; j++) {
                    this.drawChar(j);
                }
                const rect = this.itemRect(this._index);
                this.setCursorRect(rect.x, rect.y, rect.width, rect.height);
            }
        };
    };
    Knight.Editor.Window_NumberInput.prototype._refreshCursor = Window.prototype._refreshCursor;

    //=============================================================================
    // Yanfly Overwrites
    //=============================================================================
    DoodadManager.doodads = function() {
        return $dataDoodads[this.mapId()] || [];
    };

    DoodadManager.size = function() {
        return this.doodads().length;
    };

    DoodadManager.isDirty = function(mapId = this.mapId()) {
        this._isDirty = this._isDirty || [];
        return this._isDirty[mapId] || false;
    };

    DoodadManager.setDirty = function(isDirty, mapId = this.mapId()) {
        this._isDirty = this._isDirty || [];
        this._isDirty[mapId] = isDirty;
    };

    DoodadManager.isMouseOnDoodads = function() {
        const doodadSprites = SceneManager._scene._spriteset._doodads;
        if (doodadSprites) {
            const mx = $gameMap.canvasToMapPX(TouchInput.x);
            const my = $gameMap.canvasToMapPY(TouchInput.y);
            return doodadSprites.find((sprite) => DoodadManager.isMouseOnDoodad(sprite, mx, my));
        } else {
            return false;
        }
    };

    DoodadManager.isMouseOnDoodad = function(sprite, x, y) {
        const doodad = sprite._data;
        const doodadW = sprite.width * sprite.scale.x;
        const doodadH = sprite.height * sprite.scale.y;
        const minX = doodad.x - doodadW * sprite.anchor.x;
        const minY = doodad.y - doodadH * sprite.anchor.y;
        const maxX = minX + doodadW;
        const maxY = minY + doodadH;
        return x >= minX && y >= minY && x < maxX && y < maxY;
    };

    DoodadManager.getTemplate = function(folderName, bitmapName) {
        const obj = {
            folder: folderName || '',
            bitmap: bitmapName || '',
            x: 0,
            y: 0,
            z: 3,
            toneRed: 0,
            toneGreen: 0,
            toneBlue: 0,
            toneGrey: 0,
            iconIndex: 0,
            anchorX: 0.5,
            anchorY: 1.0,
            scaleX: 1,
            scaleY: 1,
            blend: 0,
            opacity: 255,
            xFrames: 1,
            yFrames: 1,
            frameUpdate: 20,
            smooth: Yanfly.Param.GFDSmooth,
        };
        if (Imported.YEP_X_ExtDoodadPack1) {
            obj.switchOn = [];
            obj.switchOff = [];
            obj.partyHave = [];
            obj.partyMiss = [];
        }
        return obj;
    };

    DoodadManager.blendNames = function() {
        return ['Normal', 'Additive', 'Multiply', 'Screen'];
    };

    DoodadManager.current = function() {
        return SceneManager._scene.currentDoodad();
    };

    DoodadManager.currentCopy = function() {
        return JsonEx.makeDeepCopy(SceneManager._scene.currentDoodad());
    };

    DoodadManager.updateManualMove = function() {
        if (this._manualMove !== false) return;
        if (SceneManager._scene._editor.inPlacementMode()) {
            this._manualX = TouchInput.x;
            this._manualY = TouchInput.y;
        } else {
            const doodad = SceneManager._scene._editor._currentDoodad;
            this._manualX = doodad.x;
            this._manualY = doodad.y;
        }
    };

    DoodadManager.keyDown = function(code) {
    };

    DoodadManager.usingManualMove = function() {
        return this._manualMove;
    };

    DoodadManager.manualMoveUp = function() {
        this.setManualMove(true);
        this._manualY -= this._gridLockMode ? this.gridLockY() : 1;
        this.adjustManualMove();
        return this._manualY;
    };

    DoodadManager.manualMoveDown = function() {
        this.setManualMove(true);
        this._manualY += this._gridLockMode ? this.gridLockY() : 1;
        this.adjustManualMove();
        return this._manualY;
    };

    DoodadManager.manualMoveLeft = function() {
        this.setManualMove(true);
        this._manualX -= this._gridLockMode ? this.gridLockX() : 1;
        this.adjustManualMove();
        return this._manualX;
    };

    DoodadManager.manualMoveRight = function() {
        this.setManualMove(true);
        this._manualX += this._gridLockMode ? this.gridLockX() : 1;
        this.adjustManualMove();
        return this._manualX;
    };

    Sprite_Doodad.prototype.update = function() {
        Sprite_Base.prototype.update.call(this);
        this.updatePosition();
        if (!this._loadedData) return;
        this.updateCustomA();
        this.updateFrame();
        this.updateCustomZ();
        if (!this._loadedData) return;
        this.updateSettingsOpacity();
        this.updateSwitches();
    };

    Sprite_Doodad.prototype.updateSettingsOpacity = function() {
        this.opacity = this._data.opacity;
        if (Knight.Editor.isActive()) {
            const editor = SceneManager._scene._editor;
            if (editor.inPlacementMode()) {
                if (DoodadManager.current().z !== this._data.z) this.opacity /= 2;
            } else if (editor.inSelectionMode() && editor._currentDoodad) {
                const doodad = editor._currentDoodad;
                if (doodad.z !== this._data.z) this.opacity /= 2;
            }
        }
    };

    Sprite_DoodadCursor.prototype.updatePosition = function() {
        if (DoodadManager._manualMove) {
            this.x = this.gridX(DoodadManager._manualX);
            this.y = this.gridY(DoodadManager._manualY);
        } else {
            this.x = this.gridX(TouchInput.x);
            this.y = this.gridY(TouchInput.y);
        }
    };

    Sprite_DoodadCursor.prototype.gridX = function(value) {
        if (DoodadManager._gridLockMode && DoodadManager.current()) {
            const gridWidth = Math.floor(($gameMap.width() * $gameMap.tileWidth()) / DoodadManager.gridLockX());
            const gridTileWidth = DoodadManager.gridLockX();
            const originX = $gameMap._displayX * $gameMap.tileWidth();
            let gridX = Math.floor((originX + value) / gridTileWidth);
            gridX = $gameMap.isLoopHorizontal() ? gridX.mod(gridWidth) : gridX;
            value = ((gridX + 0.5) * gridTileWidth) - originX;
        }
        return value;
    };

    Sprite_DoodadCursor.prototype.gridY = function(value) {
        if (DoodadManager._gridLockMode && DoodadManager.current()) {
            const gridHeight = Math.floor(($gameMap.height() * $gameMap.tileHeight()) / DoodadManager.gridLockY());
            const gridTileHeight = DoodadManager.gridLockY();
            const originY = $gameMap._displayY * $gameMap.tileHeight();
            let gridY = Math.floor((originY + value) / gridTileHeight);
            gridY = $gameMap.isLoopHorizontal() ? gridY.mod(gridHeight) : gridY;
            value = ((gridY + 1.0) * gridTileHeight) - originY;
        }
        return value;
    };

    Spriteset_Map.prototype.currentDoodadX = function() {
        let value = this._doodadCursor.x;
        const tileWidth = $gameMap.tileWidth();
        value += $gameMap._displayX * tileWidth;
        if ($gameMap.isLoopHorizontal()) {
            value = value % ($gameMap.width() * tileWidth);
        }
        return Math.floor(value);
    };

    Spriteset_Map.prototype.currentDoodadY = function() {
        let value = this._doodadCursor.y;
        const tileHeight = $gameMap.tileHeight();
        value += $gameMap._displayY * $gameMap.tileHeight();
        if ($gameMap.isLoopVertical()) {
            value = value % ($gameMap.height() * tileHeight);
        }
        return Math.floor(value);
    };

    Spriteset_Map.prototype.doodadDragX = function() {
        let value;
        if (DoodadManager._gridLockMode) {
            const offset = $gameMap._displayX * $gameMap.tileWidth();
            value = this._doodadCursor.gridX(TouchInput.x) + offset;
        } else {
            value = $gameMap.canvasToMapPX(TouchInput.x);
        }
        return Math.floor(value);
    };

    Spriteset_Map.prototype.doodadDragY = function() {
        let value;
        if (DoodadManager._gridLockMode) {
            const offset = $gameMap._displayY * $gameMap.tileHeight();
            value = this._doodadCursor.gridY(TouchInput.y) + offset;
        } else {
            value = $gameMap.canvasToMapPY(TouchInput.y);
        }
        return Math.floor(value);
    };

    Scene_Map.prototype.createGFDWindows = function() {
    };
//=============================================================================
}; // Play Test Only
