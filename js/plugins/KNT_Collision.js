// =============================================================================
// KNT_Collision
// Version:     1.0
// Date:        1/2/2020
// Author:      Kaelan
// =============================================================================

//=============================================================================
/*:
 * @plugindesc Enables pixel collision and pathfinding
 * @version 1.0.0
 * @author Kaelan
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * By default, Pathfinding is done using the Anya16 algorithm. Grid dilation is
 * used to adjust the pathfinding grid size to the size of player characters.
 *
 * This works, so long as all characters pathfinding are relatively close in
 * size. To pathfind with larger characters, you'll need to create and maintain
 * a separate grid.
 *
 * The grid is subdivided for extra precision - each tile is turned into a set
 * of (Knight.COLLISION.GridSubdivisions) by (Knight.COLLISION.GridSubdivisions)
 * tiles. By default, this is 3. So a 100x100 map would have a 300x300 pathfinding
 * grid.
 *
 * Increasing the subdivision value will produce a more precise grid, but will
 * also increase memory usage and loading times. Increasing the subdivisions by
 * 2 roughly doubles map loading time.
 *
 * When changing grid subdivision, you may also want ot adjust the grid dilation
 * Kernel.
 */

var Imported = Imported || {}; // eslint-disable-line no-var
Imported.KNT_Collision = '1.0.0';

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.COLLISION = Knight.COLLISION || {};

//=============================================================================
// Globals
//=============================================================================
$gameCollisions = new Map([
    ['collision', new Sinova.Collisions()],
    ['interaction', new Sinova.Collisions()],
    ['overlap', new Sinova.Collisions()],
]);
$gameResult = $gameCollisions.get('collision').createResult();

Knight.COLLISION.GridSubdivisions = 3;

//=============================================================================
// Utility Functions
//=============================================================================
Knight.COLLISION.tileSizeX = function() {
    return $gameMap.tileWidth() / Knight.COLLISION.GridSubdivisions;
};

Knight.COLLISION.tileSizeY = function() {
    return $gameMap.tileHeight() / Knight.COLLISION.GridSubdivisions;
};

Knight.COLLISION.gridWidth = function() {
    return $gameMap.width() * Knight.COLLISION.GridSubdivisions;
};

Knight.COLLISION.gridHeight = function() {
    return $gameMap.height() * Knight.COLLISION.GridSubdivisions;
};

Knight.COLLISION.gridPixelWidth = function() {
    return Knight.COLLISION.gridWidth() * Knight.COLLISION.tileSizeX();
};

Knight.COLLISION.gridPixelHeight = function() {
    return Knight.COLLISION.gridHeight() * Knight.COLLISION.tileSizeY();
};

Knight.COLLISION.pointToGridTile = function(px, py) {
    return {
        x: Math.floor(px / Knight.COLLISION.tileSizeX()),
        y: Math.floor(py / Knight.COLLISION.tileSizeY()),
    };
};

// Returns the point in the center-bottom of the tile, as that is what
// MV normally anchors characters to
Knight.COLLISION.gridTileToPoint = function(tx, ty) {
    return {
        x: (tx + 0.5) * Knight.COLLISION.tileSizeX(),
        y: (ty + 1.0) * Knight.COLLISION.tileSizeY() - 6, // shiftY
    };
};

// Returns the pixel coordinates of the top-left of the tile
Knight.COLLISION.gridTileToPixelTile = function(tx, ty) {
    return {
        x: tx * Knight.COLLISION.tileSizeX(),
        y: ty * Knight.COLLISION.tileSizeY(),
    };
};

//=============================================================================
// Game_System
//=============================================================================
Knight.COLLISION.Game_System_onBeforeSave = Game_System.prototype.onBeforeSave;
Game_System.prototype.onBeforeSave = function() {
    Knight.COLLISION.Game_System_onBeforeSave.call(this);
    // Remove data that can't be saved, like colliders and debug info
};

Knight.COLLISION.Game_System_onAfterLoad = Game_System.prototype.onAfterLoad;
Game_System.prototype.onAfterLoad = function() {
    Knight.COLLISION.Game_System_onAfterLoad.call(this);
    // Reload collision/path data that was removed
};

//=============================================================================
// Game_CharacterBase
//=============================================================================
Knight.COLLISION.Game_CharacterBase_initMembers = Game_CharacterBase.prototype.initMembers;
Game_CharacterBase.prototype.initMembers = function() {
    Knight.COLLISION.Game_CharacterBase_initMembers.call(this);
    this._colliders = {};
    this._collidingWith = new Map();
    this.clearDebugData();
};

Game_CharacterBase.prototype.clearDebugData = function() {
    this._lastOccupiedTiles = [];
    this._lastOccupiedCollisionTiles = [];
};

Game_CharacterBase.prototype.loadCollisionData = function() {
    // By default, characters have 2 collision volumes:
    // - 1. a 'collision' volume
    // - 2. an 'interaction' volume
    // The collision volume is used for normal collision detection and to trigger
    // events.
    // The interaction volume is used for selecting the character for interaction by
    // hovering over them with the mouse.
    this.clearDebugData();
    this.createColliders();
    this.updateColliderPositions();
    this.addToPathGrid(false);
    this.createDebugTiles();
    this._collisionInteraction = null;
};

Game_CharacterBase.prototype.createColliders = function() {
    this._colliders = {};
    this.createColliderCollision();
    this.createColliderInteraction();
    this.createColliderOverlap();
};

Game_CharacterBase.prototype.createColliderCollision = function() {
    const tw = $gameMap.tileWidth();
    const th = $gameMap.tileHeight();
    const radius = 22;
    const collision = $gameCollisions.get('collision').createCircle(0, 0, radius, 1, 0, '0xff0000');
    collision.parent = this;
    collision.anchor = new PIXI.Point(tw / 2, th / 2);
    this._colliders['collision'] = collision;
};

Game_CharacterBase.prototype.createColliderInteraction = function() {
    const tw = $gameMap.tileWidth();
    const th = $gameMap.tileHeight();
    const interaction = $gameCollisions.get('interaction').createRectangle(0, 0, tw, th * 2, 0, 1, 1, 0, '0xffd800');
    interaction.parent = this;
    interaction.anchor = new PIXI.Point(0, -th);
    this._colliders['interaction'] = interaction;
};

Game_CharacterBase.prototype.createColliderOverlap = function() {
    const tw = $gameMap.tileWidth();
    const th = $gameMap.tileHeight();
    const overlap = $gameCollisions.get('overlap').createRectangle(0, 0, tw, th * 2, 0, 1, 1, 0, '0xcccccc');
    overlap.parent = this;
    overlap.anchor = new PIXI.Point(0, -th);
    this._colliders['overlap'] = overlap;
};

Game_CharacterBase.prototype.updateColliderPositions = function() {
    Object.values(this._colliders).forEach(function(collider) {
        collider.x = this._realPX + collider.anchor.x;
        collider.y = this._realPY + collider.anchor.y;
    }, this);
};

Game_CharacterBase.prototype.hasCollider = function(type) {
    return (type) ? (this._colliders && this._colliders.hasOwnProperty(type)) : this._colliders;
};

Game_CharacterBase.prototype.getCollider = function(type) {
    return this._colliders[type];
};

Game_CharacterBase.prototype.canCollideWith = function(character) {
    if (!(character instanceof Game_CharacterBase)) return true; // non-character objects (i.e. walls)
    if (character.isHidden() || character.isThrough() || character.isDebugThrough()) {
        return false;
    }
    return true;
};

Game_CharacterBase.prototype.isHidden = function() {
    let hidden = this.isTransparent() || this._erased;
    if (this.isVisible) hidden = hidden || !this.isVisible();
    return hidden;
};

Game_CharacterBase.prototype.canBeBlockedBy = function(body) {
    // Ignore collisions between characters of different priority (Below, Normal or Above Player)
    return !body.parent || this._priorityType === body.parent._priorityType;
};

Game_CharacterBase.prototype.canBlockPathfinding = function() {
    return this.isNormalPriority() && this.hasCollider('collision') && Game_CharacterBase.prototype.canCollideWith.call($gamePlayer, this);
};

Game_CharacterBase.prototype.shouldCheckCollision = function() {
    if (!this.isMoving() || // Only need to test collision when moving
        this.isHidden() || this.isThrough() || this.isDebugThrough() ||
        !this.hasCollider('collision')
    ) {
        return false;
    }
    return true;
};

Game_CharacterBase.prototype.collisionTest = function() {
    this._collisionInteraction = null;
    this.checkCollisions();
    if (this._collisionInteraction && !$gameMap.isEventRunning()) {
        this.triggerCollisionEvent();
    }
};

Game_CharacterBase.prototype.overlapTest = function() {
    if (this.hasCollider('overlap')) {
        const collider = this.getCollider('overlap');
        const potentials = collider.potentials();
        for (const body of potentials) {
            const doodad = body.parent;
            if (doodad instanceof Sprite_Doodad && doodad.hideOnOverlap() &&
                collider.y < body.y && collider.collides(body)) {
                doodad.setOverlap(true);
            }
        }
    }
};

Game_CharacterBase.prototype.triggerCollisionEvent = function() {
    console.log("Event Trigger!");
    this._collisionInteraction.getEvent().start();
    this._collisionInteraction = null;
};

Game_CharacterBase.prototype.checkCollisions = function(forced = false) {
    if (forced || this.shouldCheckCollision()) {
        // Broad Phase: only check possible collisions
        const collider = this.getCollider('collision');
        const potentials = collider.potentials();
        const collidedWith = [];
        const hasPath = this.hasPath();
        for (const body of potentials) {
            // Narrow Phase: evaluate individual collisions
            if (this.canCollideWith(body.parent) && collider.collides(body, $gameResult)) {
                this.resolveCollision(body);
                if (hasPath) collidedWith.push(body);
            }
        }
        this.adjustPathOnCollision(collidedWith);
    }
};

Game_CharacterBase.prototype.resolveCollision = function(body) {
    if (this.canBeBlockedBy(body)) {
        // Push the character out by the overlap distance
        const px = Math.round( this._realPX - ($gameResult.overlap * $gameResult.overlap_x) );
        const py = Math.round( this._realPY - ($gameResult.overlap * $gameResult.overlap_y) );
        this.setPixelPosition(px, py);
    }
    this.resolveInteraction(body);
};

Game_CharacterBase.prototype.resolveInteraction = function(body) {
};

Game_CharacterBase.prototype.adjustPathOnCollision = function(collidedWith) {
    // If we collide with the same object too many times in a row, retry the path.
    if (this.hasPath()) {
        // Remove objs we didn't collide with this frame from the list
        for (const obj of this._collidingWith.keys()) {
            if (!collidedWith.includes(obj)) {
                this._collidingWith.delete(obj);
            }
        }

        // Increment counter for objs we did collide with
        const max_count = 20;
        for (const body of collidedWith) {
            if (this._collidingWith.has(body)) {
                counter = this._collidingWith.get(body);
                counter.count++;
                if (counter.count > max_count) {
                    this.retryPath(body.parent);
                    this._collidingWith.clear();
                    break;
                }
            } else {
                this._collidingWith.set(body, {count: 1});
            }
        }
    }
};

Game_CharacterBase.prototype.canPass = function(x, y, d) {
    return true;
};

Game_CharacterBase.prototype.canPassDiagonally = function(x, y, horz, vert) {
    return true;
};

Game_CharacterBase.prototype.isMapPassable = function(x, y, d) {
    return true;
};

Game_CharacterBase.prototype.isCollidedWithCharacters = function(x, y) {
    return false;
};

Game_CharacterBase.prototype.isCollidedWithEvents = function(x, y) {
    return false;
};

Game_CharacterBase.prototype.removeFromPathGrid = function() {
    const pathGrid = $gameMap.pathGrid();
    const collisionGrid =$gameMap.collisionGrid();
    this._lastOccupiedTiles.forEach(function(t) {
        pathGrid.setBlocked(t.x, t.y, false);
    });
    this._lastOccupiedTiles = [];

    this._lastOccupiedCollisionTiles.forEach(function(t) {
        collisionGrid.setBlocked(t.x, t.y, false);
    });
    this._lastOccupiedCollisionTiles = [];
};

Game_CharacterBase.prototype.addToPathGrid = function(refreshTiles = true) {
    if (!this.canBlockPathfinding()) return;

    const bounds = this.getCollider('collision').getBounds();
    const minTile = Knight.COLLISION.pointToGridTile(bounds.min_x, bounds.min_y);
    const maxTile = Knight.COLLISION.pointToGridTile(bounds.max_x, bounds.max_y);
    const pathGrid = $gameMap.pathGrid();
    const collisionGrid = $gameMap.collisionGrid();

    for (let y = minTile.y; y <= maxTile.y; ++y) {
        for (let x = minTile.x; x <= maxTile.x; ++x) {
            collisionGrid.setBlocked(x, y, true);
            this._lastOccupiedCollisionTiles.push({x: x, y: y});
        }
    }

    // Dilate the character collider. Increasing by 1 tile in every direction is
    // equivalent to dilating by a 3x3 Kernel when the grid is a uniform square box.
    // Followers are left un-dilated so we can move closer to them. If we bump into
    // them, we'll push them out of the way.
    if (!(this instanceof Game_Follower)) {
        minTile.x--; minTile.y--; maxTile.x++; maxTile.y++;
    }

    for (let y = minTile.y; y <= maxTile.y; ++y) {
        for (let x = minTile.x; x <= maxTile.x; ++x) {
            pathGrid.setBlocked(x, y, true);
            this._lastOccupiedTiles.push({x: x, y: y});
        }
    }
    if (refreshTiles) this.refreshDebugTiles();
};

Game_CharacterBase.prototype.refreshDebugTiles = function() {
    if (this._debugTiles) {
        if (this._lastOccupiedTiles.length > this._debugTiles.children.length) {
            console.log(`ERROR: Have ${this._debugTiles.children.length}, need ${this._lastOccupiedTiles.length}!`);
        }
        for (let i = 0; i < this._debugTiles.children.length; ++i) {
            const sprite = this._debugTiles.children[i];
            if (i < this._lastOccupiedTiles.length) {
                const pos = Knight.COLLISION.gridTileToPixelTile(this._lastOccupiedTiles[i].x, this._lastOccupiedTiles[i].y);
                sprite.ox = pos.x;
                sprite.oy = pos.y;
            } else {
                sprite.ox = -1;
                sprite.oy = -1;
            }
        }
    }
};

Game_CharacterBase.prototype.createDebugTiles = function() {
    if (Imported.KNT_CollisionViewer && this.isNormalPriority()) {
        if (this._debugTiles) {
            this._debugTiles.destroy(true);
        }
        this._debugTiles = new PIXI.Container();

        const tile = new PIXI.Graphics();
        tile.lineStyle(1, '0xff0000', 0.25);
        tile.beginFill('0xff0000', 0.1);
        tile.drawRect(0, 0, Knight.COLLISION.tileSizeX(), Knight.COLLISION.tileSizeY());
        tile.endFill();
        tile.position.set(0, 0);

        const maxDebugTiles = Math.pow(Knight.COLLISION.GridSubdivisions * 2, 2);
        const texture = Graphics._renderer.generateTexture(tile);
        for (let i = 0; i < maxDebugTiles; ++i) {
            const sprite = new PIXI.Sprite(texture);
            sprite.ox = 0;
            sprite.oy = 0;
            this._debugTiles.addChild(sprite);
        }
        tile.destroy(true);

        SceneManager._scene.debugContainer().addChild(this._debugTiles);
        this.refreshDebugTiles();
    }
};

Game_CharacterBase.prototype.updateDebugTiles = function() {
    if (this._debugTiles) {
        const tw = $gameMap.tileWidth();
        const th = $gameMap.tileHeight();
        for (sprite of this._debugTiles.children) {
            const valid = sprite.ox >= 0;
            if (valid) {
                sprite.visible = !this.isHidden() && Knight.COLLISION.debugDraw;
                const screenX = Math.round(sprite.ox - ($gameMap._displayX * tw));
                const screenY = Math.round(sprite.oy - ($gameMap._displayY * th));
                sprite.position.set(screenX, screenY);
            } else {
                sprite.visible = false;
            }
        }
    }
};

//=============================================================================
// Game_Player
//=============================================================================
Game_Player.prototype.canCollideWith = function(character) {
    const isPlayer = (character instanceof Game_Follower) || (character instanceof Game_Player);
    if (isPlayer && (this.hasPath() || character.hasPath())) return false;
    return Game_CharacterBase.prototype.canCollideWith.call(this, character);
};

Game_Player.prototype.resolveCollision = function(body) {
    if (this.canBeBlockedBy(body)) {
        const other = body.parent;
        const canPush = other && other instanceof Game_Follower &&
                        (!other.startedMoving()) &&
                        (this.selectedParty().length === 1);
        if (canPush) {
            // Push static followers that are blocking the way
            const px = Math.round( other._realPX + ($gameResult.overlap * $gameResult.overlap_x) );
            const py = Math.round( other._realPY + ($gameResult.overlap * $gameResult.overlap_y) );
            other.setPixelPosition(px, py);
            other.removeFromPathGrid();
            other.addToPathGrid();
            other.checkCollisions(true);
        } else {
            // Push self away from objects
            const px = Math.round( this._realPX - ($gameResult.overlap * $gameResult.overlap_x) );
            const py = Math.round( this._realPY - ($gameResult.overlap * $gameResult.overlap_y) );
            this.setPixelPosition(px, py);
        }
    }
    this.resolveInteraction(body);
};

Game_Player.prototype.checkCollisions = function() {
    if (this.isTransferring()) return;
    Game_CharacterBase.prototype.checkCollisions.call(this);
};

Game_Player.prototype.resolveInteraction = function(body) {
    Game_CharacterBase.prototype.resolveInteraction.call(this, body);
    const parent = body.parent;
    const event = (parent && parent.getEvent) ? parent.getEvent() : null;
    if (event && event instanceof Game_Event && !this._collisionInteraction) {
        const thisIsMoving = this.startedMoving();
        const eventIsPathDestination = this.isDestinationEvent(parent);
        // (_trigger: 2) -> event touches player
        // (_trigger: 1) -> player touches event
        // (_trigger: 0) -> action button
        if ((event._trigger === 2) ||
            (event._trigger === 1 && thisIsMoving) ||
            (event._trigger === 0 && eventIsPathDestination)) {
            this._collisionInteraction = parent;
            if (this.hasPath() && eventIsPathDestination) this.clearPath();
        }
    }
};

Game_Player.prototype.triggerCollisionEvent = function() {
    const event = this._collisionInteraction;
    Game_CharacterBase.prototype.triggerCollisionEvent.call(this);
    // Clear destination events on all characters to prevent triggering
    // the same event multiple times
    this.visibleParty().forEach(function(member) {
        if (member.isDestinationEvent(event)) {
            member.setDestinationEvent(null);
        }
        if (member._collisionInteraction === event) {
            member._collisionInteraction = null;
        }
    }, this);
};

Game_Player.prototype.actor = function() {
    return $gameParty.leader();
};

//=============================================================================
// Game_Follower
//=============================================================================
Knight.COLLISION.Game_Follower_initialize = Game_Follower.prototype.initialize;
Game_Follower.prototype.initialize = function(memberIndex) {
    Knight.COLLISION.Game_Follower_initialize.call(this, memberIndex);
    this.setThrough(false); // turn this off so we can collide with followers
};

Game_Follower.prototype.canCollideWith = function(character) {
    const isPlayer = (character instanceof Game_Follower) || (character instanceof Game_Player);
    if (isPlayer && (this.hasPath() || character.hasPath())) return false;
    return Game_CharacterBase.prototype.canCollideWith.call(this, character);
};

Game_Follower.prototype.resolveInteraction = function(body) {
    Game_CharacterBase.prototype.resolveInteraction.call(this, body);
    const parent = body.parent;
    const event = (parent && parent.getEvent) ? parent.getEvent() : null;
    if (event && event instanceof Game_Event && !this._collisionInteraction) {
        const thisIsMoving = this.startedMoving();
        const eventIsPathDestination = this.isDestinationEvent(parent);
        // (_trigger: 2) -> event touches player
        // (_trigger: 1) -> player touches event
        // (_trigger: 0) -> action button
        if ((event._trigger === 2) ||
            (event._trigger === 1 && thisIsMoving) ||
            (event._trigger === 0 && eventIsPathDestination)) {
            this._collisionInteraction = parent;
            if (this.hasPath() && eventIsPathDestination) this.clearPath();
        }
    }
};

Game_Follower.prototype.triggerCollisionEvent = function() {
    const event = this._collisionInteraction;
    Game_CharacterBase.prototype.triggerCollisionEvent.call(this);
    // Clear destination events on all characters to prevent triggering
    // the same event multiple times
    $gamePlayer.visibleParty().forEach(function(member) {
        if (member.isDestinationEvent(event)) {
            member.setDestinationEvent(null);
        }
        if (member._collisionInteraction === event) {
            member._collisionInteraction = null;
        }
    }, this);
};

//=============================================================================
// Game_Vehicle
//=============================================================================
Game_Vehicle.prototype.loadCollisionData = function() {
    // We don't use vehicles, so leave this blank
    // If adding these, be careful not to put them on the path grid when
    // they're not visible.
};

//=============================================================================
// Game_Event
//=============================================================================
Knight.COLLISION.Game_Event_initMembers = Game_Event.prototype.initMembers;
Game_Event.prototype.initMembers = function() {
    Knight.COLLISION.Game_Event_initMembers.call(this);
    this._hasDoodad = false;
};

Game_Event.prototype.hasDoodad = function() {
    return this._hasDoodad;
};

Game_Event.prototype.loadCollisionData = function(hasDoodad = false) {
    this._hasDoodad = hasDoodad;
    if (!hasDoodad) {
        Game_Character.prototype.loadCollisionData.call(this);
    }
};

Game_Event.prototype.getEvent = function() {
    return this;
};

//=============================================================================
// Scene_Map
//=============================================================================
Knight.COLLISION.Scene_Map_initialize = Scene_Map.prototype.initialize;
Scene_Map.prototype.initialize = function() {
    Knight.COLLISION.Scene_Map_initialize.call(this);
    this._selectingUnits = false;
    this._selectionPoint = null;
    this._selectionCount = 0;
};

Knight.COLLISION.Scene_Map_update = Scene_Map.prototype.update;
Scene_Map.prototype.update = function() {
    this.updateCharacterSelection();
    Knight.COLLISION.Scene_Map_update.call(this);
    this.overlapUpdate();
};

Knight.COLLISION.Scene_Map_updateMain = Scene_Map.prototype.updateMain;
Scene_Map.prototype.updateMain = function() {
    Knight.COLLISION.Scene_Map_updateMain.call(this);
    this.collisionUpdate();
};

Scene_Map.prototype.collisionUpdate = function() {
    // Update collision systems
    $gameCollisions.forEach((collisions) => collisions.update());

    // Check for collisions
    $gameMap.getAllCharacters().forEach((character) => character.collisionTest());
};

Scene_Map.prototype.overlapUpdate = function() {
    $gamePlayer.party().forEach((character) => character.overlapTest());
};

Knight.COLLISION.Scene_Map_onMapLoaded = Scene_Map.prototype.onMapLoaded;
Scene_Map.prototype.onMapLoaded = function() {
    Knight.COLLISION.Scene_Map_onMapLoaded.call(this);
    if (this._transfer) {
        $gamePlayer.placeFollowers();
        $gamePlayer.party().forEach((character) => character.clearPath());
    }
};

Knight.COLLISION.Scene_Map_createDisplayObjects = Scene_Map.prototype.createDisplayObjects;
Scene_Map.prototype.createDisplayObjects = function() {
    Knight.COLLISION.Scene_Map_createDisplayObjects.call(this);
    $gameMap.loadCollisionData();
};

Scene_Map.prototype.updateCharacterSelection = function() {
    if (this.isSelecting()) {
        this._selectionCount++;
        if (TouchInput.isReleased()) {
            if (this._selectionCount < 8) {
                this.cancelSelection();
            } else {
                TouchInput.update();
                this.endSelection();
            }
        }
    } else if (TouchInput.isPressed()) {
        TouchInput.update();
        this.startSelection();
    }
};

Scene_Map.prototype.isSelecting = function() {
    return this._selectingUnits;
};

Scene_Map.prototype.startSelection = function() {
    this._selectingUnits = true;
    this._selectionPoint = {
        x: $gameMap.canvasToMapPX(TouchInput.x),
        y: $gameMap.canvasToMapPY(TouchInput.y),
    };
    this._selectionCount = 0;
};

Scene_Map.prototype.cancelSelection = function() {
    this._selectingUnits = false;
    this._selectionPoint = null;
    this._selectionCount = 0;
};

Scene_Map.prototype.endSelection = function() {
    const bounds = this.getSelectionPoints();
    const aabb = new Sinova.Rectangle(bounds.min.x, bounds.min.y,
        bounds.max.x - bounds.min.x,
        bounds.max.y - bounds.min.y);
    const selected = [];
    const system = $gameCollisions.get('collision');

    for (character of $gamePlayer.visibleParty()) {
        const body = character.getCollider('collision');
        if (system.collides(body, aabb)) {
            character.select();
            selected.push(character);
            console.log(`Selected ${character.actor().name()}`);
        } else {
            character.deselect();
        }
    }

    if (selected.length === 1) $gameParty.setLeader(selected[0]);
    this._selectingUnits = false;
    this._selectionPoint = null;
    aabb.clear();
};

Scene_Map.prototype.getSelectionPoints = function() {
    const maxPoint = {
        x: this._selectionPoint.x,
        y: this._selectionPoint.y,
    };
    const minPoint = {
        x: $gameMap.canvasToMapPX(TouchInput.x),
        y: $gameMap.canvasToMapPY(TouchInput.y),
    };
    if (minPoint.x > maxPoint.x) Knight.Utility.swapValue(minPoint, maxPoint, "x");
    if (minPoint.y > maxPoint.y) Knight.Utility.swapValue(minPoint, maxPoint, "y");
    return {min: minPoint, max: maxPoint};
};

Knight.COLLISION.Scene_Map_terminate = Scene_Map.prototype.terminate;
Scene_Map.prototype.terminate = function() {
    Knight.COLLISION.Scene_Map_terminate.call(this);
    $gameMap.removeCharactersFromGrid();
    this._spriteset.removeDoodadsFromGrid();
    this._spriteset.free();
    Knight.Utility.cleanUpPreallocatedMemory();
};

//=============================================================================
// Sprite_SelectionBox
//=============================================================================
/**
 * @class Sprite_SelectionBox
 * @extends {Sprite}
 */
class Sprite_SelectionBox extends PIXI.Sprite {
    /**
     * Creates an instance of Sprite_SelectionBox.
     * @memberof Sprite_SelectionBox
     */
    constructor() {
        super();
        this._box = new PIXI.Graphics();
        this._box.position.set(0, 0);
        this.addChild(this._box);
    }

    /**
     * @memberof Sprite_SelectionBox
     */
    update() {
        this.visible = SceneManager._scene.isSelecting();
        if (this.visible) {
            const bounds = SceneManager._scene.getSelectionPoints();
            this._box.clear();
            this._box.lineStyle(1, '0x00ff00', 1);
            this._box.beginFill('0x000000', 0);
            this._box.drawRect(bounds.min.x, bounds.min.y,
                bounds.max.x - bounds.min.x,
                bounds.max.y - bounds.min.y);
            this._box.endFill();
            const screenX = Math.round(0 - ($gameMap._displayX * $gameMap.tileWidth()));
            const screenY = Math.round(0 - ($gameMap._displayY * $gameMap.tileHeight()));
            this._box.position.set(screenX, screenY);
        }
    }

    /**
     * @memberof Sprite_SelectionBox
     */
    free() {
        this.removeChild(this._box);
        this._box.destroy(true);
    }
};

//=============================================================================
// Spriteset_Map
//=============================================================================
Knight.COLLISION.Spriteset_Map_createUpperLayer = Spriteset_Map.prototype.createUpperLayer;
Spriteset_Map.prototype.createUpperLayer = function() {
    Knight.COLLISION.Spriteset_Map_createUpperLayer.call(this);
    this._selectionBox = new Sprite_SelectionBox();
    this.addChild(this._selectionBox);
};

//=============================================================================
// Game_Map
//=============================================================================
Knight.COLLISION.Game_Map_setup = Game_Map.prototype.setup;
Game_Map.prototype.setup = function(mapId) {
    Knight.COLLISION.Game_Map_setup.call(this, mapId);
    this._needsCollisionReload = true;
    this._colliders = [];
};

Knight.COLLISION.Game_Map_update = Game_Map.prototype.update;
Game_Map.prototype.update = function(sceneActive) {
    Knight.COLLISION.Game_Map_update.call(this, sceneActive);
    this.updatePathGrid();
    this.updateMouseCollider();
};

Game_Map.prototype.getAllCharacters = function() {
    const characters = this.events().concat(this.vehicles()).concat($gamePlayer.party());
    return characters;
};

Game_Map.prototype.loadCollisionData = function() {
    if (this._needsCollisionReload) {
        console.time('Collision Load Time');
        this.clearCollisionData();
        console.time('loadMapCollision');
        this.loadPathGrid();
        this.loadMapCollision();
        this.loadPathDebugInfo();
        console.timeEnd('loadMapCollision');
        console.time('loadCharacterCollision');
        this.loadCharacterCollision();
        console.timeEnd('loadCharacterCollision');
        this._needsCollisionReload = false;
        console.timeEnd('Collision Load Time');
    } else {
        if (Imported.KNT_CollisionViewer) this.updateCollisionViewer();
    }
};

// Called when the map doesn't need to reload. Re-add display objects to the scene.
Game_Map.prototype.updateCollisionViewer = function() {
    for (const system of $gameCollisions.values()) {
        system.updateScene();
    }
    if (Imported.KNT_CollisionViewer) {
        container = SceneManager._scene.debugContainer();
        container.addChild(this._debugGrid);
        container.addChild(this._debugTiles);
        this.getAllCharacters().forEach(function(character) {
            if (character._debugTiles) {
                container.addChild(character._debugTiles);
            }
        });
    }
};

Game_Map.prototype.clearCollisionData = function() {
    for (const system of $gameCollisions.values()) {
        system.clear();
    }
    if (Imported.KNT_CollisionViewer) {
        if (this._debugGrid) {
            this._debugGrid.clear();
        } else {
            this._debugGrid = new PIXI.Graphics();
        }
        if (this._debugTiles) {
            this._debugTiles.destroy(true);
        }
        this._debugTiles = new PIXI.Container();
        this._debugBodyTiles = [];
        $gameCollisions.forEach((collisions) => collisions.onMapLoad());
    }
};

Game_Map.prototype.loadMapCollision = function() {
    ColliderManager.colliders().forEach(function(colliderData) {
        this.addCollider(colliderData, true);
    }, this);
};

Game_Map.prototype.addCollider = function(colliderData, isCached) {
    const collider = new Knight.EDITOR.Map_Collider(colliderData, isCached);
    this._colliders.push(collider);
};

Game_Map.prototype.removeCollider = function(colliderData) {
    const index = this._colliders.findIndex((collider) => collider._data === colliderData);
    if (index >= 0) {
        this._colliders[index].clearCollisionData();
        this._colliders.splice(index, 1);
    }
};

Game_Map.prototype.getCollider = function(colliderData) {
    return this._colliders.find((collider) => collider._data === colliderData);
};

Game_Map.prototype.loadCharacterCollision = function() {
    const doodadEvents = new Set();
    DoodadManager.doodads().forEach(function(doodad) {
        if (doodad.eventId) doodadEvents.add(doodad.eventId);
    });
    this.getAllCharacters().forEach(function(character) {
        const isEvent = character instanceof Game_Event;
        if (isEvent) {
            character.loadCollisionData(doodadEvents.has(character.eventId()));
        } else {
            character.loadCollisionData();
        }
    });
    this.loadMouseCollision();
};

Game_Map.prototype.loadMouseCollision = function() {
    this._mouseCollider = $gameCollisions.get('interaction').createPoint(0, 0, 0, '0xffd800');
};

Game_Map.prototype.updateMouseCollider = function() {
    this._mouseCollider.x = this.canvasToMapPX(TouchInput.x);
    this._mouseCollider.y = this.canvasToMapPY(TouchInput.y);
};

Game_Map.prototype.getMouseCollision = function() {
    const potentials = this._mouseCollider.potentials();
    let collidedWith = null;
    let maxY = -1;
    for (const body of potentials) {
        if (this._mouseCollider.collides(body, $gameResult)) {
            if (body.parent && body.parent.y > maxY) {
                maxY = body.parent.y;
                collidedWith = body.parent;
            }
        }
    }
    return collidedWith;
};

Game_Map.prototype.loadPathGrid = function() {
    console.time("create Grid");
    const mapId = this.mapId();
    $dataPathfinding[mapId] = $dataPathfinding[mapId] || {};
    const grids = $dataPathfinding[mapId];
    if (!grids.pathGrid) {
        const w = Knight.COLLISION.gridWidth();
        const h = Knight.COLLISION.gridHeight();
        grids.pathGrid = new BitpackedGrid(w, h);
        grids.collisionGrid = new BitpackedGrid(w, h);
    }
    this._pathGrid = grids.pathGrid;
    this._collisionGrid = grids.collisionGrid;
    console.timeEnd("create Grid");
};

Game_Map.prototype.loadPathDebugInfo = function() {
    console.time("loadPathDebugInfo");
    this.createDebugGrid();
    console.timeEnd("loadPathDebugInfo");
};

Game_Map.prototype.createDebugGrid = function() {
    // TODO: Cache this to a texture for better peformance?
    if (Imported.KNT_CollisionViewer) {
        this._debugGrid.lineStyle(1, '0x000000', 0.1);
        this._debugGrid.beginFill('0x000000', 0);

        // Grid Tiles
        const tileW = Knight.COLLISION.tileSizeX();
        const tileH = Knight.COLLISION.tileSizeY();
        const xTiles = Knight.COLLISION.gridWidth();
        const yTiles = Knight.COLLISION.gridHeight();
        for (let y = 0; y < yTiles; ++y) {
            const ty = y * tileH;
            for (let x = 0; x < xTiles; ++x) {
                const tx = x * tileW;
                this._debugGrid.drawRect(tx, ty, tileW, tileH);
            }
        }
        this._debugGrid.endFill();
        this._debugGrid.position.set(0, 0);
        SceneManager._scene.debugContainer().addChild(this._debugGrid);
    }
};

Game_Map.prototype.updatePathGrid = function() {
    const tw = this.tileWidth();
    const th = this.tileHeight();
    if (this._debugGrid) {
        this._debugGrid.visible = Knight.COLLISION.debugDraw;
        const screenX = Math.round(0 - (this._displayX * tw));
        const screenY = Math.round(0 - (this._displayY * th));
        this._debugGrid.position.set(screenX, screenY);
    }
    this._colliders.forEach((collider) => collider.update());
};

//=============================================================================
// RunnablePath
//=============================================================================
Knight.COLLISION.RunnablePath = class {
    /**
     * Creates an instance of RunnablePath.
     * @param {Array<Array<Number>>} path   The path, as returned by Anya. Coordinates are in tiles.
     * @param {Number} startX               Pixel coordinates of the path start location
     * @param {Number} startY
     * @param {Number} destX                Pixel coordinates of the path destination
     * @param {Number} destY
     * @memberof RunnablePath
     */
    constructor(path, startX, startY, destX, destY) {
        this._path = path;
        this._destination = {x: destX, y: destY};

        // Discard the first node, if it snaps to our current location
        const start = Knight.COLLISION.pointToGridTile(startX, startY);
        if (this._path[0][0] === start.x && this._path[0][1] === start.y) {
            this._path.shift();
        }

        // Convert the locations on the pathfiding grid back to pixel coordinates
        // By default, this snaps to the middle-bottom pixel of each tile, so this
        // is a lossy conversion
        for (let i = 0; i < this._path.length; ++i) {
            this._path[i] = Knight.COLLISION.gridTileToPoint(this._path[i][0], this._path[i][1]);
        }

        // Replace last node with its exact location, to avoid having
        // weird looking paths as a result of the conversion above
        const lastIndex = this._path.length-1;
        const lastNode = this._path[lastIndex];
        if (lastNode.x !== destX || lastNode.y !== destY) {
            this._path[lastIndex].x = destX;
            this._path[lastIndex].y = destY;
        }
    }

    /**
     * Checks if the new pathfinding destination differs from the current one. Used
     * to avoid repeating redundant pathfinding checks.
     *
     * Snaps the destination to the grid, so pathfinding does not run again if the
     * destination only changed by a few pixels, as the path would be the same.
     *
     * @param {Number} px
     * @param {Number} py
     * @return {Boolean}
     */
    destinationChanged(px, py) {
        if (this._path.length === 0) return true;
        const curDestGrid = Knight.COLLISION.pointToGridTile(this._destination.x, this._destination.y);
        const newDestGrid = Knight.COLLISION.pointToGridTile(px, py);
        return !(curDestGrid.x === newDestGrid.x && curDestGrid.y === newDestGrid.y);
    }

    /**
     * @return {Object}
     */
    curr() {
        if (this.isEmpty()) return null;
        return this._path[0];
    }

    /**
     * @return {Object}
     */
    next() {
        this._path.shift();
        return this.curr();
    }

    /**
     * @return {Object}
     */
    destination() {
        return this._destination;
    }

    /**
     * @return {Boolean}
     */
    isEmpty() {
        return this._path.length === 0;
    }
};
