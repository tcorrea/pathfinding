var Sinova = Sinova || {}; // eslint-disable-line no-var

/**
 * A rectangle used to detect collisions
 * @class
 */
Sinova.Rectangle = class extends Sinova.Polygon {
    /**
     * @constructor
     * @param {Number} [x = 0] The starting X coordinate
     * @param {Number} [y = 0] The starting Y coordinate
     * @param {Number} [width = 1] The rectangle width
     * @param {Number} [height = 1] The rectangle height
     * @param {Number} [angle = 0] The starting rotation in radians
     * @param {Number} [scale_x = 1] The starting scale along the X axis
     * @param {Number} [scale_y = 1] The starting scale long the Y axis
     * @param {Number} [padding = 0] The amount to pad the bounding volume when testing for potential collisions
     */
    constructor(x = 0, y = 0, width = 1, height = 1, angle = 0, scale_x = 1, scale_y = 1, padding = 0) {
        super(x, y,
            [
                [0, 0],
                [width, 0],
                [width, height],
                [0, height],
            ],
            angle, scale_x, scale_y, padding);

        /** @private */
        this._rectangle = true;

        /** @private */
        this._width = width;

        /** @private */
        this._height = height;
    }

    /**
     * Recalculates the polygon's internal coordinates when the rectangle's dimensions change.
     */
    updatePoints() {
        const points = this._points;
        points[0] = 0;
        points[1] = 0;

        points[2] = this._width;
        points[3] = 0;

        points[4] = this._width;
        points[5] = this._height;

        points[6] = 0;
        points[7] = this._height;
        this._dirty_coords = true;
    }
};

/**
 * @static
 * @property width
 * @type {Number}
 */
Object.defineProperty(Sinova.Rectangle.prototype, 'width', {
    set: function set(value) {
        this._width = value;
        this.updatePoints();
    },
    get: function get() {
        return this._width;
    },
});

/**
 * @static
 * @property height
 * @type {Number}
 */
Object.defineProperty(Sinova.Rectangle.prototype, 'height', {
    set: function set(value) {
        this._height = value;
        this.updatePoints();
    },
    get: function get() {
        return this._height;
    },
});
