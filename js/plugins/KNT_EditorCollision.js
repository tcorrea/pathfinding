// =============================================================================
// Knight Engine Plugins - Knight Editor Addons: Collision
// KNT_EditorCollision.js
// =============================================================================

//=============================================================================
/*:
 * @plugindesc Collision extensions for Knight Editor.
 * @version 1.0.0
 * @author Kaelan
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.00:
 * - First Version
 */
var Imported = Imported || {}; // eslint-disable-line no-var
Imported.KNT_EditorCollision = '1.0.0';

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.EDITOR = Knight.EDITOR || {};
Knight.INPUT = Knight.INPUT || {};
Knight.Editor = Knight.Editor || {};

//=============================================================================
// DataManager
//=============================================================================
var $dataColliders = null; // eslint-disable-line no-var
var $dataPathfinding = null; // eslint-disable-line no-var

if (!DataManager.isBattleTest() && !DataManager.isEventTest()) {
    DataManager._databaseFiles.push({name: '$dataColliders', src: 'Colliders.json'});

    // Pathfinding data needs to be loaded with type information
    Knight.EDITOR.DataManager_loadDatabase = DataManager.loadDatabase;
    DataManager.loadDatabase = function() {
        Knight.EDITOR.DataManager_loadDatabase.call(this);
        console.time("loadPathfindingData");
        $dataPathfinding = StorageManager.loadPathfindingData() || [];
        console.timeEnd("loadPathfindingData");
    };
};

//=============================================================================
// Utility Functions
//=============================================================================
if (!Imported.KNT_Movement) {
    Knight.degToRad = function(degrees) {
        return degrees * Math.PI / 180;
    };

    Knight.radToDeg = function(radians) {
        return radians * 180 / Math.PI;
    };
}

//=============================================================================
// Sprite Doodad
//=============================================================================
Knight.EDITOR.Sprite_Doodad_initialize = Sprite_Doodad.prototype.initialize;
Sprite_Doodad.prototype.initialize = function(data) {
    Knight.EDITOR.Sprite_Doodad_initialize.call(this, data);
    this._collisionLoaded = false;
    this._colliders = {};
    this._priorityType = 1;
    this._lastOccupiedTiles = [];
    this._lastOccupiedCollisionTiles = [];
    this._hideOnOverlap = false;
};

Knight.EDITOR.Sprite_Doodad_initData = Sprite_Doodad.prototype.initData;
Sprite_Doodad.prototype.initData = function() {
    this._hideOnOverlap = this._data.hideOnOverlap || false;
    Knight.EDITOR.Sprite_Doodad_initData.call(this);
};

Sprite_Doodad.prototype.setPriorityType = function(priorityType) {
    this._priorityType = priorityType;
};

Sprite_Doodad.prototype.isNormalPriority = function() {
    return this._priorityType === 1;
};

Sprite_Doodad.prototype.isHidden = function() {
    return !this.visible;
};

Sprite_Doodad.prototype.hasEvent = function() {
    return !!this._data.eventId;
};

Sprite_Doodad.prototype.getEvent = function() {
    return this.hasEvent() ? $gameMap.event(this._data.eventId) : null;
};

Sprite_Doodad.prototype.hideOnOverlap = function() {
    return this._hideOnOverlap;
};

Sprite_Doodad.prototype.setOverlap = function(overlap) {
    // Cut opacity in half when overlapping players
    if (this.hideOnOverlap()) {
        this.opacity = this._data.opacity * ((2 - (+overlap)) / 2);
    }
};

Knight.EDITOR.Sprite_Doodad_update = Sprite_Doodad.prototype.update;
Sprite_Doodad.prototype.update = function() {
    const prevX = this.x;
    const prevY = this.y;
    Knight.EDITOR.Sprite_Doodad_update.call(this);
    if (!this._collisionLoaded) return;
    this.updateCollider();
    if (prevX !== this.x || prevY !== this.y) {
        this.updateColliderPosition();
        this.removeFromPathGrid();
        this.addToPathGrid();
    }
    this.updateDebugTiles();
    this.setOverlap(false);
};

Sprite_Doodad.prototype.clearPathTiles = function() {
    this._lastOccupiedTiles = [];
    this._lastOccupiedCollisionTiles = [];
};

Sprite_Doodad.prototype.clearCollisionData = function() {
    this.removeFromPathGrid();
    for (const [key, collider] of Object.entries(this._colliders)) {
        collider.remove();
        collider.clear();
        delete this._colliders[key];
    }
    this.destroyDebugTiles();
};

Sprite_Doodad.prototype.loadCollisionData = function() {
    this.clearCollisionData();
    this.createColliders();
    this.addToPathGrid(false);
    this.createDebugTiles();
    this._collisionLoaded = true;
};

Sprite_Doodad.prototype.createColliders = function() {
    this.createColliderCollision();
    this.createColliderInteraction();
    this.createColliderOverlap();
    this.updateColliderPosition();
};

Sprite_Doodad.prototype.createColliderCollision = function() {
    if (this.hasColliderData()) {
        let collider;
        const system = $gameCollisions.get('collision');
        switch (this._data.collider.type) {
        case "Circle":
            collider = system.createCircle(0, 0, this._data.collider.radius, 1, 0, '0xff0000');
            break;
        case "Rectangle":
            const w = this._data.collider.width;
            const h = this._data.collider.height;
            const angle = Knight.degToRad(this._data.collider.angle);
            collider = system.createRectangle(0, 0, w, h, angle, 1, 1, 0, '0xff0000');
            break;
        }
        collider.anchor = new PIXI.Point(this._data.collider.anchor.x, this._data.collider.anchor.y);
        collider.parent = this;
        this._colliders['collision'] = collider;
        this._data.collider.dirty = undefined;
    }
};

Sprite_Doodad.prototype.createColliderInteraction = function() {
    if (this.hasEvent()) {
        const system = $gameCollisions.get('interaction');
        const width = this.width;
        const height = this.height;
        const angle = 0;
        const collider = system.createRectangle(0, 0, width, height, angle, 1, 1, 0, '0xffd800');
        collider.parent = this;
        collider.anchor = new PIXI.Point(-width/2, -(this.height/2 + height/2));
        this._colliders['interaction'] = collider;
    }
};

Sprite_Doodad.prototype.createColliderOverlap = function() {
    if (this.hideOnOverlap()) {
        const system = $gameCollisions.get('overlap');
        const width = this.width;
        const height = this.height;
        const angle = 0;
        const collider = system.createRectangle(0, 0, width, height, angle, 1, 1, 0, '0xcccccc');
        collider.parent = this;
        collider.anchor = new PIXI.Point(-width/2, -(this.height/2 + height/2));
        this._colliders['overlap'] = collider;
    }
};

Sprite_Doodad.prototype.updateColliderPosition = function() {
    Object.values(this._colliders).forEach(function(collider) {
        collider.x = this._data.x + collider.anchor.x;
        collider.y = this._data.y + collider.anchor.y;
    }, this);
};

Sprite_Doodad.prototype.hasColliderData = function() {
    return this._data && this._data.collider && this._data.collider.type;
};

Sprite_Doodad.prototype.hasCollider = function(type) {
    return (type) ? (this._colliders && this._colliders.hasOwnProperty(type)) : this._colliders;
};

Sprite_Doodad.prototype.getCollider = function(type) {
    return this._colliders[type];
};

Sprite_Doodad.prototype.isColliderDirty = function() {
    return this._data.collider ? this._data.collider.dirty : false;
};

Sprite_Doodad.prototype.addToPathGrid = function(refreshTiles = true) {
    if (!this.hasCollider('collision')) return;

    const bounds = this.getCollider('collision').getBounds();
    const minTile = Knight.COLLISION.pointToGridTile(bounds.min_x, bounds.min_y);
    const maxTile = Knight.COLLISION.pointToGridTile(bounds.max_x, bounds.max_y);
    const pathGrid = $gameMap.pathGrid();
    const collisionGrid = $gameMap.collisionGrid();

    for (let y = minTile.y; y <= maxTile.y; ++y) {
        for (let x = minTile.x; x <= maxTile.x; ++x) {
            collisionGrid.setBlocked(x, y, true);
            this._lastOccupiedCollisionTiles.push({x: x, y: y});
        }
    }

    // Dilate the doodad collider. Increasing by 1 tile in every direction is
    // equivalent to dilating by a 3x3 Kernel when the grid is a uniform square box.
    minTile.x--; minTile.y--; maxTile.x++; maxTile.y++;

    for (let y = minTile.y; y <= maxTile.y; ++y) {
        for (let x = minTile.x; x <= maxTile.x; ++x) {
            pathGrid.setBlocked(x, y, true);
            this._lastOccupiedTiles.push({x: x, y: y});
        }
    }
    if (refreshTiles) this.refreshDebugTiles();
};

Sprite_Doodad.prototype.removeFromPathGrid = function() {
    const pathGrid = $gameMap.pathGrid();
    const collisionGrid = $gameMap.collisionGrid();
    this._lastOccupiedTiles.forEach(function(t) {
        pathGrid.setBlocked(t.x, t.y, false);
    });
    this._lastOccupiedTiles = [];

    this._lastOccupiedCollisionTiles.forEach(function(t) {
        collisionGrid.setBlocked(t.x, t.y, false);
    });
    this._lastOccupiedCollisionTiles = [];
};

// Functions only used for debugging. No-op when Collision Viewer is turned off.
Sprite_Doodad.prototype.destroyDebugTiles = function() {
};

Sprite_Doodad.prototype.updateCollider = function() {
};

Sprite_Doodad.prototype.updateColliderDebugPosition = function() {
};

Sprite_Doodad.prototype.refreshDebugTiles = function() {
};

Sprite_Doodad.prototype.createDebugTiles = function() {
};

Sprite_Doodad.prototype.updateDebugTiles = function() {
};

//=============================================================================
// Map Collider
//=============================================================================
Knight.EDITOR.Map_Collider = class {
    /**
     * Creates an instance of Map_Collider.
     *
     * @param {Object} collisionData
     * @param {Boolean} isCached        When true, the collider will not write itself to the pathfinding grid
     *                                  on load. This is used when the pathfinding grid has been cached offline.
     */
    constructor(collisionData, isCached) {
        this._data = collisionData;
        this._collider = null;
        this._dirty = false;
        this._hasCachedTiles = isCached;
        this._lastOccupiedTiles = [];
        this._lastOccupiedCollisionTiles = [];
        this.loadCollisionData();
    }

    /**
     * @param {Boolean} dirty
     */
    setDirty(dirty) {
        this._dirty = dirty;
    }

    /**
     * @return {Boolean}
     */
    isDirty() {
        return this._dirty;
    }

    /**
     * @return {Boolean}
     */
    hasColliderData() {
        return !!this._data;
    };

    /**
     * @return {Boolean}
     */
    hasCollider() {
        return !!this._collider;
    };

    /**
     * @return {Sinova.Body}
     */
    getCollider() {
        return this._collider;
    };

    /**
     * @param {Boolean} destroyDebugTiles
     */
    clearPathTiles(destroyDebugTiles = true) {
        this._lastOccupiedTiles = [];
        this._lastOccupiedCollisionTiles = [];
        if (destroyDebugTiles) this.destroyDebugTiles();
    };

    /**
     * @param {Boolean} destroyDebugTiles
     */
    clearCollisionData(destroyDebugTiles = true) {
        if (this._collider) {
            this.removeFromPathGrid();
            this._collider.remove();
            this._collider.clear();
            this._collider = null;
            this.clearPathTiles(destroyDebugTiles);
        }
    };

    /**
     */
    loadCollisionData() {
        this.clearCollisionData();
        this.createColliders();
        this.createDebugTiles();
        this.addToPathGrid(true);
    };

    /**
     */
    createColliders() {
        if (this.hasColliderData()) {
            const data = this._data;
            const system = $gameCollisions.get('collision');
            const drawColor = this._hasCachedTiles ? '0xff00d8' : '0xff0000';
            switch (data.type) {
            case "Circle":
                this._collider = system.createCircle(data.x, data.y, data.radius, 1, 0, drawColor);
                break;
            case "Rectangle":
                const w = data.width;
                const h = data.height;
                const angle = Knight.degToRad(data.angle);
                this._collider = system.createRectangle(data.x, data.y, w, h, angle, 1, 1, 0, drawColor);
                break;
            }
            this._collider._data = data;
            if (Imported.KNT_CollisionViewer) this._collider.update();
        }
    };

    /**
     * @param {Boolean} refreshTiles    When true, updates the position of the tiles used for debug output
     * @param {Boolean} updateGrid      When true, blocks occupied tiles on the pathfinding grid. When false, only
     *                                  updates the collider's internal index of which tiles it's occupying without
     *                                  modifying the real pathfinding grid.
     */
    addToPathGrid(refreshTiles = true, updateGrid = true) {
        if (this.hasCollider() && (!this._hasCachedTiles || !updateGrid)) {
            // For axis-aligned polygons and circles, it's enough to mark all of the tiles
            // within the bounding box of the shape as occupied. For angled polygons, we
            // need to perform a slower AABB-Polygon test between the poly and the grid
            // tile to check if it actually fills the given tile.
            if ((this._collider._polygon && !this._collider._rectangle) || (this._collider._rectangle && this._collider._angle !== 0)) {
                this.addToPathGridWithCheck(updateGrid);
            } else {
                this.addToPathGridFast(updateGrid);
            }
            if (refreshTiles) this.refreshDebugTiles();
        }
    };

    /**
     * @param {Boolean} updateGrid      When true, blocks occupied tiles on the pathfinding grid
     */
    addToPathGridFast(updateGrid) {
        const bounds = this._collider.getBounds();
        const minTile = Knight.COLLISION.pointToGridTile(bounds.min_x, bounds.min_y);
        const maxTile = Knight.COLLISION.pointToGridTile(bounds.max_x, bounds.max_y);
        const pathGrid = $gameMap.pathGrid();
        const collisionGrid = $gameMap.collisionGrid();

        for (let y = minTile.y; y <= maxTile.y; ++y) {
            for (let x = minTile.x; x <= maxTile.x; ++x) {
                this._lastOccupiedCollisionTiles.push({x: x, y: y});
            }
        }

        // For axis-aligned polygons (or circles), increasing the bounds of the occupied tiles by 1
        // is effectively the same as dilating the shape with a 3x3 kernel
        minTile.x--; minTile.y--; maxTile.x++; maxTile.y++;

        for (let y = minTile.y; y <= maxTile.y; ++y) {
            for (let x = minTile.x; x <= maxTile.x; ++x) {
                this._lastOccupiedTiles.push({x: x, y: y});
            }
        }

        if (updateGrid) {
            this._lastOccupiedCollisionTiles.forEach(function(t) {
                collisionGrid.setBlocked(t.x, t.y, true);
            });
            this._lastOccupiedTiles.forEach(function(t) {
                pathGrid.setBlocked(t.x, t.y, true);
            });
        }
    }

    /**
     * @param {Boolean} updateGrid      When true, blocks occupied tiles on the pathfinding grid
     */
    addToPathGridWithCheck(updateGrid) {
        const system = $gameCollisions.get('collision');
        const body = this._collider;
        const bounds = body.getBounds();
        const minTile = Knight.COLLISION.pointToGridTile(bounds.min_x, bounds.min_y);
        const maxTile = Knight.COLLISION.pointToGridTile(bounds.max_x, bounds.max_y);
        const tx = Knight.COLLISION.tileSizeX();
        const ty = Knight.COLLISION.tileSizeY();
        const rect = new Sinova.Rectangle(0, 0, tx, ty);
        const blockedTiles = new Map();
        const pathGrid = $gameMap.pathGrid();
        const collisionGrid = $gameMap.collisionGrid();

        for (let y = minTile.y; y <= maxTile.y; ++y) {
            for (let x = minTile.x; x <= maxTile.x; ++x) {
                const tilePosition = Knight.COLLISION.gridTileToPixelTile(x, y);
                rect.x = tilePosition.x;
                rect.y = tilePosition.y;
                if (!system.collides(body, rect)) {
                    continue;
                }
                this._lastOccupiedCollisionTiles.push({x: x, y: y});
                blockedTiles.set(`${x}_${y}`, {x: x, y: y});
            }
        }
        rect.clear();

        // Since the polygon isn't axis-aligned, we have to actually run the full dilation operation
        const kernelSizeX = Knight.COLLISION.GridSubdivisions;
        const kernelSizeY = Knight.COLLISION.GridSubdivisions;
        const kernelOriginX = Math.floor(kernelSizeX / 2);
        const kernelOriginY = Math.floor(kernelSizeY / 2);
        this.dilate(blockedTiles, kernelSizeX, kernelSizeY, kernelOriginX, kernelOriginY);

        if (updateGrid) {
            this._lastOccupiedCollisionTiles.forEach(function(t) {
                collisionGrid.setBlocked(t.x, t.y, true);
            });
            this._lastOccupiedTiles.forEach(function(t) {
                pathGrid.setBlocked(t.x, t.y, true);
            });
        }
    }

    /**
     * Performs a dilation operation on the collider tiles, based on an input kernel.
     * For more information, see: http://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
     *
     * @param {Set<Object>} blockedTiles
     * @param {Number} kernelSizeX
     * @param {Number} kernelSizeY
     * @param {Number} kernelOriginX
     * @param {Number} kernelOriginY
     */
    dilate(blockedTiles, kernelSizeX, kernelSizeY, kernelOriginX, kernelOriginY) {
        // For each blocked tile, render the kernel to the grid in that tile's position
        const dilatedTiles = new Map();
        for (let y = 0; y < kernelSizeY; ++y) {
            for (let x = 0; x < kernelSizeX; ++x) {
                const offsetX = x - kernelOriginX;
                const offsetY = y - kernelOriginY;
                for (const tile of blockedTiles.values()) {
                    const tx = tile.x + offsetX;
                    const ty = tile.y + offsetY;
                    dilatedTiles.set(`${tx}_${ty}`, {x: tx, y: ty});
                }
            }
        }
        for (const tile of dilatedTiles.values()) {
            this._lastOccupiedTiles.push({x: tile.x, y: tile.y});
        }
    }

    /**
     */
    removeFromPathGrid() {
        const pathGrid = $gameMap.pathGrid();
        const collisionGrid = $gameMap.collisionGrid();
        this._lastOccupiedTiles.forEach(function(t) {
            pathGrid.setBlocked(t.x, t.y, false);
        });
        this._lastOccupiedTiles = [];

        this._lastOccupiedCollisionTiles.forEach(function(t) {
            collisionGrid.setBlocked(t.x, t.y, false);
        });
        this._lastOccupiedCollisionTiles = [];
    };

    /**
     */
    update() {
    };

    /**
     */
    createDebugTiles() {
    };

    /**
     */
    destroyDebugTiles() {
    }

    /**
     */
    refreshDebugTiles() {
    };

    /**
     */
    updateDebugTiles() {
    };
};

//=============================================================================
// Game_Map
//=============================================================================
Knight.EDITOR.Game_Map_loadMapCollision = Game_Map.prototype.loadMapCollision;
Game_Map.prototype.loadMapCollision = function() {
    Knight.EDITOR.Game_Map_loadMapCollision.call(this);
    console.time("loadDoodadCollision");
    SceneManager._scene._spriteset.loadDoodadCollision();
    console.timeEnd("loadDoodadCollision");
};

Game_Map.prototype.colliders = function() {
    return $dataColliders ? $dataColliders[this.mapId()] : undefined;
};

Game_Map.prototype.pathfindingGrids = function() {
    return $dataPathfinding ? $dataPathfinding[this.mapId()] : undefined;
};

Game_Map.prototype.pathGrid = function() {
    return this._pathGrid;
};

Game_Map.prototype.collisionGrid = function() {
    return this._collisionGrid;
};

//=============================================================================
// Spriteset_Map
//=============================================================================
Knight.EDITOR.Spriteset_Map_removeCurrentDoodads = Spriteset_Map.prototype.removeCurrentDoodads;
Spriteset_Map.prototype.removeCurrentDoodads = function() {
    this._doodads = this._doodads || [];
    this._doodads.forEach(function(doodad) {
        doodad.clearCollisionData();
    });
    Knight.EDITOR.Spriteset_Map_removeCurrentDoodads.call(this);
};

Spriteset_Map.prototype.loadDoodadCollision = function() {
    this.doodads().forEach(function(doodad) {
        doodad.loadCollisionData();
    });
};

//=============================================================================
// Play Test Only
//=============================================================================
if (Knight.EDITOR.isTestPlay()) {
    //=============================================================================
    // StorageManager
    //=============================================================================
    StorageManager.getDataPath = function() {
        const path = require('path');
        const base = path.dirname(process.mainModule.filename);
        return path.join(base, 'data/');
    };

    StorageManager.saveCollisionData = function() {
        StorageManager.saveCollisionSettings();
        StorageManager.savePathfindingData();
    };

    StorageManager.saveCollisionSettings = function() {
        const data = JSON.stringify($dataColliders, null, 2);
        const fs = require('fs');
        const path = StorageManager.getDataPath();
        if (!fs.existsSync(path)) fs.mkdirSync(path);
        const filePath = path + 'Colliders.json';
        console.log(filePath);
        fs.writeFileSync(filePath, data);
    };

    // The pathfinding grid has to be serialized/deserialized with object type
    // information, so it has slightly different save/load methods from collision data
    // Using base64 compression to keep the file size low
    StorageManager.savePathfindingData = function() {
        const json = JsonEx.stringify($dataPathfinding);
        const data = LZString.compressToBase64(json);
        const fs = require('fs');
        const path = StorageManager.getDataPath();
        if (!fs.existsSync(path)) fs.mkdirSync(path);
        const filePath = path + 'Pathfinding.json';
        console.log(filePath);
        fs.writeFileSync(filePath, data);
    };

    StorageManager.loadPathfindingData = function() {
        let data = null;
        const fs = require('fs');
        const path = StorageManager.getDataPath();
        const filePath = path + 'Pathfinding.json';
        if (fs.existsSync(filePath)) {
            data = fs.readFileSync(filePath, {encoding: 'utf8'});
            const json = LZString.decompressFromBase64(data);
            if (json != null) return JsonEx.parse(json);
        }
        return null;
    };

    //=============================================================================
    // Doodad Manager
    //=============================================================================
    Knight.EDITOR.DoodadManager_getTemplate = DoodadManager.getTemplate;
    DoodadManager.getTemplate = function(folderName, bitmapName) {
        const template = Knight.EDITOR.DoodadManager_getTemplate.call(this, folderName, bitmapName);
        template.hideOnOverlap = false;
        template.collider = {};
        return template;
    };

    DoodadManager.refresh = function() {
        const scene = SceneManager._scene;
        if (scene) {
            scene._spriteset.createDoodads();
            scene._spriteset.loadDoodadCollision();
            scene._spriteset.update();
        }
    };

    //=============================================================================
    // ColliderManager
    //=============================================================================
    /**
     * Class that manages map collider data. Works like the Doodad Manager. Doesn't
     * store doodad colliders, that's handled by the Doodad Manager.
     */
    function ColliderManager() {
        throw new Error('This is a static class');
    };

    ColliderManager.mapId = function() {
        return $gameMap.mapId();
    };

    ColliderManager.addNew = function(collider) {
        $dataColliders[this.mapId()] = $dataColliders[this.mapId()] || [];
        $dataColliders[this.mapId()].push(collider);
        this.sortColliders($dataColliders[this.mapId()]);
        $gameMap.addCollider(collider, false);
    };

    ColliderManager.delete = function(collider) {
        $dataColliders[this.mapId()] = $dataColliders[this.mapId()] || [];
        const index = $dataColliders[this.mapId()].indexOf(collider);
        if (index >= 0) $dataColliders[this.mapId()].splice(index, 1);
        this.sortColliders($dataColliders[this.mapId()]);
        $gameMap.removeCollider(collider);
    };

    ColliderManager.sortColliders = function(arr) {
        arr.sort(function(a, b) {
            if (a.z !== b.z) {
                return b.z - a.z;
            } else if (a.y !== b.y) {
                return a.y - b.y;
            } else {
                return a.x - b.x;
            }
        });
    };

    ColliderManager.clearMap = function() {
        $dataColliders[this.mapId()].forEach((collider) => $gameMap.removeCollider(collider));
        $dataColliders[this.mapId()] = [];
    };

    ColliderManager.colliders = function() {
        return $dataColliders[this.mapId()] || [];
    };

    ColliderManager.size = function() {
        return this.colliders().length;
    };

    ColliderManager.isDirty = function(mapId = this.mapId()) {
        this._isDirty = this._isDirty || [];
        return this._isDirty[mapId] || false;
    };

    ColliderManager.setDirty = function(isDirty, mapId = this.mapId()) {
        this._isDirty = this._isDirty || [];
        this._isDirty[mapId] = isDirty;
    };

    ColliderManager.isMouseOnColliders = function() {
        return $gameMap.getMouseCollisionWithMapColliders();
    };

    ColliderManager.getTemplate = function() {
        return {
            x: 0,
            y: 0,
            z: 3,
            type: 'Circle',
            radius: 48,
            anchor: {
                x: 0,
                y: -48,
            },
        };
    };

    ColliderManager.setManualMove = function(setting) {
        if (setting) this.updateManualMove();
        this._manualMove = setting;
    };

    ColliderManager.current = function() {
        return SceneManager._scene.currentCollider();
    };

    ColliderManager.currentCopy = function() {
        return JsonEx.makeDeepCopy(SceneManager._scene.currentCollider());
    };

    ColliderManager.updateManualMove = function() {
        if (this._manualMove !== false) return;
        if (SceneManager._scene._editor.inPlacementMode()) {
            this._manualX = TouchInput.x;
            this._manualY = TouchInput.y;
        } else {
            const doodad = SceneManager._scene._editor._currentDoodad;
            this._manualX = doodad.x;
            this._manualY = doodad.y;
        }
    };

    ColliderManager.usingManualMove = function() {
        return this._manualMove;
    };

    ColliderManager.manualMoveUp = function() {
        this.setManualMove(true);
        this._manualY -= DoodadManager._gridLockMode ? DoodadManager.gridLockY() : 1;
        this.adjustManualMove();
        return this._manualY;
    };

    ColliderManager.manualMoveDown = function() {
        this.setManualMove(true);
        this._manualY += DoodadManager._gridLockMode ? DoodadManager.gridLockY() : 1;
        this.adjustManualMove();
        return this._manualY;
    };

    ColliderManager.manualMoveLeft = function() {
        this.setManualMove(true);
        this._manualX -= DoodadManager._gridLockMode ? DoodadManager.gridLockX() : 1;
        this.adjustManualMove();
        return this._manualX;
    };

    ColliderManager.manualMoveRight = function() {
        this.setManualMove(true);
        this._manualX += DoodadManager._gridLockMode ? DoodadManager.gridLockX() : 1;
        this.adjustManualMove();
        return this._manualX;
    };

    ColliderManager.updateCurrentCollider = function() {
        // SceneManager._scene._spriteset.update();
    };

    ColliderManager.updateNewSettings = function() {
        if (this._canvasMode) {
            this.updateCurrentCollider();
        } else {
            this.refresh();
        }
    };

    ColliderManager.revertSettings = function(collider1, collider2) {
        for (key in collider1) {
            if (collider1.hasOwnProperty(key)) {
                collider1[key] = collider2[key];
            }
        }
    };

    //=============================================================================
    // Spriteset_Map
    //=============================================================================
    Spriteset_Map.prototype.removeDoodadsFromGrid = function() {
        this._doodads.forEach((doodad) => doodad.removeFromPathGrid());
    };

    Spriteset_Map.prototype.addDoodadsToGrid = function() {
        this._doodads.forEach((doodad) => doodad.addToPathGrid(false));
    };

    //=============================================================================
    // Game_Map
    //=============================================================================
    Game_Map.prototype.removeCharactersFromGrid = function() {
        this.getAllCharacters().forEach((character) => character.removeFromPathGrid());
    };

    Game_Map.prototype.addCharactersToGrid = function() {
        this.getAllCharacters().forEach((character) => character.addToPathGrid(false));
    };

    if (Imported.KNT_CollisionViewer) {
        Knight.EDITOR.Game_Map_updateCollisionViewer = Game_Map.prototype.updateCollisionViewer;
        Game_Map.prototype.updateCollisionViewer = function() {
            Knight.EDITOR.Game_Map_updateCollisionViewer.call(this);
            const container = SceneManager._scene.debugContainer();
            SceneManager._scene._spriteset.doodads().forEach(function(doodad) {
                if (doodad._debugTiles) container.addChild(doodad._debugTiles);
            });
        };
    }

    Knight.EDITOR.Game_Map_loadMouseCollision = Game_Map.prototype.loadMouseCollision;
    Game_Map.prototype.loadMouseCollision = function() {
        Knight.EDITOR.Game_Map_loadMouseCollision.call(this);
        this._editorMouseCollider = $gameCollisions.get('collision').createPoint(0, 0, 0, '0xffd800');
    };

    Knight.EDITOR.Game_Map_updateMouseCollider = Game_Map.prototype.updateMouseCollider;
    Game_Map.prototype.updateMouseCollider = function() {
        Knight.EDITOR.Game_Map_updateMouseCollider.call(this);
        this._editorMouseCollider.x = this._mouseCollider.x;
        this._editorMouseCollider.y = this._mouseCollider.y;
    };

    Game_Map.prototype.getMouseCollisionWithMapColliders = function() {
        const potentials = this._editorMouseCollider.potentials();
        let collidedWith = null;
        let maxY = -1;
        for (const body of potentials) {
            if (this._editorMouseCollider.collides(body, $gameResult)) {
                if (!body.parent && body._data && body.y > maxY) {
                    maxY = body.y;
                    collidedWith = body;
                }
            }
        }
        return collidedWith ? this.getCollider(collidedWith._data) : null;
    };

    //=============================================================================
    // Scene_Map
    //=============================================================================
    Knight.EDITOR.Scene_Map_isSelecting = Scene_Map.prototype.isSelecting;
    Scene_Map.prototype.isSelecting = function() {
        if (Knight.Editor.isActive()) return this._editor._draggingCollider;
        else return Knight.EDITOR.Scene_Map_isSelecting.call(this);
    };

    Knight.EDITOR.Scene_Map_getSelectionPoints = Scene_Map.prototype.getSelectionPoints;
    Scene_Map.prototype.getSelectionPoints = function() {
        if (Knight.Editor.isActive()) return this._editor.getSelectionBounds();
        else return Knight.EDITOR.Scene_Map_getSelectionPoints.call(this);
    };

    //=============================================================================
    // Sprite_Doodad
    //=============================================================================
    if (Imported.KNT_CollisionViewer) {
        Sprite_Doodad.prototype.destroyDebugTiles = function() {
            if (this._debugTiles) {
                this._debugTiles.destroy(true);
                this._debugTiles = null;
            }
        };

        Sprite_Doodad.prototype.updateColliderDebugPosition = function() {
            Object.values(this._colliders).forEach((collider) => collider.update());
        };

        // Update the position of the collider's debug graphics.
        // Otherwise, it will be drawn incorrectly for 1 frame, until the BVH update fixes it.
        Knight.EDITOR.Sprite_Doodad_createColliders = Sprite_Doodad.prototype.createColliders;
        Sprite_Doodad.prototype.createColliders = function() {
            Knight.EDITOR.Sprite_Doodad_createColliders.call(this);
            Object.values(this._colliders).forEach((collider) => collider.update());
        };

        Sprite_Doodad.prototype.refreshDebugTiles = function() {
            if (this._debugTiles) {
                if (this._lastOccupiedTiles.length > this._debugTiles.children.length) {
                    // Make more tiles if we don't have enough
                    const newTileCount = this._lastOccupiedTiles.length - this._debugTiles.children.length;
                    const texture = this._debugTiles.children[0].texture;
                    for (let i = 0; i < newTileCount; ++i) {
                        const sprite = new PIXI.Sprite(texture);
                        sprite.ox = 0;
                        sprite.oy = 0;
                        this._debugTiles.addChild(sprite);
                    }
                }
                for (let i = 0; i < this._debugTiles.children.length; ++i) {
                    const sprite = this._debugTiles.children[i];
                    if (i < this._lastOccupiedTiles.length) {
                        const pos = Knight.COLLISION.gridTileToPixelTile(this._lastOccupiedTiles[i].x, this._lastOccupiedTiles[i].y);
                        sprite.ox = pos.x;
                        sprite.oy = pos.y;
                    } else {
                        sprite.ox = -1;
                        sprite.oy = -1;
                    }
                }
            }
        };

        Sprite_Doodad.prototype.createDebugTiles = function() {
            if (this.hasCollider('collision')) {
                if (this._debugTiles) {
                    this._debugTiles.destroy(true);
                }
                this._debugTiles = new PIXI.Container();

                const tile = new PIXI.Graphics();
                tile.lineStyle(1, '0xff0000', 0.25);
                tile.beginFill('0xff0000', 0.1);
                tile.drawRect(0, 0, Knight.COLLISION.tileSizeX(), Knight.COLLISION.tileSizeY());
                tile.endFill();
                tile.position.set(0, 0);

                const maxDebugTiles = Math.pow(Knight.COLLISION.GridSubdivisions * 2, 2);
                const texture = Graphics._renderer.generateTexture(tile);
                for (let i = 0; i < maxDebugTiles; ++i) {
                    const sprite = new PIXI.Sprite(texture);
                    sprite.ox = 0;
                    sprite.oy = 0;
                    this._debugTiles.addChild(sprite);
                }
                tile.destroy(true);

                SceneManager._scene.debugContainer().addChild(this._debugTiles);
                this.refreshDebugTiles();
            }
        };

        Sprite_Doodad.prototype.updateDebugTiles = function() {
            if (this._debugTiles) {
                const tw = $gameMap.tileWidth();
                const th = $gameMap.tileHeight();
                const debugTilesVisible = this.visible && this.hasCollider('collision');
                for (sprite of this._debugTiles.children) {
                    const valid = debugTilesVisible && sprite.ox >= 0;
                    if (valid) {
                        sprite.visible = Knight.COLLISION.debugDraw;
                        const screenX = Math.round(sprite.ox - ($gameMap._displayX * tw));
                        const screenY = Math.round(sprite.oy - ($gameMap._displayY * th));
                        sprite.position.set(screenX, screenY);
                    } else {
                        sprite.visible = false;
                    }
                }
            }
        };
    }

    Sprite_Doodad.prototype.updateCollider = function() {
        if (!this.isColliderDirty()) return;
        if (this.colliderTypeChanged()) {
            this.reloadCollision();
        } else {
            let graphicsChanged = false;
            const collider = this.getCollider('collision');
            switch (this._data.collider.type) {
            case "Circle":
                graphicsChanged = collider.radius !== this._data.collider.radius;
                collider.radius = this._data.collider.radius;
                break;
            case "Rectangle":
                graphicsChanged = (collider.width !== this._data.collider.width) ||
                    (collider.height !== this._data.collider.height) ||
                    (collider.angle !== this._data.collider.angle);
                collider.width = this._data.collider.width;
                collider.height = this._data.collider.height;
                collider.angle = Knight.degToRad(this._data.collider.angle);
                break;
            }
            collider.anchor.x = this._data.collider.anchor.x;
            collider.anchor.y = this._data.collider.anchor.y;
            if (graphicsChanged) {
                collider.createGraphics();
            }
            this.updateColliderPosition();
            this.removeFromPathGrid();
            this.addToPathGrid();
        }
        this._data.collider.dirty = undefined;
    };

    Sprite_Doodad.prototype.colliderTypeChanged = function() {
        let curColliderType = null;
        const collider = this.getCollider('collision');
        if (collider instanceof Sinova.Circle) curColliderType = 'Circle';
        else if (collider instanceof Sinova.Rectangle) curColliderType = 'Rectangle';
        const newColliderType = this.hasColliderData() ? this._data.collider.type : null;
        return curColliderType !== newColliderType;
    };

    Sprite_Doodad.prototype.reloadCollision = function() {
        this.clearCollisionData();
        this._colliders = {};
        this._hideOnOverlap = this._data.hideOnOverlap || false;
        this.createColliders();
        this.addToPathGrid();
        if (!this._debugTiles) this.createDebugTiles();
    };

    // When in test-play mode, reload the overlap collider when overlap state changes
    Knight.EDITOR.Sprite_Doodad_initData_testPlay = Sprite_Doodad.prototype.initData;
    Sprite_Doodad.prototype.initData = function() {
        const overlapChanged = this._hideOnOverlap !== (!!this._data.hideOnOverlap);
        Knight.EDITOR.Sprite_Doodad_initData_testPlay.call(this);
        if (overlapChanged && this._collisionLoaded) {
            if (this.hasCollider('overlap')) {
                const collider = this.getCollider('overlap');
                collider.remove();
                collider.clear();
                delete this._colliders['overlap'];
            }
            this.createColliderOverlap();
            this.updateColliderPosition();
        }
    };

    //=============================================================================
    // Map Collider
    //=============================================================================
    if (Imported.KNT_CollisionViewer) {
        Knight.EDITOR.Map_Collider.prototype.createDebugTiles = function() {
            if (this.hasCollider()) {
                if (this._debugTiles) {
                    this._debugTiles.destroy(true);
                }
                this._debugTiles = new PIXI.Container();

                const tile = new PIXI.Graphics();
                tile.lineStyle(1, '0xff0000', 0.25);
                tile.beginFill('0xff0000', 0.1);
                tile.drawRect(0, 0, Knight.COLLISION.tileSizeX(), Knight.COLLISION.tileSizeY());
                tile.endFill();
                tile.position.set(0, 0);

                const maxDebugTiles = 1;
                const texture = Graphics._renderer.generateTexture(tile);
                for (let i = 0; i < maxDebugTiles; ++i) {
                    const sprite = new PIXI.Sprite(texture);
                    sprite.ox = 0;
                    sprite.oy = 0;
                    this._debugTiles.addChild(sprite);
                }
                tile.destroy(true);

                SceneManager._scene.debugContainer().addChild(this._debugTiles);
                this.refreshDebugTiles();
            }
        };

        Knight.EDITOR.Map_Collider.prototype.destroyDebugTiles = function() {
            if (this._debugTiles) {
                this._debugTiles.destroy(true);
                this._debugTiles = null;
            }
        };

        Knight.EDITOR.Map_Collider.prototype.refreshDebugTiles = function() {
            if (this._debugTiles) {
                if (this._lastOccupiedTiles.length > this._debugTiles.children.length) {
                    // Make more tiles if we don't have enough
                    const newTileCount = this._lastOccupiedTiles.length - this._debugTiles.children.length;
                    const texture = this._debugTiles.children[0].texture;
                    for (let i = 0; i < newTileCount; ++i) {
                        const sprite = new PIXI.Sprite(texture);
                        sprite.ox = 0;
                        sprite.oy = 0;
                        this._debugTiles.addChild(sprite);
                    }
                }
                for (let i = 0; i < this._debugTiles.children.length; ++i) {
                    const sprite = this._debugTiles.children[i];
                    if (i < this._lastOccupiedTiles.length) {
                        const pos = Knight.COLLISION.gridTileToPixelTile(this._lastOccupiedTiles[i].x, this._lastOccupiedTiles[i].y);
                        sprite.ox = pos.x;
                        sprite.oy = pos.y;
                    } else {
                        sprite.ox = -1;
                        sprite.oy = -1;
                    }
                }
            }
        };

        Knight.EDITOR.Map_Collider.prototype.updateDebugTiles = function() {
            if (this._debugTiles) {
                const tw = $gameMap.tileWidth();
                const th = $gameMap.tileHeight();
                const debugTilesVisible = this.hasCollider();
                for (sprite of this._debugTiles.children) {
                    const valid = debugTilesVisible && sprite.ox >= 0;
                    if (valid) {
                        sprite.visible = Knight.COLLISION.debugDraw;
                        const screenX = Math.round(sprite.ox - ($gameMap._displayX * tw));
                        const screenY = Math.round(sprite.oy - ($gameMap._displayY * th));
                        sprite.position.set(screenX, screenY);
                    } else {
                        sprite.visible = false;
                    }
                }
            }
        };
    }

    Knight.EDITOR.Map_Collider.prototype.update = function() {
        this.updateCollider();
        this.updateDebugTiles();
    };

    Knight.EDITOR.Map_Collider.prototype.updateCollider = function() {
        const collider = this.getCollider();
        let colliderDataChanged = false;
        const oldX = collider.x;
        const oldY = collider.y;
        const posChanged = (this._data.x !== oldX || this._data.y !== oldY);
        collider.x = this._data.x;
        collider.y = this._data.y;

        if (this.isDirty()) {
            if (this.colliderTypeChanged()) {
                this.reloadCollision();
            } else {
                switch (this._data.type) {
                case "Circle":
                    colliderDataChanged = collider.radius !== this._data.radius;
                    collider.radius = this._data.radius;
                    break;
                case "Rectangle":
                    colliderDataChanged = (collider.width !== this._data.width) ||
                        (collider.height !== this._data.height) ||
                        (collider.angle !== this._data.angle);
                    collider.width = this._data.width;
                    collider.height = this._data.height;
                    collider.angle = Knight.degToRad(this._data.angle);
                    break;
                }
            }
            this.setDirty(false);
        }

        if (colliderDataChanged) collider.createGraphics();
        if (posChanged || colliderDataChanged) {
            collider.x = oldX;
            collider.y = oldY;
            this.invalidateCachedTiles();
            collider.x = this._data.x;
            collider.y = this._data.y;
            this.removeFromPathGrid();
            this.addToPathGrid(true);
        }
    };

    /**
     * Called when the cached pathfinding grid tiles need to change, such as when the
     * collider moves or changes shape. Used to update path grid information during
     * level editing. This should never need to be called during the live game, as
     * the in-game level colliders are static.
     */
    Knight.EDITOR.Map_Collider.prototype.invalidateCachedTiles = function() {
        if (this._hasCachedTiles) {
            this.addToPathGrid(false, false);
            this._collider._drawColor = '0xff0000';
            this._collider.createGraphics();
            this._hasCachedTiles = false;
        }
    };

    /**
     * @return {Boolean}
     */
    Knight.EDITOR.Map_Collider.prototype.colliderTypeChanged = function() {
        let curColliderType = null;
        const collider = this.getCollider();
        if (collider instanceof Sinova.Circle) curColliderType = 'Circle';
        else if (collider instanceof Sinova.Rectangle) curColliderType = 'Rectangle';
        const newColliderType = this.hasColliderData() ? this._data.type : null;
        return curColliderType !== newColliderType;
    };

    /**
     */
    Knight.EDITOR.Map_Collider.prototype.reloadCollision = function() {
        this.invalidateCachedTiles();
        this.clearCollisionData(false);
        this.createColliders();
        this.addToPathGrid(true);
    };

    /**
     * @param {Array} filters
     */
    Knight.EDITOR.Map_Collider.prototype.setFilters = function(filters) {
        if (this._collider && this._collider._context) this._collider._context.filters = filters;
    };

    /**
     */
    Knight.EDITOR.Map_Collider.prototype.clearFilters = function() {
        if (this._collider && this._collider._context) this._collider._context.filters = [];
    };

    //=============================================================================
    // Knight Editor
    //=============================================================================
    Knight.EDITOR.KnightEditor_initData = Knight.Editor.KnightEditor.prototype.initData;
    Knight.Editor.KnightEditor.prototype.initData = function(scene) {
        Knight.EDITOR.KnightEditor_initData.call(this, scene);
        this._currentCollider = null;
        this._hoverCollider = null;
        this._managers['Collision'] = ColliderManager;
        this._draggingCollider = false;
        this._dragCount = 0;
        this._dragPoint = null;
        this._movingCollider = false;
        this._moveAnchor = null;
    };

    Knight.EDITOR.KnightEditor_windowGroups = Knight.Editor.KnightEditor.windowGroups;
    Knight.Editor.KnightEditor.windowGroups = function() {
        const arr = Knight.EDITOR.KnightEditor_windowGroups.call(this);
        arr.push("Collision");
        return arr;
    };

    Knight.Editor.KnightEditor.prototype.isCollisionGroup = function() {
        return this.isActiveGroup('Collision');
    };

    Knight.EDITOR.KnightEditor_currentObject = Knight.Editor.KnightEditor.prototype.currentObject;
    Knight.Editor.KnightEditor.prototype.currentObject = function() {
        let obj = Knight.EDITOR.KnightEditor_currentObject.call(this);
        if (this.isCollisionGroup()) {
            obj = this._currentCollider ? this._currentCollider._data : null;
        }
        return obj;
    };

    Knight.EDITOR.KnightEditor_createWindows = Knight.Editor.KnightEditor.prototype.createWindows;
    Knight.Editor.KnightEditor.prototype.createWindows = function() {
        this.createColliderWindows();
        Knight.EDITOR.KnightEditor_createWindows.call(this);
    };

    Knight.Editor.KnightEditor.prototype.createColliderWindows = function() {
        const ew = 350;
        const eh = Graphics.height - Knight.Editor.Window_EditorCommand.windowHeight();
        this._colliderEditWindow = new Knight.Editor.Window_ColliderProperties(Graphics.width - ew, 0, ew, eh, this);
        this._windows.push(this._colliderEditWindow);
    };

    Knight.EDITOR.KnightEditor_updateActiveGroup = Knight.Editor.KnightEditor.prototype.updateActiveGroup;
    Knight.Editor.KnightEditor.prototype.updateActiveGroup = function() {
        Knight.EDITOR.KnightEditor_updateActiveGroup.call(this);
        if (this._activeGroup === 'Collision') {
            this.updateColliderPosition();
        }
    };

    Knight.Editor.KnightEditor.prototype.updateColliderPosition = function() {
        if (this._movingCollider) {
            const oldX = this._currentCollider._data.x;
            const oldY = this._currentCollider._data.y;
            this._currentCollider._data.x = this._moveAnchor.x + $gameMap.canvasToMapPX(TouchInput.x);
            this._currentCollider._data.y = this._moveAnchor.y + $gameMap.canvasToMapPY(TouchInput.y);
            if (this._currentCollider._data.x !== oldX || this._currentCollider._data.y !== oldY) {
                this._colliderEditWindow.refresh();
                this.setDirty(true);
            }
        }
    };

    Knight.EDITOR.KnightEditor_setInputHandlers = Knight.Editor.KnightEditor.prototype.setInputHandlers;
    Knight.Editor.KnightEditor.prototype.setInputHandlers = function() {
        Knight.EDITOR.KnightEditor_setInputHandlers.call(this);
        const group = 'Collision';
        this.setHandler(group, 'handlePlacementInput', this.handleColliderPlacementInput.bind(this));
        this.setHandler(group, 'handleSelectionInput', this.handleColliderSelectionInput.bind(this));
        this.setHandler(group, 'scrollUp', this.scrollColliderUp.bind(this));
        this.setHandler(group, 'scrollDown', this.scrollColliderDown.bind(this));
        this.setHandler(group, 'scrollLeft', this.scrollColliderLeft.bind(this));
        this.setHandler(group, 'scrollRight', this.scrollColliderRight.bind(this));
        this.setHandler(group, 'moveUp', this.moveColliderUp.bind(this));
        this.setHandler(group, 'moveDown', this.moveColliderDown.bind(this));
        this.setHandler(group, 'moveLeft', this.moveColliderLeft.bind(this));
        this.setHandler(group, 'moveRight', this.moveColliderRight.bind(this));
        this.setHandler(group, 'layerIncr', this.layerIncrCollider.bind(this));
        this.setHandler(group, 'layerDecr', this.layerDecrCollider.bind(this));
        this.setHandler(group, 'defaultSettings', this.defaultColliderSettings.bind(this));
        this.setHandler(group, 'toggleGridLock', this.toggleColliderGridLock.bind(this));
        this.setHandler(group, 'setManualMove', this.setColliderManualMove.bind(this));
    };

    Knight.Editor.KnightEditor.prototype.handleColliderPlacementInput = function() {
    };

    Knight.Editor.KnightEditor.prototype.handleColliderSelectionInput = function() {
        const collider = ColliderManager.isMouseOnColliders();
        this.setHoverCollider(collider);

        if (collider && TouchInput.isTriggered()) {
            this.setCurrentCollider(collider);
        }

        if (this._draggingCollider) {
            this._dragCount++;
            if (TouchInput.isReleased()) {
                this._draggingCollider = false;
                if (this._dragCount >= 8) {
                    this.endColliderDraw();
                }
            } else if (TouchInput.isCancelled()) {
                this.cancelColliderDraw();
            }
        } else {
            if (TouchInput.isTriggered()) {
                if (collider) {
                    this._movingCollider = true;
                    this._moveAnchor = {
                        x: collider._collider.x - $gameMap.canvasToMapPX(TouchInput.x),
                        y: collider._collider.y - $gameMap.canvasToMapPY(TouchInput.y),
                    };
                } else {
                    this.startColliderDraw();
                }
            }
            if (this._movingCollider && TouchInput.isReleased()) this._movingCollider = false;
            if (Input.isTriggered(Knight.INPUT['del'])) this.delete();
            if (Input.isTriggered(Knight.INPUT['c'])) this.copy();
        }
    };

    Knight.Editor.KnightEditor.prototype.startColliderDraw = function() {
        this._dragCount = 0;
        this._draggingCollider = true;
        this._dragPoint = {
            x: $gameMap.canvasToMapPX(TouchInput.x),
            y: $gameMap.canvasToMapPY(TouchInput.y),
        };
    };

    Knight.Editor.KnightEditor.prototype.cancelColliderDraw = function() {
        this._dragCount = 0;
        this._draggingCollider = false;
        this._dragPoint = null;
    };

    Knight.Editor.KnightEditor.prototype.endColliderDraw = function() {
        const bounds = this.getSelectionBounds();
        const collider = ColliderManager.getTemplate();
        collider.angle = 0;
        collider.x = bounds.min.x;
        collider.y = bounds.min.y;
        collider.width = bounds.max.x - bounds.min.x;
        collider.height = bounds.max.y - bounds.min.y;
        collider.type = "Rectangle";
        collider.radius = undefined;
        collider.anchor = undefined;
        ColliderManager.addNew(collider);
        this.updateButtonState('Clear');
        this.setDirty(true);
        this.cancelColliderDraw();
    };

    Knight.Editor.KnightEditor.prototype.getSelectionBounds = function() {
        const maxPoint = {
            x: this._dragPoint.x,
            y: this._dragPoint.y,
        };
        const minPoint = {
            x: $gameMap.canvasToMapPX(TouchInput.x),
            y: $gameMap.canvasToMapPY(TouchInput.y),
        };
        if (minPoint.x > maxPoint.x) Knight.Utility.swapValue(minPoint, maxPoint, "x");
        if (minPoint.y > maxPoint.y) Knight.Utility.swapValue(minPoint, maxPoint, "y");
        return {min: minPoint, max: maxPoint};
    };

    Knight.Editor.KnightEditor.prototype.scrollColliderUp = function() {
        $gameMap.startScroll(8, 1, 6);
        ColliderManager.setManualMove(false);
    };

    Knight.Editor.KnightEditor.prototype.scrollColliderDown = function() {
        $gameMap.startScroll(2, 1, 6);
        ColliderManager.setManualMove(false);
    };

    Knight.Editor.KnightEditor.prototype.scrollColliderLeft = function() {
        $gameMap.startScroll(4, 1, 6);
        ColliderManager.setManualMove(false);
    };

    Knight.Editor.KnightEditor.prototype.scrollColliderRight = function() {
        $gameMap.startScroll(6, 1, 6);
        ColliderManager.setManualMove(false);
    };

    Knight.Editor.KnightEditor.prototype.moveColliderUp = function() {
        ColliderManager.manualMoveUp();
        this._currentCollider._data.y = ColliderManager._manualY;
        this._colliderEditWindow.refresh();
    };

    Knight.Editor.KnightEditor.prototype.moveColliderDown = function() {
        ColliderManager.manualMoveDown();
        this._currentCollider._data.y = ColliderManager._manualY;
        this._colliderEditWindow.refresh();
    };

    Knight.Editor.KnightEditor.prototype.moveColliderLeft = function() {
        ColliderManager.manualMoveLeft();
        this._currentCollider._data.x = ColliderManager._manualX;
        this._colliderEditWindow.refresh();
    };

    Knight.Editor.KnightEditor.prototype.moveColliderRight = function() {
        ColliderManager.manualMoveRight();
        this._currentCollider._data.x = ColliderManager._manualX;
        this._colliderEditWindow.refresh();
    };

    Knight.Editor.KnightEditor.prototype.layerIncrCollider = function() {
        this._colliderEditWindow.layerIncr();
    };

    Knight.Editor.KnightEditor.prototype.layerDecrCollider = function() {
        this._colliderEditWindow.layerDecr();
    };

    Knight.Editor.KnightEditor.prototype.defaultColliderSettings = function() {
        this._colliderEditWindow.default();
    };

    Knight.Editor.KnightEditor.prototype.toggleColliderGridLock = function() {
        this._colliderEditWindow.toggleGridLock();
    };

    Knight.Editor.KnightEditor.prototype.setColliderManualMove = function() {
        ColliderManager.setManualMove(false);
    };

    Knight.EDITOR.KnightEditor_deactivate = Knight.Editor.KnightEditor.prototype.deactivate;
    Knight.Editor.KnightEditor.prototype.deactivate = function() {
        Knight.EDITOR.KnightEditor_deactivate.call(this);
        this.clearCollider();
    };

    // Does nothing if there's already a currently selected collider. Is only
    // used to display what collider you're currently mousing over when there
    // is no collider selected.
    Knight.Editor.KnightEditor.prototype.setHoverCollider = function(collider) {
        if (collider !== this._hoverCollider && this._currentCollider === null) {
            if (this._hoverCollider) this._hoverCollider.clearFilters();
            this._hoverCollider = collider;
            if (this._hoverCollider) this._hoverCollider.setFilters(this.outlineFilter());
        }
    };

    Knight.Editor.KnightEditor.prototype.setCurrentCollider = function(collider) {
        if (this._currentCollider) this._currentCollider.clearFilters();
        this._currentCollider = collider;
        if (this._currentCollider) this._currentCollider.setFilters(this.outlineFilter());
        this.updateEditWindow(this._currentCollider ? this._currentCollider._data : null);
    };

    Knight.Editor.KnightEditor.prototype.clearCollider = function() {
        this.setCurrentCollider(null);
        this.setHoverCollider(null);
        this.exitPlacementMode();
    };

    Knight.EDITOR.KnightEditor_updateEditWindow = Knight.Editor.KnightEditor.prototype.updateEditWindow;
    Knight.Editor.KnightEditor.prototype.updateEditWindow = function(object) {
        Knight.EDITOR.KnightEditor_updateEditWindow.call(this, object);
        if (this.isCollisionGroup()) {
            const collider = object;
            this._colliderEditWindow.setCollider(collider);
            this.closeWindow(this._colliderEditWindow);
            if (collider && !this._minimized) {
                this.openWindow(this._colliderEditWindow);
                this.updateUndoQueue($dataColliders);
            }
        }
    };

    Knight.EDITOR.KnightEditor_onPropertyChange = Knight.Editor.KnightEditor.prototype.onPropertyChange;
    Knight.Editor.KnightEditor.prototype.onPropertyChange = function() {
        Knight.EDITOR.KnightEditor_onPropertyChange.call(this);
        if (this.isCollisionGroup()) {
            this._currentCollider.setDirty(true);
            this.setDirty(true);
        }
    };

    Knight.EDITOR.KnightEditor_minimize = Knight.Editor.KnightEditor.prototype.minimize;
    Knight.Editor.KnightEditor.prototype.minimize = function() {
        Knight.EDITOR.KnightEditor_minimize.call(this);
        if (!this._minimized && this.isCollisionGroup()) {
            this._colliderEditWindow._collider ? this.openWindow(this._colliderEditWindow) : this.closeWindow(this._colliderEditWindow);
        }
    };

    Knight.EDITOR.KnightEditor_save = Knight.Editor.KnightEditor.prototype.save;
    Knight.Editor.KnightEditor.prototype.save = function() {
        // Only cache static colliders when saving data
        // Temporarily remove everything else from the grid
        $gameMap.removeCharactersFromGrid();
        this._spriteset.removeDoodadsFromGrid();

        StorageManager.saveCollisionData();

        this._spriteset.addDoodadsToGrid();
        $gameMap.addCharactersToGrid();
        Knight.EDITOR.KnightEditor_save.call(this);
    };

    Knight.EDITOR.KnightEditor_copy = Knight.Editor.KnightEditor.prototype.copy;
    Knight.Editor.KnightEditor.prototype.copy = function() {
        Knight.EDITOR.KnightEditor_copy.call(this);
        if (this.isCollisionGroup() && this._currentCollider) {
            const data = JsonEx.makeDeepCopy(this._currentCollider._data);
            data.x -= 8; // Move the copy a bit so the new collider isn't directly
            data.y -= 8; // on top of the old one
            ColliderManager.addNew(data);
            this.setCurrentCollider($gameMap.getCollider(data));
            this.setDirty(true);
            this.playSound(Knight.Param.KESECopy);
        }
    };

    Knight.EDITOR.KnightEditor_delete = Knight.Editor.KnightEditor.prototype.delete;
    Knight.Editor.KnightEditor.prototype.delete = function() {
        Knight.EDITOR.KnightEditor_delete.call(this);
        if (this.isCollisionGroup() && this._currentCollider) {
            ColliderManager.delete(this._currentCollider._data);
            this.clearCollider();
            this.playSound(Knight.Param.KESEDelete);
            this.setDirty(true);
            this.updateButtonState('Clear');
        };
    };

    Knight.EDITOR.KnightEditor_clear = Knight.Editor.KnightEditor.prototype.clear;
    Knight.Editor.KnightEditor.prototype.clear = function() {
        Knight.EDITOR.KnightEditor_clear.call(this);
        if (this.isCollisionGroup() && ColliderManager.colliders().length > 0) {
            ColliderManager.clearMap();
            this.clearCollider();
            this.playSound(Knight.Param.KESEClear);
            this.setDirty(true);
            this.updateButtonState('Clear');
        }
    };

    Knight.EDITOR.KnightEditor_importConfirm = Knight.Editor.KnightEditor.prototype.importConfirm;
    Knight.Editor.KnightEditor.prototype.importConfirm = function() {
        const mapId = this._importWindow.mapId();
        if (mapId) {
            this.clearCollider();
            ColliderManager.clearMap();
            $dataColliders[mapId] = $dataColliders[mapId] || [];
            const data = JsonEx.makeDeepCopy($dataColliders[mapId]);
            data.forEach((collider) => ColliderManager.addNew(collider));
        }
        Knight.EDITOR.KnightEditor_importConfirm.call(this);
    };

    //=============================================================================
    // Doodad Property Window
    //=============================================================================
    Knight.EDITOR.Window_DoodadProperties_windowGroups = Knight.Editor.Window_DoodadProperties.windowGroups;
    Knight.Editor.Window_DoodadProperties.windowGroups = function() {
        if (!this._windowGroupNames) {
            Knight.EDITOR.Window_DoodadProperties_windowGroups.call(this);
            this._windowGroupNames.push("Collision");
            return this._windowGroupNames;
        }
        return Knight.EDITOR.Window_DoodadProperties_windowGroups.call(this);
    };

    // Read collision data from doodad and use it to set widget values
    Knight.EDITOR.Window_DoodadProperties_updateAllValues = Knight.Editor.Window_DoodadProperties.prototype.updateAllValues;
    Knight.Editor.Window_DoodadProperties.prototype.updateAllValues = function() {
        Knight.EDITOR.Window_DoodadProperties_updateAllValues.call(this);
        if (this._doodad) {
            // Collider Data
            const colliderType = (this._doodad.collider == null || this._doodad.collider.type == null) ? "None" : this._doodad.collider.type;
            this._colliderDropDown.setValue(colliderType);
            switch (colliderType) {
            case 'Circle':
                this._colliderRadius.setValue(this._doodad.collider.radius);
                this._colliderAnchorX.setValue(this._doodad.collider.anchor.x);
                this._colliderAnchorY.setValue(this._doodad.collider.anchor.y);
                break;
            case 'Rectangle':
                this._colliderWidth.setValue(this._doodad.collider.width);
                this._colliderHeight.setValue(this._doodad.collider.height);
                this._colliderAngle.setValue(this._doodad.collider.angle);
                this._colliderAnchorX.setValue(this._doodad.collider.anchor.x);
                this._colliderAnchorY.setValue(this._doodad.collider.anchor.y);
                break;
            }
            this.updateCollisionWindowVisibility();

            // Event Data
            const hasEvent = !!this._doodad.eventId;
            let eventName = "None";
            if (hasEvent) {
                const mapEvent = $gameMap.event(this._doodad.eventId);
                if (mapEvent) {
                    const eventData = mapEvent.event();
                    eventName = `${eventData.id}: ${eventData.name}`;
                }
            }
            this._eventDropDown.setValue(eventName);

            // Transparency When Behind Flag
            this._doodad.hideOnOverlap ? this._hideOnOverlap.check(false) : this._hideOnOverlap.uncheck(false);
        }
    };

    // Write collision data from widget to doodad
    Knight.EDITOR.Window_DoodadProperties_updateValue = Knight.Editor.Window_DoodadProperties.prototype.updateValue;
    Knight.Editor.Window_DoodadProperties.prototype.updateValue = function(name, widget) {
        // Don't need to mark colliders dirty when in placement mode, they don't get created
        // until the Doodad is actually placed.
        const dirty = this._editor.inPlacementMode() ? undefined : true;
        switch (name) {
        case 'hideOnOverlap':
            this._doodad.hideOnOverlap = widget.checked();
            break;
        case 'event':
            this._doodad.eventId = this._eventDropDownIds.get(widget.value());
            break;
        case 'collider':
            const type = widget.value();
            const sprite = this._editor.currentDoodadSprite();
            let anchor;
            switch (type) {
            case 'None':
                this._doodad.collider = {};
                break;
            case 'Circle':
                // When making a new circle, adjust it so it envelops the doodad by default
                const radius = Math.max(sprite.width, sprite.height) / 2;
                anchor = new PIXI.Point(0, -sprite.height/2);
                this._doodad.collider = {type: type, radius: radius, anchor: anchor};
                this.updateAllValues();
                break;
            case 'Rectangle':
                // When making a new rectangle, adjust it so it envelops the doodad by default
                const width = sprite.width;
                const height = sprite.height;
                const angle = 0;
                anchor = new PIXI.Point(-width/2, -(sprite.height/2 + height/2));
                this._doodad.collider = {type: type, width: width, height: height, angle: angle, anchor: anchor};
                this.updateAllValues();
                break;
            }
            this.updateCollisionWindowVisibility();
            this.refresh();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderAnchorx':
            this._doodad.collider.anchor.x = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderAnchory':
            this._doodad.collider.anchor.y = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderRadius':
            this._doodad.collider.radius = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderWidth':
            this._doodad.collider.width = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderHeight':
            this._doodad.collider.height = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        case 'colliderAngle':
            this._doodad.collider.angle = widget.value();
            this._doodad.collider.dirty = dirty;
            break;
        }
        Knight.EDITOR.Window_DoodadProperties_updateValue.call(this, name, widget);
    };

    Knight.EDITOR.Window_DoodadProperties_createWindows = Knight.Editor.Window_DoodadProperties.prototype.createWindows;
    Knight.Editor.Window_DoodadProperties.prototype.createWindows = function() {
        Knight.EDITOR.Window_DoodadProperties_createWindows.call(this);
        const spacing = this.compactMode() ? 54 : 60;
        const x = this.x + 15;
        let y = this._commandWindow.y + this._commandWindow.height + (this.compactMode() ? 5 : 10);
        const colliderTypes = ["None", "Circle", "Rectangle"];
        this._colliderWindowGroups = {"All": []};
        colliderTypes.forEach((type) => this._colliderWindowGroups[type] = []);

        //////////////////////////////////////////////////////////////////
        // Page 4: Collision
        //////////////////////////////////////////////////////////////////
        page = "Collision";

        this._hideOnOverlap = new Knight.EditorCheckbox(x, y, 'Check_On', 'Check_Off', 'Check_Hover', true, 'Hide When Obscuring Players');
        this._hideOnOverlap.width = 350;
        this._hideOnOverlap.createContents();
        this._hideOnOverlap.uncheck();
        this._hideOnOverlap.refresh();
        this._hideOnOverlap.setHandler('onChange', this.updateValue.bind(this, 'hideOnOverlap', this._hideOnOverlap));
        this._windows.push(this._hideOnOverlap);
        this._windowGroups[page].push(this._hideOnOverlap);
        y += spacing * 0.5;

        // EventID
        const dropWidth = 245;
        this._eventDropDownIds = new Map();
        const eventNames = $gameMap.events().map(function(value) {
            const event = value.event();
            str = `${event.id}: ${event.name}`;
            this._eventDropDownIds.set(str, event.id);
            return str;
        }, this);
        eventNames.unshift("None");
        this._eventDropDownIds.set("None", undefined);
        this._eventDropDown = new Knight.EditorDropDown(x, y, dropWidth, eventNames, 0, "Event");
        this._eventDropDown.setHandler('onChange', this.updateValue.bind(this, 'event', this._eventDropDown));
        y += spacing;

        // Collider Type
        this._colliderDropDown = new Knight.EditorDropDown(x, y, dropWidth, colliderTypes, 0, "Collider");
        this._colliderDropDown.setHandler('onChange', this.updateValue.bind(this, 'collider', this._colliderDropDown));
        y += spacing;

        // Anchor
        const anchorMax = 192;
        const anchorMin = -anchorMax;
        this._colliderAnchorX = new Knight.EditorSlider(x, y, anchorMin, anchorMax, 0, "Anchor", null, null, true);
        this._colliderAnchorY = new Knight.EditorSlider(x, y + 52, anchorMin, anchorMax, 0, null, null, null, true);
        this._colliderAnchorX.setHandler('onChange', this.updateValue.bind(this, 'colliderAnchorx', this._colliderAnchorX));
        this._colliderAnchorY.setHandler('onChange', this.updateValue.bind(this, 'colliderAnchory', this._colliderAnchorY));
        this._windows.push(this._colliderAnchorX);
        this._windows.push(this._colliderAnchorY);
        this._windowGroups[page].push(this._colliderAnchorX);
        this._windowGroups[page].push(this._colliderAnchorY);
        this._colliderWindowGroups["All"].push(this._colliderAnchorX);
        this._colliderWindowGroups["All"].push(this._colliderAnchorY);
        this._colliderWindowGroups["Circle"].push(this._colliderAnchorX);
        this._colliderWindowGroups["Circle"].push(this._colliderAnchorY);
        this._colliderWindowGroups["Rectangle"].push(this._colliderAnchorX);
        this._colliderWindowGroups["Rectangle"].push(this._colliderAnchorY);
        y += spacing * 1.3;

        // Circle Options
        const radiusMax = anchorMax / 2;
        this._colliderRadius = new Knight.EditorSlider(x, y, 1, radiusMax, 22, "Radius", null, null, true);
        this._colliderRadius.setHandler('onChange', this.updateValue.bind(this, 'colliderRadius', this._colliderRadius));
        this._windows.push(this._colliderRadius);
        this._windowGroups[page].push(this._colliderRadius);
        this._colliderWindowGroups["All"].push(this._colliderRadius);
        this._colliderWindowGroups["Circle"].push(this._colliderRadius);

        // Rectangle Options
        this._colliderWidth = new Knight.EditorSlider(x, y, 1, anchorMax, 48, "Width", null, null, true);
        this._colliderWidth.setHandler('onChange', this.updateValue.bind(this, 'colliderWidth', this._colliderWidth));
        this._windows.push(this._colliderWidth);
        this._windowGroups[page].push(this._colliderWidth);
        this._colliderWindowGroups["All"].push(this._colliderWidth);
        this._colliderWindowGroups["Rectangle"].push(this._colliderWidth);
        y += spacing;

        this._colliderHeight = new Knight.EditorSlider(x, y, 1, anchorMax, 48, "Height", null, null, true);
        this._colliderHeight.setHandler('onChange', this.updateValue.bind(this, 'colliderHeight', this._colliderHeight));
        this._windows.push(this._colliderHeight);
        this._windowGroups[page].push(this._colliderHeight);
        this._colliderWindowGroups["All"].push(this._colliderHeight);
        this._colliderWindowGroups["Rectangle"].push(this._colliderHeight);
        y += spacing;

        this._colliderAngle = new Knight.EditorSlider(x, y, 0, 359, 0, "Angle", null, null, true);
        this._colliderAngle.setHandler('onChange', this.updateValue.bind(this, 'colliderAngle', this._colliderAngle));
        this._windows.push(this._colliderAngle);
        this._windowGroups[page].push(this._colliderAngle);
        this._colliderWindowGroups["All"].push(this._colliderAngle);
        this._colliderWindowGroups["Rectangle"].push(this._colliderAngle);
        y += spacing;

        // Add Drop-down last so it draws above other windows
        this._windows.push(this._colliderDropDown);
        this._windowGroups[page].push(this._colliderDropDown);
        this._colliderWindowGroups["All"].push(this._colliderDropDown);
        this._colliderWindowGroups["None"].push(this._colliderDropDown);
        this._colliderWindowGroups["Circle"].push(this._colliderDropDown);
        this._colliderWindowGroups["Rectangle"].push(this._colliderDropDown);

        this._windows.push(this._eventDropDown);
        this._windowGroups[page].push(this._eventDropDown);
    };

    Knight.EDITOR.Window_DoodadProperties_updateWindowGroupVisibility = Knight.Editor.Window_DoodadProperties.prototype.updateWindowGroupVisibility;
    Knight.Editor.Window_DoodadProperties.prototype.updateWindowGroupVisibility = function(group) {
        Knight.EDITOR.Window_DoodadProperties_updateWindowGroupVisibility.call(this, group);
        this.updateCollisionWindowVisibility();
    };

    Knight.Editor.Window_DoodadProperties.prototype.updateCollisionWindowVisibility = function() {
        for (const window of this._colliderWindowGroups["All"]) {
            window.hide();
            window.disable();
            window.deactivate();
        }
        if (this._activeGroup === "Collision") {
            const type = this._colliderDropDown.value();
            for (const window of this._colliderWindowGroups[type]) {
                window.show();
                window.enable();
                window.activate();
            }
        }
    };

    Knight.EDITOR.Window_DoodadProperties_show = Knight.Editor.Window_DoodadProperties.prototype.show;
    Knight.Editor.Window_DoodadProperties.prototype.show = function() {
        Knight.EDITOR.Window_DoodadProperties_show.call(this);
        for (const window of this._colliderWindowGroups["All"]) {
            window.hide();
        }
        if (this._activeGroup === "Collision") {
            const type = this._colliderDropDown.value();
            for (const window of this._colliderWindowGroups[type]) {
                window.show();
            }
        }
    };

    Knight.EDITOR.Window_DoodadProperties_enable = Knight.Editor.Window_DoodadProperties.prototype.enable;
    Knight.Editor.Window_DoodadProperties.prototype.enable = function() {
        Knight.EDITOR.Window_DoodadProperties_enable.call(this);
        for (const window of this._colliderWindowGroups["All"]) {
            window.disable();
        }
        if (this._activeGroup === "Collision") {
            const type = this._colliderDropDown.value();
            for (const window of this._colliderWindowGroups[type]) {
                window.enable();
            }
        }
    };

    Knight.EDITOR.Window_DoodadProperties_activate = Knight.Editor.Window_DoodadProperties.prototype.activate;
    Knight.Editor.Window_DoodadProperties.prototype.activate = function() {
        Knight.EDITOR.Window_DoodadProperties_activate.call(this);
        for (const window of this._colliderWindowGroups["All"]) {
            window.deactivate();
        }
        if (this._activeGroup === "Collision") {
            const type = this._colliderDropDown.value();
            for (const window of this._colliderWindowGroups[type]) {
                window.activate();
            }
        }
    };

    Knight.EDITOR.Window_DoodadProperties_drawProperties = Knight.Editor.Window_DoodadProperties.prototype.drawProperties;
    Knight.Editor.Window_DoodadProperties.prototype.drawProperties = function() {
        Knight.EDITOR.Window_DoodadProperties_drawProperties.call(this);
        let x; let y;
        if (this._colliderAnchorX.visible) {
            x = this._colliderAnchorX.x - this.x - 3;
            y = this._colliderAnchorX.y - this.y + this._colliderAnchorX.barTextY();
            this.drawText('X', x, y, this.width, 'left');
            x = this._colliderAnchorY.x - this.x - 3;
            y = this._colliderAnchorY.y - this.y + this._colliderAnchorY.barTextY();
            this.drawText('Y', x, y, this.width, 'left');
        }
    };

    //=============================================================================
    // Collider Property Window
    //=============================================================================
    Knight.Editor.Window_ColliderProperties = class extends Knight.Editor.Window_DoodadProperties {
        /**
         *Creates an instance of Window_ColliderProperties.
        *
        * @param {Number} x
        * @param {Number} y
        * @param {Number} width
        * @param {Number} height
        * @param {KnightEditor} editor
        */
        constructor(x, y, width, height, editor) {
            super(x, y, width, height, editor);
            this._collider = null;
        }

        /**
         * @static
         * @return {Array<String>}
         */
        windowGroups() {
            return ["Collision"];
        }

        /**
         * @param {Object} collider
         */
        setCollider(collider) {
            if (this._collider !== collider) {
                this._collider = collider;
                this.updateAllValues();
                this.refresh();
            }
        }

        /**
         */
        updateAllValues() {
            if (this._collider) {
                this._layer.setValue(this._collider.z);
                DoodadManager._gridLockMode ? this._gridLock.check(false) : this._gridLock.uncheck(false);
                this._gridLockX.setValue(DoodadManager.gridLockX());
                this._gridLockY.setValue(DoodadManager.gridLockY());
                this._colliderDropDown.setValue(this._collider.type);
                switch (this._collider.type) {
                case 'Circle':
                    this._colliderRadius.setValue(this._collider.radius);
                    break;
                case 'Rectangle':
                    this._colliderWidth.setValue(this._collider.width);
                    this._colliderHeight.setValue(this._collider.height);
                    this._colliderAngle.setValue(this._collider.angle);
                    break;
                }
                this.updateCollisionWindowVisibility();
            }
        }

        /**
         * @param {*} name
         * @param {*} widget
         */
        updateValue(name, widget) {
            switch (name) {
            case 'layer':
                this._collider.z = widget.value();
                break;
            case 'gridLock':
                DoodadManager.setGridLockMode(widget.checked());
                break;
            case 'gridx':
                DoodadManager._gridLockX = widget.value();
                break;
            case 'gridy':
                DoodadManager._gridLockY = widget.value();
                break;
            case 'collider':
                this._collider.type = widget.value();
                switch (this._collider.type) {
                case 'Circle':
                    this._collider.radius = this._colliderRadius.value();
                    this._collider.width = undefined;
                    this._collider.height = undefined;
                    this._collider.angle = undefined;
                    break;
                case 'Rectangle':
                    this._collider.width = this._colliderWidth.value();
                    this._collider.height = this._colliderHeight.value();
                    this._collider.angle = this._colliderAngle.value();
                    this._collider.radius = undefined;
                    break;
                }
                this.updateCollisionWindowVisibility();
                this.refresh();
                break;
            case 'colliderRadius':
                this._collider.radius = widget.value();
                break;
            case 'colliderWidth':
                this._collider.width = widget.value();
                break;
            case 'colliderHeight':
                this._collider.height = widget.value();
                break;
            case 'colliderAngle':
                this._collider.angle = widget.value();
                break;
            }
            this.onChange();
        }

        /**
         */
        layerDecr() {
            this._layer.setValue(this._collider.z - 1);
            this._collider.z = this._layer.value();
            this.onChange();
        }

        /**
         */
        layerIncr() {
            this._layer.setValue(this._collider.z + 1);
            this._collider.z = this._layer.value();
            this.onChange();
        }

        /**
         */
        createWindows() {
            const groupNames = this.windowGroups();

            // Pages
            const cy = this.y + (this.compactMode() ? 40 : 90);
            this._commandWindow = new Knight.Editor.Window_EditorPageCommand(this.x, cy, this);
            this._commandWindow.refresh();
            this._windows.push(this._commandWindow);
            for (const group in this._windowGroups) {
                if (this._windowGroups.hasOwnProperty(group)) this._windowGroups[group].push(this._commandWindow);
            }
            this._commandWindow.setHandler('ok', this.updateWindowGroup.bind(this));

            const spacing = this.compactMode() ? 54 : 60;
            const x = this.x + 15;
            let y = this._commandWindow.y + this._commandWindow.height + (this.compactMode() ? 5 : 10);

            const colliderTypes = ["Circle", "Rectangle"];
            this._colliderWindowGroups = {"All": []};
            colliderTypes.forEach((type) => this._colliderWindowGroups[type] = []);

            //////////////////////////////////////////////////////////////////
            // Page 1: Position
            //////////////////////////////////////////////////////////////////
            const page = groupNames[0];

            // layer
            const layerNames = ["Lowest", "Low", "Below", "Same", "Above", "5", "6", "7", "8", "9", "Highest"];
            this._layer = new Knight.EditorSlider(x, y, 0, layerNames.length-1, 5, "Layer", layerNames, null, true);
            this._layer.setHandler('onChange', this.updateValue.bind(this, 'layer', this._layer));
            this._windows.push(this._layer);
            this._windowGroups[page].push(this._layer);
            y += spacing;

            // grid lock
            const gridMin = 1;
            const gridMax = 240;
            const gridNames = [];
            for (let i = gridMin; i <= gridMax; ++i) {
                gridNames[i] = i.toString();
            }
            this._gridLock = new Knight.EditorCheckbox(x, y, 'Check_On', 'Check_Off', 'Check_Hover', true, 'Grid Lock');
            this._gridLock.width = 350;
            this._gridLock.createContents();
            this._gridLock.uncheck();
            this._gridLock.refresh();
            this._gridLock.setHandler('onChange', this.updateValue.bind(this, 'gridLock', this._gridLock));
            this._windows.push(this._gridLock);
            this._windowGroups[page].push(this._gridLock);
            this._gridLockX = new Knight.EditorSlider(x, y + 22, gridMin, gridNames.length-1, Yanfly.Param.GFDGridWidth, null, gridNames, null, true);
            this._gridLockY = new Knight.EditorSlider(x, y + 44, gridMin, gridNames.length-1, Yanfly.Param.GFDGridHeight, null, gridNames, null, true);
            this._gridLockX.setHandler('onChange', this.updateValue.bind(this, 'gridx', this._gridLockX));
            this._gridLockY.setHandler('onChange', this.updateValue.bind(this, 'gridy', this._gridLockY));
            this._windows.push(this._gridLockX);
            this._windows.push(this._gridLockY);
            this._windowGroups[page].push(this._gridLockX);
            this._windowGroups[page].push(this._gridLockY);
            y += spacing * 1.3;

            // Collider Type
            const dropWidth = 245;
            this._colliderDropDown = new Knight.EditorDropDown(x, y, dropWidth, colliderTypes, 0, "Collider");
            this._colliderDropDown.setHandler('onChange', this.updateValue.bind(this, 'collider', this._colliderDropDown));
            y += spacing;

            // Circle Options
            const maxWidth = 600;
            const radiusMax = maxWidth / 2;
            this._colliderRadius = new Knight.EditorSlider(x, y, 1, radiusMax, 22, "Radius", null, null, true);
            this._colliderRadius.setHandler('onChange', this.updateValue.bind(this, 'colliderRadius', this._colliderRadius));
            this._windows.push(this._colliderRadius);
            this._windowGroups[page].push(this._colliderRadius);
            this._colliderWindowGroups["All"].push(this._colliderRadius);
            this._colliderWindowGroups["Circle"].push(this._colliderRadius);

            // Rectangle Options
            this._colliderWidth = new Knight.EditorSlider(x, y, 1, maxWidth, 48, "Width", null, null, true);
            this._colliderWidth.setHandler('onChange', this.updateValue.bind(this, 'colliderWidth', this._colliderWidth));
            this._windows.push(this._colliderWidth);
            this._windowGroups[page].push(this._colliderWidth);
            this._colliderWindowGroups["All"].push(this._colliderWidth);
            this._colliderWindowGroups["Rectangle"].push(this._colliderWidth);
            y += spacing;

            this._colliderHeight = new Knight.EditorSlider(x, y, 1, maxWidth, 48, "Height", null, null, true);
            this._colliderHeight.setHandler('onChange', this.updateValue.bind(this, 'colliderHeight', this._colliderHeight));
            this._windows.push(this._colliderHeight);
            this._windowGroups[page].push(this._colliderHeight);
            this._colliderWindowGroups["All"].push(this._colliderHeight);
            this._colliderWindowGroups["Rectangle"].push(this._colliderHeight);
            y += spacing;

            this._colliderAngle = new Knight.EditorSlider(x, y, 0, 359, 0, "Angle", null, null, true);
            this._colliderAngle.setHandler('onChange', this.updateValue.bind(this, 'colliderAngle', this._colliderAngle));
            this._windows.push(this._colliderAngle);
            this._windowGroups[page].push(this._colliderAngle);
            this._colliderWindowGroups["All"].push(this._colliderAngle);
            this._colliderWindowGroups["Rectangle"].push(this._colliderAngle);
            y += spacing;

            // Add Drop-down last so it draws above other windows
            this._windows.push(this._colliderDropDown);
            this._windowGroups[page].push(this._colliderDropDown);
            this._colliderWindowGroups["All"].push(this._colliderDropDown);
            this._colliderWindowGroups["Circle"].push(this._colliderDropDown);
            this._colliderWindowGroups["Rectangle"].push(this._colliderDropDown);

            //////////////////////////////////////////////////////////////////
            // Buttons
            //////////////////////////////////////////////////////////////////
            const buttonSpacing = 28;
            const buttonY = this.y + 6;
            const buttonNames = ['Default', 'Copy', 'Delete'];
            let buttonX = this.x + this.width - (buttonSpacing * buttonNames.length);
            for (let i = 0; i < buttonNames.length; ++i) {
                const name = buttonNames[i];
                const button = new Knight.Button(buttonX, buttonY, name, `${name}_Hover`);
                button.setOkSound(null);
                this._windows.push(button);
                this._buttons.push(button);
                button.setHandler('ok', this[name.toLowerCase()].bind(this));
                buttonX += buttonSpacing;
            }
        }

        /**
         */
        default() {
            const defaultCollider = ColliderManager.getTemplate();
            const ignoreList = ["x", "y"];
            for (const property in defaultCollider) {
                if (ignoreList.includes(property)) continue;
                if (this._collider.hasOwnProperty(property)) {
                    this._collider[property] = defaultCollider[property];
                }
            }
            DoodadManager.setGridLockMode(false);
            DoodadManager._gridLockX = Yanfly.Param.GFDGridWidth;
            DoodadManager._gridLockY = Yanfly.Param.GFDGridHeight;
            this.updateAllValues();
            this.onChange();
            this._editor.playSound(Knight.Param.KESEReset);
        }

        /**
         */
        refresh() {
            if (this.contents) {
                this.contents.clear();
                this.drawBackground();
                if (this._collider) {
                    this.drawProperties();
                }
            }
        }

        /**
         */
        drawProperties() {
            this.contents.fontFace = Knight.Param.KEPanelFont;
            this.contents.fontSize = 20;
            this.contents.outlineWidth = 4;
            this.contents.outlineColor = 'rgba(0, 0, 0, 0.5)';
            this.drawText(`${this._collider.type}`, 0, this.compactMode() ? 0 : 20, this.width, 'center');

            this.contents.fontSize = this.compactMode() ? 12 : 16;
            this.drawText(`X: ${this._collider.x}`, this.compactMode() ? 10 : 110, this.compactMode() ? 0 : 50, this.width/2, 'left');
            this.drawText(`Y: ${this._collider.y}`, this.compactMode() ? 50 : 180, this.compactMode() ? 0 : 50, this.width/2, 'left');

            let x; let y;
            if (this.windows().includes(this._gridLock)) {
                x = this._gridLockX.x - this.x - 3;
                y = this._gridLockX.y - this.y + this._gridLockX.barTextY();
                this.drawText('W', x, y, this.width, 'left');
                x = this._gridLockY.x - this.x - 3;
                y = this._gridLockY.y - this.y + this._gridLockY.barTextY();
                this.drawText('H', x, y, this.width, 'left');
            }
        }
    };
};
