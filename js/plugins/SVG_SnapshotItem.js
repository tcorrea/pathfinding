// =============================================================================
// SVG_SnapshotItem.js
// Version:     1.0
// Date:        1/21/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Contains a [x1,y1,x2,y2] or [x,y] and a color.
 *
 * @class SnapshotItem
 */
Knight.PATH.SnapshotItem = class {
    /**
     *Creates an instance of SnapshotItem.
     * @param {Array<Number>} path
     * @param {String} color
     * @memberof SnapshotItem
     */
    constructor(path, color) {
        this.path = path;
        this.color = color;
    }

    /**
     * @static
     * @param {Array<Number>} path
     * @param {String} [color=null]
     * @return {SnapshotItem}
     * @memberof SnapshotItem
     */
    static generate(path, color = null) {
        return this.getCached(new SnapshotItem(path, color));
    }

    /**
     * @static
     * @memberof SnapshotItem
     */
    static clearCached() {
        if (this._cached === null) return;
        this._cached.clear();
        this._cached = null;
    }

    /**
     * @static
     * @param {SnapshotItem} item
     * @return {SnapshotItem}
     * @memberof SnapshotItem
     */
    static getCached(item) {
        if (this._cached === null) {
            this._cached = new Map();
        }
        const get = this._cached.get(item);
        if (get) {
            return get;
        } else {
            this._cached.set(get, get);
            return item;
        }
    }

    /**
     * @param {SnapshotItem} obj
     * @return {Boolean}
     * @memberof SnapshotItem
     */
    equals(obj) {
        if (this === obj) return true;
        if (obj === null) return false;
        if (!(obj instanceof SnapshotItem)) return false;
        if (color === null && obj.color !== null) return false;
        if (color !== obj.color) return false;
        return this.pathEquals(obj.path);
    }

    /**
     * @param {Array<Number>} otherPath
     * @return {Boolean}
     * @memberof SnapshotItem
     */
    pathEquals(otherPath) {
        if (this.path.length !== otherPath.length) return false;
        return this.path.every((x, index) => x === otherPath[index]);
    }
};

Knight.PATH.SnapshotItem._cached = null;
