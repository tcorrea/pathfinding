// =============================================================================
// KNT_CollisionViewer
// Version:     1.0
// Date:        1/2/2020
// Author:      Kaelan
// =============================================================================

//=============================================================================
/*:
 * @plugindesc Draws active collision data.
 * @version 1.0.0
 * @author Kaelan
 *
 * @param Enabled
 * @desc Whether collision viewer should be enabled by default. F10 to toggle in-game.
 * @type Boolean
 * @default true
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * Enables pixel movement
 */

var Imported = Imported || {}; // eslint-disable-line no-var
Imported.KNT_CollisionViewer = '1.0.0';

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.COLLISION = Knight.COLLISION || {};

//=============================================================================
// Parameter Variables & Plugin Initialization
//=============================================================================
Knight.Param = Knight.Param || {};
Knight.Param.COLLISION = PluginManager.parameters('KNT_CollisionViewer') || {};
Knight.Param.COLLISION.Default = eval(Knight.Param.COLLISION['Enabled']);

Knight.COLLISION.debugDraw = Knight.Param.COLLISION.Default;
Knight.COLLISION.constructed = [];
Knight.COLLISION.called = [];

//=============================================================================
// Hack to monkey patch class constructors
// From: https://codereview.stackexchange.com/questions/20400/monkey-patching-native-javascript-constructors
//=============================================================================
/* eslint-disable no-invalid-this */
/* eslint-disable no-multi-spaces */
Knight.COLLISION.patchConstructor = (function(global) {
    "use strict";
    const has = Object.prototype.hasOwnProperty;
    const slice = Array.prototype.slice;
    const bind = Function.bind;
    return function(original, originalRef, newName, patches) {
        global[originalRef] = original; // Maintain a reference to the original constructor as a new property on the global object
        let newRef; // This will be the new patched constructor

        patches.called = patches.called || originalRef; // If we are not patching static calls just pass them through to the original function

        if (patches.constructed) { // This string is evaluated to create the patched constructor body in the case that we are patching newed calls
            // Nasty hack to make the debugger show the correct object type names. Overwrite the constructor name by abusing new Function() behavior
            // This should be OK because CollisionViewer code won't be shipped in final build anyway. This is a dev-only hack.
            Knight.COLLISION.constructed[newName] = (this && this !== global ? patches.constructed : patches.called);
            const str = "return function " + newName + "(...args) { return (Knight.COLLISION.constructed[\'"+ newName +"\']).apply(this, args); }";
            newRef = new Function(str)();
        } else { // This string is evaluated to create the patched constructor body in the case that we are only patching static calls
            newRef = function(...args) {
                return this && this !== global  ? new (bind.apply(original, [].concat({}, slice.call(args)))) :
                    patches.called.apply(this, args);
            };
        }

        // Create a new function to wrap the patched constructor
        // Keep a reference to the original prototype to ensure instances of the patch appear as instances of the original
        newRef.prototype = original.prototype;
        // Ensure the constructor of patched instances is the patched constructor
        newRef.prototype.constructor = newRef;

        // Add any "static" properties of the original constructor to the patched one
        Object.getOwnPropertyNames(original).forEach(function(property) {
            // Don't include static properties of Function since the patched constructor will already have them
            if (!has.call(Function, property)) {
                newRef[property] = original[property];
            }
        });

        return newRef; // Return the patched constructor
    };
})(this);
/* eslint-enable no-invalid-this */
/* eslint-enable no-multi-spaces */

//=============================================================================
// Utility Functions
//=============================================================================
// Unfortunately the constructor monkey-patching doesn't work with inheritance,
// so we need to duplicate the call to patch() for each subclass to get it to
// work. We can at least avoid copy-pasting the constructor logic itself by
// factoring it out into a common static function.
Knight.COLLISION.initBody = function(body, color = '0xff0000') {
    body._context = new PIXI.Graphics();
    body._drawColor = color;
    body.createGraphics();
    body.addToScene();
};

//=============================================================================
// Collisions
//=============================================================================
Sinova.Collisions.prototype.createCircle = function(x = 0, y = 0, radius = 0, scale = 1, padding = 0, color = '0xff0000') {
    const body = new Sinova.Circle(x, y, radius, scale, padding, color);
    this._bvh.insert(body);
    return body;
};

Sinova.Collisions.prototype.createPolygon = function(x = 0, y = 0, points = [[0, 0]],
    angle = 0, scale_x = 1, scale_y = 1, padding = 0, color = '0xff0000') {
    const body = new Sinova.Polygon(x, y, points, angle, scale_x, scale_y, padding, color);
    this._bvh.insert(body);
    return body;
};

Sinova.Collisions.prototype.createPoint = function(x = 0, y = 0, padding = 0, color = '0xff0000') {
    const body = new Sinova.Point(x, y, padding, color);
    this._bvh.insert(body);
    return body;
};

Sinova.Collisions.prototype.createRectangle = function(x = 0, y = 0, width = 1, height = 1,
    angle = 0, scale_x = 1, scale_y = 1, padding = 0, color = '0xff0000') {
    const body = new Sinova.Rectangle(x, y, width, height, angle, scale_x, scale_y, padding, color);
    this._bvh.insert(body);
    return body;
};

Sinova.Collisions.prototype.updateScene = function() {
    for (const body of this._bvh._bodies) {
        body.addToScene();
    }
};

Sinova.Collisions.prototype.onMapLoad = function() {
    this._bvh.onMapLoad();
};

//=============================================================================
// Body
//=============================================================================
// Called in the constructor of inherited classes
Sinova.Body.prototype.createGraphics = function() {
    const x = this.x;
    const y = this.y;
    this.x = 0; // Always draw at the origin
    this.y = 0;
    this._context.clear();
    this._context.lineStyle(1, this._drawColor, 0.5);
    this._context.beginFill(this._drawColor, 0.25);
    this.draw(this._context);
    this._context.endFill();
    this.x = x;
    this.y = y;
    this.updateGraphicsVisibility();
};

Sinova.Body.prototype.updateGraphicsVisibility = function() {
    const parentVisible = (this.parent) ? (!this.parent.isHidden()) : true;
    this._context.visible = parentVisible && Knight.COLLISION.debugDraw;
};

Sinova.Body.prototype.updateGraphics = function() {
    const tw = $gameMap.tileWidth();
    const th = $gameMap.tileHeight();
    const screenX = Math.round(this.x - ($gameMap._displayX * tw));
    const screenY = Math.round(this.y - ($gameMap._displayY * th));
    this._context.position.set(screenX, screenY);
};

Sinova.Body.prototype.update = function() {
    this.updateGraphics();
};

Sinova.Body.prototype.addToScene = function() {
    SceneManager._scene.debugContainer().addChild(this._context);
};

Sinova.Body.prototype.clear = function() {
    SceneManager._scene.debugContainer().removeChild(this._context);
    this._context.destroy();
    this._context = null;
};

//=============================================================================
// BVH
//=============================================================================
Knight.COLLISION.BVH_update = Sinova.BVH.prototype.update;
Sinova.BVH.prototype.update = function() {
    Knight.COLLISION.BVH_update.call(this);

    // Debug position display
    if (Input.isTriggered(Knight.INPUT['tab'])) {
        Input.update();
        const x = TouchInput.x;
        const y = TouchInput.y;
        const g = Knight.COLLISION.pointToGridTile(x, y);
        const blocked = $gameMap.pathGrid().isBlocked(g.x, g.y);
        console.log(`x: ${x}, y: ${y} | Grid x: ${g.x}, y: ${g.y} | Blocked: ${blocked}`);
    }

    // Toggle visibility
    if (Input.isTriggered(Knight.INPUT['f7'])) {
        Input.update();
        Knight.COLLISION.debugDraw = !Knight.COLLISION.debugDraw;
    }

    if (this._collisionLastEnabled !== Knight.COLLISION.debugDraw) {
        this._bodies.forEach((body) => body.updateGraphicsVisibility());
    }

    // A bit of code duplication, but it's faster this way
    if (Knight.COLLISION.debugDraw) {
        this._bodies.forEach((body) => body.update());
    }

    this._collisionLastEnabled = Knight.COLLISION.debugDraw;
};

Sinova.BVH.prototype.onMapLoad = function() {
    // Force a visibility refresh
    this._collisionLastEnabled = null;
};

//=============================================================================
// Point
//=============================================================================
Sinova.Point = Knight.COLLISION.patchConstructor(Sinova.Point, "SinovaPointOriginal", "Point", {
    constructed: function(...args) {
        const obj = new (Function.prototype.bind.apply(SinovaPointOriginal, [{}].concat(args)));
        Knight.COLLISION.initBody(obj, args[args.length - 1]);
        return obj;
    },
});

//=============================================================================
// Circle
//=============================================================================
Sinova.Circle = Knight.COLLISION.patchConstructor(Sinova.Circle, "SinovaCircleOriginal", "Circle", {
    constructed: function(...args) {
        const obj = new (Function.prototype.bind.apply(SinovaCircleOriginal, [{}].concat(args)));
        Knight.COLLISION.initBody(obj, args[args.length - 1]);
        return obj;
    },
});

//=============================================================================
// Polygon
//=============================================================================
Sinova.Polygon = Knight.COLLISION.patchConstructor(Sinova.Polygon, "SinovaPolygonOriginal", "Polygon", {
    constructed: function(...args) {
        const obj = new (Function.prototype.bind.apply(SinovaPolygonOriginal, [{}].concat(args)));
        Knight.COLLISION.initBody(obj, args[args.length - 1]);
        return obj;
    },
});

//=============================================================================
// Rectangle
//=============================================================================
Sinova.Rectangle = Knight.COLLISION.patchConstructor(Sinova.Rectangle, "SinovaRectangleOriginal", "Rectangle", {
    constructed: function(...args) {
        const obj = new (Function.prototype.bind.apply(SinovaRectangleOriginal, [{}].concat(args)));
        Knight.COLLISION.initBody(obj, args[args.length - 1]);
        return obj;
    },
});
