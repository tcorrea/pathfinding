// =============================================================================
// KNT_Utility.js
// Version:     1.0
// Date:        1/21/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
let testGrid;

/**
 * Polyfill for Java's Arrays.copyOf() function. Clones an array with a new size,
 * inserting zeroes in all of the new empty spots, if any.
 * @param {Array} orig
 * @param {Number} newSize
 * @return {Array}
 */
Knight.arraysCopyOf = function(orig, newSize) {
    const arr = new Array(newSize).fill(0);
    const copyLength = Math.min(orig.length, newSize);
    for (let i = 0; i < copyLength; ++i) {
        arr[i] = orig[i];
    }
    return arr;
};

/**
 * A simple class that extends the default PIXI container with a few utility functions.
 * @class Container
 */
Knight.Container = class extends PIXI.Container {
    /**
     */
    update() {
        let i = this.children.length;
        while (i--) {
            if (this.children[i].update) {
                this.children[i].update();
            }
        }
    }
};

/**
 * @class Utility
 */
Knight.Utility = class {
    /**
     * @static
     * @param {*} a
     * @param {*} b
     * @param {String} name
     */
    static swapValue(a, b, name) {
        const temp = a[name];
        a[name] = b[name];
        b[name] = temp;
    };


    /**
     * Returns coordinates on an ellipse
     * @static
     * @param {Number} a        Ellipse horizontal half-width
     * @param {Number} b        Ellipse vertical half-height
     * @param {Number} theta    Clockwise angle of the point, in degrees, in the range [0, 360].
     * @return {Object}
     */
    static getEllipsePoint(a, b, theta) {
        return {
            x: this.getEllipseX(a, b, theta),
            y: this.getEllipseY(a, b, theta),
        };
    }

    /**
     * Returns X coordinate on an ellipse
     * @static
     * @param {Number} a        Ellipse horizontal half-width
     * @param {Number} b        Ellipse vertical half-height
     * @param {Number} theta    Clockwise angle of the point, in degrees, in the range [0, 360].
     * @return {Object}
     */
    static getEllipseX(a, b, theta) {
        const tan = Math.tan(theta * Math.PI / 180);
        let ret = (a * b) / Math.sqrt(b*b + (a*a*tan*tan));
        if (theta > 90 && theta <= 270) ret *= -1;
        return ret;
    }

    /**
     * Returns Y coordinate on an ellipse
     * @static
     * @param {Number} a        Ellipse horizontal half-width
     * @param {Number} b        Ellipse vertical half-height
     * @param {Number} theta    Clockwise angle of the point, in degrees, in the range [0, 360].
     * @return {Object}
     */
    static getEllipseY(a, b, theta) {
        const tan = Math.tan(theta * Math.PI / 180);
        let ret = (a * b * tan) / Math.sqrt(b*b + (a*a*tan*tan));
        if (theta > 90 && theta <= 270) ret *= -1;
        return ret;
    }

    /**
     * Count number of trailing zeros. For compatibility with the expected Java output,
     * this assumes @param v is a 32-bit integer. When v is 0, returns 32.
     *
     * Based on implementation by Mikola Lysenko:
     * https://github.com/mikolalysenko/count-trailing-zeros
     *
     * The original implementation is released under the MIT License.
     *
     * @param {Number} v
     * @return {Number}
     * @memberof Utility
     */
    static numberOfTrailingZeros(v) {
        let c = 32;
        v &= -v;
        if (v) c--;
        if (v & 0x0000FFFF) c -= 16;
        if (v & 0x00FF00FF) c -= 8;
        if (v & 0x0F0F0F0F) c -= 4;
        if (v & 0x33333333) c -= 2;
        if (v & 0x55555555) c -= 1;
        return c;
    }

    /**
     * Tests output of numberOfTrailingZeros()
     * @static
     * @param {Number} min
     * @param {Number} max
     */
    static trailingZeroTest(min = 0, max = 16) {
        for (let i = min; i <= max; ++i) {
            console.log(`i (bin ${i.toString(2)}) has ${this.numberOfTrailingZeros(i)} trailing zeros`);
        }
    }

    /**
     * Performs integer division on two numbers
     * @param {Number} a
     * @param {Number} b
     * @return {Number}
     * @memberof Utility
     */
    static intDivision(a, b) {
        return (a - a % b) / b;
    }

    /**
     * @static
     * @param {Array<Array<Number>>} path
     * @return {Array<Array<Number>>}
     * @memberof Utility
     */
    static removeDuplicatesInPath(path) {
        if (path.length <= 2) return path;

        const newPath = [];
        let index = 0;
        newPath[0] = path[0];
        for (let i = 1; i < path.length-1; ++i) {
            if (this.isCollinear(path[i][0], path[i][1], path[i+1][0], path[i+1][1], newPath[index][0], newPath[index][1])) {
                // skip
            } else {
                index++;
                newPath[index] = path[i];
            }
        }
        index++;
        newPath[index] = path[path.length-1];
        return newPath;
    }

    /**
     * @static
     * @param {Number} x1
     * @param {Number} y1
     * @param {Number} x2
     * @param {Number} y2
     * @param {Number} x3
     * @param {Number} y3
     * @return {Boolean}
     * @memberof Utility
     */
    static isCollinear(x1, y1, x2, y2, x3, y3) {
        return (y3-y1)*(x2-x1) === (x3-x1)*(y2-y1);
    }

    /**
     * Generates a path between two points on a grid.
     * @static
     * @param {PathFindingAlgorithm} algoFunction
     * @param {GridGraph} gridGraph
     * @param {Number} sx   Starting X coordinate
     * @param {Number} sy   Starting Y coordinate
     * @param {Number} ex   Destination X coordinate
     * @param {Number} ey   Destination Y coordinate
     * @return {Array<Array<Number>>} Array of Number[2] indicading the coordinates of the path
     * @memberof Utility
     */
    static generatePath(algoFunction, gridGraph, sx, sy, ex, ey) {
        const algo = algoFunction.getAlgo(gridGraph, sx, sy, ex, ey);
        algo.computePath();
        let path = algo.getPath();
        path = this.removeDuplicatesInPath(path);
        return path;
    }

    /**
     * Compute path using optimal offline algorithm. (preprocessing for faster pathfinding)
     * @static
     * @param {GridGraph} gridGraph
     * @param {Number} sx   Starting X coordinate
     * @param {Number} sy   Starting Y coordinate
     * @param {Number} ex   Destination X coordinate
     * @param {Number} ey   Destination Y coordinate
     * @return {Array<Array<Number>>} Array of Number[2] indicading the coordinates of the path
     * @memberof Utility
     */
    static computeOptimalPathOffline(gridGraph, sx, sy, ex, ey) {
        console.time('ENLSVG Total Path Time');
        const algo = Knight.PATH.ENLSVGAlgorithm.graphReuse(gridGraph, sx, sy, ex, ey);
        algo.computePath();
        let path = algo.getPath();
        path = this.removeDuplicatesInPath(path);
        console.timeEnd('ENLSVG Total Path Time');
        return path;
    }

    /**
     * Compute path using optimal online algorithm. (no preprocessing)
     * @static
     * @param {GridGraph} gridGraph
     * @param {Number} sx   Starting X coordinate
     * @param {Number} sy   Starting Y coordinate
     * @param {Number} ex   Destination X coordinate
     * @param {Number} ey   Destination Y coordinate
     * @return {Array<Array<Number>>} Array of Number[2] indicading the coordinates of the path
     * @memberof Utility
     */
    static computeOptimalPathOnline(gridGraph, sx, sy, ex, ey) {
        console.time('Anya Total Path Time');
        const algo = new Knight.PATH.Anya(gridGraph, sx, sy, ex, ey);
        algo.computePath();
        let path = algo.getPath();
        path = this.removeDuplicatesInPath(path);
        console.timeEnd('Anya Total Path Time');
        return path;
    }

    /**
     * Compute path through a grid defined in the given file. Used for pathfind testing.
     * @static
     * @param {String} filename     Name of the file with grid data. File should be in the predefinedgrids/ folder.
     * @param {Number} sx           Path starting point
     * @param {Number} sy
     * @param {Number} ex           Path endpoint
     * @param {Number} ey
     * @param {Boolean} online      Decides which algorithm to use. True uses Anya, false uses ENLSVG.
     * @return {Array<Array<Number>>} Array of Number[2] indicading the coordinates of the path
     * @memberof Utility
     */
    static testPath(filename, sx, sy, ex, ey, online = true) {
        if (!testGrid) {
            if (filename) {
                this.testFileLoad(filename);
            } else {
                testGrid = new BitpackedGrid(10, 10);
                const blocked = [
                    [1, 1],
                    [3, 3],
                    [3, 4],
                    [3, 5],
                    [6, 1],
                    [6, 2],
                    [6, 4],
                    [6, 5],
                ];
                blocked.forEach((tile) => testGrid.setBlocked(tile[0], tile[1], true));
            }
        }
        return online ? this.computeOptimalPathOnline(testGrid, sx, sy, ex, ey) : this.computeOptimalPathOffline(testGrid, sx, sy, ex, ey);
    }

    /**
     * Loads a predefined grid into the pathfinding system from a text file. Used to test pathfinding behavior.
     * @static
     * @param {String} filename
     * @memberof Utility
     */
    static testFileLoad(filename) {
        const fs = require("fs");
        const raw = fs.readFileSync(`./predefinedgrids/${filename}.txt`, 'utf8');
        const lines = raw.split('\n');
        let numCol;
        let numRow;
        lines.forEach(function(line, row) {
            const nums = line.split(' ');
            if (row === 0) {
                numCol = parseInt(nums[0]); // sizeX = # of columns
                numRow = parseInt(nums[1]); // sizeY = # of rows
                testGrid = new BitpackedGrid(numCol, numRow);
                console.log(`new Graph ${numCol} ${numRow}`);
            } else {
                nums.forEach(function(num, col) {
                    if (parseInt(num) === 1) {
                        // X position is columns
                        // Y position is rows
                        testGrid.setBlocked(col, row-1, true);
                        // console.log(`setBlocked ${col} ${row-1}`);
                    }
                });
            }
        });
    }

    /**
     * @static
     * @param {GridGraph} gridGraph
     * @param {Array<Array<Number>>} path
     * @return {Boolean}
     * @memberof Utility
     */
    static isPathTaut(gridGraph, path) {
        let v1 = 0;
        let v2 = 0;
        for (let v3 = 2; v3 < path.length; ++v3) {
            if (!gridGraph.isTaut(path[v1][0], path[v1][1], path[v2][0], path[v2][1], path[v3][0], path[v3][1])) {
                return;
            }
            ++v1;
            ++v2;
        }
        return true;
    }

    /**
     * @static
     * @param {Number} length
     * @param {Number} optimalLength
     * @return {Boolean}
     * @memberof Utility
     */
    static isOptimal(length, optimalLength) {
        return (length - optimalLength) < 0.0001;
    }

    /**
     * Compute the length of a given path. (Using euclidean distance)
     * @static
     * @param {GridGraph} gridGraph
     * @param {Array<Array<Number>>} path
     * @return {Number}
     */
    static pathLength(gridGraph, path) {
        let pathLength = 0;
        for (let i = 0; i < path.length-1; i++) {
            pathLength += gridGraph.distance(path[i][0], path[i][1], path[i+1][0], path[i+1][1]);
        }
        return pathLength;
    }

    /**
     * @static
     * @memberof Utility
     */
    static cleanUpPreallocatedMemory() {
        Knight.PATH.Memory.clearMemory();
        Knight.PATH.Heap.clearMemory();
        Knight.PATH.LineOfSightScanner.clearMemory();
        Knight.PATH.ENLSVG.clearMemory();
        Knight.PATH.Anya.clearMemory();
        Knight.PATH.SnapshotItem.clearCached();
        global.gc();
    }
};
