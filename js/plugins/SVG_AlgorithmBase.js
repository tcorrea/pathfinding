// =============================================================================
// SVG_AlgorithmBase.js
// Version:     1.0
// Date:        1/11/2020
// Author:      Kaelan
// =============================================================================

var Knight = Knight || {}; // eslint-disable-line no-var
Knight.PATH = Knight.PATH || {};

/**
 * Template for all Pathfinding Algorithms.
 * @class Algorithm
 */
Knight.PATH.Algorithm = class {
    /**
     *Creates an instance of Algorithm.
     * @param {GridGraph} graph
     * @param {Number} sizeX
     * @param {Number} sizeY
     * @param {Number} sx
     * @param {Number} sy
     * @param {Number} ex
     * @param {Number} ey
     * @memberof Algorithm
     */
    constructor(graph, sizeX, sizeY, sx, sy, ex, ey) {
        this._snapshotCountdown = 0;
        this._graph = graph;
        this._sizeX = sizeX;
        this._sizeXplusOne = null;
        this._sizeY = sizeY;
        this._sx = sx;
        this._sy = sy;
        this._ex = ex;
        this._ey = ey;
        this._snapShotList = [];
        this._parent = null;
        this._ticketNumber = 1;
        this._recordingMode = null;
        this._usingStaticMemory = false;
    }

    /**
     *
     *
     * @param {Number} size
     * @param {Number} defaultDistance
     * @param {Number} defaultParent
     * @param {Boolean} defaultVisited
     * @memberof Algorithm
     */
    initializeMemory(size, defaultDistance, defaultParent, defaultVisited) {
        this._usingStaticMemory = true;
        this._ticketNumber = Knight.PATH.Memory.initialize(size, defaultDistance, defaultParent, defaultVisited);
    }

    /**
     * Call to start tracing the algorithm's operation.
     * @memberof Algorithm
     */
    startRecording() {
        this._recordingMode = true;
    }

    /**
     * Call to stop tracing the algorithm's operation.
     * @memberof Algorithm
     */
    stopRecording() {
        this._recordingMode = false;
    }

    /**
     * Retrieve the trace of the algorithm that has been recorded.
     * @return {Array<Array<SnapshotItem>>}
     * @memberof Algorithm
     */
    retrieveSnapshotList() {
        return this._snapShotList;
    }

    /**
     * Call this to compute the path.
     * @memberof Algorithm
     */
    computePath() {};

    /**
     * Retrieve the path computed by the algorithm
     * @return {Array<Array<Number>>}
     * @memberof Algorithm
     */
    getPath() {
        return [];
    };

    /**
     * Directly get path length without computing path. Has to run fast, unlike getPath.
     * @return {Number}
     * @memberof Algorithm
     */
    getPathLength() {
        return 0;
    };

    /**
     * An optimal overridable method which prints some statistics when called for.
     * @memberof Algorithm
     */
    printStatistics() {};

    /**
     * @param {Number} x
     * @param {Number} y
     * @return {Number}
     * @memberof Algorithm
     */
    toOneDimIndex(x, y) {
        return this._graph.toOneDimIndex(x, y);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof Algorithm
     */
    toTwoDimX(index) {
        return this._graph.toTwoDimX(index);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof Algorithm
     */
    toTwoDimY(index) {
        return this._graph.toTwoDimY(index);
    }

    /**
     * @memberof Algorithm
     */
    maybeSaveSearchSnapshot() {
        if (this._recordingMode) {
            if (this._usingStaticMemory && this._ticketNumber !== Knight.PATH.currentTicket()) {
                throw new Error("Ticket does not match!");
            }
            this.saveSearchSnapshot();
        }
    }

    /**
     * @return {Array<SnapshotItem>}
     * @memberof Algorithm
     */
    getCurrentSearchSnapshot() {
        return this.computeSearchSnapshot();
    }

    /**
     * @return {Boolean}
     * @memberof Algorithm
     */
    isRecording() {
        return this._recordingMode;
    }

    /**
     * @memberof Algorithm
     */
    saveSearchSnapshot() {
        if (this._snapshotCountdown > 0) {
            this._snapshotCountdown--;
            return;
        }
        this._snapshotCountdown = Knight.PATH.Algorithm.SNAPSHOT_INTERVAL;
        this._snapShotList.push(this.computeSearchSnapshot());
    }

    /**
     * @param {Array<SnapshotItem>} snapshotItemList
     * @memberof Algorithm
     */
    addSnapshot(snapshotItemList) {
        this._snapShotList.push(snapshotItemList);
    }

    /**
     * @return {Number}
     * @memberof Algorithm
     */
    goalParentIndex() {
        return this.toOneDimIndex(this._ex, this._ey);
    }

    /**
     * @param {Number} index
     * @return {Number}
     * @memberof Algorithm
     */
    getParent(index) {
        if (this._usingStaticMemory) return Knight.PATH.Memory.parent(index);
        else return this._parent[index];
    }

    /**
     * @param {Number} index
     * @param {Number} value
     * @return {Number}
     * @memberof Algorithm
     */
    setParent(index, value) {
        if (this._usingStaticMemory) return Knight.PATH.Memory.setParent(index, value);
        else this._parent[index] = value;
    }

    /**
     * @return {Number}
     * @memberof Algorithm
     */
    getSize() {
        if (this._usingStaticMemory) return Knight.PATH.Memory.size();
        else return this._parent.length;
    }

    /**
     * @return {Array<SnapshotItem>}
     * @memberof Algorithm
     */
    computeSearchSnapshot() {
        const list = [];
        let current = this.goalParentIndex();
        let finalPathSet = null;
        if (this.getParent(current) >= 0) {
            finalPathSet = new Set();
            while (current >= 0) {
                finalPathSet.add(current);
                current = this.getParent(current);
            }
        }

        size = this.getSize();
        for (let i = 0; i < size; ++i) {
            if (this.getParent(i) != -1) {
                if (finalPathSet !== null && finalPathSet.has(i)) {
                    list.push(Knight.PATH.SnapshotItem.generate(this.snapshotEdge(i), Knight.COLOR.BLUE));
                } else {
                    list.push(Knight.PATH.SnapshotItem.generate(this.snapshotEdge(i)));
                }
            }
            const vertexSnapshot = this.snapshotVertex(i);
            if (vertexSnapshot !== null) {
                list.push(Knight.PATH.SnapshotItem.generate(vertexSnapshot));
            }
        }
        return list;
    }

    /**
     * @param {Number} endIndex
     * @return {Array<Number>}
     * @memberof Algorithm
     */
    snapshotEdge(endIndex) {
        const edge = new Array(4);
        const startIndex = this.getParent(endIndex);
        edge[2] = this.toTwoDimX(endIndex);
        edge[3] = this.toTwoDimY(endIndex);
        if (startIndex < 0) {
            edge[0] = edge[2];
            edge[1] = edge[3];
        } else {
            edge[0] = this.toTwoDimX(startIndex);
            edge[1] = this.toTwoDimY(startIndex);
        }
        return edge;
    }

    /**
     * @param {Number} index
     * @return {Array<Number>}
     * @memberof Algorithm
     */
    snapshotVertex(index) {
        if (this.selected(index)) {
            const edge = [this.toTwoDimX(index), this.toTwoDimY(index)];
            return edge;
        }
        return null;
    }

    /**
     * @param {Number} index
     * @return {Boolean}
     * @memberof Algorithm
     */
    selected(index) {
        return false;
    }

    /**
     * @param {Algorithm} algo
     * @memberof Algorithm
     */
    inheritSnapshotListFrom(algo) {
        this._snapShotList = algo._snapShotList;
    }
};

Knight.PATH.Algorithm.SNAPSHOT_INTERVAL = 0;
