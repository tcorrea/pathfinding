// =============================================================================
// KNT_Color.js
// Version:     1.0
// Date:        1/11/2020
// Author:      Kaelan
// =============================================================================
/* eslint-disable key-spacing */

var Knight = Knight || {}; // eslint-disable-line no-var

Knight.COLOR = {
    WHITE:          '#ffffff',
    LIGHT:          '#fefefe',
    GREY:           '#A9A8A9',
    MID_GREY:       '#343232',
    DARK_GREY:      '#282729',
    NIGHT:          '#222222',
    DARK:           '#111111',
    BLACK:          '#000000',

    BROWN:          '#a58b76',
    SEPIA:          '#e4cbb7',
    ROSE:           '#c85c5c',
    DARK_ROSE:      '#963232',

    PASTEL_GREEN:   '#a3d39c',
    LIGHT_GREEN:    '#7fff7f',
    GREEN:          '#00a651',
    DARK_GREEN:     '#32965e',
    AQUA:           '#00a99d',
    LIGHT_BLUE:     '#00bff3',
    PASTEL_BLUE:    '#0072bc',
    NAVY_BLUE:      '#0054a6',
    DARK_BLUE:      '#325396',
    BLUE:           '#0000ff',

    YELLOW:         '#fff200',
    DARK_YELLOW:    '#ffd800',
    LIGHT_ORANGE:   '#f7941d',
    ORANGE:         '#ff7f00',
    DARK_ORANGE:    '#ff4000',
    RED:            '#ff0000',
    IMPERIAL_RED:   '#ed1c24',
    BLOOD_RED:      '#bf0000',
    BRIGHT_PINK:    '#ed145b',
    LIGHT_PINK:     '#f06eaa',
    HOT_PINK:       '#ff00d8',
    PINK:           '#ff00ff',
    PASTEL_PURPLE:  '#a864a8',
};
